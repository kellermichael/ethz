# define problem
n1 = 3
n2 = 2
pTypes = 3

# global vars
field = [[-1 for _ in range(n2)] for _ in range(n1)]
maxWeight = -9999999999
sols = []

def p(a):
    quantities = [1,2,3]
    return quantities[a]

# definition: b is good for a
def w(a, b):
    if abs(a-b) <= 1:
        return 1
    else:
        return -1

def solver(fields, typ):
    state = [i for i in range(p(typ))]
    while state[0] != len(fields) - p(typ):
        # do recursive call
        removed = []
        for pos in list(reversed(state)):
            pixel = fields.pop(pos)
            field[pixel[0]][pixel[1]] = typ
            removed.append(pixel)
        if typ < pTypes:
            solver(fields, typ+1)
        else:
            weight = 0
            for x1 in range(n1):
                for y1 in range(n2):
                    for x2 in range(n1):
                        for y2 in range(n2):
                            if x1 != x2 and y1 != y2 and abs(x1-x2) <= 1 and abs(y1-y2) <=1:
                                weight += w(field[x1][y1], field[x2,y2])
            if weight > maxWeight:
                maxWeight = weight
                sols = []
            if weight == maxWeight:
                sols.append(field)
            print("weight ", weight)
        
        # insert back removed fields
        for i in range(p(typ)):
            fields.insert(i, removed[i])
        
        # update state
        minPos = 0
        while minPos < p(typ)-1 and state[minPos] + 1 == state[minPos + 1]:
            minPos += 1
        state[minPos] += 1
        for i in range(minPos):
            state[i] = i
            
                                



# driver
def main():
    fields = [(x, y) for x in range(n1) for y in range(n2)]
    print(fields, field)
    solver(fields, 0)
    print(maxWeight, sols)
    

    

main()