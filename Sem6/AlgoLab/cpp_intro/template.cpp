#include <iostream>

// how to compile:
// g++ -std=c++11 -Wall -O3 template.cpp -o a

// how to test:
// ./a < ./testsets/sample.in | diff - ./testsets/sample.out

void testcase() {
    int n; std::cin >> n;

    int result = 0;
    for(int i = 0; i < n; i++) {
        int a; std::cin >> a; // Read the next number;
        result += a;
    }

    std::cout << result << std::endl; // output the final result
}

int main() {
    std::ios_base::sync_with_stdio(false); // Always!

    int t; std::cin >> t; // read number of test cases
    for(int i = 0; i < t; i++) {
        testcase();
    }
}