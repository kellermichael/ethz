#include <iostream>
#include <string>
#include <iomanip>

// how to compile:
// g++ -std=c++11 -Wall -O3 sol.cpp -o a

// how to test:
// ./a < ./testsets/sample.in | diff - ./testsets/sample.out

void testcase() {
    int a;
    long b;
    std::string c;
    double d;

    std::cin >> a;
    std::cin >> b;
    std::cin >> c;
    std::cin >> d;

    std::cout << std::fixed;
    std::cout << std::setprecision(2);
    std::cout << a << " " << b << " " << c << " " << d << std::endl;
}

int main() {
    std::ios_base::sync_with_stdio(false); // Always!

    int t; std::cin >> t; // read number of test cases
    for(int i = 0; i < t; i++) {
        testcase();
    }
}