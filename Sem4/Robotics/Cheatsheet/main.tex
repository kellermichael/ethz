% Template From: http://seanbone.ch/latex-template-exam-summary/

% Use extarticle for the smaller font size:
% https://tex.stackexchange.com/questions/5339/how-to-specify-font-size-less-than-10pt-or-more-than-12pt
\documentclass[a4paper, 8pt]{extarticle}

% Multi-column layout
\usepackage{multicol}

% Manually set page margins
\usepackage[margin=0.5cm]{geometry}

% Manually set margins on lists
% \usepackage{enumitem}
% Change list margins - https://tex.stackexchange.com/questions/10684/vertical-space-in-lists
% \setlist{leftmargin=3mm, nosep}
% or \setlist{noitemsep} to leave space around whole list

\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{tensor}
\usepackage{graphicx}
\graphicspath{ {./images/} }

\newcommand{\header}[1]{
  \subsection*{#1}
}

% pre + post script, also slightly funny ;)
\newcommand{\pp}[3]{
    \tensor[_{#1}]{\pmb{#2}}{_{#3}}
}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Ra}{\Rightarrow}
\newcommand{\ra}{\rightarrow}

\title{Compressed exam summary}
\author{Sean Bone}

\begin{document}

% 3-column layout
\begin{multicols*}{3}
    \section{Kinematics}
    \header{Dynamic vs Static Gaits}
    \begin{itemize}
        \item Dynamic: System is stabilized on a limit
              cycle, falls over if stopped
        \item Static: System is statically stable, doesn't fall
              over if stopped
    \end{itemize}

    \header{Translations}
    \begin{align*}
        \pp{I}{r}{OP_1} = \pp{I}{r}{OB} + \pp{I}{r}{BP_1}
    \end{align*}

    \header{Rotations}
    \begin{align*}
        \pp{B}{r}{OP_1} = \pp{B}{r}{OB} + \pp{}{R}{BI} \cdot \pp{I}{r}{BP_1}
    \end{align*}
    e.g. rotation from frame $B$ to $I$ s.t. $\pp{I}{r}{} = \pp{}{R}{IB} \cdot \pp{B}{r}{}$
    \begin{align*}
        \pp{}{R}{IB} =
        \begin{bmatrix}
            \cos(\varphi) & -\sin(\varphi) & 0 \\
            \sin(\varphi) & \cos(\varphi)  & 0 \\
            0             & 0              & 1
        \end{bmatrix}
    \end{align*}
    This matrix would rotate around the $z$-Axis by $\varphi$.
    \begin{center}
        \includegraphics[scale=0.25]{rotation.png}
    \end{center}
    The inverse rotation corresponds to the transpose:
    \begin{align*}
        \pp{I}{r}{} =
        \pp{}{R}{IB} \cdot \pp{I}{r}{} &  &
        \pp{}{R}{BI} = \pp{}{R}{IB}^T  &  &
        \pp{B}{r}{} = \pp{}{R}{BI} \cdot \pp{I}{r}{}
    \end{align*}
    While we can also compose rotations:
    \begin{align*}
        \pp{C}{r}{} = \pp{}{R}{CI} \cdot \pp{I}{r}{} &  &
        \pp{}{R}{CI} = \pp{}{R}{CB} \cdot \pp{}{R}{BI}
    \end{align*}

    \header{Velocities}
    \begin{align*}
        \pp{I}{v}{P} = \pp{I}{\dot{r}}{OP} \in \R^{3 \times 1}
    \end{align*}

    \header{Angular Velocities}
    Equal at any point of the body and we have:
    \begin{align*}
        \pp{I}{\omega}{IB} = \pp{}{R}{IB} \cdot \pp{B}{\omega}{IB} &  &
        \pp{I}{\omega}{IC} = \pp{I}{\omega}{IB} + \pp{I}{\omega}{BC}
    \end{align*}
    While it might be clear what
    the angular velocity is (eg. rotating
    around the $z$-Axis is:)
    \begin{align*}
        \pp{I}{\omega}{IB} = \begin{pmatrix}
            0 \\
            0 \\
            \dot{\varphi}
        \end{pmatrix}
    \end{align*}
    we can also get it from:
    \begin{align*}
        \pp{I}{\hat{\omega}}{IB}
                                & = \pp{}{\dot{R}}{IB} \cdot \pp{}{R}{IB}^T
        \\
        \pp{}{\hat{\omega}}{IB} & =
        \begin{bmatrix}
            0                    & - \pp{}{\omega}{IB}^z & \pp{}{\omega}{IB}^y  \\
            \pp{}{\omega}{IB}^z  & 0                     & -\pp{}{\omega}{IB}^x \\
            -\pp{}{\omega}{IB}^y & \pp{}{\omega}{IB}^x   & 0
        \end{bmatrix}                                          \\
        \pp{}{\omega}{IB}       & = \begin{pmatrix}
            \pp{}{\omega}{IB}^x \\
            \pp{}{\omega}{IB}^y \\
            \pp{}{\omega}{IB}^z
        \end{pmatrix}
    \end{align*}

    \header{Differentiation of (position) Vectors}
    \begin{align*}
        \pp{B}{r}{} \Ra \pp{B}{(\dot{r})}{} = \frac{d \pp{B}{r}{} }{dt} + \pp{B}{\omega}{IB} \times \pp{B}{r }{}
    \end{align*}

    \header{Homogenous Transformations}
    \begin{align*}
        \begin{pmatrix}
            \pp{I}{r}{OP_1} \\
            I
        \end{pmatrix}
        =
        \begin{bmatrix}
            \pp{}{R}{IB} & \pp{I}{r}{OB} \\
            0            & 1
        \end{bmatrix}
        \cdot
        \begin{pmatrix}
            \pp{B}{r}{BP_1} \\
            1
        \end{pmatrix} \\
        \pp{I}{v}{P} = \pp{I}{\dot{r}}{OP}
        = \pp{I}{\dot{r}}{OB} + \pp{I}{\omega}{IB}
        \times \pp{I}{r}{BP}
    \end{align*}

    \header{Generalized Coordinates}
    \begin{align*}
        q = \begin{pmatrix}
            q_b & \text{(Un-actuated base)} \\
            q_j & \text{(Actuated joints)}
        \end{pmatrix}
    \end{align*}

    \header{Jacobian}
    Partial derivative of position vector, where $q$ are the generalized coordinates:
    \begin{align*}
        \pp{}{J}{P} =
        \frac{\partial \pp{}{r}{OP}(q)}{\partial q}
        = \begin{bmatrix}
            \frac{\partial r_1}{\partial q_1} & \dots  & \frac{\partial r_1}{\partial q_n} \\
            \vdots                            & \ddots & \vdots                            \\
            \frac{\partial r_m}{\partial q_1} & \dots  & \frac{\partial r_m}{\partial q_n}
        \end{bmatrix}
    \end{align*}

    \header{Inverse Kinematics}
    Given a desired endeffector position $\pp{}{r}{OF}(q) = \pp{}{r}{OF}^{goal}$
    determine generalized coordinates $q$ (plan move into a position). Approach:
    Iteratively perform the following steps $(q^i, r^i)$:
    \begin{itemize}
        \item Start from known solution (e.g. given by forward kin)
        \item Lineaize $\pp{}{r}{OF}(q)$ resulting in the Jacobian
              $J = \frac{\partial r}{\partial q } \Big |_{q = q^i} \Leftrightarrow \Delta r = J \Delta q$
        \item Invert the Jacobian to obtain the change we need to apply to q: $\Delta q = \pp{}{J}{}^+ \Delta r$ so $q^{i+1} = q^i + \pp{}{J}{}^+ \left( r^{goal} - r^i\right)$.
        \item Update generalized coordinates and possibly recalculate $r$ to see if we're now close enough.
    \end{itemize}
    Unfortunately the Jacobian is often not
    invertible, so we use the Moore-Penrose pseudo inverse $\pp{}{J}{}^+$.
    \begin{itemize}
        \item Redundancy: Jacobian is column-rank
              defficient. Pseudo inverse minimizes $\| \dot{q}\|_2 $.
              Can have multiple solutions $\dot{q} = \pp{}{J}{}^+ \dot{r}^{(goal)} + (I - \pp{}{J}{}^+ \pp{}{J}{}) \dot{q_0}$
        \item Singularity: Jacobian is row-rank deficient. Pseudo inverse minimizes the error $\| \dot{r}^{(goal)} - \dot{r} \|_2$.
    \end{itemize}

    \header{Contact Constraints}
    E.g. Footpoint is not allowed to move:
    \begin{align*}
        \pp{}{\dot{r}}{F} = \pp{}{J}{F} \dot{q} = 0
    \end{align*}
    Choose the joint speed such that this
    constraint is satisfied:
    \begin{align*}
        \dot{q} = \pp{}{J}{F} \dot{q} + \pp{}{N}{F} \pp{}{\dot{q}}{0}
        = 0 + \pp{}{N}{F} \pp{}{\dot{q}}{0}
    \end{align*}
    And now move the body upward:
    \begin{align*}
        \pp{}{\dot{r}}{body} & = \pp{}{J}{body} \dot{q} = \pp{}{J}{body} \pp{}{N}{F} \pp{}{\dot{q}}{0}                                                                           \\
        \pp{}{\dot{q}}{}     & = \pp{}{J}{F}^+ \pp{}{\dot{r}}{F} + \pp{}{N}{F}  \pp{}{\dot{q}}{0} = \pp{}{N}{F} \left( \pp{}{J}{body} \pp{}{N}{F} \right)^+ \pp{}{\dot{r}}{body}
    \end{align*}
    \header{Wheel Types}
    \begin{itemize}
        \item Standard wheel: Rotation around the wheel axle, Free rotation
              around contact point (2)
        \item Castor wheel: Rotation around the wheel axle, Free rotation
              around contact point, Rotation around the caster axle (3)
        \item Swedish wheel: Rotation around the wheel axle, Free rotation
              around the rollers, free rotation around the contact point (3)
        \item Spherical wheel (ball): Three degrees of freedom (3)
    \end{itemize}

    \header{Holonomic vs Non-holonomic}
    \begin{itemize}
        \item Holonomic: Differential constraints are integrable.
              They may be exressed as constraints on the robot's pose.
              This means the robot can move anywhere instantaneously (e.g. office chair).
        \item Non-Holonomic: Differential constraints are $\textbf{not}$ integrable.
              There is no way to express them as constraints on the robot's pose. The robot
              is not able to move instantaneously in every direction in the space of
              its degrees of freedom (e.g. car).
    \end{itemize}

    \header{Wheel equation}
    \begin{center}
        \includegraphics[scale=0.2]{wheel-equation-sketch.png}\\
        \includegraphics[scale=0.3]{wheel-equation.png}
    \end{center}


    \header{Wheel constraints}
    \begin{center}
        \includegraphics[scale=0.2]{wheel-constraints.png}
    \end{center}
    \begin{itemize}
        \item Rolling constraint: $\pp{}{\dot{y}}{W} = r \dot{\varphi}$ (also $J_1(\beta_s) R(\theta) \dot{\xi}_I - \dot{\varphi} r = 0$)
        \item No-sliding constraint: $\pp{}{\dot{x}}{W} = 0$ (also $C_1(\beta_s) R(\theta) \dot{\xi}_I = 0$)
        \item Planar motion assumption: $\pp{}{\dot{z}}{W} = 0$
        \item Unconstrained rotation around contact point
    \end{itemize}

    \header{Degree of Maneuverability}
    \begin{itemize}
        \item Mobility: Restricted by no-sliding constraint $(\delta_m = 3 - \text{rank}[C_1(\beta_s)])$.
              For a robot with $\delta_m = 2$ the ICR (instantaneous center of rotation)
              is constrained to lie on a line. For any robot with $\delta_m = 3$ the ICR can be
              any point on the 2D plane.
        \item Steerability: Additional freedom due to steering mechanisms $(\delta_s = \text{rank}[C_{1s}(\beta_s)])$,
              depends on how many wheels can be steered independently (car has $\delta_s = 1$)
        \item Maneuverability: $\delta_M = \delta_m + \delta_s$
    \end{itemize}

    \header{Sensor Types}
    \begin{itemize}
        \item Proprioceptive: Measure internal values (robot)
        \item Exteroceptive: Measure robot's environment
        \item Passive: Measure energy coming from the environment (influenced by environment)
        \item Active: Emit energy and measure reaction (better performance but influences environment)
    \end{itemize}

    \header{Measuring Error Types}
    There are two types:
    \begin{itemize}
        \item Systematic errors (deterministic): Caused by processes that can in theory be modeled
        \item Random errors: Can't be modelled
    \end{itemize}

    \header{Error Propagation Law}
    Error propagation in a multiple-input multiple-output system with $n$ inputs
    and $m$ outputs:
    $$
        \pp{}{Y}{j} = f_j(X_1 \dots X_n)
    $$
    It can be shown that the output covariance matrix $\pp{}{C}{Y}$ is given by the error
    propagation law:
    $$
        \pp{}{C}{Y} = \pp{}{F}{X} \cdot \pp{}{C}{X} \cdot \pp{}{F}{X}^T
    $$
    where
    \begin{itemize}
        \item $\pp{}{C}{X}$: Covariance matrix representing the input uncertanties
        \item $\pp{}{C}{Y}$: Covariance matrix representing the propagated
              uncertanties for the output
        \item $\pp{}{F}{X}$: The Jacobian matrix defined as:
              $$
                  \pp{}{F}{X} =
                  \begin{bmatrix}
                      \frac{\partial f_1}{\partial X_1} & \dots  & \frac{\partial f_1}{\partial X_n} \\
                      \vdots                            & \vdots & \vdots                            \\
                      \frac{\partial f_m}{\partial X_1} & \dots  & \frac{\partial f_m}{\partial X_n}
                  \end{bmatrix}
              $$
    \end{itemize}

    \header{Computer Vision Challenges}
    \begin{itemize}
        \item Viewpoint changes
        \item Illumination changes
        \item Object intra-class variations (e.g. chairs with 3 vs 4 legs)
        \item Inherent ambiguities: many different 3D scenes
              can give rise to a particular 2D picture
              (e.g. Butterfly painted on street looks like it's flying)
    \end{itemize}

    \header{How Cameras Work}
    We use a lense to ensure all light rays from the same origin
    end up on the same place on the film. Rays going through the
    optical center are not deviated:
    \begin{center}
        \includegraphics[scale=0.3]{lens.png}
    \end{center}
    All rays parallel to the optical axis converge at the
    focal point:
    \begin{center}
        \includegraphics[scale=0.3]{focal-point.png}
    \end{center}

    \header{Thin Lens Equation}
    \begin{center}
        \includegraphics[scale=0.3]{thin-lens.png}
    \end{center}
    $$
        \frac{1}{f} = \frac{1}{z} + \frac{1}{e}
    $$
    If further $z \ra \infty$ (objects at infinity are in focus) we
    have the pinhole approximation:
    $$
        \frac{1}{f} \approx \frac{1}{e} \Ra f \approx e
    $$

    \header{Camera Pixel Mapping}
    \begin{center}
        \includegraphics[scale=0.3]{camera-mapping.png}
    \end{center}
    We have for $u$ (and $v$ analogously):
    $$
        u = u_0 + kx \Ra u_0 + k \frac{f X_C}{Z_C}
    $$
    While the homogonious coordinates can be written
    like this (to make the system linear):
    $$
        \begin{bmatrix}
            \lambda u \\
            \lambda v \\
            \lambda
        \end{bmatrix}
        =
        \begin{bmatrix}
            kf & 0  & u_0 \\
            0  & kf & v_0 \\
            0  & 0  & 1
        \end{bmatrix}
        \begin{bmatrix}
            X_C \\
            Y_C \\
            Z_C
        \end{bmatrix}
    $$

    \header{Epipolar Constraint}
    Used to make corresponding point search
    for stereo vision easier:
    \begin{center}
        \includegraphics[scale=0.2]{epipolar-constraint.png}
    \end{center}
    To make two images have the same scan lines first remove radial
    distortion, then compute homographies and rectify:
    \begin{center}
        \includegraphics[scale=0.15]{rectified-pics.png}
    \end{center}

    \header{Depth Computation}
    Identify the same point in both images, then
    compute:
    $$
        Z = \frac{bf}{u_l - u_r}
    $$
    where $b$ is the baseline, $f$ the focal
    length and $u$ the respective coordinates.

    \header{1D Image Filtering}
    Formally Correlation is:
    $$
        J(x) = F \circ I(x) = \sum_{i = -N}^N f(i) I(x + i)
    $$
    so a smoothing example would be:
    $$
        F(i) =
        \begin{cases}
            1/3 & i \in [-1, 1]      \\
            0   & i \not \in [-1, 1]
        \end{cases}
    $$
    (sums left, middle and right pixels in an array and multiplies by $1/3$)

    \header{2D Image Filtering}
    Collelation in 2D:
    $$
        F \circ I(x, y) = \sum_{j = -M}^M \sum_{i = -N}^N f(i, j) I(x+i, y + j)
    $$
    so a general 2D Gaussian would be:
    $$
        G(x, y) = \frac{1}{2 \pi |S|^{1/2}}e^{\frac{1}{2} \begin{pmatrix}
            x \\
            y
        \end{pmatrix} S^{-1} \begin{pmatrix}
            x & y
        \end{pmatrix}}
    $$
    With $S$ usually being:
    $$
        S = \begin{bmatrix}
            \sigma^2 & 0        \\
            0        & \sigma^2
        \end{bmatrix}
    $$
    Gaussian filters are also seperable:
    $$
        G_\sigma (x, y) = G_\sigma(x) \cdot G_\sigma(y)
    $$

    \header{Correlation vs Convolution}
    \begin{itemize}
        \item Convolution: $J(x) = F * I(x) = \sum_{i = -N}^N F(i) I(x-i)$
        \item Correlation: $J(x) = F \circ I(x) = \sum_{i = -N}^N F(i) I(x+i)$
    \end{itemize}
    (Same for 2D.) A key difference is that $\textbf{Convolution is associative}$:
    $F * (G * I) = (F * G) * I$

    \header{Derivative Theorem Of Convolution}
    This corresponds to using that convolution is associative.
    Instead of first smoothing the image and then performing the derivative,
    we convolve the derivative filter on the gaussian filter. Thus we only
    have to pass over the image once:
    $$
        s'(x) = \frac{d}{dx} \left( G_\sigma(x) * I(x) \right) = G'_\sigma (x) * I(x)
    $$
    \begin{center}
        \includegraphics[scale=0.2]{convolution-derivation.png}
    \end{center}

    \header{Harris Corner Detection}
    Around a corner the image gradient has two or more dominant
    directions. Shifting a window in any direction would give
    a large change in intensity in at least 2 directions.\\ \\
    The detection is:
    \begin{itemize}
        \item Invariant to rotation
        \item Invariant to linear intensity changes
        \item Not invariant to Scale changes
        \item Not invariant to geometric affine changes (distorting corner neighborhood)
    \end{itemize}

    \header{Sum Of Squared Differences}
    Two image patches of size $P$ one centered at $(x, y)$
    and another centered at $(x + \Delta x, y + \Delta y)$ has the
    SSD:
    $$
        SSD (\Delta x, \Delta y) = \sum_{x, y \in P} \left( I(x, y) - I(x + \Delta x, y + \Delta y) \right)^2
    $$
    or approximately with the first order Taylor expantion:
    $$
        SSD (\Delta x, \Delta y) \approx \sum_{x, y \in P} \left( I_x(x, y) \Delta x + I_y(x, y) \Delta y \right)^2
    $$
    or in matrix form:
    \begin{align*}
        SSD(\Delta x, \Delta y) & \approx
        \begin{bmatrix}
            \Delta x & \Delta y
        \end{bmatrix}
        M
        \begin{bmatrix}
            \Delta x \\
            \Delta y
        \end{bmatrix}                    \\
        M                       & = \sum_{x, y \in P}
        \begin{bmatrix}
            I_x^2   & I_x I_y \\
            I_x I_y & I_y^2
        \end{bmatrix}
    \end{align*}
    Since $M$ is symmetric we have the Eigenvalues $\lambda_1, \lambda_2$:
    $$
        M = R^{-1}
        \begin{bmatrix}
            \lambda_1 & 0         \\
            0         & \lambda_2
        \end{bmatrix}
        R
    $$
    If both $\lambda_1, \lambda_2$ are large we have a corner,
    if only one $\lambda$ is large an edge, otherwise a flat region.

    \header{SIFT}
    Scale invariant feature transform. Reasonably invariant to
    changes in rotation, scaling, small viewpoint changes and
    illumination. Very powerful in capturing + describing
    distinctive features, also computationally demanding.\\\\
    The SIFT detector works as follows:
    \begin{itemize}
        \item Scale-space pyramid: Subsample and blur original image
        \item Difference of Gaussians (DoG) pyramid: Subtract successive smoothed images
        \item Keypoints: Local extrema in the DoG pyramid
    \end{itemize}
    The Keypoint orientation is given by:
    \begin{itemize}
        \item Sample intensities around the keypoint
        \item Compute a histogram of orientations of intensity gradients
        \item Keypoint orientation = histogram peak
    \end{itemize}
    The Keypoint descriptor:
    \begin{itemize}
        \item 128-long vector
        \item Describe all gradient orientations relative to the keypoint orientation
        \item Divide keypoint neighborhood in $4 \times 4$ regions
              and compute orientation histograms along 8 directions
        \item SIFT descriptor: Concatination of all $4 \cdot 4 \cdot 8 = 128$ values
    \end{itemize}
    \begin{center}
        \includegraphics[scale=0.2]{keypoint descriptor.png}
    \end{center}


    \header{FAST Detector}
    Features from Accelerated Segment Test. A very fast detector that studies the intensity
    of pixels on a circle around candidate pixel $C$. The area centered at $C$
    is a FAST corner if a set of $N$ contiguous pixels on a circle are
    significantly darker/brighter than $C$ (Normally $12$ on a $16$ circle).
    Very fast detector.

    \header{BRIEF Descriptor}
    Binary Robust independent Elementary Features. A Binary descriptor
    $=$ concatination of simple intensity tests between random pixel pairs
    (Pattern pre-selected):
    \begin{center}
        \includegraphics[scale=0.2]{brief.png}
    \end{center}
    Not scale/ rotation invariant, but allows
    very fast hamming distance matching: Count the number of bits that are different
    in the descriptors matched.

    \header{BRISK Features}
    Binary Robust Invariant Scalable Keypoints. Detects corners in
    scale-space based on FAST. Faster than SIFT, SURF. Rotation and scale invariant.
    \begin{center}
        \includegraphics[scale=0.3]{brisk.png}
    \end{center}

    \header{BRISK Descriptor}
    Binary, formed by pairwise intensity comparisons. Pattern
    defines intensity comparisons in the keypoint neighborhood.
    Red circles are the size of the smoothing kernel applied. Blue circles
    are the smoothed pixel value uesed. About $\approx 10$ times faster
    than SURF, slower than BRIEF, but scale and rotation invariant.
    $\Ra$ Compare short- and long-distance pairs for orientation assignment \&
    descriptor formation
    \begin{center}
        \includegraphics[scale=0.2]{brisk-descriptor.png}
    \end{center}


    \header{Place recognition}
    Given a collection of images, perform these steps:
    \begin{itemize}
        \item Extract Features from Image collection
        \item Cluster feature descriptors into visual words (put similar descriptors together)
        \item Describe each scene as a collection of words
        \item Build a vocabulary tree via hierarchical clustering (e.g. k-means clustering)
        \item Use the Inverted File System to index efficiently
    \end{itemize}
    Note that this procedure discards spatial relationships between features!

    \header{K-Means Clustering}
    \begin{center}
        \includegraphics[scale=0.3]{clustering.png}
    \end{center}

    \header{Vocuabulary Tree/ Inverted File System}
    Attach label for training image at leaf node for each feature in training
    image. When a test image comes pass all features down the tree and count
    which model image it is most like:
    \begin{center}
        \includegraphics[scale=0.2]{vocab-tree.png}
    \end{center}

    \header{TF-IDF}
    Term frequency-inverse document frequency measures the importance
    of a word inside a document (as part of a document db):
    \begin{itemize}
        \item term frequency: frequency of word $w_i$ in image $j$: $tf_{ij} = \frac{n_{i, j}}{\sum_k n_{k, j}} = \frac{\text{Occurrences of this word in this image}}{\text{All words in the image}}$
        \item inverse document frequency: $idf_i = \log \frac{|D|}{|\{ d : w_i \in d \}|}$ (all images / all images with word $w_i$)
        \item tf-idf of word $w_i$ in image $j$ is $= tf_{ij} \cdot idf_i$
        \item Use to weigh the importance of each word when voting for the corresponding image
    \end{itemize}

    \header{FABMAP}
    A method for place recognition:
    \begin{itemize}
        \item Use training images to build a Bag of Words database
        \item Probabalistically model the world as a set of discrete places (place $=$ a vector of occurrences of visual words in the local scene)
        \item Capture the dependencies of words to distinguish the most characteristic structure of each scene.
    \end{itemize}
    Very sure when it's in a recognizable place, not so certain when looking at a blank wall.



    \header{Split-And-Merge}
    Initialize set $S$ to contain all points\\
    $\textbf{Split}$
    \begin{itemize}
        \item Fit a line to points in current set $S$
        \item Find the most distant point to the line
        \item If distance $>$ threshold $\Ra$ split set \& repeat with left and right sets
    \end{itemize}
    $\textbf{Merge}$
    \begin{itemize}
        \item If two consecutive segments are close/colinear enough, obtain
              the common line and find the most distant point
        \item If distance $\leq$ threshold, merge both segments
    \end{itemize}
    Example:
    \begin{center}
        \includegraphics[scale=0.1]{split-and-merge.png}
    \end{center}


    \header{Ransac}
    Random Sample Consensus. A generic \& robust fitting algorithm of models in the presence of outliers.
    Applicable to any problem where the goal is to identify the inliers which satisfy a predefined model.
    Iterative \& non-deterministic.\\\\
    Line extraction example:
    \begin{itemize}
        \item Select sample of two points at random
        \item Calculate model parameters that fit the data in the sample
        \item Calculate error function for each data point
        \item Select data that supports the current hypothesis (and save)
        \item Repeat for $k$ steps and return best hypothesis (most datapoints supported by hypothesis)
    \end{itemize}
    But how large should $k$ be? Let $N$ be the
    total number of data points and $w$ the number
    of inliers $/ N$. Hence:
    \begin{align*}
        p         & = \text{P(select a minimal set free of outliers)} \\
        w         & = \text{P(select inlier from dataset)}            \\
        w^2       & = \text{P(both selected points are inliers)}      \\
        1-w^2     & = \text{P(at least one of the points is outlier)} \\
        (1-w^2)^k & = \text{P(RANSAC never selects two inliers)}      \\
        1-p       & = (1-w^2)^k
    \end{align*}
    In practice we only need a rough estimate of $w$
    and then do:
    $$
        k = \frac{\log(1-p)}{\log(1 - w^2)}
    $$
    iterations.

    \header{Hough-Transform}
    Each point in the image space votes for
    line-parameters in Hough parameter
    space (normally using polar representation):
    \begin{center}
        \includegraphics[scale=0.3]{hough-transform.png}
    \end{center}

    \header{Map-Based Localization}
    The robot estimates its position using
    perceived information and a map
    (might be given or built in parallel (SLAM)).
    Challenges are:
    \begin{itemize}
        \item Measurement and map are inherently error prone
        \item Robot has to use probabilistic map based localization
    \end{itemize}
    Robot estimates the belief state through ACT and SEE cycle.

    \header{SEE ACT Cycle}
    \begin{center}
        \includegraphics[scale=0.175]{SEE_ACT_0.png}
        \includegraphics[scale=0.175]{SEE_ACT_1.png}
        \includegraphics[scale=0.175]{SEE_ACT_2.png}
        \includegraphics[scale=0.275]{SEE_THINK_ACT.png}
    \end{center}


    \header{Probabilistic Localization Belief Representation}
    There are these options:
    \begin{itemize}
        \item Contiguous map with a single hypothesis probability distribution
        \item Contiguous map with multiple hypothesis probability distributions
        \item Discretized matric map (grid $k$) with probability distribution
        \item Discretized topological map (e.g. Kitchen, Office) map (nodes $n$) with probability distribution
    \end{itemize}

    \header{Probability Stuff}
    \begin{align*}
        p(x)                      & = P(X = x)                   \\
        X, Y \text{ independent } & \Ra p(x, y) = p(x) p(y)      \\
        p(x | y)                  & = \frac{p(x, y)}{p(y)}       \\
        p(x | y)                  & = \frac{p(y | x) p(x)}{p(y)} \\
        p(x)                      & = \sum_y p(x|y) \cdot p(y)   \\
        p(x)                      & = \int_y p(x | y) \cdot p(y)
    \end{align*}

    \header{Map Representation}
    \begin{itemize}
        \item Architecture Map: Set of finite and infinite lines
        \item Exact Cell Decomposition: Polygons describe stuff
              \begin{center}
                  \includegraphics[scale=0.2]{exact-cell-decomposition.png}
              \end{center}
        \item Approximate Cell Decomposition: Possibly makes narrow passages disappear
              \begin{center}
                  \includegraphics[scale=0.2]{approx-dell-decomp.png}
              \end{center}
        \item Adaptive Cell Decomposition: Iteratively Cut field in half until either fully empty or entirely full
              \begin{center}
                  \includegraphics[scale=0.2]{adaptive-cell-decomp.png}
              \end{center}
        \item Topological Map: Use nodes (localizations) and edges (connectivity)
              \begin{center}
                  \includegraphics[scale=0.2]{topological-map.png}
              \end{center}
    \end{itemize}

    \header{Markov Localization}
    Assumptions are:
    \begin{itemize}
        \item Discretized pose representation $x_t \ra$ grid map
        \item Tracks the robot's belief state $bel(x_t)$ using an
              arbitrary probability density function to represent the robot's position
        \item Markov assumption: The output of the estiation process
              is a function $x_t$ only of the robot's previous state $x_{t-1}$ and
              its most recent actions (odometry) $u_t$ and perception $z_t$:
              \begin{align*}
                  p(x_t | x_0, u_t, \dots, u_0, z_t, \dots, z_0) \\
                  = p(x_t | x_{t-1}, u_t, z_t)
              \end{align*}
        \item Markov localization addresses the global Localization
              problem, the position tracking problem and the kidnapped robot problem
        \item ACT: The new belief is: $\overline{bel}(x_t) = \sum_{x_{t-1}} p(x_t | u_t, x_{t-1}) \cdot bel(x_{t-1})$
        \item SEE: The new belief is (Bayes rule): $bel(x_t) = p(y)^{-1} \cdot p(z_t | x_t, M) \cdot \overline{bel}(x_t)$
              where $p(z_t | x_t, M)$ is the probabilistic measurment model for observing the measurment data $z_t$ given the Map $M$ and robot's position $x_t$.
              $p(y)^{-1}$ normalizes s.t. $\sum p = 1$.
        \item Perform SEE \& ACT steps on all $x_t$ every round
        \item Very computationally expensive for large maps $\Ra$ use adaptive cell decomposition
    \end{itemize}

    \header{Kalman Filter Localization}
    \begin{itemize}
        \item Error Approximation with normal distribution
        \item Output is a linear(ized) function of the input distribution $y = Ax_1 + Bx_2$
        \item Tracks the robot's belief state $p(x_t)$ typically as a single hypothesis with normal distribution
        \item Addresses tracking, not global localization or kidnapped robot problem
        \item ACT: Determine new $x_t$ using motion model and current position. Uncertainty:
              \begin{align*}
                  \hat{P}_t & = F_X P_{t-1} F_x^T + F_u Q_t F_u^T \\
                  Q_t       & = \begin{bmatrix}
                      k_r |\Delta s_r| & 0                \\
                      0                & k_l |\Delta s_l|
                  \end{bmatrix}
              \end{align*}
        \item SEE: We measure with a laser scanner and get a set of points we do
              line segmentation for. Line uncertainty (for line in polar form):
              $$
                  R_t^i = \begin{bmatrix}
                      \sigma_{\alpha \alpha} & \sigma_{\alpha r} \\
                      \sigma_{r \alpha}      & \sigma_{rr}
                  \end{bmatrix}_t^i
              $$
              \begin{center}
                  \includegraphics[scale=0.2]{kalman.png}
              \end{center}
        \item For each found match we now estimate a position update $x_t = \hat{x}_t + K_tv_t$
              where $K_t = \hat{P}_t H_t^T (\Sigma_{IN_t})^{-1}$ is the Kalman gain. The corrsponding position covariance $P_t = \hat{P}_t - K_t \Sigma_{IN_t} K_t^T$
    \end{itemize}

    \header{SLAM}
    Simultaneous Localization and Mapping. How can a body navigate in a previously
    unknown environment while constantly building and updating a map of its workspace?



\end{multicols*}
\end{document}