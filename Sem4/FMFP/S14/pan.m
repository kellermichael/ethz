#define rand	pan_rand
#define pthread_equal(a,b)	((a)==(b))
#if defined(HAS_CODE) && defined(VERBOSE)
	#ifdef BFS_PAR
		bfs_printf("Pr: %d Tr: %d\n", II, t->forw);
	#else
		cpu_printf("Pr: %d Tr: %d\n", II, t->forw);
	#endif
#endif
	switch (t->forw) {
	default: Uerror("bad forward move");
	case 0:	/* if without executable clauses */
		continue;
	case 1: /* generic 'goto' or 'skip' */
		IfNotBlocked
		_m = 3; goto P999;
	case 2: /* generic 'else' */
		IfNotBlocked
		if (trpt->o_pm&1) continue;
		_m = 3; goto P999;

		 /* PROC :init: */
	case 3: // STATE 1 - philosophers_skeleton1_2.pml:71 - [((i<5))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][1] = 1;
		if (!((((P1 *)_this)->i<5)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 4: // STATE 2 - philosophers_skeleton1_2.pml:72 - [printf('philosopher %d takes a seat\\n',i)] (0:0:0 - 1)
		IfNotBlocked
		reached[1][2] = 1;
		Printf("philosopher %d takes a seat\n", ((P1 *)_this)->i);
		_m = 3; goto P999; /* 0 */
	case 5: // STATE 3 - philosophers_skeleton1_2.pml:73 - [(run philosopher(i))] (0:0:0 - 1)
		IfNotBlocked
		reached[1][3] = 1;
		if (!(addproc(II, 1, 0, ((P1 *)_this)->i)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 6: // STATE 4 - philosophers_skeleton1_2.pml:74 - [i = (i+1)] (0:0:1 - 1)
		IfNotBlocked
		reached[1][4] = 1;
		(trpt+1)->bup.oval = ((P1 *)_this)->i;
		((P1 *)_this)->i = (((P1 *)_this)->i+1);
#ifdef VAR_RANGES
		logval(":init::i", ((P1 *)_this)->i);
#endif
		;
		_m = 3; goto P999; /* 0 */
	case 7: // STATE 6 - philosophers_skeleton1_2.pml:76 - [((i==5))] (0:0:1 - 1)
		IfNotBlocked
		reached[1][6] = 1;
		if (!((((P1 *)_this)->i==5)))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: i */  (trpt+1)->bup.oval = ((P1 *)_this)->i;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P1 *)_this)->i = 0;
		_m = 3; goto P999; /* 0 */
	case 8: // STATE 11 - philosophers_skeleton1_2.pml:78 - [-end-] (0:0:0 - 3)
		IfNotBlocked
		reached[1][11] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC philosopher */
	case 9: // STATE 1 - philosophers_skeleton1_2.pml:37 - [printf('philosopher %d thinks\\n',p)] (0:0:0 - 1)
		IfNotBlocked
		reached[0][1] = 1;
		Printf("philosopher %d thinks\n", ((P0 *)_this)->p);
		_m = 3; goto P999; /* 0 */
	case 10: // STATE 3 - philosophers_skeleton1_2.pml:51 - [((isHungry==0))] (5:0:2 - 1)
		IfNotBlocked
		reached[0][3] = 1;
		if (!((((int)((P0 *)_this)->isHungry)==0)))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: isHungry */  (trpt+1)->bup.ovals = grab_ints(2);
		(trpt+1)->bup.ovals[0] = ((P0 *)_this)->isHungry;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)_this)->isHungry = 0;
		/* merge: isHungry = 1(0, 4, 5) */
		reached[0][4] = 1;
		(trpt+1)->bup.ovals[1] = ((int)((P0 *)_this)->isHungry);
		((P0 *)_this)->isHungry = 1;
#ifdef VAR_RANGES
		logval("philosopher:isHungry", ((int)((P0 *)_this)->isHungry));
#endif
		;
		_m = 3; goto P999; /* 1 */
	case 11: // STATE 5 - philosophers_skeleton1_2.pml:53 - [((((isHungry==1)&&(fork[(p%5)]==0))&&(((p%5)<((p+1)%5))||(fork[((p+1)%5)]==1))))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][5] = 1;
		if (!((((((int)((P0 *)_this)->isHungry)==1)&&(((int)now.fork[ Index((((P0 *)_this)->p%5), 5) ])==0))&&(((((P0 *)_this)->p%5)<((((P0 *)_this)->p+1)%5))||(((int)now.fork[ Index(((((P0 *)_this)->p+1)%5), 5) ])==1)))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 12: // STATE 6 - philosophers_skeleton1_2.pml:10 - [((fork[(p%5)]==0))] (10:0:1 - 1)
		IfNotBlocked
		reached[0][6] = 1;
		if (!((((int)now.fork[ Index((((P0 *)_this)->p%5), 5) ])==0)))
			continue;
		/* merge: fork[(p%5)] = 1(0, 7, 10) */
		reached[0][7] = 1;
		(trpt+1)->bup.oval = ((int)now.fork[ Index((((P0 *)_this)->p%5), 5) ]);
		now.fork[ Index((((P0 *)_this)->p%5), 5) ] = 1;
#ifdef VAR_RANGES
		logval("fork[(philosopher:p%5)]", ((int)now.fork[ Index((((P0 *)_this)->p%5), 5) ]));
#endif
		;
		_m = 3; goto P999; /* 1 */
	case 13: // STATE 10 - philosophers_skeleton1_2.pml:54 - [((((isHungry==1)&&(fork[((p+1)%5)]==0))&&((((p+1)%5)<(p%5))||(fork[(p%5)]==1))))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][10] = 1;
		if (!((((((int)((P0 *)_this)->isHungry)==1)&&(((int)now.fork[ Index(((((P0 *)_this)->p+1)%5), 5) ])==0))&&((((((P0 *)_this)->p+1)%5)<(((P0 *)_this)->p%5))||(((int)now.fork[ Index((((P0 *)_this)->p%5), 5) ])==1)))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 14: // STATE 11 - philosophers_skeleton1_2.pml:17 - [((fork[((p+1)%5)]==0))] (15:0:1 - 1)
		IfNotBlocked
		reached[0][11] = 1;
		if (!((((int)now.fork[ Index(((((P0 *)_this)->p+1)%5), 5) ])==0)))
			continue;
		/* merge: fork[((p+1)%5)] = 1(0, 12, 15) */
		reached[0][12] = 1;
		(trpt+1)->bup.oval = ((int)now.fork[ Index(((((P0 *)_this)->p+1)%5), 5) ]);
		now.fork[ Index(((((P0 *)_this)->p+1)%5), 5) ] = 1;
#ifdef VAR_RANGES
		logval("fork[((philosopher:p+1)%5)]", ((int)now.fork[ Index(((((P0 *)_this)->p+1)%5), 5) ]));
#endif
		;
		_m = 3; goto P999; /* 1 */
	case 15: // STATE 15 - philosophers_skeleton1_2.pml:56 - [((((isHungry==1)&&(fork[((p+1)%5)]==1))&&(fork[(p%5)]==1)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][15] = 1;
		if (!((((((int)((P0 *)_this)->isHungry)==1)&&(((int)now.fork[ Index(((((P0 *)_this)->p+1)%5), 5) ])==1))&&(((int)now.fork[ Index((((P0 *)_this)->p%5), 5) ])==1))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 16: // STATE 16 - philosophers_skeleton1_2.pml:41 - [printf('philosopher %d eats\\n',p)] (0:20:1 - 1)
		IfNotBlocked
		reached[0][16] = 1;
		Printf("philosopher %d eats\n", ((P0 *)_this)->p);
		/* merge: hasEaten = 1(20, 18, 20) */
		reached[0][18] = 1;
		(trpt+1)->bup.oval = ((int)((P0 *)_this)->hasEaten);
		((P0 *)_this)->hasEaten = 1;
#ifdef VAR_RANGES
		logval("philosopher:hasEaten", ((int)((P0 *)_this)->hasEaten));
#endif
		;
		_m = 3; goto P999; /* 1 */
	case 17: // STATE 20 - philosophers_skeleton1_2.pml:62 - [(((hasEaten==1)&&(fork[(p%5)]==1)))] (0:0:0 - 1)
		IfNotBlocked
		reached[0][20] = 1;
		if (!(((((int)((P0 *)_this)->hasEaten)==1)&&(((int)now.fork[ Index((((P0 *)_this)->p%5), 5) ])==1))))
			continue;
		_m = 3; goto P999; /* 0 */
	case 18: // STATE 21 - philosophers_skeleton1_2.pml:24 - [((fork[(p%5)]==1))] (25:0:1 - 1)
		IfNotBlocked
		reached[0][21] = 1;
		if (!((((int)now.fork[ Index((((P0 *)_this)->p%5), 5) ])==1)))
			continue;
		/* merge: fork[(p%5)] = 0(0, 22, 25) */
		reached[0][22] = 1;
		(trpt+1)->bup.oval = ((int)now.fork[ Index((((P0 *)_this)->p%5), 5) ]);
		now.fork[ Index((((P0 *)_this)->p%5), 5) ] = 0;
#ifdef VAR_RANGES
		logval("fork[(philosopher:p%5)]", ((int)now.fork[ Index((((P0 *)_this)->p%5), 5) ]));
#endif
		;
		_m = 3; goto P999; /* 1 */
	case 19: // STATE 25 - philosophers_skeleton1_2.pml:63 - [(((hasEaten==1)&&(fork[((p+1)%5)]==1)))] (0:0:1 - 1)
		IfNotBlocked
		reached[0][25] = 1;
		if (!(((((int)((P0 *)_this)->hasEaten)==1)&&(((int)now.fork[ Index(((((P0 *)_this)->p+1)%5), 5) ])==1))))
			continue;
		if (TstOnly) return 1; /* TT */
		/* dead 1: hasEaten */  (trpt+1)->bup.oval = ((P0 *)_this)->hasEaten;
#ifdef HAS_CODE
		if (!readtrail)
#endif
			((P0 *)_this)->hasEaten = 0;
		_m = 3; goto P999; /* 0 */
	case 20: // STATE 26 - philosophers_skeleton1_2.pml:31 - [((fork[((p+1)%5)]==1))] (30:0:1 - 1)
		IfNotBlocked
		reached[0][26] = 1;
		if (!((((int)now.fork[ Index(((((P0 *)_this)->p+1)%5), 5) ])==1)))
			continue;
		/* merge: fork[((p+1)%5)] = 0(0, 27, 30) */
		reached[0][27] = 1;
		(trpt+1)->bup.oval = ((int)now.fork[ Index(((((P0 *)_this)->p+1)%5), 5) ]);
		now.fork[ Index(((((P0 *)_this)->p+1)%5), 5) ] = 0;
#ifdef VAR_RANGES
		logval("fork[((philosopher:p+1)%5)]", ((int)now.fork[ Index(((((P0 *)_this)->p+1)%5), 5) ]));
#endif
		;
		/* merge: .(goto)(0, 31, 30) */
		reached[0][31] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 21: // STATE 33 - philosophers_skeleton1_2.pml:66 - [-end-] (0:0:0 - 1)
		IfNotBlocked
		reached[0][33] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */
	case  _T5:	/* np_ */
		if (!((!(trpt->o_pm&4) && !(trpt->tau&128))))
			continue;
		/* else fall through */
	case  _T2:	/* true */
		_m = 3; goto P999;
#undef rand
	}

