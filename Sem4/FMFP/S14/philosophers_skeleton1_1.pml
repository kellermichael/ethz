#define n 8

bit fork[n]

#define leftFork(p) p % n
#define rightFork(p) (p + 1) % n

inline pickUpLeft(p) {
    atomic{
        fork[leftFork(p)] == 0
        fork[leftFork(p)] = 1
    }
}

inline pickUpRight(p) {
    atomic{
        fork[rightFork(p)] == 0
        fork[rightFork(p)] = 1
    }
}

inline putDownLeft(p) {
    atomic{
        fork[leftFork(p)] == 1
        fork[leftFork(p)] = 0
    }
}

inline putDownRight(p) {
    atomic{
        fork[rightFork(p)] == 1
        fork[rightFork(p)] = 0
    }
}

inline think(p) {
    printf("philosopher %d thinks\n", p)
}

inline eat(p) {
    printf("philosopher %d eats\n", p)
}

proctype philosopher(int p) {
    bit isHungry = 0
    bit hasEaten = 0
    do
    :: // philosopher decides to think
       think(p)
    :: // philosopher decides to eat
       isHungry == 0 -> isHungry = 1
       // pick up forks in non-deterministic order
       isHungry == 1 && fork[leftFork(p)] == 0 -> pickUpLeft(p)
       isHungry == 1 && fork[rightFork(p)] == 0 -> pickUpRight(p)
       // eventually eat some spaghetti
       isHungry == 1 && fork[rightFork(p)] == 1 && fork[leftFork(p)] == 1 -> {
           eat(p)
           hasEaten = 1
           }
       /* ... implement me ... */
       // put down forks in non-deterministic order
       hasEaten == 1 && fork[leftFork(p)] == 1 -> putDownLeft(p)
       hasEaten == 1 && fork[rightFork(p)] == 1 -> putDownRight(p)
       /* ... implement me ... */
    od
}

init {
    int i = 0
    do
    :: i < n -> atomic { 
           printf("philosopher %d takes a seat\n", i)
           run philosopher(i)
           i = i + 1
       }
    :: i == n -> break
    od
}