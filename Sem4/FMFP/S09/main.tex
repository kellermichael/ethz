\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[a4paper,total={6in,9in}]{geometry}
\usepackage[ruled]{algorithm2e}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{physics}
\usepackage{graphicx}
\usepackage{bussproofs}
\graphicspath{ {./imgs/} }
\usepackage{stmaryrd}

\newcommand{\ra}{{\rightarrow}}
\newcommand{\Ra}{{\Rightarrow}}
\def\fCenter{\mbox{\ $\vdash$\ }}

\newenvironment{scprooftree}[1]%
  {\gdef\scalefactor{#1}\begin{center}\proofSkipAmount \leavevmode}%
  {\scalebox{\scalefactor}{\DisplayProof}\proofSkipAmount \end{center} }

\title{FMFP Assignment 9}
\author{Michael Keller}
\date{April 2021}

\begin{document}

\maketitle

\section*{Exercise 1}
\subsection*{1.1)}
We use the definitions from p.44 of the Power Point (sorry this is ugly :/):
\begin{align*}
  \sigma[x \mapsto v_1][x \mapsto v_2] = \sigma[x \mapsto v_2]
  &\Leftrightarrow \forall y. (\sigma[x \mapsto v_1][x \mapsto v_2](y) = \sigma[x \mapsto v_2](y))\\
  &= \forall y. \Bigg (
    \begin{cases}
      v & \text{if } y \equiv x\\
      \sigma[x \mapsto v_1](y) & \text{if } y \not \equiv x
    \end{cases}
    =
    \begin{cases}
      v & \text{if } y \equiv x\\
      \sigma(y) & \text{if } y \not \equiv x
    \end{cases}
    \Bigg)\\
  &= \forall y. \Bigg (
    \begin{cases}
      v & \text{if } y \equiv x\\
      \begin{cases}
        v_1 & \text{if } y \equiv x\\
        \sigma(y) & \text{if } y \not \equiv x
      \end{cases} & \text{if } y \not \equiv x
    \end{cases}
    =
    \begin{cases}
      v & \text{if } y \equiv x\\
      \sigma(y) & \text{if } y \not \equiv x
    \end{cases}
    \Bigg)\\
    &= \forall y. \Bigg (
      \begin{cases}
        v & \text{if } y \equiv x\\
        \sigma(y) & \text{if } y \not \equiv x
      \end{cases}
      =
      \begin{cases}
        v & \text{if } y \equiv x\\
        \sigma(y) & \text{if } y \not \equiv x
      \end{cases}
      \Bigg)\\
\end{align*}
\subsection*{1.2)}
We prove this by case distinction. Further we know that two states are equal
if they are equal as functions:
\begin{align*}
  \sigma_1 &= \sigma[x \mapsto v][y \mapsto w]\\
  \sigma_2 &= \sigma[y \mapsto w][x \mapsto v]\\
  \sigma_1 = \sigma_2
  &\Leftrightarrow \forall z. (\sigma_1(z) = \sigma_2(z))
\end{align*}
First we consider $z \equiv x$:
\begin{align*}
  \sigma_1(x)
  &= \sigma[x \mapsto v][y \mapsto w](x) & \text{Def. of } \sigma_1\\
  &= v & \text{Def. of update function}\\
  &= \sigma[y \mapsto w][x \mapsto v](x) & \text{Def. of update function}\\
  &= \sigma_2(x) & \text{Def. of } \sigma_2
\end{align*}
Next we consider $z \equiv y$:
\begin{align*}
  \sigma_1(y)
  &= \sigma[x \mapsto v][y \mapsto w](y) & \text{Def of } \sigma_1\\
  &= w & \text{Def of update function}\\
  &= \sigma[y \mapsto w][x \mapsto v](y) & \text{Def of update function}\\
  &= \sigma_2(y) & \text{Def of } \sigma_2
\end{align*}
Lastly we consider $z \not \equiv x$ and $z \not \equiv y$:
\begin{align*}
  \sigma_1(z)
  &= \sigma[x \mapsto v][y \mapsto w](z) & \text{Def of } \sigma_1\\
  &= \sigma(z) & \text{Def of update function}\\
  &= \sigma[y \mapsto w][x \mapsto v](z) & \text{Def of update function}\\
  &= \sigma_2(z) & \text{Def of } \sigma_2
\end{align*}\\
As we have considered all possibles cases for $z$ we can conclude $\sigma_1 = \sigma_2$.
\subsection*{1.3)}
We prove this with weak structural induction over $n$
(the length of the list of (arbitrary) updates).
Further we will utilize the previous two subtasks:\\ \\
Case $n = 0$:
\begin{align*}
  \sigma[x \mapsto v_1][x \mapsto v_2] = \sigma[x \mapsto v_2] && \text{Task 1.1}
\end{align*}
Hence our induction Hypothesis (I.H.) given $n$:
\begin{align*}
  \overrightarrow{y} &= \langle y_1, \dots, y_n \rangle\\
  \overrightarrow{w} &= \langle w_1, \dots, w_n \rangle\\
  \sigma[x \mapsto v_1][\overrightarrow{y} \mapsto \overrightarrow{w}][x \mapsto v_2]
  &= \sigma[\overrightarrow{y} \mapsto \overrightarrow{w}][x \mapsto v_2]
\end{align*}
Case $n+1$:\\
Subcase $y_{n+1} \not \equiv x$:
\begin{align*}
  \overrightarrow{y} &= \langle y_1, \dots, y_{n+1} \rangle\\
  \overrightarrow{w} &= \langle w_1, \dots, w_{n+1} \rangle\\
  \sigma[x \mapsto v_1][\overrightarrow{y} \mapsto \overrightarrow{w}][x \mapsto v_2]
  &= \sigma[x \mapsto v_1][y_1 \mapsto w_1] \dots [y_{n+1} \mapsto w_{n+1}][x \mapsto v_2] && \text{Def. list of updates}\\
  &= \sigma[x \mapsto v_1][y_1 \mapsto w_1] \dots [y_n \mapsto w_n][x \mapsto v_2][y_{n+1} \mapsto w_{n+1}] && \text{Task 1.2 \& Subcase}\\
  &= \sigma[y_1 \mapsto w_1] \dots [y_n \mapsto w_n][x \mapsto v_2][y_{n+1} \mapsto w_{n+1}] && \text{I.H.}\\
  &= \sigma[y_1 \mapsto w_1] \dots [y_n \mapsto w_n][y_{n+1} \mapsto w_{n+1}][x \mapsto v_2] && \text{Task 1.2 \& Subcase}\\
  &= \sigma[\overrightarrow{y} \mapsto \overrightarrow{w}][x \mapsto v_2] && \text{Def. list of updates}
\end{align*}
Subcase $y_{n+1} \equiv x$:
\begin{align*}
  \overrightarrow{y} &= \langle y_1, \dots, y_{n+1} \rangle\\
  \overrightarrow{w} &= \langle w_1, \dots, w_{n+1} \rangle\\
  \sigma[x \mapsto v_1][\overrightarrow{y} \mapsto \overrightarrow{w}][x \mapsto v_2]
  &= \sigma[x \mapsto v_1][y_1 \mapsto w_1] \dots [y_{n+1} \mapsto w_{n+1}][x \mapsto v_2] && \text{Def. list of updates}\\
  &= \sigma[x \mapsto v_1][y_1 \mapsto w_1] \dots [y_n \mapsto w_n][x \mapsto v_2] && \text{Task 1.1 \& Subcase}\\
  &= \sigma[y_1 \mapsto w_1] \dots [y_n \mapsto w_n][x \mapsto v_2] && \text{I.H.}\\
  &= \sigma[y_1 \mapsto w_1] \dots [y_n \mapsto w_n][y_{n+1} \mapsto w_{n+1}][x \mapsto v_2] && \text{Task 1.1 \& Subcase}\\
  &= \sigma[\overrightarrow{y} \mapsto \overrightarrow{w}][x \mapsto v_2] && \text{Def. list of updates}
\end{align*}

\section*{Exercise 2}
Let $\sigma, e$ and $x$ be arbitrary. We define the predicate:
\begin{align*}
  P(b) \equiv \Big( \mathcal{B}\llbracket b[x \mapsto e]\rrbracket \sigma
  = \mathcal{B}\llbracket b \rrbracket (\sigma[x \mapsto \mathcal{A} \llbracket e \rrbracket \sigma]) \Big )
\end{align*}
and prove $\forall b . P(b)$ by strong structural induction on $b$. We have to show $P(b)$
for some arbitrary expression $b$ and assume $\forall b' \sqsubset b \cdot P(b')$
as our induction hypothesis. We proceed by a case analysis on $b$:
\begin{itemize}
  \item Case $b \equiv i$ or $j$:
  \begin{align*}
    \mathcal{B}\llbracket (i \text{ or } j)[x \mapsto e]\rrbracket \sigma
    &= \mathcal{B}\llbracket i[x \mapsto e] \text{ or } j[x \mapsto e] \rrbracket \sigma
    && \text{Def. Substitution}\\
    &= \mathcal{B}\llbracket i[x \mapsto e] \rrbracket \sigma \text{ or } \mathcal{B}\llbracket j[x \mapsto e] \rrbracket \sigma
    && \text{Def. Boolean Expressions}\\
    &= \mathcal{B}\llbracket i \rrbracket (\sigma[x \mapsto \mathcal{A} \llbracket e \rrbracket \sigma])
    \text{ or }
    \mathcal{B}\llbracket j \rrbracket (\sigma[x \mapsto \mathcal{A} \llbracket e \rrbracket \sigma])
    && \text{I.H.}\\
    &= \mathcal{B}\llbracket i \text{ or } j \rrbracket (\sigma[x \mapsto \mathcal{A} \llbracket e \rrbracket \sigma])
    && \text{Def. Boolean Expressions}\\
  \end{align*}
  \item Case $b \equiv i$ and $j$:
  \begin{align*}
    \mathcal{B}\llbracket (i \text{ and } j)[x \mapsto e]\rrbracket \sigma
    &= \mathcal{B}\llbracket i[x \mapsto e] \text{ and } j[x \mapsto e] \rrbracket \sigma
    && \text{Def. Substitution}\\
    &= \mathcal{B}\llbracket i[x \mapsto e] \rrbracket \sigma \text{ and } \mathcal{B}\llbracket j[x \mapsto e] \rrbracket \sigma
    && \text{Def. Boolean Expressions}\\
    &= \mathcal{B}\llbracket i \rrbracket (\sigma[x \mapsto \mathcal{A} \llbracket e \rrbracket \sigma])
    \text{ and }
    \mathcal{B}\llbracket j \rrbracket (\sigma[x \mapsto \mathcal{A} \llbracket e \rrbracket \sigma])
    && \text{I.H.}\\
    &= \mathcal{B}\llbracket i \text{ and } j \rrbracket (\sigma[x \mapsto \mathcal{A} \llbracket e \rrbracket \sigma])
    && \text{Def. Boolean Expressions}\\
  \end{align*}
  \item Case $b \equiv$ not $i$:
  \begin{align*}
    \mathcal{B}\llbracket (\text{not } i)[x \mapsto e]\rrbracket \sigma
    &= \mathcal{B}\llbracket \text{not } (i[x \mapsto e])\rrbracket \sigma
    && \text{Def. Substitution}\\
    &= \text{not } \mathcal{B}\llbracket i[x \mapsto e]\rrbracket \sigma
    && \text{Def. Boolean Expressions}\\
    &= \text{not } \mathcal{B}\llbracket i \rrbracket (\sigma[x \mapsto \mathcal{A} \llbracket e \rrbracket \sigma])
    && \text{I.H.}\\
    &= \mathcal{B}\llbracket \text{not } i \rrbracket (\sigma[x \mapsto \mathcal{A} \llbracket e \rrbracket \sigma])
    && \text{Def. Boolean Expressions}\\
  \end{align*}
  \item Case $b \equiv i$ op $j$:
  \begin{align*}
    \mathcal{B}\llbracket (i \text{ op } j)[x \mapsto e]\rrbracket \sigma
    &= \mathcal{B}\llbracket i[x \mapsto e] \text{ op } j[x \mapsto e] \rrbracket \sigma
    && \text{Def. Substitution}\\
    &= \mathcal{B}\llbracket i[x \mapsto e] \rrbracket \sigma
    \;\; \overline{\text{op}} \;\;
    \mathcal{B}\llbracket j[x \mapsto e] \rrbracket \sigma
    && \text{Def. Boolean Expressions}\\
    &= \mathcal{A}\llbracket i \rrbracket (\sigma[x \mapsto \mathcal{A} \llbracket e \rrbracket \sigma])
    \;\; \overline{\text{op}} \;\;
    \mathcal{A}\llbracket j \rrbracket (\sigma[x \mapsto \mathcal{A} \llbracket e \rrbracket \sigma])
    && \text{I.H.}\\
    &= \mathcal{B}\llbracket i \text{ op } j \rrbracket (\sigma[x \mapsto \mathcal{A} \llbracket e \rrbracket \sigma])
    && \text{Def. Boolean Expressions}\\
  \end{align*}
\end{itemize}
\section*{Exercise 3}
Let $e$ be an arbitrary arithmetic expression. We define the predicates:
\begin{align*}
  Q &= (\forall x \cdot x \in FV(e) \hspace{0.2cm} \Ra \hspace{0.2cm} \sigma(x) = \sigma'(x))\\
  P(e) &= Q
  \hspace{0.2cm} \Ra \hspace{0.2cm} \mathcal{A} \llbracket e \rrbracket \sigma
  = \mathcal{A} \llbracket e \rrbracket \sigma'
\end{align*}
and prove $\forall e \cdot P(e)$ by strong structural induction on $e$. Thus we have
to prove $P(e)$ for $e$ and assume $\forall e' \sqsubset e \cdot P(e')$ as our induction
hypothesis. We proceed by a case analysis on $e$:
\begin{itemize}
  \item Case $e \equiv i$ op $j$ assuming $Q$:
  \begin{align*}
    \mathcal{A} \llbracket i \text{ op } j \rrbracket \sigma
    &= \mathcal{A} \llbracket i\rrbracket \sigma
    \;\; \overline{\text{op}} \;\;
    \mathcal{A} \llbracket j \rrbracket \sigma
    && \text{Def. Arithm. Eval.}\\
    &= \mathcal{A} \llbracket i\rrbracket \sigma'
    \;\; \overline{\text{op}} \;\;
    \mathcal{A} \llbracket j \rrbracket \sigma'
    && \text{I.H. and }Q\\
    &= \mathcal{A} \llbracket i \text{ op } j \rrbracket \sigma'
    && \text{Def. Arithm. Eval.}\\
  \end{align*}
  Hence $Q \hspace{0.2cm} \Ra \hspace{0.2cm} \mathcal{A} \llbracket i \text{ op } j \rrbracket \sigma = \mathcal{A} \llbracket i \text{ op } j \rrbracket \sigma'$
  \item Case $e \equiv y$ for some variable $y$ assuming $Q$:
  \begin{align*}
    \mathcal{A} \llbracket y \rrbracket \sigma
    &= \sigma(y)
    && \text{Def. Arithm. Eval.}\\
    &= \sigma'(y)
    && y \in FV(e) \text{ and Q}\\
    &= \mathcal{A} \llbracket y \rrbracket \sigma'
    && \text{Def. Arithm. Eval.}\\
  \end{align*}
  Hence $Q \hspace{0.2cm} \Ra \hspace{0.2cm} \mathcal{A} \llbracket y \rrbracket \sigma = \mathcal{A} \llbracket y \rrbracket \sigma'$
  \item Case $e \equiv n$ for some numeral $n$:
  \begin{align*}
    \mathcal{A} \llbracket n \rrbracket \sigma
    &= \mathcal{N} \llbracket n \rrbracket
    && \text{Def. Arithm. Eval.}\\
    &= \mathcal{A} \llbracket n \rrbracket \sigma'
    && \text{Def. Arithm. Eval.}\\
  \end{align*}
  Hence $\mathcal{A} \llbracket n \rrbracket \sigma = \mathcal{A} \llbracket n \rrbracket \sigma'$ which implies $Q \hspace{0.2cm} \Ra \hspace{0.2cm} \mathcal{A} \llbracket n \rrbracket \sigma = \mathcal{A} \llbracket n \rrbracket \sigma'$
\end{itemize}
\end{document}