\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[a4paper,total={6in,9in}]{geometry}
\usepackage[ruled]{algorithm2e}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{physics}
\usepackage{graphicx}
\usepackage{bussproofs}
\graphicspath{ {./imgs/} }
\usepackage{stmaryrd}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric}
\usepackage{ stmaryrd }

\newcommand{\ra}{{\; \rightarrow \;}}
\newcommand{\Ra}{{\; \Rightarrow \;}}
\def\fCenter{\mbox{\ $\vdash$\ }}
\newcommand{\subtree}[3]{
  \begin{tikzpicture}
    \node [trapezium, trapezium angle=#3, trapezium stretches = true, minimum width=#2 mm, draw] {#1};
  \end{tikzpicture}
}

\newenvironment{scprooftree}[1]%
  {\gdef\scalefactor{#1}\begin{center}\proofSkipAmount \leavevmode}%
  {\scalebox{\scalefactor}{\DisplayProof}\proofSkipAmount \end{center} }

\title{FMFP Assignment 10}
\author{Michael Keller}
\date{May 2021}

\begin{document}

\maketitle

\section*{Exercise 1}
We define:
\begin{align*}
  l_1 &:= a := a + n\\
  l_2 &:= b := b * n\\
  l_3 &:= n := n - 1\\
  l &:= (l_1; l_2); l_3
\end{align*}
then derive helper $h_1$:
\begin{prooftree}
  \AxiomC{}
  \RightLabel{$ASS_{NS}$}
  \UnaryInfC{$\langle l_1, [0,1,2]\rangle \ra [2,1,2]$}
  \AxiomC{}
  \RightLabel{$ASS_{NS}$}
  \UnaryInfC{$\langle l_2, [2,1,2]\rangle \ra [2,2,2]$}
  \RightLabel{$SEQ_{NS}$}
  \BinaryInfC{$\langle (l_1;l_2), [0,1,2]\rangle \ra [2,2,2]$}
  \AxiomC{}
  \RightLabel{$ASS_{NS}$}
  \UnaryInfC{$\langle l_3, [2,2,2]\rangle \ra [2,2,1]$}
  \RightLabel{$SEQ_{NS}$}
  \BinaryInfC{$\langle l, [0,1,2]\rangle \ra [2,2,1]$}
\end{prooftree}
helper $h_2$:
\begin{prooftree}
  \AxiomC{}
  \RightLabel{$ASS_{NS}$}
  \UnaryInfC{$\langle l_1, [2,2,1]\rangle \ra [3,2,1]$}
  \AxiomC{}
  \RightLabel{$ASS_{NS}$}
  \UnaryInfC{$\langle l_2, [3,2,1]\rangle \ra [3,2,1]$}
  \RightLabel{$SEQ_{NS}$}
  \BinaryInfC{$\langle (l_1;l_2), [2,2,1]\rangle \ra [3,2,1]$}
  \AxiomC{}
  \RightLabel{$ASS_{NS}$}
  \UnaryInfC{$\langle l_3, [3,2,1]\rangle \ra [3,2,0]$}
  \RightLabel{$SEQ_{NS}$}
  \BinaryInfC{$\langle l, [2,2,1]\rangle \ra [3,2,0]$}
\end{prooftree}
and finally the main statement:
\begin{scprooftree}{0.9}
  % Subtree 1
  \AxiomC{}
  \RightLabel{$h_1$}
  \UnaryInfC{$\langle l, [0,1,2]\rangle \ra [2,2,1]$}

  % Subtree 2
  \AxiomC{}
  \RightLabel{$h_2$}
  \UnaryInfC{$\langle l, [2,2,1]\rangle \ra [3,2,0]$}
  \AxiomC{}
  \RightLabel{$WHF_{NS}$ (as $\sigma(n) = 0$)}
  \UnaryInfC{$\langle \text{while n \# 0 do l end}, [3,2,0]\rangle \ra [3,2,0]$}
  \RightLabel{$WHT_{NS}$ (as $\sigma(n) = 1 \; \# \; 0$)}
  \BinaryInfC{$\langle \text{while n \# 0 do l end}, [2,2,1]\rangle \ra [3,2,0]$}
  
  % Split 1
  \RightLabel{$WHT_{NS}$ (as $\sigma(n) = 2 \; \# \; 0$)}
  \BinaryInfC{$\langle \text{while n \# 0 do l end}, [0,1,2]\rangle \ra [3,2,0]$}
\end{scprooftree}
\section*{Exercise 2}
Let $\sigma, \sigma', b$ and $s$ be arbitrary. We assume
a derivation tree $T$ exists with
$$root(T) \equiv \langle \text{if b then s; while b do s end else skip end, }\sigma \rangle \ra \sigma'$$
The last rule application in $T$ must be one of the two rules for if:
\begin{itemize}
  \item Case: IF F$_{NS}$ is the last rule applied. Thus $T$ is of the form:
  \begin{scprooftree}{1.0}
    \AxiomC{}
    \RightLabel{Skip$_{NS}$}
    \UnaryInfC{$\langle \text{skip}, \sigma \rangle \ra \sigma$}
    \RightLabel{IF F$_{NS}$}
    \UnaryInfC{$\langle \text{if b then s; while b do s end else skip end, }\sigma \rangle \ra \sigma$}
  \end{scprooftree}
  and we know:
  \begin{enumerate}
    \item $\sigma = \sigma'$
    \item $\mathcal{B} \llbracket b \rrbracket \sigma = \text{ff}$
  \end{enumerate}
  Thus we can construct the derivation tree:
  \begin{scprooftree}{1.0}
    \AxiomC{}
    \RightLabel{WH F$_{NS}$}
    \UnaryInfC{$\langle \text{while b do s end, }\sigma \rangle \ra \sigma'$}
  \end{scprooftree}
  \item Case: IF T$_{NS}$ is the last rule applied. Thus $T$ is of the form:
  \begin{scprooftree}{1.0}
    \AxiomC{\subtree{$T_1$}{20}{110}}
    \UnaryInfC{$\langle s, \sigma \rangle \ra \sigma''$}
    \AxiomC{\subtree{$T_2$}{20}{110}}
    \UnaryInfC{$\langle \text{while b do s end}, \sigma'' \rangle \ra \sigma'$}
    \RightLabel{Seq$_{NS}$}
    \BinaryInfC{$\langle \text{s; while b do s end}, \sigma \rangle \ra \sigma'$}
    \RightLabel{IF T$_{NS}$}
    \UnaryInfC{$\langle \text{if b then s; while b do s end else skip end, }\sigma \rangle \ra \sigma'$}
  \end{scprooftree}
  and we know $\mathcal{B} \llbracket b \rrbracket \sigma = \text{tt}$. Thus:
  \begin{scprooftree}{1.0}
    \AxiomC{\subtree{$T_1$}{20}{110}}
    \UnaryInfC{$\langle s, \sigma \rangle \ra \sigma''$}
    \AxiomC{\subtree{$T_2$}{20}{110}}
    \UnaryInfC{$\langle \text{while b do s end}, \sigma'' \rangle \ra \sigma'$}
    \RightLabel{WH T$_{NS}$}
    \BinaryInfC{$\langle \text{while b do s end}, \sigma \rangle \ra \sigma'$}
  \end{scprooftree}
\end{itemize}
As the statements holds in all cases it must be true.
\section*{Exercise 3}
Here we perform an induction on the shape of derivation trees to prove
the property $\forall T . P(T)$ where:
$$
P(T) \equiv \forall s, \sigma, \sigma', x \cdot \Big( (root(T) \equiv \langle s, \sigma \rangle \ra \sigma') \land x \not \in FV(s) \Ra \sigma'(x) = \sigma(x) \Big)
$$
Let $T$ be an arbitrary derivation tree and let $s, \sigma, \sigma', x$ be arbitrary too.
We assume:
\begin{itemize}
  \item $root(T) \equiv \langle s, \sigma \rangle \ra \sigma'$
  \item $x \not \in FV(s)$
\end{itemize}
Now we perform a case distinction on the last rule applied in $T$. This yields
seven different cases:
\begin{itemize}
  \item Case Skip$_{NS}$: This implies $\sigma \equiv \sigma'$ and hence $\sigma(x) = \sigma'(x)$
  \item Case Ass$_{NS}$: This implies $s \equiv \langle a := e, \sigma \rangle \ra \sigma[a \mapsto \mathcal{A} \llbracket e \rrbracket \sigma]$,
  hence $a$ is free in $s$. Thus either the left side of the implication won't be true or
  when $x \neq a$ we have $\sigma(x) \neq \sigma'(x)$
  \item Case Wh F$_{NS}$: This implies $\sigma \equiv \sigma'$ and hence $\sigma(x) = \sigma'(x)$
  \item Case Wh T$_{NS}$: Applying the rule yields the two statements $\langle s, \sigma \rangle \ra \sigma'$
        and $\langle \text{while b do s end, } \sigma'\rangle \ra \sigma''$. As these are proper
        substatements by our induction hypothesis both statements satisfy for $x \not \in FV(s)$
        $\sigma(x) \equiv\sigma'(x)$. Thus for the combination of the statements we can conclude
        the same.
  \item Case If F$_{NS}$: This implies $\sigma \equiv \sigma'$ and hence $\sigma(x) = \sigma'(x)$
  \item Case If T$_{NS}$: Applying the rule yields $\langle s, \sigma \rangle \ra \sigma'$. As this
        is a proper substatement we apply our induction hypothesis and again arrive at the conclusion
        for $x \not \in FV(s)$ that $\sigma(x) = \sigma'(x)$.
\end{itemize}
This concludes the proof.
\end{document}