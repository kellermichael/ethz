# Exercise 8: Network Layer

## Question 1: Network Address Translation (NAT)

Unlike the traditional client-server model, in peer-to-peer (P2P) applications, peers communicate with each other directly.
In this question, we explore how network address translation (NAT) complicates P2P communication.
When two peers both have public IP addresses, P2P communication works without a hitch, as both peers are directly reachable.
But in reality, most Internet users are behind a NAT device, and these devices can disrupt P2P communication.
Despite NAT devices, we can enjoy P2P applications, such as BitTorrent, Skype, etc. We will explore here how peers behind NAT devices can still connect to each other for P2P applications.

For the following questions, assume that all communication is taking place over UDP.
Also, assume that the NAT devices behave identically for UDP and TCP, i.e, they perform the same address and port translation:

### Q1.1

Revisiting the traditional client-server model, the Figure below illustrates a typical communication model that we see in the Internet, where a client is behind a NAT and a server has a public IP address.
How can the client access the server?
How can the server respond back to the client?
Assume that the server listens for UDP packets over port 10,000.
Please illustrate the information the NAT needs to maintain and how this information is used by the NAT.

| ![](assets/nat_cs.png) |
|:--:|
| *Client/Server Model* |

Basically the NAT performs the clients request on the clients behalf. Once it's done it returns the information to the client. The NAT can distinguish between incoming responses by the port number the responses are addressed to (and then relay them to the correct device).

### Q1.2

Now let's shift our discussion to P2P communication.
As shown in the Figure below, both Peer A and B are behind NAT devices.
Can peers communicate with each other?
Explain and provide a brief justification.
Assume that peers know each other's external IP addresses *a priori*.

| ![](assets/nat_pp.png) |
|:--:|
| *P2P Model* |

Only the external IP address is not sufficient. The NAT needs to know to what device in the network it needs to forward. This could be fixed by both hosts first communicating with the Skype server and informing each other on what port the NAT will redirect to them. (Assuming the external IP address does not include the port)

### Q1.3

Most P2P systems (including Skype) have special servers called *rendezvous* servers to assist Peer A to talk with Peer B as shown in the Figure below.
Let's consider a Skype call between the two peers, where Peer A (whose Skype ID is "OS") calls Peer B (whose Skype ID is "NET").
How can this call be made?
Please explain the steps that are necessary for Peer A, Peer B and the rendezvous server to make this call work.
Furthermore, please explain the information that may be necessary at the two NATs and the rendezvous server.
Assume that the rendezvous server listens for UDP packets over port 10,000.
**Hint:** You may want to define application layer messages between the peers and the rendezvous server.

| ![](assets/nat_skype.png) |
|:--:|
| *P2P Model with Rendezvous Server* |

Peer A needs to contact the rendezvous server (Port 10'000 via UDP) with B's skype id. B regularly keeps in touch with the rendezvous server to let it know the port it can be contacted on via the NAT. A then gets that port from the Rendezvous server and can contact B.


## Question 2: Dijkstra's Algorithm

### Q2.1

A network between A and B is depicted in the Figure below.
The numbers on the links, $`f_i`$, correspond to the probability that the respective link may fail.
We assume link failures to be independent from each other.

How could you find the most reliable path from A to B?

*Hint:* Define appropriate weights based on the $`f_i`$ and use Dijkstra's algorithm.

| ![](assets/dijkstra.png) |
|:--:|
| *Network Topology with failure probabilities of links, $`f_i`$* |

We define the Weight as $100\% - f_i$ and then run Dijkstra.

(Should be A - C - D - B)


## Question 3: Distance-Vector Routing

### Q3.1

In the Figure below, after the network stabilizes, the link connecting nodes C and D becomes broken. 
Show how nodes A, B and C might experience the count-to-infinity phenomenon for their distances to node D. 
In demonstrating the count-to-infinity problem, show at least two updates containing distance-to-D, for each of the nodes A, B and C. 
For each of these updates, specify the distance-to-D advertised to the neighbors. 
Will the problem be resolved after some iterations?

| ![](assets/dv.png) |
|:--:|
| *Network Topology* |

Node A:
Iter. | B | C | D |<br>
0       2   1   3 |<br>
1       2   1   6 |<br>
2       2   1   8 |<br>

Node B:
Iter. | A | C | D |<br>
0       2   1   3 |<br>
1       2   1   6 |<br>
2       2   1   8 |<br>

Node c:
Iter. | A | B | D |<br>
0       1   1   2 |<br>
1       1   1   5 |<br>
2       1   1   7 |<br>

These issues will eventually be resolved, because B can provide a path to C. Thus once they all count to 101 for C-D the system will stabilize.