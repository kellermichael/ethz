# Exercise 4: Transport principles

## Question 1: Bandwidth delay product

If Alice and Bob are connected via a 100 Mbps connection, which has a constant 5 ms RTT, what would be the ideal congestion window size *W* in kilobytes?

Senders send rate is $\approx \frac{cwnd}{0.005} = 100Mbps$. Hence $cwnd = 0.5$ Mb $= \frac{512}{8} = 64$ KB


## Question 2: Sliding window

A sliding window is used in a transport protocol to enable high throughput. Part of such a transport protocol is to negotiate the sender window size and receiver window size. Why is it a desirable property that *sender window* <= *receiver window*?

If the sender sends everything in their window and the receivers window is smaller, then should the packets get reordered or some lost, the later packets that are in the senders window but not currently the receivers will get dropped by the receiver. Thus the sender will need to resend these, resulting in inefficiency.


## Question 3: 

In this exercise, there are two machines, each having one flow going over
the same link. Both machines use additive increase multiplicative
decrease (AIMD) to control their rates.

1. Label the two lines that are drawn in the graph (not the axes).

Line with negative slope: Efficiency line (Network Utilization is maxed out here)

Line with positive slope: Fairness Line (Both senders are sending the same amount of data)

2. Point A shows the current bandwidth of machines 1 and 2. Which machine is using more bandwidth, and is the link saturated?

Machine 1 is using more Bandwidth, and the link is not saturated (far away from efficiency line)

3. Following AIMD, draw the vector resulting from point A until the state of the algorithm changes. Do this one more time. Explain your reasoning.

First 45 degree smaller increase, then larger decrease in the direction of the origin:
![image](assets/fair_sharing_question_my_sol.png)

4. Will AIMD convergence to fair sharing in this case?

Yes

![image](assets/fair_sharing_question.png)


## Question 4: Congestion signals (Previous Exam Question \[1\])

List three signals that warn about congestion in the network, and mention one advantage and
one disadvantage of using each signal.

Packet Delay (Problem: Signal is noisy, delay varies considerably, Good: Warning signal without having to actually loose packets)

Packet Loss (Problem: If it comes to this it's kind of already a bit late, Good: We have to measure this anyway, might as well use the info)

Network itself detects and notifies (Problem: What if network notification gets lost, Good: Global state information)

[1] Operating Systems & Networks (252-0062-00L); Spring Semester 2016
    (FS16). Question 9a.


## Question 5: Reliable transport protocol

1. Name five undesirable effects the network can have on individual packets and/or a stream of packets when attempting to deliver them to the destination.

Data Corruption, Change of the packet ordering, Dropping packets, Sending duplicate packets, overwhelming the receiver

2. What are the four goals of reliable transfer? Describe them shortly.

Avoid Data Corruption, ensure delivery, ensure in order delivery, rate limit to avoid network congestion/ overwhelming the receiver

3. What mechanism is used to detect corrupted packets? Shortly explain.

Corrupt packets can be detected with a hash of the relevant Data. If the data changes during transport the hash won't match up anymore (with a very high probability)

4. How would a reliable transport protocol deal with packets that were duplicated by the network? 

Resend the acknowledgement, the sender most likely didn't get our ACK (or not in time). As we already know the contents however we can drop the packets.


## Question 6: Timeouts

1. In reliable transmission, what is the effect of setting a too short timeout, and conversely, what is the effect of setting a too long timeout?

Too short timeout => Resend data too ofter because we aren't patient enough to wait for the ACK's that are on their way.

Too long timeout => Waiting around unnecessarily even tough we know the packet probably got lost
    
2. RTT is a higher-level network property that is mainly used to decide what the re-transmission timeout should be set to. Which four network delays determine the RTT? 

Processing Delay, Queuing Delay, Transmission Delay, Propagation Delay

3. Of these four network delays, what changes in the network can you do to reduce the RTT?

Upgrading Networking Infrastructure can improve Processing and Transmission Delay, while improving bandwidth specifically can improve the Queuing Delay. As the propagation is bounded by the speed of light and geography in many places this can no longer be improved.

4. When a timeout is triggered in the sender, (how) can the sender be sure that the receiver has really not received the packet?

This is possible either with a negative ACK or a logical equivalent (as multiple ACK's for the same lower object when sending higher numbered objects). However with the absence of any ACK's it is impossible to know.


## Question 7: Go-Back-N

For each of the following statements indicate for each if it is true or false.

1. Go-Back-N acknowledges only the last in-order packet it has received. TRUE
     
2. Go-Back-N senders maintain a timer for each packet sent out. FALSE

3. If a Go-Back-N receiver receives an out-of-order packet, it is possible to move the sliding receiver window along. TRUE, provided this fills the "leftmost"/ most immediate gap, otherwise FALSE
