#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stddef.h>
#include <assert.h>
#include <poll.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <netinet/in.h>

#include "rlib.h"
#include "buffer.h"

struct reliable_state {
    rel_t *next;			/* Linked list for traversing all connections */
    rel_t **prev;

    conn_t *c;			/* This is the connection object */

    /* Add your own data fields below this */

    // parameters
    size_t max_window;
    size_t timeout;

    // windows
    size_t snd_una;
    size_t snd_nxt;
    size_t rcv_nxt;

    buffer_t* send_buffer;
    buffer_t* rec_buffer;

    // EOF
    size_t EOF_partner;
    size_t EOF_me;
    size_t wait_to_terminate;


};
rel_t *rel_list;

long
get_current_time() {
    struct timeval now;
    gettimeofday(&now, NULL);
    return (long) now.tv_sec * 1000 + now.tv_usec / 1000;
}


packet_t*
build_data_packet(size_t data_length, size_t seqno, size_t ackno, char *data) {
    // build
    packet_t* pkt = xmalloc(12 + data_length);
    pkt->len = htons(12 + data_length);
    pkt->seqno = htonl(seqno);
    pkt->ackno = htonl(ackno);
    memcpy(pkt->data, data, data_length);

    // sign
    pkt->cksum = 0;
    pkt->cksum = cksum(pkt, 12 + data_length);

    return pkt;
}

packet_t*
build_ack_packet(size_t ackno) {
    // build
    packet_t *ack;
    ack = xmalloc(8);
    ack->len = htons(8);
    ack->ackno = htonl(ackno);

    // sign
    ack->cksum = 0;
    ack->cksum = cksum (ack, 8);

    return ack;
}

size_t
verify_signature(packet_t* pkt) {
    uint16_t pkt_cksum = pkt->cksum;
    pkt->cksum = 0;
    return cksum(pkt, ntohs(pkt->len)) == pkt_cksum;
}




/* Creates a new reliable protocol session, returns NULL on failure.
* ss is always NULL */
rel_t *
rel_create (conn_t *c, const struct sockaddr_storage *ss,
const struct config_common *cc)
{
    rel_t *r;

    r = xmalloc (sizeof (*r));
    memset (r, 0, sizeof (*r));

    if (!c) {
        c = conn_create (r, ss);
        if (!c) {
            free (r);
            return NULL;
        }
    }

    r->c = c;
    r->next = rel_list;
    r->prev = &rel_list;
    if (rel_list)
    rel_list->prev = &r->next;
    rel_list = r;

    /* Do any other initialization you need here */
    // windows
    r->snd_una = 1; // lowest seqno of outstanding frame
    r->snd_nxt = 1; // seqno of next frame to be sent out
    r->rcv_nxt = 1; // next expected seqno

    r->max_window = cc->window;
    r->timeout = cc->timeout;

    r->send_buffer = xmalloc(sizeof(buffer_t));
    r->rec_buffer = xmalloc(sizeof(buffer_t));
    r->rec_buffer->head = NULL;
    r->send_buffer->head = NULL;

    // EOF
    r->EOF_partner = 0;
    r->EOF_me = 0;
    r->wait_to_terminate = 0;

    return r;
}

void
rel_destroy (rel_t *r)
{
    if (r->next)
        r->next->prev = r->prev;
    *r->prev = r->next;
    conn_destroy (r->c);

    /* Free any other allocated memory here */
    buffer_clear(r->send_buffer);
    free(r->send_buffer);
    buffer_clear(r->rec_buffer);
    free(r->rec_buffer);

}


void
rel_recvpkt (rel_t *r, packet_t *pkt, size_t n)
{
    // Test bad len or corruption
    if(n != ntohs(pkt->len) || !verify_signature(pkt)) {
        return;
    }

    if(n == 8) {
        // remove packet from sent buffer
        buffer_remove(r->send_buffer, ntohl(pkt->ackno));

        // move up window if possible
        size_t ackno = ntohl(pkt->ackno);
        if(r->snd_una < ackno) {
            r->snd_una = ackno;

            // send new data
            rel_read(r);
        }
    } else {

        if(r->rcv_nxt > ntohl(pkt->seqno) || ntohl(pkt->seqno) >= r->rcv_nxt + r->max_window) {
            // packet is out of frame
        } else {
            // insert into receive buffer
            if(!buffer_contains(r->rec_buffer, ntohl(pkt->seqno))) {
                buffer_insert(r->rec_buffer, pkt, 0);
            }
        }

        // move up rcv_nxt if possible & ACK either way
        rel_output(r);
    }

}


void
rel_read (rel_t *s)
{
    size_t max_length = 500;
    char* buf = (char*) xmalloc(max_length * sizeof(char));

    // While there is data to be sent and place in our buffer for it
    while((s->snd_nxt - s->snd_una) < s->max_window && !s->EOF_me) {

        // get data
        size_t data_length = conn_input(s->c, buf, max_length);
        if(data_length == 0) {
            break;
        }

        // Handle EOF crap
        if(data_length == -1) {
            data_length = 0;
            s->EOF_me = 1;
        }
        
        // build packet
        packet_t* pkt = build_data_packet(data_length, s->snd_nxt, s->rcv_nxt, buf);
        s->snd_nxt++;

        // send
        buffer_insert(s->send_buffer, pkt, get_current_time());
        conn_sendpkt(s->c, pkt, 12 + data_length);

        // cleanup
        free(pkt);
    }

    // cleanup
    free(buf);
}

void
rel_output (rel_t *r)
{
    buffer_node_t* cur = buffer_get_first(r->rec_buffer);
    while(cur && ntohs(cur->packet.len) - 12 < conn_bufspace(r->c) && ntohl(cur->packet.seqno) == r->rcv_nxt) {
        // detect partner EOF
        if(ntohs(cur->packet.len) == 12) {
            r->EOF_partner = 1;
        }
        
        // print out
        conn_output(r->c, cur->packet.data, ntohs(cur->packet.len) - 12);
        buffer_remove_first(r->rec_buffer);

        // ACK prep
        r->rcv_nxt++;

        // move
        cur = cur->next;
    }

    // Reset Timeout if Applicable
    if(r->wait_to_terminate) {
        r->wait_to_terminate = 0;
    }

    // ACK
    packet_t* ack = build_ack_packet(r->rcv_nxt);
    conn_sendpkt(r->c, ack, 8);
            
    // cleanup
    free(ack);
    
}

void
rel_timer ()
{
    // Iterate through connections
    rel_t* r = rel_list;
    while(r) {
        // Resend Some Packets
        buffer_node_t* next = buffer_get_first(r->send_buffer);
        while(next) {

            // Test if too old & resend
            if(next->last_retransmit + r->timeout < get_current_time()) {
                conn_sendpkt(r->c, &(next->packet), ntohs(next->packet.len));
                next->last_retransmit = get_current_time();
            }

            next = next->next;
        }

        // Terminate Connection if applicable
        if(r->EOF_me && r->EOF_partner && buffer_size(r->send_buffer) == 0 && buffer_size(r->rec_buffer) == 0) {
            if(r->wait_to_terminate > 2) {
                rel_destroy(r);
            } else {
                r->wait_to_terminate++;
            }
        }

        r = r->next;
    }
}
