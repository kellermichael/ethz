# Exercise 9: IP / BGP

## Question 1: Network Adress Translation

[RFC 1918](https://tools.ietf.org/html/rfc1918)
"Address Allocation for Private Internets" specifies 10.0.0.0/8, 172.16.0.0/12, and 192.168.0.0/16 as private addresses suitable for unrestricted private internal use.
Many home networks use the 192.168.1.0/24 address space and use NAT to a single public address.

### Q1.1

Could 172.28.2.0/24 be used as the address space for an internal network? Explain your answer.

Yes, it is a subspace of the reserved 172.16.0.0/12 Block.<br>
172 = 172, 16/4 = 28/4

### Q1.2

Could 10.255.255.0/24 be used as the address space for an internal network? Explain your answer.

Yes, as listed above: Any IP-Block that starts with 10 can be used
as an address space for an internal network.

### Q1.3

Assume a technically illiterate user configures 8.8.8.0/24 as the internal address space on their home router.
Assume that NAT is used at the router, and there is a public interface on that router with IP address 19.33.93.140.
Briefly explain what happens if a user at a system with IP 8.8.8.2 attempts to establish a network connection with a system on the Internet at IP address 8.8.8.8 (Google's public DNS).
Can the two systems communicate? Why? Why not?

The two services can't communicate, because the network will think the user is trying to communicate with a device on the private internal network. Thus the query never goes onto the public internet.

## Question 2: The Border Gateway Protocol

Consider the AS-level topology in the picture below:

![Network topology](assets/fig3.png)

Say AS1 starts by announcing prefix 13.13.0.0/16 into the network at time 0.
All routing messages exchanged in this problem pertain to the prefix 13.13.0.0/16.
At time 1, each of AS1's neighbors -- AS2, AS5 and AS6 -- propagate the route to their neighbors, and so on.
*Hint:* Note that ASes only announce paths for traffic for which they can earn money and they prefer paths that increase their revenue.

Your answers should follow the following format:
- Prefix: 13.13.0.0/16
- Path: {AS1}
- Send To: AS2, AS5, AS6

### Q2.1

Show the path vector that AS5 sends out at time 1.
To which of its neighbors does AS5 send this path vector?

- Prefix: 13.13.0.0/16
- Path: {AS1, AS5}
- Send To: AS4, AS7

Note: this isn't sent to AS6 because that would mean AS5 does free work for AS6.

### Q2.2

Show the path vectors that AS7 sends out at time 2.
To which of its neighbors does AS7 send the path vectors?


To nobody, as AS7 only peers and 13.13.0.0/16 isn't in it's own network.

### Q2.3

A valid AS-level path is one that is allowed by economic policies.
List all possible, valid, AS-level paths from AS8 to the prefix 13.13.0.0/16.
Argue why {AS8, AS7, AS6, AS1} is a valid (or invalid) AS-level path from AS 8 to the prefix 13.13.0.0/16?

The only valid path is: {AS8, AS4, AS5, AS1}. All other paths (including {AS8, AS7, AS6, AS1}) involve routes that use a peer on a part of the path that isn't the final destination. Hence the peer would be doing free work.

### Q2.4

Which path will AS8 use to route to the prefix 13.13.0.0/16?
Why?

It will use {AS8, AS4, AS5, AS1}, as that is the only option.

### Q2.5

Assume that Link 5--7 is removed from the topology.
Would AS7 announce the prefix 13.13.0.0/16 with {AS7, AS6, AS1} to AS8?
Briefly explain.

It wouldn't. The reason is that AS7 doesn't actually know that that path exists. Peers only tell each other of paths in their own network, not ones they can forward to. Otherwise they would be doing free work.

## Question 3: BGP Convergence

 In this problem, we study the convergence of the BGP routing protocol.
We will consider BGP routers that implement two policies: the route selection policy and the export policy.
The route selection policy is used to select the preferred route to the destination, based on the routes learned from the neighboring routers.
The export policy is used to select which routes would be announced to each neighboring router.

![Network topology](assets/fig4.png)

For this problem and the ASes in the figure above, we make the following assumptions:
* All ASes in Figure 1 have the same route selection and export policies, as follows:
  - **Route Selection Policy:** The boxes with the dotted lines in the figure above show the preferred routes of each AS towards the destination D. If there are two routes in the box, ASes prefer the upper route over the lower route. For example, AS 3 prefers route {321D} over route {34D} to reach AS D, and uses route {34D} only if route {321D} is not available. Moreover, if no preferred route is available, ASes do not select any route to the destination, and the corresponding routing table becomes empty. For example, if no preferred routes for AS 2 is available, then AS 2 keeps its routing table empty rather than using route {2D}.
  - **Export Policy:** Announce the route that has been selected by the route selection policy to all neighboring ASes. For example, AS 1 announces the route {1D} to AS 2.
* Each AS has one BGP router.
  When we refer to an AS, we refer to its BGP router.
* All BGP announcements are received by ASes in a synchronized manner, and upon reception, ASes immediately run their route selection policies.
  You do not have to consider race conditions among the events of sending, receiving, and computing routes.

### Q3.1

Initially, assume that the BGP routing tables at ASes 1, 2, 3, and 4 are empty.
Then, AS D announces itself to its neighbors.
What routes do ASes 1, 2, 3, 4 use to reach AS D?

| AS1 | AS2 | AS3 | AS4 |
|-----|-----|-----|-----|
| 1D  | -   | -   | 4D  |

### Q3.2

Then all ASes (i.e., ASes 1, 2, 3, 4, and D) simultaneously make BGP announcements based on the Export Policy.
What routes do ASes 1, 2, 3, and 4 use to reach AS D?

| AS1 | AS2 | AS3 | AS4 |
|-----|-----|-----|-----|
| 1D  | 21D | 34D | 4D  |

### Q3.3

Again, all ASes simultaneously make BGP announcements based on the Export Policy.
What routes do ASes 1, 2, 3, and 4 use to reach AS D?

| AS1 | AS2 | AS3 | AS4 |
|-----|-----|-----|-----|
| 1D  | 234D| 321D| 4D  |

### Q3.4

Again, all ASes simultaneously make BGP announcements based on the Export Policy.
What routes do ASes 1, 2, 3, and 4 use to reach AS D?

| AS1 | AS2 | AS3 | AS4 |
|-----|-----|-----|-----|
| 1D  | 21D | 34D | 4D  |

### Q3.5

Again, all ASes simultaneously make BGP announcements based on the Export Policy.
What routes do ASes 1, 2, 3, and 4 use to reach AS D?

| AS1 | AS2 | AS3 | AS4 |
|-----|-----|-----|-----|
| 1D  | 234D| 321D| 4D  |

### Q3.6

Do you see any pattern?
Comment about BGP convergence.

This won't converge, because 2 and 3 keep "passing the buck" and setting each other as the preferred path. This results in a cycle and then needs to be fixed. Rinse and repeat.

