# Exercise 5: Transport Protocols

## Question 1: Reliable transport (exam-style question).\[1\]
\[1\] Adapted from exercises of Communication Networks (227-0120-00L).

Consider a Go-Back-N sender and receiver directly connected by a 10 Mbps link with a propagation delay of 100 milliseconds. The retransmission timer is set to 3 seconds. The retransmission timer restarts on every received ACK (including duplicate ones). The window has a length of 4 segments.

Draw a time-sequence diagram showing the transmission of 10 segments (each segment contains 10000bits). An ACK is transmitted as soon as the last bit of the corresponding data segment is received, and the size of the ACK is very small. (The number of an ACK is always RCV.NXT, i.e., +1 of the highest sequence number until it has received everything)

1. Draw the time-sequence diagram for the case where there are no losses. (fill in the following figure).

2. Draw the time-sequence diagram for the case where the 3rd and last segments are lost once.  (fill in the following figure).

![image](assets/cn_5.jpg)


## Question 2: TCP \[Previous Exam Question (adapted slightly)]\[2\]

\[2\]  Operating Systems & Networks (252-0062-00L); Spring Semester 2016
    (FS16). Question 9.
    <https://vis.ethz.ch/en/services/examcollection/Operating_Systems_and_Computer_Networks/>

The Transmission Control Protocol uses congestion control in order to
moderate the rate at which traffic enters the network. The following figure shows how the size of the congestion window
(y-axis) fluctuates as time passes (x-axis). Timeouts are not explicitly
shown, but you should be able to determine this based on the plot. Based
on the figure, answer the following questions.

![cwnd](assets/cwnd.png)

1. Identify the time intervals when congestion control is operating in slow start. Briefly explain slow start and what triggers it.

It is operating at 1, 7 and 23. At the beginning of the connection
or when we are in fast recovery and get a timeout.

2. Identify the time intervals when congestion control is operating in congestion avoidance, i.e., it uses additive increase. Briefly explain additive increase and what triggers congestion avoidance.

11 to 15, 16 to 22 and 26 to 32. Congestion avoidance is triggered after we recover from an error, go to the last known working data rate and then slowly inch up the send rate.

1. The fast retransmit and fast recovery algorithms usually operate in conjunction.
  1. Identify at which point(s) in time there is fast retransmission and explain why.

At point 16. There is no slow start inbetween additive increase periods.

  2. Briefly explain how fast retransmission and fast recovery work and the main intuition behind them.

A packet doesn't arrive and we try to resend it. If resending works we go back to congestion avoidance. This way we only need to slow start again if there really is congestion instead of with every network fluke.

## Question 3: UDP
1. In the lecture, you have seen that the operating system maps UDP and TCP connections to sockets. For UDP connections, each socket is identified by only the local IP address and port. In contrast, for TCP connections, local IP address and port *and* remote IP address and port are used as an identifier for the mapping.
What is the reason for this?

TCP needs to be able to maintain state, while UDP is done as soon as the packets are sent.

2. True or false: A UDP packet can be the payload of an IP packet.

True

## Question 4: Unreliable transport protocol
In the lecture, you have learned how a reliable transport protocol can be built on top of a best-effort delivery network. However, some applications still use an unreliable transport protocol.

1. What are the characteristics of best-effort and reliable transport?

Best-effort doesn't guarantee delivery, in order delivery or correctly transmitted data. Reliable transport on the other hand does.

2. What could be advantages of using an unreliable transport protocol?

No overhead, especially if speed is more important than getting absolutely every packet.

3. What type of applications are suitable to use unreliable transport protocols?

Video Streaming services, video chat, telephone, gaming, anything where latency is more important that slight usage glitches.

4. The User Datagram Protocol (UDP) only provides unreliable transport. Assume you are forced to use a network which only supports UDP as a transport protocol. You must transmit an important document which eventually should be correctly transmitted. Do you see a way to implement some of the reliable transport mechanisms despite using UDP?

Yes, basically one programs all of the features of tcp onto UDP, as Google did with QUIC.


## Question 5: QUIC
1. At what layer does QUIC operate? Can, and if so how, does it offer the same functionality as the TCP+TLS stack?

It offers the same functionality, operating on the network and transport layer.

2. What are the advantages of QUIC over TCP+TLS? Name at least three.

Mainly speed benefits by:
- Merging TLS and Hello Handshake
- Connection handoff when switching IP address (for example from wifi to cellular on a Phone)
- Better support for parallelism (No head of line blocking)

3. Why did QUIC not design its own custom transport layer header instead of UDP?

Network providers filter out suspicious packets, and a new protocol would probably sometimes be classified as suspicious.
