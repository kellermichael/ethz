# Exercise 3: Web & Video

## Question 1: CDNs: TRUE or FALSE?
1. Anycast-based CDNs have greater flexibility in load management than DNS-based ones. FALSE
    
2. Anycast-based services resolve the same name to different IP addresses for different clients. FALSE

3. For DNS-based CDNs, the content provider must dynamically rewrite URLs for each client request. TRUE

## Question 2: Browsing: TRUE or FALSE?

1. HTTP headers are usually human readable. TRUE

2. The critical path is the shortest path in the dependency graph. FALSE
    
3. Having more compute power cannot speed up the page load time. FALSE

4. Fetching **n** HTTP object from a web page with **n** non-persistent TCP connections without pipelining requires **n** RTT.  TRUE

## Question 3: 

### Q3.1
Bob and Alice are trying to fetch the same **n** (with **n** being large) web page elements from different servers. In particular, Bob tries to fetch the contents from a server with RTT = _rtt_, while Alice from one with RTT = 3 * _rtt_. Bob uses a single non-persistent TCP connection for each object sequentially (one after the other), while Alice uses at most _M_ parallel non-persistent TCP connections. Non-persistent means that for each object a new TCP connection is opened. Both Bob and Alice do not use pipelining. What is the minimum number of parallel connections that Alice needs in order to be faster than Bob in downloading the objects? Assume infinite bandwidth and that each object is small enough that it can be transmitted in one go (without multiple rounds of packets and ACKs). Assume also there are no dependencies across the **n** objects.

Alice would need at least $m = 4$ concurrent connections in order to be faster than Bob.

### Q3.2
If Bob would have used a persistent (kept-alive) TCP connection instead of opening a new connection for each object, what would his completion time have been? And if Alice would have used persistent kept-alive TCP connections? 

Bob's completion time would have been $2 \cdot (n+1) \cdot rrt$.<br>
Alice's completion time would have been $\frac{2 \cdot (n+1) \cdot 3 \cdot rrt}{m} = \frac{6 \cdot (n+1) \cdot rrt}{m}$

For $`n=100`$, would Bob have been faster than Alice assuming Alice kept the same strategy of $`M`$ non-persistent connections?

By inserting $n = 100, m = 4$ into our equations we can see:<br>
$$\frac{6 \cdot (100+1) \cdot rrt}{4} < 2 \cdot(100 + 1) \cdot rrt$$
that Alice would still be faster.

## Question 4

### Q4.1: Video Streaming:
Assume you are watching a video on Netflix, and your buffer contains 30s of unplayed video when the player starts to download chunk X. Assume chunk X to be 1.4MB and 4s long, your link bandwidth to be 1Mbps and the RTT between you and Netflix CDN to be of 50ms. Do you experience rebuffering? If not, how many seconds of unplayed video will your buffer contain after the download of chunk X? Assume that the player is using a single persistent TCP connection to download the segments.

I don't experience buffering. The time in the buffer after I download chunk $x$ is $$30 - \frac{8 \cdot 1.4}{1} - 0.05 + 4 = 22.75$$

### Q4.2:
Assume now that the chunk size of segment X in KB is $`\sim Poisson(3700KB)`$, meaning that the chunk size follows a discrete distribution with granularity of 1KB. What is the probability of rebuffering?

In order for us to experience buffering the chunk would have to be
at least $$\frac{8 \cdot X}{1} + 0.05 = 30\\ X = \frac{29.95}{8} \approx 3.74 \, \text{MB}$$

Hence the probability would be (approx, by wolfram alpha) $$\sum_{k=3740}^{\infty} \frac{3700^k e^{-3700}}{k!} \approx 0.26$$

## Question 5: Video Streaming: TRUE or FALSE?
1. Video resolution is a synonym of video bitrate. TRUE (in most cases)
   
2. In video streaming, the entire content is usually provided as a continuous flow of bytes. FALSE
   
3. Adaptation algorithms may vary depending on users' devices. TRUE

4. Usually, all chunks of a particular video content are stored on a single server. TRUE
   
5. Video chunks duration is typically in the order of seconds. TRUE
    
6. Video providers usually geographically replicate their video content. TRUE
    
## Question 6 
In VOD (Video-On-Demand) video streaming, video is divided into chunks of equal length. Discuss briefly what is the tradeoff between short and long video chunks.

Shorter video chunks allow the server to only send the necessary information. If for example there were minute chunks and we only needed
the last couple of seconds (to proceed where we left off from before)
if would be wasteful to send that big chunk. Another benefit of smaller chunks is probably smoother playback, as we can better adjust to instable connections (Example: downloaded half of big chunk and then enter tunnel
$\Rightarrow$ probably will buffer because can't download chunk in time)<br>
A downside of smaller chunks is that more connections have to be made, which means more load on the server and a lot of wasted "hello" messages.

## Question 7 
You are starting your brand new video streaming service. Should you deploy the logic for adapting video quality over time on the _client_ or on the _server_? Discuss pros and cons.

I would deploy it on the client. The client knows much better what their internet connection can handle. Furthermore there isn't really a need to enforce the choice the client makes. If they go around our code and request a different version they might experience buffering, but choose to do so.<br>
A benefit of doing it on the server side is possibly enforcing a user to stick with the low quality version, so they will upgrade to a higher plan. However this can also with the approach mentioned above. Hence other than that I don't really see any good reason to make this decision on the server side, as the server doesn't really know the client's internet situation.