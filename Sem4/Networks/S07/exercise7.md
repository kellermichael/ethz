# Exercise 7: Network Layer

## Question 1: Packet Switching

In packet-switching networks, the source host segments long application-layer messages (for example images or music files) into smaller packets and sends the packets into the network.
The receiver re-assembles the packets back into the original message.
This figure illustrates the end-to-end transmission of a message with and without segmentation:

| ![](assets/packet-switching.png) |
|:--:|
| *Transmission (a) without message segmentation, and (b) with message segmentation.* |

Consider a 7.5 * 10^6 bits long message that is to be sent from the source to the destination as shown in the figure.
We assume that each link has capacity 1.5 Mbps and ignore propagation, queuing and processing delays.

### Q1.1

Consider sending the message from source to destination without message segmentation.
Keep in mind that each packet switch uses a store and forward packet switching.

i) How long does it take to move the message from the source host to the first packet switch?

$\frac{7.5}{1.5} = 5s$

ii) What is the total time to move the message from the source host to the destination host?

Because we have to wait until the message arrives in full at each link,
it's three times the duration from above, so: $3 \cdot 5s = 15s$

### Q1.2

Now suppose that the message is segmented into 5,000 packets, with each packet being 1500 bits long.
How long does it take to move the the first packet from the source host to the first packet switch?

$\frac{1500}{1.5 \cdot 10^6} = 0.001s$

### Q1.3

How long does it take to move the file from the source host to the destination host when message segmentation is used?
Compare and comment.

$0.001s \cdot (5000 + 2)= 5.002s$

If we split the message up the entire message can be transmitted to the destination in the same time it would take to otherwise send the message to the first link. This is because we can utilize pipelining.

### Q1.4

What are the drawbacks of message segmentation?

We have to deal with Out of order Packets. (Other drawbacks like packet corruption are also present, but these we also have in the first approach)


## Question 2: Routing Tables \& Longest Prefix Matching

### Q2.1
Without using longest prefix matching, a forwarding table looks like the one below.
If we use longest prefix matching, we can combine a few entries.
Fill in the reduced table below.

Original forwarding table:

| Prefix           | Outgoing Interface |
|------------------|-------------------:|
| `128.128.0.0/9`  |             `eth0` |
| `128.160.0.0/11` |             `eth1` |
| `128.176.0.0/12` |             `eth0` |
| `128.192.0.0/10` |             `eth0` |
| default          |             `eth2` |

Reduced forwarding table:

| Prefix           | Outgoing Interface |
|------------------|-------------------:|
| `128.128.0.0/9`  |               eth0 |
| `128.160.0.0/12` |               eth1 |
| ...              |                ... |
| default          |             `eth2` |

### Q2.2
The following is a forwarding table at router R.
Suppose packets with the following destination IP addresses arrive at router R. Determine the next hop.
Give only one answer for each destination, using longest prefix matching.

| Destination Network | Next Hop |
|---------------------|---------:|
| 139.179.200.0/23    |       R1 |
| 139.179.128.0/18    |       R2 |
| 139.179.112.0/20    |       R3 |
| 139.179.216.0/21    |       R4 |
| 139.179.0.0/16      |       R5 |

\(a) 139.179.60.10 (b) 139.179.226.40 (c) 139.179.124.55 (d) 139.179.220.180<br>
\(a) R5 (b) R5 (c) R3 (d) R4
### Q2.3

The following figure shows a network with 3 routers (shown as hexagons with an R), connected by an Ethernet switch (shown as a circle with an S).
The routing table for the left-hand router is also shown.

| ![](assets/l3-topo.png) |
|:--:|
| *Network Topology* |

\(a) Complete the routing table for the right-hand router, so that packets will be delivered appropriately (use no more than 5 route table entries).
To receive credit, use the longest possible prefixes for all entries; no credits will be awarded otherwise.

| Prefix      | Next Hop Output | Next Hop IP |
|-------------|-----------------|-------------|
| 1.2.5.3/32  | 1               | 1.2.5.3     |
| 1.2.3.0/26  | 1               | 1.2.5.4     |
| 1.2.2.0/27  | 1               | 1.2.5.2     |
| 1.2.1.10/32 | 2               | 1.2.1.10    |
| 1.2.1.30/32 | 3               | 1.2.1.30    |


\(b) Are all entries in the left-hand router's table necessary?
If not, show how to reduce the number of entries, without changing the routing behavior.
To receive full credit, use the fewest number entries and the longest possible prefixes for all entries.

| Prefix      | Next Hop Output | Next Hop IP |
|-------------|-----------------|-------------|
| 1.2.2.0/27  | 2               | 1.2.5.2     |

All other entries (expect for the two we merged above into one) remain the same, because exit IP or output is always different.
