`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/30/2020 09:33:27 AM
// Design Name: 
// Module Name: alu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module alu(input[31:0] A, input[31:0] B, input[3:0] OP, output reg[31:0] O);

    always @ (*)
        case(OP)
             4'b0000: O <= ThirtyTwoBitAdder(A, B, O);
             4'b0010: O <= A - B;
             4'b0100: O <= A & B;
             4'b0101: O <= A | B;
             4'b0110: O <= A ^ B;
             4'b0111: O <= A |~ B;
             4'b1010: begin // SLT
				if (A < B) O <= 1;
					else O <= 0;
				end
             default: O <=  32'b00;
         endcase
         
        
endmodule

module FullAdder(input a, input b, input ci, output s, output co);
    wire xor1, and1, and2;
    
    xor (xor1, a, b);
    xor (s, xor1, ci);
    
    and (and1, ci, xor1);
    and (and2, a, b);
    or (co, and1, and2);
endmodule

module FourBitAdder(input[3:0] a, input[3:0] b, input ci, output[3:0] s, output co);
    wire n1, n2, n3;
    FullAdder (a[0], b[0], ci, s[0], n1);
    FullAdder (a[1], b[1], n1, s[1], n2);
    FullAdder (a[2], b[2], n2, s[2], n3);
    FullAdder (a[3], b[3], n3, s[3], co);
endmodule

module ThirtyTwoBitAdder(input[31:0] a, input[31:0] b, output[31:0] s);
    wire n1, n2, n3, n4, n5, n6, n7, n8;
    FourBitAdder(a[3:0], b[3:0], 0, s[3:0], n1);
    FourBitAdder(a[7:4], b[7:4], n1, s[7:4], n2);
    FourBitAdder(a[11:8], b[11:8], n2, s[11:8], n3);
    FourBitAdder(a[15:12], b[15:12], n3, s[15:12], n4);
    FourBitAdder(a[19:16], b[19:16], n4, s[19:16], n5);
    FourBitAdder(a[23:20], b[23:20], n5, s[23:20], n6);
    FourBitAdder(a[27:24], b[27:24], n6, s[27:24], n7);
    FourBitAdder(a[31:28], b[31:28], n7, s[31:28], n8);
endmodule
