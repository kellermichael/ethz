`timescale 1ns / 1ps


module Lights_test;

	// Inputs
   reg reset, right, left;

   // Outputs
   wire [5:0]	lights;

   // Test clock 
   reg			clk ; // in this version we do not really need the clock

   // Expected outputs
   reg [5:0]	exp_lights;

   // Vector and Error counts
   reg [10:0]	vec_cnt, err_cnt;

   // TO DO:
   // Define an array called 'testvec' that is wide enough to hold the inputs:
   //   aluop, a, b 
	//
   // and the expected output
   //   exp_result
	//
   // for each testcase.
   // Note: we will not store 'exp_zero' in this array.
   reg[8:0] testvec [9:0];
     initial begin
       $display("Loading rom.");
       $readmemb("testvectors.mem", testvec);
     end
     
   // The test clock generation
   always				// process always triggers
	begin
		clk=1; #50;		// clk is 1 for 50 ns 
		clk=0; #50;		// clk is 0 for 50 ns
	end					// generate a 100 ns clock

   // Initialization
	initial
	begin
		// TO DO:
		// Read the content of the file testvectors_hex.txt into the 
		// array testvec. The file contains values in hexadecimal format

		err_cnt=0; // number of errors
		vec_cnt=0; // number of vectors
	end
   
   // Tests
	always @ (posedge clk)		// trigger with the test clock
	begin
		// Wait 20 ns, so that we can safely apply the inputs
		#20; 

		// Assign the signals from the testvec array
		{reset,left,right,exp_lights}= testvec[vec_cnt]; 

		// Wait another 60ns after which we will be at 80ns
		#60; 

		// Check if output is not what we expect to see
		if (lights !== exp_lights)
		begin                                         
			// Display message
			$display("Error at %5d ns: Reset %b Left=%b Right=%b", $time, reset,left,right);	// %h displays hex
			// $display("       %h (%h expected)",result,exp_result);
			// $display(" Zero: %b (%b expected)",zero,exp_zero);							// %b displays binary
			$display("Computed Output: %b, Expected Output: %b", lights, exp_lights);
			err_cnt = err_cnt + 1;																// increment error count
		end

		vec_cnt = vec_cnt + 1;																	// next vector
	
		// We use === so that we can also test for X
		// if ((testvec[vec_cnt][99:96] === 4'bxxxx))
		if (vec_cnt == 10)
		begin
			// End of test, no more entries...
			$display ("%d tests completed with %d errors", vec_cnt, err_cnt);
			
			// Wait so that we can see the last result
			#20; 
			
			// Terminate simulation
			$finish;
		end
	end

   // TO DO:
   // Instantiate the Unit Under Test (UUT)
   // check results on falling edge of clk
   
   // instantiate device under test
   // sillyfunction dut (a, b, c, y);
   lights dut(clk,reset,right,left,lights);
endmodule

