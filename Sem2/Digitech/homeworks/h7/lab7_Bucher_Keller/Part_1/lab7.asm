#
# Calculate sum from A to B.
#
# Authors: 
#	Joel Bucher, Michael Keller
#
#

.text
main:
	# initialize input
	addi $t0, $zero, 0
	addi $t1, $zero, 5
	
	# swap input if not in correct order
	slt $t3, $t1, $t0
	beq $t3, 1, swap
	
	# fix fenceposting issue
	addi $t1, $t1, 1
	
	# start loop
loop:	beq $t0, $t1, end
	add $t2, $t2, $t0
	addi $t0, $t0, 1
	j loop
	
end:	
	j	end	# Infinite loop at the end of the program. 
	
swap:
	addi $t8, $t0, 0
	addi $t0, $t1, 0
	addi $t1, $t8, 0
	# fix fenceposting issue
	addi $t1, $t1, 1
	j loop
