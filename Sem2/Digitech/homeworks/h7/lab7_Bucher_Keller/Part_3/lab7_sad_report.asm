#
# Sum of Absolute Differences Algorithm
#
# Authors: 
#	Joel Bucher, Michael Keller
#
#

.text


main:


# Initializing data in memory... 
# Store in $s0 the address of the first element in memory
	# lui sets the upper 16 bits of thte specified register
	# ori the lower ones
	# (to be precise, lui also sets the lower 16 bits to 0, ori ORs it with the given immediate)

	# define red, green and blue color vectors for the two images
	     lui     $s0, 0x0000 # Address of first element in the vector
	     ori     $s0, 0x0000
	     addi   $t0, $0, 1			# R_left_image[0]	
	     sw      $t0, 0x0($s0)
	     addi   $t0, $0, 2			# R_left_image[1]		
	     sw      $t0, 0x4($s0)
	     addi   $t0, $0, 3			# R_left_image[2]	
	     sw      $t0, 0x8($s0)
	     addi   $t0, $0, 4			# R_left_image[3]
	     sw      $t0, 0xC($s0)
	     addi   $t0, $0, 5			# R_left_image[4]
	     sw      $t0, 0x10($s0)
	     addi   $t0, $0, 6			# R_left_image[5]
	     sw      $t0, 0x14($s0)
	     addi   $t0, $0, 7			# R_left_image[6]
	     sw      $t0, 0x18($s0)
	     addi   $t0, $0, 8			# R_left_image[7]
	     sw      $t0, 0x1C($s0)
	     addi   $t0, $0, 9			# R_left_image[8]
	     sw      $t0, 0x20($s0)
	     
	     addi   $t0, $0, 10		# G_left_image[0]	
	     sw      $t0, 0x24($s0)
	     addi   $t0, $0, 11		# G_left_image[1]		
	     sw      $t0, 0x28($s0)
	     addi   $t0, $0, 12		# G_left_image[2]	
	     sw      $t0, 0x2C($s0)
	     addi   $t0, $0, 13		# G_left_image[3]
	     sw      $t0, 0x30($s0)
	     addi   $t0, $0, 14		# G_left_image[4]
	     sw      $t0, 0x34($s0)
	     addi   $t0, $0, 15		# G_left_image[5]
	     sw      $t0, 0x38($s0)
	     addi   $t0, $0, 16		# G_left_image[6]
	     sw      $t0, 0x3C($s0)
	     addi   $t0, $0, 17		# G_left_image[7]
	     sw      $t0, 0x40($s0)
	     addi   $t0, $0, 18		# G_left_image[8]
	     sw      $t0, 0x44($s0)
	     
	     addi   $t0, $0, 19		# B_left_image[0]	
	     sw      $t0, 0x48($s0)
	     addi   $t0, $0, 20		# B_left_image[1]		
	     sw      $t0, 0x4C($s0)
	     addi   $t0, $0, 21		# B_left_image[2]	
	     sw      $t0, 0x50($s0)
	     addi   $t0, $0, 22		# B_left_image[3]
	     sw      $t0, 0x54($s0)
	     addi   $t0, $0, 23		# B_left_image[4]
	     sw      $t0, 0x58($s0)
	     addi   $t0, $0, 24		# B_left_image[5]
	     sw      $t0, 0x5C($s0)
	     addi   $t0, $0, 25		# B_left_image[6]
	     sw      $t0, 0x60($s0)
	     addi   $t0, $0, 26		# B_left_image[7]
	     sw      $t0, 0x64($s0)
	     addi   $t0, $0, 27		# B_left_image[8]
	     sw      $t0, 0x68($s0)
	     
	     
	     addi   $t0, $0, 28		# R_right_image[0]	
	     sw      $t0, 0x6C($s0)
	     addi   $t0, $0, 29		# R_right_image[1]		
	     sw      $t0, 0x70($s0)
	     addi   $t0, $0, 30		# R_right_image[2]	
	     sw      $t0, 0x74($s0)
	     addi   $t0, $0, 31		# R_right_image[3]
	     sw      $t0, 0x78($s0)
	     addi   $t0, $0, 32		# R_right_image[4]
	     sw      $t0, 0x7C($s0)
	     addi   $t0, $0, 33		# R_right_image[5]
	     sw      $t0, 0x80($s0)
	     addi   $t0, $0, 34		# R_right_image[6]
	     sw      $t0, 0x84($s0)
	     addi   $t0, $0, 35		# R_right_image[7]
	     sw      $t0, 0x88($s0)
	     addi   $t0, $0, 36		# R_right_image[8]
	     sw      $t0, 0x8C($s0)
	     
	     addi   $t0, $0, 37		# G_right_image[0]	
	     sw      $t0, 0x90($s0)
	     addi   $t0, $0, 38		# G_right_image[1]		
	     sw      $t0, 0x94($s0)
	     addi   $t0, $0, 39		# G_right_image[2]	
	     sw      $t0, 0x98($s0)
	     addi   $t0, $0, 40		# G_right_image[3]
	     sw      $t0, 0x9C($s0)
	     addi   $t0, $0, 41		# G_right_image[4]
	     sw      $t0, 0xA0($s0)
	     addi   $t0, $0, 42		# G_right_image[5]
	     sw      $t0, 0xA4($s0)
	     addi   $t0, $0, 43		# G_right_image[6]
	     sw      $t0, 0xA8($s0)
	     addi   $t0, $0, 44		# G_right_image[7]
	     sw      $t0, 0xAC($s0)
	     addi   $t0, $0, 45		# G_right_image[8]
	     sw      $t0, 0xB0($s0)
	     
	     addi   $t0, $0, 46		# B_right_image[0]	
	     sw      $t0, 0xB4($s0)
	     addi   $t0, $0, 47		# B_right_image[1]		
	     sw      $t0, 0xB8($s0)
	     addi   $t0, $0, 48		# B_right_image[2]	
	     sw      $t0, 0xBC($s0)
	     addi   $t0, $0, 49		# B_right_image[3]
	     sw      $t0, 0xC0($s0)
	     addi   $t0, $0, 50		# B_right_image[4]
	     sw      $t0, 0xC4($s0)
	     addi   $t0, $0, 51		# B_right_image[5]
	     sw      $t0, 0xC8($s0)
	     addi   $t0, $0, 52		# B_right_image[6]
	     sw      $t0, 0xCC($s0)
	     addi   $t0, $0, 53		# B_right_image[7]
	     sw      $t0, 0xD0($s0)
	     addi   $t0, $0, 54		# B_right_image[8]
	     sw      $t0, 0xD4($s0)
	     
	     
	     
	# Loop over the elements of all the colors of left_image and right_image
	addi $s1, $0, 0 # $s1 = i = 0
	addi $s2, $0, 9	# $s2 = image_size = 9

loop:

	# Check if we have traverse all the elements 
	# of the loop. If so, jump to end_loop:
	
	
	beq $s1, $s2, end_loop
	 
	
	# red 1
	sll    $t3, $s1, 2
	add    $t3, $t3, $s0 
	lw     $a0, 0($t3)
	
	# red 2
	add    $t3, $t3, 0x6C
	lw     $a1, 0($t3)
	
	# calc red
	jal abs_diff
	add $t4, $0, $v0
	
	
	# green 1
	sll    $t3, $s1, 2
	addi    $t3, $t3, 0x24
	lw     $a0, 0($t3)
	
	# green 2
	add    $t3, $t3, 0x6C
	lw     $a1, 0($t3)
	
	# calc green
	jal abs_diff
	add $t4, $t4, $v0
	
	# blue 1
	sll    $t3, $s1, 2
	addi    $t3, $t3, 0x48
	lw     $a0, 0($t3)
	
	# blue 2
	add    $t3, $t3, 0x6C
	lw     $a1, 0($t3)
	
	# calc blue
	jal abs_diff
	add $t4, $t4, $v0



	# final result
	sll    $t3, $s1, 2
	add    $t3, $t3, 0xD8
	sw 	$t4, 0($t3)
	 
	# Increment variable i and repeat loop:
	
	addi $s1, $s1, 1
	j loop
	

	
end_loop:

	#TODO5: Call recursive_sum and store the result in $t2
	#Calculate the base address of sad_array (first argument
	#of the function call)and store in the corresponding register   
	
	add $a0, $0, 0x48
	
	# Prepare the second argument of the function call: the size of the array
	
	add $a1, $0, $s2
	
	# Call to funtion
	
	jal recursive_sum
	  
	
	#Store the returned value in $t2
	
	addi $t2, $v0, 0
	

end:	
	j	end	# Infinite loop at the end of the program. 




# TODO2: Implement the abs_diff routine here, or use the one provided
abs_diff:
	sub $t1, $a0, $a1
	sra $t2,$t1,31   
	xor $t1,$t1,$t2   
	sub $v0,$t1,$t2    

	jr $ra

abs_diff_three:
	

# TODO3: Implement the recursive_sum routine here, or use the one provided
recursive_sum:    
	addi $sp, $sp, -8       # Adjust sp
        addi $t0, $a1, -1       # Compute size - 1
        sw   $t0, 0($sp)        # Save size - 1 to stack
        sw   $ra, 4($sp)        # Save return address
        bne  $a1, $zero, else   # size == 0 ?
        addi  $v0, $0, 0        # If size == 0, set return value to 0
        addi $sp, $sp, 8        # Adjust sp
        jr $ra                  # Return
else:     
	add  $a1, $t0, $0		#update the second argument
        jal   recursive_sum 
        lw    $t0, 0($sp)       # Restore size - 1 from stack
        sll  $t1, $t0, 2        # Multiply size by 4
        add   $t1, $t1, $a0     # Compute & arr[ size - 1 ]
        lw    $t2, 0($t1)       # t2 = arr[ size - 1 ]
        add   $v0, $v0, $t2     # retval = $v0 + arr[size - 1]
        lw    $ra, 4($sp)       # restore return address from stack         
        addi $sp, $sp, 8        # Adjust sp
        jr $ra                  # Return
