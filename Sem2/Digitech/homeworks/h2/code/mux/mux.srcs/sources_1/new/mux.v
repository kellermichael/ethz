`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/10/2020 04:23:43 PM
// Design Name: 
// Module Name: mux
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux(input s0, input s1, input d, output y);
    wire and1, and2, notWire;
    
    and(and1,s0,d);
    not(notWire,d);
    and(and2,s1,notWire);
    or(y,and1,and2);
    
endmodule
