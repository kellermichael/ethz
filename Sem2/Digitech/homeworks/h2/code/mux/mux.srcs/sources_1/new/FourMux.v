`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/10/2020 04:37:32 PM
// Design Name: 
// Module Name: FourMux
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module FourMux(input[3:0] s, input[1:0] d, output y);
    wire mux1, mux2;
    mux(s[0], s[1], d[0], mux1);
    mux(s[2], s[3], d[0], mux2);
    
    mux(mux1, mux2, d[1],y);
    
endmodule