`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/10/2020 03:45:47 PM
// Design Name: 
// Module Name: decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module decoder(input[1:0] s, output[3:0] y);
    wire notS0, notS1;
    not(notS0, s[0]);
    not(notS1, s[1]);
    
    
    and(y[3], s[0], s[1]);
    and(y[2], notS0, s[1]);
    and(y[1], s[0], notS1);
    and(y[0], notS0, notS1);

endmodule