`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: ETH Z�rich
// Engineer: Michael Keller, Joel Bucher
// 
// Create Date: 03/13/2020 08:39:40 AM
// Design Name: 
// Module Name: Decoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Decoder(input[3:0] s, output[6:0] o);

    // Declare Inverters
    wire not3, not2, not1, not0;
    not(not3, s[3]);
    not(not2, s[2]);
    not(not1, s[1]);
    not(not0, s[0]);
   
    // Declare wires
    wire w[15:0];

    // Encode 0
    and(w[0], not3, not2, not1, not0);
    
    
    // Encode 1
    and(w[1], not3, not2, not1, s[0]);
    
    // Encode 2
    and(w[2], not3, not2, s[1], not0);
    
    // Encode 3
    and(w[3], not3, not2, s[1], s[0]);
    
    // Encode 4
    and(w[4], not3, s[2], not1, not0);
    
    // Encode 5
    and(w[5], not3, s[2], not1, s[0]);
    
    // Encode 6
    and(w[6], not3, s[2], s[1], not0);
    
    // Encode 7
    and(w[7], not3, s[2], s[1], s[0]);
    
    // Encode 8
    and(w[8], s[3], not2, not1, not0);
    
    // Encode 9
    and(w[9], s[3], not2, not1, s[0]);
    
    // Encode A
    and(w[10], s[3], not2, s[1], not0);
    
    // Encode B
    and(w[11], s[3], not2, s[1], s[0]);
    
    // Encode C
    and(w[12], s[3], s[2], not1, not0);
    
    // Encode D
    and(w[13], s[3], s[2], not1, s[0]);
    
    // Encode E
    and(w[14], s[3], s[2], s[1], not0);
    
    // Encode F
    and(w[15], s[3], s[2], s[1], s[0]);
    
    //OR-Decoder
    
    or(o[0],w[1],w[4],w[11],w[13]);                 //a
    or(o[1],w[5],w[6],w[11],w[12],w[14],w[15]);     //b
    or(o[2],w[2],w[12],w[14],w[15]);                //c
    or(o[3],w[1],w[4],w[7],w[10],w[15]);            //d
    or(o[4],w[1],w[3],w[4],w[5],w[7],w[9]);         //e
    or(o[5],w[1],w[2],w[3],w[7],w[13]);             //f
    or(o[6],w[0],w[1],w[7],w[12]);                  //g
    
endmodule
