`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/17/2020 05:18:47 PM
// Design Name: 
// Module Name: DisplayDecoder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module DisplayDecoder(input [1:0] a, output [3:0] o);
    wire notA, notB;
    not(notA,a[0]);
    not(notB,a[1]);
    
    wire number[3:0];
    
    and(number[0],notA, notB);
    and(number[1],notA,a[1]);
    and(number[2],a[0],notB);
    and(number[3],a[0],a[1]);
    
    not(o[0],number[0]);
    not(o[1],number[1]);
    not(o[2],number[2]);
    not(o[3],number[3]);
    
endmodule
