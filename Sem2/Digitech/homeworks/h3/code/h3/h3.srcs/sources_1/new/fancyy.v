`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/13/2020 09:32:08 AM
// Design Name: 
// Module Name: fancyy
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fancyy(input[1:0] display, input[3:0] a, input[3:0] b, output[6:0] D, output o, output[3:0] AN);
    wire[4:0] out;
    FourBitAdder (a,b,out);
    assign o = out[4];
    
    Decoder(out[3:0], D);
    DisplayDecoder(display,AN);
endmodule
