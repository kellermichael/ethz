`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/24/2020 02:46:21 PM
// Design Name: 
// Module Name: lights
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module main(
     input clk,
     input reset,
     input left,
     input right,
     output reg [5:0] o
     );

     wire clk_en, clk_dimmed;
     reg [1:0] dimmedCounter;
     

     clock(clk, reset, clk_en);
     dimmedClock(clk, reset, clk_dimmed);

     reg [3:0] state, nextstate;

     parameter S0 = 4'b0000;
     parameter L0 = 4'b1001;
     parameter L1 = 4'b1010;
     parameter L2 = 4'b1011;
     parameter L3 = 4'b1100;
     parameter L4 = 4'b1101;
     parameter L5 = 4'b1110;
     parameter R0 = 4'b0001;
     parameter R1 = 4'b0010;
     parameter R2 = 4'b0011;
     parameter R3 = 4'b0100;
     parameter R4 = 4'b0101;
     parameter R5 = 4'b0110;


     always @ (posedge clk_en, posedge reset)
         if(reset) state <= S0;
         else if(state == S0 & left & right) state = S0;
         else if (state == S0 & left) state = L0;
         else if (state == S0 & right) state = R0;
         else state = nextstate;
         
        
     always @ (posedge clk)// posedge defines a rising edge (transition from 0 to 1)
        dimmedCounter <= dimmedCounter + 1;
     
     
     always @ (dimmedCounter)
        if(&dimmedCounter)
        begin
            case(state)
                 S0: o = 6'b000000;
                 L0: o = 6'b000100;
                 L1: o = 6'b000110;
                 L2: o = 6'b000111;
                 L3: o = 6'b000011;
                 L4: o = 6'b000001;
                 L5: o = 6'b000000;
                 R0: o = 6'b001000;
                 R1: o = 6'b011000;
                 R2: o = 6'b111000;
                 R3: o = 6'b110000;
                 R4: o = 6'b100000;
                 R5: o = 6'b000000;
                 default: o = 6'b000000;
             endcase
        end
        else
        begin
            case(state)
                 S0: o = 6'b000000;
                 L0: o = 6'b000000;
                 L1: o = 6'b000100;
                 L2: o = 6'b000010;
                 L3: o = 6'b000001;
                 L4: o = 6'b000000;
                 L5: o = 6'b000000;
                 R0: o = 6'b000000;
                 R1: o = 6'b001000;
                 R2: o = 6'b010000;
                 R3: o = 6'b100000;
                 R4: o = 6'b000000;
                 R5: o = 6'b000000;
                 default: o = 6'b000000;
             endcase
        end


     always @ (state)
         case(state)
             S0: nextstate <= S0;
             L0: nextstate <= L1;
             L1: nextstate <= L2;
             L2: nextstate <= L3;
             L3: nextstate <= L4;
             L4: nextstate <= L5;
             L5: nextstate <= S0;
             R0: nextstate <= R1;
             R1: nextstate <= R2;
             R2: nextstate <= R3;
             R3: nextstate <= R4;
             R4: nextstate <= R5;
             R5: nextstate <= S0;
             default: nextstate <= S0;
         endcase;
endmodule