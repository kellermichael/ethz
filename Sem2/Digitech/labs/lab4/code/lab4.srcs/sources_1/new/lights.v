`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/24/2020 02:46:21 PM
// Design Name: 
// Module Name: lights
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lights(
     input clk,
     input reset,
     input left,
     input right,
     output reg [5:0] o
     );

     wire clk_en;

     clock(clk, reset, clk_en);

     reg [2:0] state, nextstate;

     parameter S0 = 3'b000;
     parameter L0 = 3'b101;
     parameter L1 = 3'b110;
     parameter L2 = 3'b111;
     parameter R0 = 3'b001;
     parameter R1 = 3'b010;
     parameter R2 = 3'b011;


     always @ (posedge clk_en, posedge reset)
         if(reset) state <= S0;
         else if(state == S0 & left & right) state = S0;
         else if (state == S0 & left) state = L0;
         else if (state == S0 & right) state = R0;
         else state = nextstate;


     always @ (state)
         case(state)
             S0: nextstate <= S0;
             L0: nextstate <= L1;
             L1: nextstate <= L2;
             L2: nextstate <= S0;
             R0: nextstate <= R1;
             R1: nextstate <= R2;
             R2: nextstate <= S0;
             default: nextstate <= S0;
         endcase;


     always @ (*)
         case(state)
             S0: o = 6'b000000;
             L0: o = 6'b000100;
             L1: o = 6'b000110;
             L2: o = 6'b000111;
             R0: o = 6'b001000;
             R1: o = 6'b011000;
             R2: o = 6'b111000;
             default: o = 6'b000000;
         endcase
endmodule