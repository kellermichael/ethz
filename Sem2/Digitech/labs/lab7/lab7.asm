#
# Calculate sum from A to B.
#
# Authors: 
#	X Y, Z Q 
#
#

.text
main:
	# initialize input
	addi $t0, $zero, 0
	addi $t1, $zero, 5
	
	# fix fenceposting issue
	addi $t1, $t1, 1
	
	# start loop
loop:	beq $t0, $t1, end
	add $t2, $t2, $t0
	addi $t0, $t0, 1
	j loop
	
end:	
	j	end	# Infinite loop at the end of the program. 
