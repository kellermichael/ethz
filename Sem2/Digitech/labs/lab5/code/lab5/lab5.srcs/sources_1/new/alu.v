`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/30/2020 09:33:27 AM
// Design Name: 
// Module Name: alu
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module alu(input[31:0] A, input[31:0] B, input[3:0] OP, output reg[31:0] O);

    always @ (*)
        case(OP)
             4'b0000: O <= A + B;
             4'b0010: O <= A - B;
             4'b0100: O <= A & B;
             4'b0101: O <= A | B;
             4'b0110: O <= A ^ B;
             4'b0111: O <= A |~ B;
             4'b1010: if (A < B) O <= 1;
                      else O <= 0;
             default: O <=  32'b00;
         endcase
         
        
endmodule
