class Main {
    public static void main(String[] args) {
        // Uncomment this line if you want to read from a file
        In.open("public/test1.in");
        Out.compareTo("public/test1.out");
        
        int t = In.readInt();
        for (int i = 0; i < t; i++) {
            testCase();
        }
        
        // Uncomment this line if you want to read from a file
        In.close();
    }

    public static void testCase() {
        // Input using In.java class
        double w = (double) In.readInt();
        double b = (double) In.readInt();
        
        // Array for memoization
        double[][][] mem = new double[(int)b+1][(int)w+1][2];
        
        Out.println(recursiveSolve(b, w, false, mem));
    }
    
    // returns the probability of me winning the game in this state
    public static double recursiveSolve(double b, double w, boolean myTurn, double[][][] mem) {
      // base case
      if(w == 0) {return 1.0;}
    
      // check if already computed
      if(mem[(int)b][(int)w][myTurn ? 1 : 0] != 0.0) {
        return mem[(int)b][(int)w][myTurn ? 1 : 0];
      }
      
      // recursion
      double optOne = 0, optTwo = 0;
      if(b >= 2){
        optOne = (b / (b+w)) * ((b-1) / (b+w-1)) * recursiveSolve(b-2, w, !myTurn, mem);
      }
      if(b >= 1 && w >= 1) {
        optTwo = (b / (b+w)) * (w / (b+w-1)) * recursiveSolve(b-1, w-1, !myTurn, mem);
      }
      double options = optOne + optTwo;
      double result = myTurn ? (w / (b + w)) + options : options;
      
      // memoize
      if(mem[(int)b][(int)w][myTurn ? 1 : 0] == 0.0) {
        mem[(int)b][(int)w][myTurn ? 1 : 0] = result;
      }
      
      // done
      return result;
    }
}