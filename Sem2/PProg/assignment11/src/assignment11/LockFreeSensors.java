package assignment11;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicReference;

class LockFreeSensors implements Sensors {
	
	AtomicReference<SensorData> SensorDataRef;
	ThreadLocalRandom rand;

    LockFreeSensors()
    {
    	SensorData initialSensorData = new SensorData(0, new double[2]);
    	SensorDataRef = new AtomicReference<SensorData>(initialSensorData);
    }

    // store data and timestamp
    // if and only if data stored previously is older (lower timestamp)
    public void update(long timestamp, double[] data)
    {
    	
    	SensorData update = new SensorData(timestamp, data);
    	SensorData current = SensorDataRef.get();
    	
    	/**feedback:
    	 * current.getTimestamp() and timestamp do not change for a iteration,
    	 * so you can check it with 'if' statement. While the update of the object
    	 * could fail few times, therefore you should use 'while-loop'.
    	 */
    	while(current.getTimestamp() < timestamp) {
    		if(SensorDataRef.compareAndSet(current, update)) {
    			break;
    		}
    		current = SensorDataRef.get();
    	}
    }
    
    
    public long get(double val[])
    {
    	SensorData current = SensorDataRef.get();
    	if (current.getTimestamp() != 0) {
        	for (int i = 0; i<current.getValues().length; ++i)
        		val[i] = current.getValues()[i];
        }
    	return current.getTimestamp();
    }

    /**feedback:
     * The trick used in the solution is that data is always copied for each update and 
     * only the reference to the object containing the data is used for atomic operations. 
     * This mechanism is known as 'copy-on-write'. It is, for example, also used in file-systems 
     * in order to guarantee consistency of the underlying file system structures at all times. 
     * It is also related to the 'read-copy-update' mechanism used in the Linux kernel that allows 
     * fine-grained concurrent operations by readers and writers on complex data structures. 
     * 
     * a) yes, the lock-free algorithm is wait free because of the time-stamps. Any trial to write a 
     * value to the sensor data object will fail the latest when there are newer data available. If 
     * there are no newer data available, it will fail within finite time also because no threads 
     * provide further values.
     * 
     * b) yes, the lock free algorithm from above was a so called copy-on-write algorithm. 
     * The basic trick is that for updating the data only a single reference was changed. 
     * This would in principle also work with the expression tree.
     */
}
