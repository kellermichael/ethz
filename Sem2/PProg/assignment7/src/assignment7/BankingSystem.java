package assignment7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BankingSystem {

	protected List<Account> accountList;

	/**
	 * Initializes the BankingSystem:
	 * accountList is empty and totalMoneyInBank() should return 0.
	 */
	public BankingSystem() {
		setAccountList(new ArrayList<Account>());
	}

	/**
	 * Transfers Money from one account to another.
	 * 
	 * @param from Account to transfer money from.
	 * @param to Account to transfer money to.
	 * @param amount Amount to transfer.
	 * @return True if Money was transferred successfully.
	 *         False if there was not enough balance in from.
	 * @throws InterruptedException 
	 */
	public boolean transferMoney(Account from, Account to, int amount) {
		
		// determine safe order for locking
		Account a = from.compareTo(to) == -1 ? from : to;
		Account b = a == from ? to : from;
		
		try {
			// lock in safe order
			a.lock.lock();
			b.lock.lock();
			
			// transfer money
			if (from.getBalance() < amount) {
				return false;
			} else {
				from.setBalance(from.getBalance() - amount);
				to.setBalance(to.getBalance() + amount);
			}
			return true;
		} finally {
			// release locks
			a.lock.unlock();
			b.lock.unlock();
		}
		
//		synchronized(from) {
//			synchronized(to) {
//				if (from.getBalance() < amount) {
//					return false;
//				} else {
//					from.setBalance(from.getBalance() - amount);
//					to.setBalance(to.getBalance() + amount);
//				}
//				//System.out.println(from.getId());
//				return true;
//			}
//		}
	}

	/**
	 * Returns the sum of a given list of accounts.
	 * 
	 */
	
	/*
	 * Note: I don't really see how it would be possible to parallelize the 
	 * process of obtaining the locks for all of the accounts. If one were 
	 * to split the list of accounts (in any way) and for example give multiple 
	 * threads a subset of accounts to lock, one would run the risk of a deadlock 
	 * during a transfer, as it would be possible for a "larger" lock to be locked 
	 * before a "smaller" lock.
	 * While it would be possible to parallelize the summation process itself, 
	 * my guess is that the real time consumer is locking all the accounts.
	 * 
	 * Thus my question: Is it really possible to parallelize the locking process of
	 * the sumAccounts function?
	 * 
	 * */
	public int sumAccounts(List<Account> accounts) {
		int sum = 0;
		
		// determine safe order for locking (guarantee any transaction in progress can complete)
		Collections.sort(accounts, new Comparator<Account>() {
			@Override
			public int compare(Account arg0, Account arg1) {
				return arg0.compareTo(arg1);
			}
		});
		
		// lock accounts in safe order and sum them up
		try {
			for(Account a : accounts) {
				a.lock.lock();
				sum += a.getBalance();
			}
			return sum;	
		} finally {
			// release locks
			for(Account a : accounts) {
				a.lock.unlock();
			}
		}	
	}

	/**
	 * Calculates the total amount of money in the bank at any point in time.
	 * @return The total amount of money.
	 * 
	 * @fixme Tends to return wrong results :-(
	 */
	public int totalMoneyInBank() {
		return sumAccounts(getAccountList());
	}

	/**
	 * Adds a new account to the bank.
	 * The account needs to have a positive balance to be added to the system.
	 * 
	 * @param a New account
	 * @return True if account was added successfully.
	 *         False if account could not be added to the system 
	 *         (ie., account did not have enough balance).
	 */
	public boolean addAccount(Account a) {
		if (a.getBalance() >= 0) {
			getAccountList().add(a);
			return true;
		}
		else {
			return false;
		}

	}

	protected List<Account> getAccountList() {
		return accountList;
	}

	protected void setAccountList(List<Account> accountList) {
		this.accountList = accountList;
	}

}
