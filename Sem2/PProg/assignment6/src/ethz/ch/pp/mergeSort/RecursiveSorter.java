package ethz.ch.pp.mergeSort;
import java.util.Arrays;
import java.util.concurrent.RecursiveTask;

import ethz.ch.pp.util.ArrayUtils;

public class RecursiveSorter extends RecursiveTask<int[]> {

	/**
	 * 
	 */
	int[] input;
	int start;
	int end;
	private static final long serialVersionUID = 1L;
	
	public RecursiveSorter(int[] input, int start, int end) {
		this.input = input;
		this.start = start;
		this.end = end;
	}

	@Override
	protected int[] compute() {
		int[] result = new int[end - start + 1];
		
		
		// cover the base case for sorting, i.e. an array length of 1 or 2
		if(end-start == 0) {
			result[0] = input[end];
			return result;
		} else if(end-start == 1) {
			result[0] = input[start] < input[end] ? input[start] : input[end];
			result[1] = input[start] >= input[end] ? input[start] : input[end];
			return result;
		}
		
		// split up the work
		int middle = (start + end) / 2;
		RecursiveSorter left = new RecursiveSorter(input, start, middle);
		RecursiveSorter right = new RecursiveSorter(input, middle + 1, end);
		left.fork();
		int[] r = right.compute();
		int[] l = left.join();
		
		/*
		 * Great practice that you implemented yourself the merge array part.
		 * For this assignment it was provided in ArraUtils.java also.
		 */
		// merge the result arrays
		int posL = 0, posR = 0;
		while(posL + posR < result.length) {
			if(posL < l.length && posR < r.length) {
				result[posL + posR] = l[posL] < r[posR] ? l[posL] : r[posR];
				if(l[posL] < r[posR]) {
					posL++;
				} else {
					posR++;
				}
			}
			if(posL == l.length && posR < r.length) {
				result[posL + posR] = r[posR];
				posR++;
			}
			if(posR == r.length && posL < l.length) {
				result[posL + posR] = l[posL];
				posL++;
			}
		}
		
		
		// I wanted to do it myself to practice
		// ArrayUtils.merge(r, l, result);
		
		return result;
	}

}
