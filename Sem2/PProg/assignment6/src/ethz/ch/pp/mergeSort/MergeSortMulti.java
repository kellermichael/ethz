package ethz.ch.pp.mergeSort;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;

public class MergeSortMulti {

	private static final long serialVersionUID = 1531647254971804196L;
	static ForkJoinPool fjPool= new ForkJoinPool();

	//TODO: implement using ForkJoinPool
	// You should change this class to extend from RecursiveTask or RecursiveAction
	
	// Question: Why do we need to extend this class with RecursiveTask as the TODO prompt suggests?
	// Is it better to implement a separate class and file as I have or do it all in this file?
	/*feedback:
	 * You could implement the methods of RecursiveSorter here as well.
	 */
	public static int[] sort(int[] input, int numThreads) {
		int[] result = MergeSortMulti.fjPool.invoke(new RecursiveSorter(input, 0, input.length - 1));
		// System.out.println(Arrays.toString(result));
		// System.out.println(result.length);
		return result;
	}

}
