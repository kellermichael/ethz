package ethz.ch.pp.seq;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class LongestCommonSequenceMulti extends RecursiveTask<Sequence> {
	
	private static final long serialVersionUID = 4179716026313995745L;
		
	public static Sequence longestCommonSequence(int[] input, int numThreads) {
		
		// create forkjoinpool
		ForkJoinPool fjPool = new ForkJoinPool(numThreads);
		
		/*
		 * The idea is to compute three different longest sequences:
		 * 1. The longest sequence that starts at the left side of the array
		 * 2. The longest sequence with no edge constraints
		 * 3. the longest sequence ending at the right of the array
		 * => we always combine these sequences to produce the correct result
		 * */
		Sequence[] r = fjPool.invoke(new RecursiveJoiner(input, 0, input.length - 1));
		
		// TODO Implement
		return r[1];
	}

	@Override
	protected Sequence compute() {
		// TODO Implement		
		return new Sequence(0, 0);		
	}
}
