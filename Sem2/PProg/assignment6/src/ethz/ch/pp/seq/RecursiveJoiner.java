package ethz.ch.pp.seq;

import java.util.concurrent.RecursiveTask;

public class RecursiveJoiner extends RecursiveTask<Sequence[]> {

	private static final long serialVersionUID = 1L;
	private int[] input;
	private int start, end;
	
	public RecursiveJoiner(int[] input, int start, int end) {
		this.input = input;
		this.start = start;
		this.end = end;
	}

	@Override
	protected Sequence[] compute() {
		
		// handle the base case of length 1 or 2
		/*
		if(end - start < 2) {
			if(Math.exp(input[start]) == Math.exp(input[end])) {
				Sequence[] r = {new Sequence(start, end), new Sequence(start, end), new Sequence(start, end)};
				return r;
			} else {
				Sequence[] r = {new Sequence(start, start), new Sequence(start, start), new Sequence(end, end)};
				return r;
			}
		}
		*/
		
		/*feedback:
		 * the code below is confusing and difficult to follow. It would be much simpler to understand
		 * and to maintain the code if you reuse functions. In this case, for example you could call the
		 * function from sequential class to handle the base case.
		 */
		// Task B: Different cutoff value (mostly code from the provided sequential version)
		if(end - start < 100) {
			Sequence l = new Sequence(start, start);
			Sequence m = new Sequence(start, start);
			Sequence r = new Sequence(end, end);
			int length = end - start + 1, currLength = 1;
			boolean lDone = false;
			for (int i = start + 1; i < start + length; i++) {
				if (Math.exp(input[i - 1]) == Math.exp(input[i])) {
					currLength++;
				} else {
					if (m.lenght() < currLength) {
						m = new Sequence(i - currLength, i - 1);
						l = !lDone ? new Sequence(i - currLength, i - 1) : l;
						lDone = true;
					}
					currLength = 1;
				}
			}
			r = new Sequence(start + length - currLength, start + length - 1);
			if (m.lenght() < currLength) {
				m = new Sequence(start + length - currLength, start + length - 1);
			}
			return new Sequence[] {l, m ,r};
		}
		
		// split up input further if not at the base Case
		int middle = (start + end) / 2;
		RecursiveJoiner taskLeft = new RecursiveJoiner(input, start, middle);
		RecursiveJoiner taskRight = new RecursiveJoiner(input, middle + 1, end);
		taskLeft.fork();
		taskRight.fork();
		Sequence[] r = taskRight.join();
		Sequence[] l = taskLeft.join();
		
		// construct the result
		Sequence[] result = new Sequence[3];
		if(Math.exp(input[middle]) == Math.exp(input[middle + 1])) {
			int lLen = middle - start + 1, rLen = end - middle;
			result[0] = l[0].lenght() == lLen ? new Sequence(start, r[0].endIndex) : l[0];
			/*feedback:
			 * In the case two neighbouring elements are the same, you could create another 
			 * task that counts the common sequence.
			 */
			Sequence joinedOverBorder = new Sequence(l[2].startIndex, r[0].endIndex);
			result[1] = l[1].lenght() >= joinedOverBorder.lenght() ? l[1] : joinedOverBorder;
			result[1] = result[1].lenght() >= r[1].lenght() ? result[1] : r[1];
			result[2] = r[2].lenght() == rLen ? new Sequence(l[2].startIndex, end) : r[2];
		} else {
			result[0] = l[0];
			result[1] = l[1].lenght() >= r[1].lenght() ? l[1] : r[1];
			result[2] = r[2];
		}
		return result;
	}
	
}
