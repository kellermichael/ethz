package assignment10.Database;

public class MyBarrier {
	
	int barrierSize;
	int arrived;

	MyBarrier(int n){
		barrierSize = n;
		arrived = 0;
    }

	synchronized void await() throws InterruptedException {
		arrived++;
		while(barrierSize != arrived) {
			this.wait();
		}
		this.notifyAll();
		/**feedback:
		 * reset the counter as well
		 * current = 0;
		 */
	}
}
