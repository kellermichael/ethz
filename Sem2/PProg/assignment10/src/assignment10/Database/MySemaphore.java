package assignment10.Database;

public class MySemaphore {

	private volatile int count;
	private volatile int maxCount;
	private Object monitor = new Object();

	public MySemaphore(int maxCount) {
		this.maxCount = maxCount;
	}

	public void acquire() throws InterruptedException {
		synchronized(monitor) {
			while(count == maxCount) {
				monitor.wait();
			}
			count++;
		}
	}

	public void release() {
		synchronized(monitor) {
			count--;
			monitor.notifyAll();
		}
	}

}
