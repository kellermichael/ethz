package assignment10.Bridge;

import java.util.Random;

public class BridgeMonitor extends Bridge {
	// TODO use this object as a monitor
	// you might find that you need some additional variables.
	
	// set mode
	/*
	 * isFair = true allows you to say how many cars must have passed before
	 * a truck may enter the bridge. (This only applies if there are cars waiting)
	 * 
	 * isFair = false will in effect first allow all the cars to pass and then all the
	 * trucks, which is more efficient but not very fair
	 * */
	
	/**feedback:
	 * Great!
	 * I appreciate that you have extended the program to include 
	 * fairness as well.
	 */
	public boolean isFair = true;
	public int carsPerTruck = 5, carsPassed = 0, trucksWaiting = 0, carsWaiting = 0;
	
	
	
	// vars
	public int carsOnBridge = 0, trucksOnBridge = 0;
	private final Object monitor = new Object();

	public void enterCar() throws InterruptedException {
		if(isFair) {
			synchronized(monitor) {
				carsWaiting++;
				while(carsOnBridge == 3 | trucksOnBridge == 1 | (trucksWaiting > 0 && carsPassed >= carsPerTruck)) {
					monitor.wait();
				}
				carsWaiting--;
				carsPassed++;
				carsOnBridge++;
				monitor.notifyAll();
			}
		} else {
			synchronized(monitor) {
				while(carsOnBridge == 3 | trucksOnBridge == 1) {
					monitor.wait();
				}
				carsOnBridge++;
				monitor.notifyAll();
			}
		}
	}

	public void leaveCar() {
		synchronized(monitor) {
			carsOnBridge--;
			monitor.notifyAll();
		}
	}

	public void enterTruck() throws InterruptedException {
		if(isFair) {
			synchronized(monitor) {
				trucksWaiting++;
				while(carsOnBridge > 0 | trucksOnBridge > 0 | (carsWaiting > 0 && carsPassed < carsPerTruck)) {
					monitor.wait();
				}
				trucksWaiting--;
				trucksOnBridge++;
				monitor.notifyAll();
			}
		} else {
			synchronized(monitor) {
				while(carsOnBridge > 0 | trucksOnBridge > 0) {
					monitor.wait();
				}
				trucksOnBridge++;
				monitor.notifyAll();
			}
		}
	}

	public void leaveTruck() {
		if(isFair) {
			synchronized(monitor) {
				trucksOnBridge--;
				carsPassed -= carsPerTruck;
				monitor.notifyAll();
			}
		} else {
			synchronized(monitor) {
				trucksOnBridge--;
				monitor.notifyAll();
			}
		}
	}

	public static void main(String[] args) {
		Random r = new Random();
		BridgeMonitor b = new BridgeMonitor();
		for (int i = 0; i < 100; ++i) {
			if (r.nextBoolean()) {
				(new Car()).driveTo(b);
			} else {
				(new Truck()).driveTo(b);
			}
		}
	}

}
