package assignment10.Bridge;

import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BridgeCondition extends Bridge {

	//TODO use this given lock and create conditions form it
	//you might find that you need some additional variables
	final Lock bridgeLock = new ReentrantLock();
	/**feedback:
	 * Using two conditions is more accurate in this case, 
	 * you put all trucks waiting for the bridge to be empty,
	 * while cars wait for the bridge to be not full.
	 * One condition does not give much more flexibility than 
	 * the lock itself.
	 */
	final Condition bridgeHasFreeSpot = bridgeLock.newCondition();
	int carsOnBridge = 0, carsWaiting = 0, trucksWaiting = 0, carsPerTruck = 5, carsPassed = 0;
	boolean isFair = true;

	public void enterCar() throws InterruptedException {
		try {
			bridgeLock.lock();
			if(isFair) {
				carsWaiting++;
				while(carsOnBridge == 3 | (trucksWaiting > 0 && carsPassed >= carsPerTruck)) {
					bridgeHasFreeSpot.await();
				}
				carsPassed++;
				carsWaiting--;
			} else {
				while(carsOnBridge == 3) {
					bridgeHasFreeSpot.await();
				}
			}
			carsOnBridge++;
		} finally {
			bridgeLock.unlock();
		}
	}

	public void leaveCar() {
		try {
			bridgeLock.lock();
			carsOnBridge--;
			bridgeHasFreeSpot.signalAll();
		} finally {
			bridgeLock.unlock();
		}
	}

	public void enterTruck() throws InterruptedException {
		try {
			bridgeLock.lock();
			if(isFair) {
				trucksWaiting++;
				while(carsOnBridge > 0 | (carsWaiting > 0 && carsPassed < carsPerTruck)) {
					bridgeHasFreeSpot.await();
				}
				trucksWaiting--;
			} else {
				while(carsOnBridge > 0) {
					bridgeHasFreeSpot.await();
				}
			}
			carsOnBridge = 3;
		} finally {
			bridgeLock.unlock();
		}
	}

	public void leaveTruck() {
		try {
			bridgeLock.lock();
			carsOnBridge = 0;
			if(isFair) {
				carsPassed -= carsPerTruck;
			}
			bridgeHasFreeSpot.signalAll();
		} finally {
			bridgeLock.unlock();
		}
	}

	public static void main(String[] args) {
		Random r = new Random();
		BridgeCondition b = new BridgeCondition();
		for (int i = 0; i < 100; ++i) {
			if (r.nextBoolean()) {
				(new Car()).driveTo(b);
			} else {
				(new Truck()).driveTo(b);
			}
		}
	}

}
