package ethz.ch.pp.searchAndCount;

import java.util.concurrent.atomic.AtomicInteger;

import ethz.ch.pp.util.Workload;

public class SearchAndCountThreadDivideAndConquer {
	
	public static int countNoAppearances(int[] input, Workload.Type wt, int cutOff, int numThreads) {
		//TODO implement
		AtomicInteger occurrences = new AtomicInteger();
		AtomicInteger threadNumber = new AtomicInteger();
		WorkerB worker = new WorkerB(input, wt, occurrences, cutOff, threadNumber, numThreads);
		Thread t = new Thread(worker);
		t.start();
		try {
			t.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return occurrences.get();
	}

}