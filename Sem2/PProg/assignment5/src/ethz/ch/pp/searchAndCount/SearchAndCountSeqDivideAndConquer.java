package ethz.ch.pp.searchAndCount;

import ethz.ch.pp.util.Workload;

public class SearchAndCountSeqDivideAndConquer {

	public static int countNoAppearances(int[] input, Workload.Type wt) {
		
		// if cannot split further
		if(input.length == 1) {
			return Workload.doWork(input[0], wt) ? 1 : 0;
		} else {
			// split the work array
			int half = input.length / 2;
			/*feedback:
			 * int this task making copies of the array is an unnecessary overhead,
			 * you should only pass indexes that each sub task should handle.
			 */
			int[] subProbA = new int[half];
			int[] subProbB = new int[input.length - half];
			for(int a = 0; a < input.length; a++) {
				if(a < half) {
					subProbA[a] = input[a];
				} else {
					subProbB[a - half] = input[a];
				}
			}
			int resA = countNoAppearances(subProbA, wt);
			int resB = countNoAppearances(subProbB, wt);
			return resA + resB;
		}
	}

}