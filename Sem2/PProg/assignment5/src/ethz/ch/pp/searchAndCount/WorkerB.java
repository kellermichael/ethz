package ethz.ch.pp.searchAndCount;

import java.util.concurrent.atomic.AtomicInteger;

import ethz.ch.pp.util.Workload;
import ethz.ch.pp.util.Workload.Type;

public class WorkerB implements Runnable {

	int[] input;
	Workload.Type wt;
	AtomicInteger occurrences;
	int cutOff;
	AtomicInteger threadNumber;
	int numThreads;

	public WorkerB(int[] Input, Type Wt, AtomicInteger Occurrences, int CutOff, AtomicInteger ThreadNumber, int NumThreads) {
		// TODO Auto-generated constructor stub
		input = Input;
		wt = Wt;
		occurrences = Occurrences;
		cutOff = CutOff;
		threadNumber = ThreadNumber;
		numThreads = NumThreads;
	}

	@Override
	public void run() {
		// if cannot split further
		if (input.length <= cutOff) {
			for (int a = 0; a < input.length; a++) {
				if (Workload.doWork(input[a], wt)) {
					occurrences.incrementAndGet();
				}
			}
		} else {
			// split the work array
			int half = input.length / 2;
			/*
			 * feedback:
			 * again, it is not necessary to allocate new array.
			 */
			int[] subProbA = new int[half];
			int[] subProbB = new int[input.length - half];
			for (int a = 0; a < input.length; a++) {
				if (a < half) {
					subProbA[a] = input[a];
				} else {
					subProbB[a - half] = input[a];
				}
			}
			/*feedback:
			 * you can handle one half in the current thread, instead of creating 
			 * two new threads,
			 */
			WorkerB buddyA = new WorkerB(subProbA, wt, occurrences, cutOff, threadNumber, numThreads);
			Thread tA = new Thread(buddyA);
			boolean tAstarted = false;
			WorkerB buddyB = new WorkerB(subProbB, wt, occurrences, cutOff, threadNumber, numThreads);
			Thread tB = new Thread(buddyB);
			boolean tBstarted = false;

			/*
			 * feedback:
			 * what is the goal of the fowlloing code?
			 * It looks like if you have more tasks than threads the code
			 * will run sequentially.
			 */
			if (threadNumber.getAndIncrement() < numThreads) {
				tAstarted = true;
				tA.start();
			} else {
				for (int a = 0; a < subProbA.length; a++) {
					if (Workload.doWork(subProbA[a], wt)) {
						occurrences.incrementAndGet();
					}
				}
			}
			if (threadNumber.getAndIncrement() < numThreads) {
				tBstarted = true;
				tB.start();
			} else {
				for (int a = 0; a < subProbB.length; a++) {
					if (Workload.doWork(subProbB[a], wt)) {
						occurrences.incrementAndGet();
					}
				}
			}
			if (tAstarted) {
				try {
					tA.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(tBstarted) {
				try {
					tB.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}

}
