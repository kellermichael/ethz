package ethz.ch.pp.searchAndCount;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import ethz.ch.pp.util.RandomGenerator;
import ethz.ch.pp.util.Workload;

public class Main {
	/*feedback:
	 * Well done!
	 */
	/*
	 * feedback:
	 * it would be very helpful to add some comments that describe what
	 * is the purpose of functions or code blocks.
	 */

	public static void main(String[] args) {
		RandomGenerator dg = new RandomGenerator();
		int[] input = dg.randomArray(1024*1024);

		sequential(input, Workload.Type.HEAVY);

		taskA(input, Workload.Type.HEAVY);
		taskB(input, Workload.Type.HEAVY);
		taskC(input, Workload.Type.HEAVY);
		taskD(input, Workload.Type.HEAVY);
	}

	public static void sequential(int[] input, Workload.Type wt){
		long t0 = System.currentTimeMillis();
		for (int i = 0; i < 5; i++) {
			SearchAndCountSeq.countNoAppearances(input, wt);
		}
		long t1 = System.currentTimeMillis();
		System.out.println("For (inputsize=" + input.length + ",workload=" + wt + ") SearchAndCountSeq takes "
				+ ((t1 - t0)/5) + " msec");
	}

	public static void taskA(int[] input, Workload.Type wt){
		System.out.println("=====================================");
		System.out.println("TaskA");
		long t0 = System.currentTimeMillis();
		for (int i = 0; i < 5; i++) {
			int result = SearchAndCountSeqDivideAndConquer.countNoAppearances(input, wt);
			System.out.println("Task A Result: " + result);
		}
		long t1 = System.currentTimeMillis();
		System.out.println("For (inputsize=" + input.length + ",workload=" + wt + ") taskA takes "
				+ ((t1 - t0)/5) + " msec");
	}

	public static void taskB(int[] input, Workload.Type wt){
		System.out.println("=====================================");
		System.out.println("TaskB");
		int threadNumb = 16;
		long t0 = System.currentTimeMillis();
		for (int i = 0; i < 5; i++) {
			int result = SearchAndCountThreadDivideAndConquer.countNoAppearances(input, wt, 1, threadNumb);
			System.out.println("Task B Result: " + result);
		}
		long t1 = System.currentTimeMillis();
		System.out.println("For (inputsize=" + input.length + ",workload=" + wt + ") taskB takes "
				+ ((t1 - t0)/5) + " msec using " + threadNumb + " threads");
	}

	public static void taskC(int[] input, Workload.Type wt){
		System.out.println("=====================================");
		System.out.println("TaskC");
		
		// Note: these values were determined by experimenting on my machine
		
		int threadNumb = 16;
		int cutOff = 500;
		long t0 = System.currentTimeMillis();
		for (int i = 0; i < 5; i++) {
			int result = SearchAndCountThreadDivideAndConquer.countNoAppearances(input, wt, cutOff, threadNumb);
			System.out.println("Task C Result: " + result);
		}
		long t1 = System.currentTimeMillis();
		System.out.println("For (inputsize=" + input.length + ",workload=" + wt + ") taskC takes "
				+ ((t1 - t0)/5) + " msec using " + threadNumb + " threads and a Cutoff of " + cutOff);
	}

	public static void taskD(int[] input, Workload.Type wt){
		System.out.println("=====================================");
		System.out.println("TaskD");
		int threadNumb = 16;
		int cutOff = 500;
		
		long t0 = System.currentTimeMillis();
		for (int i = 0; i < 5; i++) {
			AtomicInteger occurrences = new AtomicInteger();
			ExecutorService exs = Executors.newFixedThreadPool(threadNumb);
			int numbOfWorkPortions = input.length % cutOff == 0 ? input.length / cutOff : input.length / cutOff + 1;
			for(int a = 0; a < numbOfWorkPortions; a++) {
				int[] portion;
				if(input.length - (a+1)*cutOff >= 0) {
					portion = new int[cutOff];
					for(int b = 0; b < cutOff; b++) {
						portion[b] = input[a*cutOff + b];
					}
				} else {
					int tmp = input.length - a*cutOff;
					portion = new int[tmp];
					for(int b = 0; b < tmp; b++) {
						portion[b] = input[a*cutOff + b];
					}
				}
				
				TaskD work = new TaskD(portion, wt, occurrences);
				exs.submit(work);
			}
			exs.shutdown();
			try {
				exs.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
			} catch (InterruptedException e) {
				  System.out.println("Did an oopsie :(");
			}
			System.out.println("Task D Result: " + occurrences.get());
		}
		long t1 = System.currentTimeMillis();
		System.out.println("For (inputsize=" + input.length + ",workload=" + wt + ") taskD takes "
				+ ((t1 - t0)/5) + " msec using " + threadNumb + " threads and a Cutoff of " + cutOff);
		
	}

}
