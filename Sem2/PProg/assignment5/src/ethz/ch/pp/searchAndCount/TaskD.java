package ethz.ch.pp.searchAndCount;

import java.util.concurrent.atomic.AtomicInteger;

import ethz.ch.pp.util.Workload;
import ethz.ch.pp.util.Workload.Type;

public class TaskD implements Runnable {
	

	int[] input;
	Workload.Type wt;
	AtomicInteger occurrences;

	public TaskD(int[] Input, Type Wt, AtomicInteger Occurrences) {
		input = Input;
		wt = Wt;
		occurrences = Occurrences;
	}

	@Override
	public void run() {
		for (int i = 0; i < input.length; i++) {
			if (Workload.doWork(input[i], wt)) {
				occurrences.getAndIncrement();
			}
		}
	}

}