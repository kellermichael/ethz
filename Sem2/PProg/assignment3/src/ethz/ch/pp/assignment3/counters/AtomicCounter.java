package ethz.ch.pp.assignment3.counters;

import java.util.concurrent.atomic.AtomicInteger;

//TODO: implement
public class AtomicCounter implements Counter {
	
	AtomicInteger counter = new AtomicInteger(0);

	@Override
	public void increment() {
		counter.incrementAndGet();
		// System.out.println(Thread.currentThread().getName());
	}

	@Override
	public int value() {
		//TODO: implement
		return counter.get();
	}

}