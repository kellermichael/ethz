package ethz.ch.pp.assignment3.counters;

//TODO: implement
public class SynchronizedCounter implements Counter {
	
	int count = 0;
	
	@Override
	public synchronized void increment() {
		//TODO: implement			
		count = count + 1;
		// Task C: Threads are always scheduled in (medium sized) blocks, because it costs to switch between threads constantly and we want to get
		// the maximum work out of a thread we can per activation
		// System.out.println(Thread.currentThread().getName());
	}

	@Override
	public synchronized int value() {
		//TODO: implement
		return count;		
	}

}