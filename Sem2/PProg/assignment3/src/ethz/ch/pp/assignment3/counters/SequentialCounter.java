package ethz.ch.pp.assignment3.counters;

//TODO: implement
public class SequentialCounter implements Counter {
	
	int count;
	
	@Override
	public void increment() {
		count = count + 1;
		// System.out.println(Thread.currentThread().getName());
	}

	@Override
	public int value() {
		//TODO: implement
		return count;		
	}
	
	/**feedback:
	 * for readability put constructors on top of other methods.
	 */
	public SequentialCounter() {
		count = 0;
	}
}