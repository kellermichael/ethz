package ethz.ch.pp.assignment3;

import java.util.ArrayList;
import java.util.List;

import ethz.ch.pp.assignment3.counters.AtomicCounter;
import ethz.ch.pp.assignment3.counters.Counter;
import ethz.ch.pp.assignment3.counters.SequentialCounter;
import ethz.ch.pp.assignment3.counters.SynchronizedCounter;
import ethz.ch.pp.assignment3.threads.ThreadCounterFactory;
import ethz.ch.pp.assignment3.threads.ThreadCounterFactory.ThreadCounterType;


public class Main {
	/**feedback:
	 * Well done!
	 */

	public static void count(final Counter counter, int numThreads, ThreadCounterType type, int numInterations) {
		List<Thread> threads = new ArrayList<Thread>();
		for (int i = 0; i < numThreads; i++) {
			threads.add(new Thread(ThreadCounterFactory.make(type, counter, i, numThreads, numInterations)));
		}

		for (int i = 0; i < numThreads; i++) {
			threads.get(i).start();
		}
		
		for (int i = 0; i < numThreads; i++) {
			try {
				threads.get(i).join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		taskASequential();
		taskAParallel();
		taskB();
		taskD();
		taskE();
		taskF();
	}
	
	public static void taskASequential(){
		Counter counter = new SequentialCounter();
		count(counter, 1, ThreadCounterType.NATIVE, 100000);
		System.out.println("Counter: " + counter.value());
	}

	public static void taskAParallel(){
		Counter counter = new SequentialCounter();
		count(counter, 4, ThreadCounterType.NATIVE, 100000);
		System.out.println("Counter: " + counter.value());
	}
	
	public static void taskB(){
		Counter counter = new SynchronizedCounter();
		count(counter, 4, ThreadCounterType.NATIVE, 100000);
		System.out.println("Counter: " + counter.value());
	}
	
	public static void taskD(){
		Counter counter = new SequentialCounter(); //TODO: Because we ensure only one thread is running and only performs one inc() op. we can use SequentialCounter
		count(counter, 4, ThreadCounterType.FAIR, 100000);
		System.out.println("Counter: " + counter.value());
	}
	
	public static void taskE(){
		Counter counter = new AtomicCounter();
		count(counter, 4, ThreadCounterType.NATIVE, 100000);
		System.out.println("Counter: " + counter.value());
	}
	
	/*
	 * Result Explanation: As far as I can tell sometimes there are special hardware instructions that atomic integer
	 * can use to increase performance (only on some machines)
	 * source: https://stackoverflow.com/questions/8878655/performance-difference-of-atomicinteger-vs-integer
	 * 
	 * IS this correct/ the main reason?
	 **/
	public static void taskF(){
		int n = 500;
		Counter counterAtomic = new AtomicCounter();
		Counter counterSynchronized = new AtomicCounter();
		
		long timeAtomic = -System.nanoTime();
		for(int a = 0; a < n; a++) {
			count(counterAtomic, 4, ThreadCounterType.NATIVE, 100000);
		}
		timeAtomic += System.nanoTime();
		timeAtomic /= (n * 1.0e6);
		
		long timeSynchronized = -System.nanoTime();
		for(int a = 0; a < n; a++) {
			count(counterSynchronized, 4, ThreadCounterType.NATIVE, 100000);
		}
		timeSynchronized += System.nanoTime();
		timeSynchronized /= (n * 1.0e6);
		
		System.out.println("Atomic took " + timeAtomic + " ms");
		System.out.println("Synchronized took " + timeSynchronized + " ms");
	}
	

}
