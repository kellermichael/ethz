package assignment8.random;

import java.util.concurrent.atomic.AtomicLong;

public class AtomicRandom implements RandomInterface {
    private static final long a = 25214903917L;
    private static final long c = 11;
    private AtomicLong state;

    public AtomicRandom(long seed) {
        state = new AtomicLong(seed);
    }
    
    /*
     * a) The while loop is necessary to ensure every thread actually moves 
     * the state up by one step. Otherwise another thread could write the 
     * result the current thread had calculated and the current thread would 
     * not have moved the state forward by one and would exit without doing so.
     * 
     * b) I.G. the Atomic version is always slower. The more threads there are the 
     * slower it gets (especially in comparison to the Locked version)! The problem
     * is that we have a lot of threads doing the same calculation only to have the work 
     * wasted when another thread gets there just slightly faster. While this is in
     * theory not an issue, in practice this leads to a lot of overhead. (A lot of memory
     * accesses for example which are unnecessary and slow down the fastest threads from
     * accessing the memory)
     * 
     * A possible solution to this issue is trying to maximize the number of successful updates.
     * As I had heard of exponential backoff I decided I'd give it a shot. I did not expect it
     * to work as well as it did (about 3x faster). By backing off exponentially we increase the
     * chance that there won't be two threads wanting to write at the same time. This means
     * we essentially get very close to back to back updates without having the overhead
     * of synchronized.
     * 
     * c) I think optimistic concurrency control is most likely best utilized when we don't expect
     * too many simultaneous updates. However even then there appear to be strategies to optimize.
     * 
     * d) see below
     * 
     * 
     * */

    @Override
    public int nextInt() {
    	long orig, next, wasRecected = 1;
    	while(true) {
    		// get the current seed value
        	orig = state.get();
        	// using recurrence equation to generate next seed
        	next = (a * orig + c) & (~0L >>> 16);
        	// store the updated seed (and sleep if necessary)
    		if(state.compareAndSet(orig, next)) {
    			break;
    		} else {
    			try {
					Thread.currentThread();
					Thread.sleep(wasRecected);
					wasRecected ^= wasRecected == 256 ? 2 : 1;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    	}
    	
    	return (int) (next >>> 16);
    }
}
