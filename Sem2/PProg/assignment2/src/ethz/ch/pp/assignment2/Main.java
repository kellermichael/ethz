package ethz.ch.pp.assignment2;
import java.util.Random;

public class Main {

	/** feedback:
	 * Well done!
	 * Some comments below.
	 */
	
	public static void main(String[] args) throws InterruptedException {
		
		/*
		 * Some Notes:
		 * - Note: I chose to modify the numPrimeFactors() function to automatically skip factors with 2 as a multiple,
		 * 	Testing on my machine made this significantly faster (the 100'000 best case by about 200ms)
		 * 
		 * Discussion of the results: As far as I am able to tell the best case 
		 * usually is around where the number of threads is equivalent to the number of cores my system has.
		 * This makes sense, because then the entire processor can work on the problem. Having only one thread is slow,
		 * while having 256 threads only causes unnecessary overhead, especially for smaller instances of the problem
		 * 
		 * 
		 * */
		
		System.out.println("System has " + Runtime.getRuntime().availableProcessors() + " available cores");
 		
		taskA();
		
		int[] input1 = generateRandomInput(1000);
		int[] input2 = generateRandomInput(10000);
		int[] input3 = generateRandomInput(100000);
		int[] input4 = generateRandomInput(1000000);
		
		// Sequential version
		taskB(input1);
		taskB(input2);
		taskB(input3);
		taskB(input4);
		
		// Tasks F and G
		int[] threadCount = {1, 2, 4, 8, 16, 32, 64, 128, 256};
		
		System.out.println("Sequential Version:");
		System.out.println("Treads\t 100\t 1000\t 10000\t 100000");
		// run test multiple times
		int noOfTestRuns = 5;
		long time1 = 0; long time2 = 0; long time3 = 0; long time4 = 0;
		for(int a = 0; a < noOfTestRuns; a++) {
			input1 = generateRandomInput(100);
			input2 = generateRandomInput(1000);
			input3 = generateRandomInput(10000);
			input4 = generateRandomInput(100000);
				
			time1 -= System.nanoTime();
			solveSeq(input1);
			time1 += System.nanoTime();
				
			time2 -= System.nanoTime();
			solveSeq(input2);
			time2 += System.nanoTime();
				
			time3 -= System.nanoTime();
			solveSeq(input3);
			time3 += System.nanoTime();
				
			time4 -= System.nanoTime();
			solveSeq(input4);
			time4 += System.nanoTime();	
		}
			
		System.out.println("1\t" + (int) (time1 / 1.0e6 / noOfTestRuns) + " ms\t" + (int) (time2 / 1.0e6 / noOfTestRuns) + " ms\t" + (int) (time3 / 1.0e6 / noOfTestRuns) + " ms\t" + (int) (time4 / 1.0e6 / noOfTestRuns) + " ms");
		
		
		System.out.println("Parallel Version:");
		System.out.println("Treads\t 100\t 1000\t 10000\t 100000");
		for(int a = 0; a < threadCount.length; a++) {
			time1 = 0; time2 = 0; time3 = 0; time4 = 0;
			for(int b = 0; b < 10; b++) {
				input1 = generateRandomInput(100);
				input2 = generateRandomInput(1000);
				input3 = generateRandomInput(10000);
				input4 = generateRandomInput(100000);
				
				time1 -=System.nanoTime();
				solveParallel(input1, threadCount[a]);
				time1 += System.nanoTime();
				
				time2 -= System.nanoTime();
				solveParallel(input2, threadCount[a]);
				time2 += System.nanoTime();
				
				time3 -= System.nanoTime();
				solveParallel(input3, threadCount[a]);
				time3 += System.nanoTime();
				
				time4 -= System.nanoTime();
				solveParallel(input4, threadCount[a]);
				time4 += System.nanoTime();
			}
			System.out.println(threadCount[a] + "\t" + (int) (time1 / 1.0e6 / noOfTestRuns) + " ms\t" + (int) (time2 / 1.0e6 / noOfTestRuns) + " ms\t" + (int) (time3 / 1.0e6 / noOfTestRuns) + " ms\t" + (int) (time4 / 1.0e6 / noOfTestRuns) + " ms");
		}
		
		long threadOverhead = taskC();
		System.out.format("Thread overhead on current system is: %d nano-seconds\n", threadOverhead);		
	}
	
	private final static Random rnd = new Random(42);

	public static int[] generateRandomInput() {
		return generateRandomInput(rnd.nextInt(10000) + 1);
	}
	
	public static int[] generateRandomInput(int length) {	
		int[] values = new int[length];		
		for (int i = 0; i < values.length; i++) {
			values[i] = rnd.nextInt(99999) + 1;				
		}		
		return values;
	}
	
	public static int[] computePrimeFactors(int[] values) {		
		int[] factors = new int[values.length];	
		for (int i = 0; i < values.length; i++) {
			factors[i] = numPrimeFactors(values[i]);
		}		
		return factors;
	}
	
	public static int numPrimeFactors(int number) {
		int primeFactors = 0;
		int n = number;		
		for (int i = 2; i <= n; i++) {
			// attempt speed things up
			if(i != 2 && i % 2 == 0) {i++;}
			while (n % i == 0) {
				primeFactors++;
				n /= i;
			}
		}
		if(n != 1) {System.out.println("Error!");}
		return primeFactors;
	}
	
	public static class ArraySplit {
		public final int startIndex;
		public final int length;
		
		ArraySplit(int startIndex, int length) {
			this.startIndex = startIndex;
			this.length = length;
		}
	}
	
	// Task A
	public static void taskA() {
		testThread cls = new testThread();
		Thread t = new Thread(cls);
		t.start();
	}
	
	public static class testThread implements Runnable {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			System.out.println("With love from " + Thread.currentThread().getName());
		}
		
	}
	// End Task A
	
	// Task B
	public static int[] taskB(final int[] values) throws InterruptedException {
		long startTime = System.nanoTime();
		computePrimeFactors(values);
		System.out.println(Thread.currentThread().getName() + " solved Task in: " + (System.nanoTime() - startTime) / 1.0e6 + " ms");
		
		startTime = System.nanoTime();
		ResultObj result = new ResultObj(values.length);
		primeClass cls = new primeClass(0, values, result);
		Thread t = new Thread(cls);
		t.start();
		/** feedback:
		 * t.join() could throw an exception.
		 */
		t.join();
		System.out.println(t.getName() + " solved Task in: " + (System.nanoTime() - startTime) / 1.0e6 + " ms");
		return result.getResult();
	}
	
	/**
	 * feedback:
	 * What is the reason of using static class?
	 */
	public static class primeClass implements Runnable {
		
		int start;
		int[] input;
		ResultObj result;

		@Override
		public void run() {
			try {
				result.setValues(start, computePrimeFactors(input));
			} catch (Error E) {
				System.out.println("Error here");
			}
		}
		
		public primeClass(int strt, int[] i, ResultObj r) {
			input = i;
			result = r;
			start = strt;
		}
		
	}
	// End Task B
	
	// Task C
	public static long taskC() throws InterruptedException {
		long sum = 0;
		for(int a = 0; a < 20; a++) {
			long startTime = System.nanoTime();
			uselessClass cls = new uselessClass();
			Thread t = new Thread(cls);
			t.start();
			t.join();
			sum += System.nanoTime() - startTime;
		}
		return sum / 20;
	}
	
	public static class uselessClass implements Runnable {

		@Override
		public void run() {
			// Do nada
		}
		
	}
	// End Task C
	
	// Task D
	// Note: Coudln't really think of anything cleverer without having access to the actual array
	// Would have (after a significant array size) considered finding the sum of the array (or some other score) and then dividing the 
	// score by the partitions and then allocating each thread as close as possible to that number an array
	/*
	 * Example (length = 6, partNumb = 2)
	 * 
	 * {1, 5, 8, 10, 11, 15 } => Sum = 50 => partitionProblemSize = 50 / 2 = 25
	 * 
	 *  => {1, 5, 8, 10} (24) and {11, 15} (26)
	 *  
	 *  Note: I would choose to preserve the order of the elements from the original array
	 * */
	/** feedback:
	 * Have a look at the master solution and openPM also on how to 
	 * divide partitions.
	 * https://software.intel.com/en-us/articles/getting-started-with-openmp
	 */
	public static ArraySplit[] PartitionData(int length, int numPartitions) {
		ArraySplit[] result = new ArraySplit[numPartitions];
		int partitionedLength = 0;
		int partitionsSize = length / numPartitions;
		for(int a = 0; a < numPartitions-1; a++) {
			result[a] = new ArraySplit(partitionedLength, partitionsSize);
			partitionedLength += partitionsSize;
		}
		result[result.length - 1] = new ArraySplit(partitionedLength, length - partitionedLength);
		return result;
	}
	// End Task D
	
	// Task E
	public static int[] taskE(final int[] values, final int numThreads) {
		long startTime = System.nanoTime();
		ResultObj result = new ResultObj(values.length);
		Thread[] threads = new Thread[numThreads];
		ArraySplit[] splitup = PartitionData(values.length, numThreads);
		for(int a = 0; a < numThreads; a++) {
			int[] subProblem = new int[splitup[a].length];
			for(int b = 0; b < splitup[a].length; b++) {
				subProblem[b] = values[splitup[a].startIndex + b];
			}
			primeClass cls = new primeClass(splitup[a].startIndex, subProblem, result);
			Thread t = new Thread(cls);
			t.start();
			threads[a] = t;
		}
		for(int a = 0; a < numThreads; a++) {
			Thread t = threads[a];
			try {
				t.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("Multithreading solved Task in: " + (System.nanoTime() - startTime) / 1.0e6 + " ms");
		return result.getResult();
	}
	public static class ResultObj {
		private int[] result;
		
		public synchronized void setValues(int startPos, int[] vals) {
			for(int a = 0; a < vals.length; a++) {
				result[startPos + a] = vals[a];
			}
		}
		
		public int[] getResult() {
			return result;
		}
		
		public ResultObj(int size) {
			result = new int[size];
		}
	}
	// End Task E
	
	// Tasks F and G
	public static int[] solveSeq(final int[] values) throws InterruptedException {
		ResultObj result = new ResultObj(values.length);
		primeClass cls = new primeClass(0, values, result);
		Thread t = new Thread(cls);
		t.start();
		t.join();
		return result.getResult();
	}
	public static int[] solveParallel(final int[] values, final int numThreads) {
		ResultObj result = new ResultObj(values.length);
		Thread[] threads = new Thread[numThreads];
		ArraySplit[] splitup = PartitionData(values.length, numThreads);
		for(int a = 0; a < numThreads; a++) {
			int[] subProblem = new int[splitup[a].length];
			for(int b = 0; b < splitup[a].length; b++) {
				subProblem[b] = values[splitup[a].startIndex + b];
			}
			primeClass cls = new primeClass(splitup[a].startIndex, subProblem, result);
			Thread t = new Thread(cls);
			t.start();
			threads[a] = t;
		}
		for(int a = 0; a < numThreads; a++) {
			Thread t = threads[a];
			try {
				t.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result.getResult();
	}
	// End Tasks F and G


}
