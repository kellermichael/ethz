package assignment13.MPI;
import java.util.Arrays;

import mpi.*;

public class HelloWorld {
	
	public static void main(String[] args) {
		// Add MPI ``Hello World'' implementation.
		MPI.Init(args);
		
		int me = MPI.COMM_WORLD.Rank();
		int[] buffer = new int[1];
		
		if(me == 0) {
			MPI.COMM_WORLD.Send(buffer, 0, 1, MPI.INT, 1, 69);
		}
		
		for(int i = 0; i < 100; i++) {
			MPI.COMM_WORLD.Recv(buffer, 0, 1, MPI.INT, (me + 1) % 2, 69);
			buffer[0]++;
			System.out.println("MPI-Dude " + me + " here, I incremented to " + buffer[0]);
			MPI.COMM_WORLD.Send(buffer, 0, 1, MPI.INT, (me + 1) % 2, 69);
		}
		
		MPI.Finalize();
	}

}
