package assignment13.MPI;

import java.util.Arrays;

import mpi.*;

public class MPISieve {
/**
	 * Create an MPI parallel prime factor sieve.
	 * 
	 * @param The maximal number up to which to compute prime
	 *        numbers.	
	 * @param NumRanks The number of different Ranks that are run.
	 * @param Rank The rank of this node.
	 */
	int Base; // lowest number we need to check
	int Max; // highest number we need to check
	int PossiblyPrime[]; // Possibly Prime Array
	int Rank;
	int totalMax;
	
	MPISieve(int MaxNumber, int Rank, int NumRanks) {
		// Calc Rank specific values
		Base = MaxNumber / NumRanks * Rank + 1;
		Max = MaxNumber / NumRanks * (Rank+1);
		PossiblyPrime = new int[Max - Base + 1];
		this.Rank = Rank;
		this.totalMax = MaxNumber;
		
		// Initialize Possibly Prime array for all Ranks
		for (int i = 0; i < PossiblyPrime.length; i++)
			PossiblyPrime[i] = 1;
		
		// initialize Possibly Prime array for Rank 0
		if(Rank == 0)
			PossiblyPrime[0] = 0; // 1 is not prime
		
	}
	
	/**
	 * Count the number of primes.
	 * 
	 * @return
	 */
	public int getNumPrimes() {
		markAllNonPrimes();
		return countPrimes();
	}
	
	/**
	 * Mark all numbers that are non-primes.
	 */
	private void markAllNonPrimes() {
		int[] buffer = new int[1];
		if(Rank == 0) {
			// basically do the sequential version
			int MaxFactor = ((int) Math.sqrt(totalMax)) + 1; 
			for (int i = 1; i < MaxFactor; i++) {
				if(PossiblyPrime[i] == 1) {
					int factor = i+1;
					buffer[0] = factor;
					MPI.COMM_WORLD.Bcast(buffer, 0, 1, MPI.INT, 0);
					for(int j = factor * 2; j <= PossiblyPrime.length; j += factor) {
						PossiblyPrime[j-1] = 0;
						// System.out.println(Arrays.toString(PossiblyPrime) + ", factor: " + factor);
					}
				}
			}
			buffer[0] = -1;
			MPI.COMM_WORLD.Bcast(buffer, 0, 1, MPI.INT, 0);
		} else {
			while(true) {
				MPI.COMM_WORLD.Bcast(buffer, 0, 1, MPI.INT, 0);
				int factor = buffer[0];
				if(factor == -1) {break;}
				int start = 0;
				// while((Base + start) % factor != 0) {start++;}
				
				if((Base + start) % factor != 0) {
					start = factor - (Base % factor);
				}
				
				for(int j = start; j < PossiblyPrime.length; j += factor) {
					PossiblyPrime[j] = 0;
					// System.out.println(Arrays.toString(PossiblyPrime) + ", factor: " + factor);
				}
			}
		}
	}

	/**
	 * Count the number of primes within the local partition
	 * and obtain the overall number of primes by reducing
	 * across all ranks.
	 * 
	 * @return The total number of primes.
	 */
	private int countPrimes() {
		// System.out.println("Rank: " + Rank + ", PossiblyPrime: " + Arrays.toString(PossiblyPrime));
		MPI.COMM_WORLD.Reduce(PossiblyPrime, 0, PossiblyPrime, 0, PossiblyPrime.length, MPI.INT, MPI.SUM, 0);
		int sum = 0;
		// System.out.println("Rank: " + Rank + ", PossiblyPrime: " + Arrays.toString(PossiblyPrime));
		if(Rank == 0) {
			for(int val : PossiblyPrime) {
				sum += val;
			}	
		}
		return sum;
	}
}