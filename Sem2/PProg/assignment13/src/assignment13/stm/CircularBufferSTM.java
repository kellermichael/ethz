//package assignment13.stm;
//
//import scala.concurrent.stm.Ref;
//import scala.concurrent.stm.TArray;
//import scala.concurrent.stm.japi.STM;
//
//import java.util.concurrent.Callable;
//
///**
// * This class implements a {@link assignment13.stm.CircularBuffer} using software-transactional memory (more
// * specifically using ScalaSTM [http://nbronson.github.io/scala-stm/]).
// */
//public class CircularBufferSTM<E> implements CircularBuffer<E> {
//	
//	TArray.View<E> items;
//	Ref.View<Integer> start = STM.newRef(0);
//	Ref.View<Integer> end = STM.newRef(0);
//	// feedback: using an additional variable, e.g., count to store the number
//	// of current items in the buffer would simplify isEmpty and isFull functions.
//	final int capacity;
//	
//    CircularBufferSTM(int capacity) {
//    	//feedback: I don't understand the intention of using capacity+1 as the size of the buffer.
//    	// Please put some comments to explain your code in the following exercises.
//        items = STM.newTArray(capacity + 1);
//        this.capacity = capacity + 1;
//    }
//
//    public void put(final E item) {
//        STM.atomic(new Runnable() {
//
//			@Override
//			public void run() {
//				if(isFull()) {
//					STM.retry();
//				}
//				Ref.View<E> space = items.refViews().apply(end.get());
//				space.set(item);
//				end.set((end.get() + 1) % capacity);
//			}
//        	
//        });
//    }
//
//    public E take() {
//    	return STM.atomic(new Callable<E>() {
//
//			@Override
//			public E call() {
//				if(isEmpty()) {
//					STM.retry();
//				}
//				Ref.View<E> space = items.refViews().apply(start.get());
//				start.set((start.get() + 1) % capacity);
//				return space.get();
//			}
//        	
//        });
//    }
//
//    public boolean isEmpty() {
//    	return STM.atomic(new Callable<Boolean>() {
//
//			@Override
//			public Boolean call() throws Exception {
//				// TODO Auto-generated method stub
//				return start.get() == end.get();
//			}
//        	
//        });
//    }
//
//    public boolean isFull() {
//        return STM.atomic(new Callable<Boolean>() {
//
//			@Override
//			public Boolean call() throws Exception {
//				// TODO Auto-generated method stub
//				return (end.get() + 1) % capacity == start.get();
//			}
//        	
//        });
//    }
//}
