/*
 * mm-naive.c - The fastest, least memory-efficient malloc package.
 * 
 * In this naive approach, a block is allocated by simply incrementing
 * the brk pointer.  A block is pure payload. There are no headers or
 * footers.  Blocks are never coalesced or reused. Realloc is
 * implemented directly using mm_malloc and mm_free.
 *
 * NOTE TO STUDENTS: Replace this header comment with your own header
 * comment that gives a high level description of your solution.
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>

#include "mm.h"
#include "memlib.h"

#define WORDSIZE 4

/* single word (4) or double word (8) alignment */
#define ALIGNMENT 8

/* rounds up to the nearest multiple of ALIGNMENT */
#define ALIGN(size) (((size) + (ALIGNMENT - 1)) & ~0x7)

#define SIZE_T_SIZE (ALIGN(sizeof(size_t)))

// Global variables
static int *heapStart;

/*
            Helper Functions
        
        1) put: Writes int into specified Address

        2) pack: encodes pointer to next block on heap

*/
void put(int *p, int content)
{
    *p = content;
}
int pack(int size, int used)
{
    return size | used;
}

/* 
 * mm_init - initialize the malloc package.
 */
int mm_init(void)
{
    // create initial heap
    if ((heapStart = mem_sbrk(4 * WORDSIZE)) == (void *)-1)
    {
        return -1;
    }

    // 0 Block at start for alignment
    put(heapStart, 0);

    // Prolog start
    put(heapStart + WORDSIZE, pack(8, 1));

    // Prolog end
    put(heapStart + (2 * WORDSIZE), pack(8, 1));

    // Epilog end
    put(heapStart + (3 * WORDSIZE), pack(0, 1));

    if (mm_malloc(ALIGNMENT / WORDSIZE) == NULL)
        return -1;

    // update global vars
    heapStart += (2 * WORDSIZE);
    return 0;
}

/* 
 * mm_malloc - Allocate a block by incrementing the brk pointer.
 *     Always allocate a block whose size is a multiple of the alignment.
 */
void *mm_malloc(size_t size)
{
    printf("Starting Allocate of %ld bytes\n", size);

    // traverse the heap
    int *current = heapStart;
    int foundSize = 0;
    while (foundSize < size)
    {
        // skip used blocks
        while (*current & 1)
        {
            foundSize = *current - 1;
            current += foundSize;

            // we are at the end of the heap
            if (foundSize == 0)
            {
                break;
            }
        }

        // we are at the end of the heap
        if (foundSize == 0)
        {
            break;
        }
    }

    // new size needed + size for start/end pointers
    int newsize = ALIGN(size + SIZE_T_SIZE) + 2 * WORDSIZE;

    // move the global end pointer forward if necessary
    if (foundSize == 0)
    {
        mem_sbrk(newsize);
    }

    // set start/end pointers of block
    put(current, pack(newsize, 1));
    put(current + newsize - WORDSIZE, pack(newsize, 1));

    printf("Alloc Success\n");

    // return address to allocated block
    return current + WORDSIZE;

    // int newsize = ALIGN(size + SIZE_T_SIZE);
    // void *p = mem_sbrk(newsize);
    // if (p == (void *)-1)
    //     return NULL;
    // else
    // {
    //     *(size_t *)p = size;
    //     return (void *)((char *)p + SIZE_T_SIZE);
    // }
}

static void coalesce(int *start, int *end)
{

    // findes sizes of blocks
    int sizeCurrentBlock = end - start + WORDSIZE;
    int sizeBeforeBlock = ((*start - WORDSIZE) >> 3) << 3;
    int sizeAfterBlock = ((*end - WORDSIZE) >> 3) << 3;

    // find adresses of adjoining blocks
    int *beforeBlock = start - sizeBeforeBlock;
    int *afterBlock = end + WORDSIZE;

    // if before block is free
    if (!(*beforeBlock & 1))
    {
        // set start of behind block to end of current block
        start = beforeBlock;
        sizeCurrentBlock += sizeBeforeBlock;
        put(start, pack(sizeCurrentBlock, 0));
    }

    // if after block is free
    if (!(*afterBlock & 1))
    {
        // set start of behind block to end of current block
        end = afterBlock + sizeAfterBlock;
        sizeCurrentBlock += sizeAfterBlock;
        put(end, pack(sizeCurrentBlock, 0));
    }
}

/*
 * mm_free - Freeing a block does nothing.
 */
void mm_free(void *ptr)
{

    int *p = (int *)ptr;
    int blockSize = (*p - 1);

    // find start/ end blocks
    int *start = p - WORDSIZE;
    int *end = start + blockSize - WORDSIZE;

    // set start/ end to available
    put(start, pack(blockSize, 0));
    put(end, pack(blockSize, 0));

    coalesce(start, end);
}

/*
 * mm_realloc - Implemented simply in terms of mm_malloc and mm_free
 */
void *mm_realloc(void *ptr, size_t size)
{
    void *oldptr = ptr;
    void *newptr;
    size_t copySize;

    newptr = mm_malloc(size);
    if (newptr == NULL)
        return NULL;
    copySize = *(size_t *)((char *)oldptr - SIZE_T_SIZE);
    if (size < copySize)
        copySize = size;
    memcpy(newptr, oldptr, copySize);
    mm_free(oldptr);
    return newptr;
}
