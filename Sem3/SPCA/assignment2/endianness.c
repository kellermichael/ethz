#include <stdio.h>

int main() {

	// Tests if 1 is stored in the "front" by converting int to a string and getting the first byte
	int i = 1;
	int isLittleEndian = (int) (((char*)&i)[0]);
	
	if(isLittleEndian) {
		printf("This machine is little endian!\n");
	} else {
		printf("This machine is big endian!\n");
	}

	return 0;
}