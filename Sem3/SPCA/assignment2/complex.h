typedef struct complex {
    double real;
    double imaginary;
} complex;

typedef struct complex_set {
    int num_points_in_set;
    complex* points;

} complex_set;

// functions for the complex object
complex add(complex x, complex y);
complex subtract(complex x, complex y);
complex multiply(complex x, complex y);
complex divide(complex x, complex y);
void printComplex(complex x, char* ending);