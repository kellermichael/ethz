#include <stdint.h>
#include <stdio.h>


void swapPos(uint32_t array[], int length, int item) {
    uint32_t tmp = array[length - item - 1];
    array[length - item - 1] = array[item];
    array[item] = tmp;
}

void printArray(uint32_t array[], int length) {
    for (int i = 0; i < length; i++) {
        printf( "%d ", array[i]);
    }
    printf("\n");
}

int main() {
    // array that should be reversed
    uint32_t array[] = {1, 2, 3};
    int length = sizeof(array) / sizeof(uint32_t);

    // Print Original Array
    printf("Original: ");
    printArray(array, length);

    // swap the array positions in place
    for(int i = 0; i < (length/2); i++) {
        swapPos(array, length, i);
    }

    // Print Reversed Array
    printf("Reversed: ");
    printArray(array, length);

    return 0;
}