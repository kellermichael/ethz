#ifndef michael_k_complex_h
#define michael_k_complex_h
#include "complex.h"
#endif

#include <stdio.h>
#include <stdlib.h>

// complex object functions
complex add(complex x, complex y)
{
    return (complex){x.real + y.real, x.imaginary + y.imaginary};
}

complex subtract(complex x, complex y)
{
    return (complex){x.real - y.real, x.imaginary - y.imaginary};
}

complex multiply(complex x, complex y)
{
    double r = (x.real * y.real) - (x.imaginary * y.imaginary);
    double c = (x.real * y.imaginary) + (x.imaginary * y.real);
    return (complex){r, c};
}

complex divide(complex x, complex y)
{
    complex complement = {y.real, -1 * y.imaginary};
    double factor = multiply(y, complement).real;
    complex tmp = multiply(x, complement);
    return (complex){tmp.real / factor, tmp.imaginary / factor};
}

void printComplex(complex x, char *ending)
{
    printf("%f %fi%s", x.real, x.imaginary, ending);
}