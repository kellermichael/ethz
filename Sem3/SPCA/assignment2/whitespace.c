#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main() {

    // initial string
    char* string = "Hello World!";
    printf("The string is: %s\n", string);

    // find first word length
    char* orig = string;
    while(*string != 0 && *string++ != ' ') {}
    int length = string - orig;

    // build new string
    char* firstWord = (char*) malloc((length + 1) * sizeof(char));
    strncpy(firstWord, orig, length);
    firstWord[length] = 0;
    printf("The first word is: %s\n", firstWord);

    // free memory
    free(firstWord);

    return 0;
}