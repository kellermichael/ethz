struct node {
    int value;
    struct node* smaller;
    struct node* larger;
};

void insert(int value);
int lookup(int value);
void burnDownTheForrest();