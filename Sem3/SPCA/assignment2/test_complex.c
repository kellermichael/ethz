#ifndef michael_k_complex_h
#define michael_k_complex_h
#include "complex.h"
#endif

#include <stdio.h>

int main() {
    complex a = {1, 2};
    complex b = {3, 4};

    complex c = add(a, b);
    printf("Add a and b: ");
    printComplex(c, "\n");

    c = subtract(a, b);
    printf("Subtract a and b: ");
    printComplex(c, "\n");

    c = multiply(a, b);
    printf("Multiply a and b: ");
    printComplex(c, "\n");

    c = divide(a, b);
    printf("Divide a and b: ");
    printComplex(c, "\n");

    return 0;
}