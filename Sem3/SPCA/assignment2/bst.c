#ifndef michael_k_bst
#define michael_k_bst
#include "bst.h"
#endif

#include <stdlib.h>
#include <stdio.h>

// should this be defined here?
struct node* head;

void insert(int value) {
    // traverse the bst
    struct node* next = head;
    while(next != 0) {
        if(next->value > value) {
            next = next->smaller;
        } else {
            next = next->larger;
        }
    }
    next = (struct node*) malloc(sizeof(struct node));
    next->value = value;
}
int lookup(int value) {
    // traverse the bst
    struct node* next = head;
    while(next != 0 && next->value != value) {
        if(next->value > value) {
            next = next->smaller;
        } else {
            next = next->larger;
        }
    }
    return (next != 0);
}

void burnDownTheForrest() {
    // if initial call set as head
    return burn(head);
}

void burn(struct node* n) {
    if(n == 0) return;

    // do recursion
    if(n->larger != 0) {
        return burn(n->larger);
    }
    if(n->smaller != 0) {
        return burn(n->smaller);
    }
    free(n);
}