#ifndef michael_k_bst
#define michael_k_bst
#include "bst.h"
#endif

#include <stdio.h>

int main() {
    // do inserts
    insert(1);

    // do lookups
    printf("Found 1: %d", lookup(1));

    // free the memory
    burnDownTheForrest();
    return 0;
}