/* This function marks the start of the farm */
int start_farm()
{
    return 1;
}

unsigned addval_370(unsigned x)
{
    return x + 3344464524U;
}

unsigned getval_143()
{
    return 1478486353U;
}

void setval_257(unsigned *p)
{
    *p = 2425393232U;
}

unsigned getval_328()
{
    return 3348711472U;
}

void setval_413(unsigned *p)
{
    *p = 2425393240U;
}

unsigned getval_128()
{
    return 2428995912U;
}

unsigned getval_267()
{
    return 3284633928U;
}

unsigned addval_408(unsigned x)
{
    return x + 3349760052U;
}

/* This function marks the middle of the farm */
int mid_farm()
{
    return 1;
}

/* Add two arguments */
long add_xy(long x, long y)
{
    return x+y;
}

unsigned getval_108()
{
    return 3525367433U;
}

void setval_159(unsigned *p)
{
    *p = 3286272328U;
}

unsigned getval_407()
{
    return 3531915673U;
}

void setval_129(unsigned *p)
{
    *p = 3223372169U;
}

unsigned addval_102(unsigned x)
{
    return x + 3674265225U;
}

void setval_330(unsigned *p)
{
    *p = 3229141641U;
}

unsigned getval_349()
{
    return 3767093417U;
}

void setval_169(unsigned *p)
{
    *p = 2430634316U;
}

unsigned addval_265(unsigned x)
{
    return x + 2425673353U;
}

unsigned addval_352(unsigned x)
{
    return x + 3376988553U;
}

unsigned addval_286(unsigned x)
{
    return x + 4190358152U;
}

void setval_125(unsigned *p)
{
    *p = 3372272265U;
}

unsigned getval_377()
{
    return 2447411528U;
}

void setval_296(unsigned *p)
{
    *p = 3281046155U;
}

unsigned getval_109()
{
    return 2429946300U;
}

unsigned getval_447()
{
    return 3232022921U;
}

unsigned getval_455()
{
    return 3353381192U;
}

void setval_391(unsigned *p)
{
    *p = 3674265225U;
}

void setval_308(unsigned *p)
{
    *p = 3767027938U;
}

void setval_226(unsigned *p)
{
    *p = 3284830571U;
}

unsigned getval_322()
{
    return 3285616969U;
}

unsigned addval_249(unsigned x)
{
    return x + 3281049289U;
}

unsigned addval_421(unsigned x)
{
    return x + 3531920905U;
}

unsigned getval_467()
{
    return 3223372169U;
}

void setval_205(unsigned *p)
{
    *p = 3286272344U;
}

unsigned getval_333()
{
    return 3252717896U;
}

void setval_268(unsigned *p)
{
    *p = 3250686433U;
}

unsigned addval_401(unsigned x)
{
    return x + 3227568777U;
}

void setval_403(unsigned *p)
{
    *p = 2425405705U;
}

void setval_220(unsigned *p)
{
    *p = 3229928073U;
}

unsigned getval_105()
{
    return 3268053293U;
}

unsigned getval_380()
{
    return 3526934923U;
}

/* This function marks the end of the farm */
int end_farm()
{
    return 1;
}
