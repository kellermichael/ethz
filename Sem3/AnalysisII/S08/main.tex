\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[a4paper,total={6in,9in}]{geometry}
\usepackage[ruled]{algorithm2e}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{physics}
\usepackage{graphicx}
\graphicspath{ {./imgs/} }

\title{Analysis II: Serie 8}
\author{Michael Keller}
\date{November 2020}

\begin{document}

\maketitle

\section*{Exercises}
\subsection*{8.1)}
\subsubsection*{a)}
We have the function
$f(x, y) = e^{-(x^2 + y^2 - 2x + 3y + 2)}$. This function
has the gradient
\begin{align*}
    \nabla f(0, 0) &= \begin{pmatrix}
        -(2x - 2)
        \cdot e^{-(x^2 + y^2 - 2x + 3y + 2)}\\
        -(2y + 3)
        \cdot e^{-(x^2 + y^2 - 2x + 3y + 2)}\\
    \end{pmatrix}\\
    &= \begin{pmatrix}
        2
        \cdot e^{-2}\\
        -3
        \cdot e^{-2}\\
    \end{pmatrix}\\
\end{align*}
at the point $(0, 0)$. Hence the tangent plane is
\begin{align*}
    f(0, 0) + \nabla f(0, 0) \cdot \begin{pmatrix}
        x - 0\\
        y - 0
    \end{pmatrix}
    &= f(0, 0) + 2x \cdot e^{-2} -3y \cdot e^{-2}\\
    &= e^{-2} + 2x \cdot e^{-2} -3y \cdot e^{-2}\\
    &= e^{-2} \cdot (1 + 2x - 3y)
\end{align*}
Hence we have in the Cartesian form
\begin{align*}
    E = \{(x, y, z)
    \;|\; (x, y) \in \mathbb{R}^2,
    0 = e^{-2} \cdot (1 + 2x - 3y) - z\}
\end{align*}
And in the parametric form
\begin{align*}
    \varphi (x, y) = (x, y, e^{-2} \cdot (1 + 2x - 3y))
\end{align*}
\subsubsection*{b)}
We can use geogebra
\begin{center}
    \includegraphics[scale=0.5]{8_1_b.PNG}
\end{center}
\subsubsection*{c)}
Here we find out at which points
$(x, y) \in \mathbb{R}^2$ the gradient is zero
\begin{align*}
    \nabla f(x, y) &= \begin{pmatrix}
        -(2x - 2)
        \cdot e^{-(x^2 + y^2 - 2x + 3y + 2)}\\
        -(2y + 3)
        \cdot e^{-(x^2 + y^2 - 2x + 3y + 2)}\\
    \end{pmatrix}
\end{align*}
As $e^x \neq 0$, we can conclude that the only
time where $\nabla f(x, y) = \begin{pmatrix}
    0\\
    0
\end{pmatrix}$ is when $x = 1$ and
$y = -\frac{3}{2}$.
\subsection*{8.2)}
\subsubsection*{a)}
For this task we will need a whole bunch of derivatives,
so let us compute them
\begin{align*}
    f(x, y)& = e^x \cdot \sin(y)\\
    \pdv*{f}{x} &= e^x \cdot \sin(y)\\
    \pdv*{f}{y} &= e^x \cdot \cos(y)\\
    \pdv*{^2f}{x^2} &= e^x \cdot \sin(y)\\
    \pdv*{^2f}{x \partial y} &= e^x \cdot \cos(y)\\
    \pdv*{^2f}{y^2} &= e^x \cdot - \sin(y)\\
\end{align*}
Hence we have the first degree Taylor Polynomial at
$(0, \frac{\pi}{2})$
\begin{align*}
    T_1 f \Bigg ( \begin{pmatrix}
        x\\
        y - \frac{\pi}{2}
    \end{pmatrix}, (0, \frac{\pi}{2}) \Bigg )
    &= f(0, \frac{\pi}{2})
    + \pdv*{f(0, \frac{\pi}{2})}{x} \cdot x
    + \pdv*{f(0, \frac{\pi}{2})}{y}
    \cdot (y - \frac{\pi}{2})\\
    &= f(0, \frac{\pi}{2})
    + \sin(\frac{\pi}{2}) \cdot x
    + \cos(\frac{\pi}{2}) \cdot (y - \frac{\pi}{2})\\
    &= f(0, \frac{\pi}{2})
    + \sin(\frac{\pi}{2}) \cdot x\\
    &= 1 + x
\end{align*}
And the second degree Taylor Polynomial is
\begin{align*}
    T_2 f \Bigg ( \begin{pmatrix}
        x\\
        y - \frac{\pi}{2}
    \end{pmatrix}, (0, \frac{\pi}{2}) \Bigg )
    &= T_1
    + \frac{x^2}{2} \cdot \pdv*{^2f}{x^2}
    + xy \pdv*{^2f}{x \partial y}
    + \frac{(y - \frac{\pi}{2})^2}{2}
    \cdot \pdv*{^2f}{y^2}\\
    &= 1 + x
    + \frac{x^2}{2} \cdot e^x \cdot \sin(y)
    + x(y - \frac{\pi}{2}) e^x \cdot \cos(y)
    + \frac{(y - \frac{\pi}{2})^2}{2} \cdot
    e^x \cdot - \sin(y)\\
    &= 1 + x
    + \frac{x^2}{2}
    - \frac{(y - \frac{\pi}{2})^2}{2}\\
\end{align*}
If we now look at the point
$(0, \frac{\pi}{2} + \frac{1}{4})$ we can get a feel
for the precision of our Taylor Polynomial
\begin{align*}
    f(0, \frac{\pi}{2} + \frac{1}{4})
    &= 0.968912422\\
    T_1 f(0, \frac{\pi}{2} + \frac{1}{4})
    &= 1\\
    T_2 f(0, \frac{\pi}{2} + \frac{1}{4})
    &= \frac{31}{32} = 0.96875\\
\end{align*}
\subsubsection*{b)}
For this task we will find an upper bound
for the following error function
\begin{align*}
    R_1 f(x, y)
    &= \frac{1}{2} \pdv*{^2f(x_s, y_s)}{x^2}
    \cdot (x - x_0)^2
    + \pdv*{^2f(x_s, y_s)}{x \partial y}
    \cdot (x - x_0) (y - y_0)
    + \frac{1}{2} \pdv*{^2f(x_s, y_s)}{y^2}
    \cdot (y - y_0)^2\\
    &= \frac{1}{2} e^{x_s} \cdot \sin(y_s)
    \cdot (x - x_0)^2
    + e^{x_s} \cos(y_s)
    \cdot (x - x_0) (y - y_0)
    - \frac{1}{2} e^{x_s} \sin(y)
    \cdot (y - y_0)^2\\
    &\leq \frac{1}{2 \cdot 4^2} e^{x_s} \cdot \sin(y_s)
    + \frac{1}{4^2} e^{x_s} \cdot \cos(y_s)\\
    &\leq \frac{1}{2 \cdot 4^2} e^{x_s}
    + \frac{1}{4^2} e^{x_s}\\
    &\leq  \frac{1}{2 \cdot 4^2} e^{\frac{1}{4}}
    + \frac{1}{4^2} e^{\frac{1}{4}}\\
    &= e^{\frac{1}{4}} \cdot \frac{3}{32}
    \approx 0.12
\end{align*}
\subsection*{8.3)}
\subsubsection*{a)}
Assuming $|xy| < 1$ we can write this as an infinite
series
\begin{align*}
    \frac{1}{1 - xy} &= 1 + xy + (xy)^2 + (xy)^3 + \dots
\end{align*}
And as the $k$'th Taylor Polynomial of a Polynomial
is simply the same polynomial with only the first
$k$ terms we have
\begin{align*}
    T_{2n} f(x, y) = \sum_{i = 0}^{2n} (xy)^i
\end{align*}
\subsubsection*{b)}
We can write $t = x^2y$ and this results in
\begin{align*}
    T_2 f(x, y)
    &= T_2 \arctan(t)\\
    &= t - \mathcal{O}(t^3)\\
    &= x^2y - \mathcal{O}(x^6y^3)
\end{align*}
\subsubsection*{c)}
We can use the fact that
$\ln(1 + t) = \sum_{k = 1}^{\infty}
\frac{(-1)^{k-1}}{k} t^k$
\begin{align*}
    T_{2n} \ln(1 + t)
    &= \sum_{k = 1}^{2n} \frac{(-1)^{k-1}}{k} t^k\\
    &= \sum_{k = 1}^{2n} \frac{(-1)^{k-1}}{k} |z^2|^k\\
\end{align*}
\subsubsection*{d)}
We have
\begin{align*}
    T_2
    &= f(x_0) + f'(x_0) + \frac{f''(x_0)}{2}\\
    &= 2^n + 2^{n-1} \cdot \sum_{i = 1}^{n} (x_i - 2)
    + 2^{n-3} \cdot \sum_{i = 1}^{n} (x_i - 2)
\end{align*}
\end{document}