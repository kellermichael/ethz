\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[a4paper]{geometry}
\usepackage[ruled]{algorithm2e}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{csquotes}
\usepackage{longtable}

\usepackage{fancyvrb} % for "\Verb" macro

\usepackage{csquotes}
% \usepackage[style=authoryear-ibid,backend=biber]{biblatex}
\usepackage[australian,american]{babel}
\usepackage{biblatex}
\addbibresource{bibliography.bib}% Syntax for version >= 1.2

\title{Calculating the probability of a decisive vote in the US presidential
election by state}
\author{Michael Keller}
\date{January 2021}
\begin{document}
\maketitle
\section{Abstract}
The US presidential election has been criticized for giving the voters of some
states significantly more power than others \cite{prokop}. The goal of this paper
is to investigate that claim. This was done by first estimating how
influential a state is in the presidential election. In a second step
an estimate of a voter's power in deciding their state
was made. Wisconsinites, Arizonans and
Iowans are shown to have significantly more voting power in comparison
to other voters. Thus these calculations highlight the imbalance of power
between voters of different states.
\section{Introduction}
The United States uses a system called the Electoral College to choose its
president. In this system, every state in the Union is awarded
electoral votes. Most states award their votes on a winner-take-all basis. At present
the total number of electors in the Electoral College is $538$. A candidate wins the
presidential election when they receive a majority, at least $270$ electoral votes \cite{usa-election-facts}.\\ \\
Recently there has been criticism that the Electoral College gives
some states (so called swing states) too much power \cite{prokop}.
The argument is that some states will reliably vote for the same
party they voted for in previous elections. These states supposedly do not yield much power in the electoral
process, because a candidate can safely assume they have or do not have the votes
of such a state. This means candidates can safely ignore the aforementioned
state and focus on states they might actually be able to influence by campaigning.
Hence hard-to-predict states hold more sway when it comes to
picking the president \cite{prokop}. If true, this would result in a voter's power
varying significantly, depending on their location. Should this be something
politicians want to change, the extent of the problem should be known. For
this a sound understanding of voter power in specific states is required.\\ \\
This report will consider the following question:
What is the probability that a single voter
of a given US state would be pivotal in the
presidential election?
This will be used as an indicator for political power based on a voter's home state.
\section{Approach to Calculation}
The calculation approach will be based on two stages. The first stage will
determine the probability of a state being decisive in the presidential election.
The second stage will calculate the probability of a voter being decisive in
their state. Finally the probability of a voter $v$ of state $s$ being
decisive in the election will be
\begin{align*}
  \text{PE} &= \text{Presidential election}\\
  Prob\{v \text{ in }s \text{ decides the PE}\}
  &= Prob\{v \text{ decides }s\}
  \cdot
  Prob\{s\text{ decides the PE}\}
\end{align*}
The decisiveness of a state can be calculated using the Banzhaf Power
index \cite{banzhaf}. Here it is first assumed that all state voting configurations are equally
probable. Then, the number of times a state can change the outcome
of the election by changing their electoral votes is determined.
This paper will also assume
all states award their votes in a winner-take-all manner
and only the Democratic and Republican parties can be voted for.
State $s$ then has power
\begin{align*}
  Power(s) &= \frac{\text{Number of voting situations where $s$ is decisive}}{\text{Number of voting situations}}
\end{align*}
It should be noted that calculating this for the $50$ states and the District of Columbia
would mean considering $2^{51} = 2'251'799'813'685'248$
voting situations. The computer used for this project can check
$\approx 30'000'000 \text{ cases}/s$. This would mean calculating
for $\approx 886 \text{ days}$, which is not feasible. By merging $10$ states
into two states, the computer would need about four days to calculate.
The most democratic and most republican states were grouped together,
as the main interest is with states that have hard to predict outcomes.
Based on 2020 election results (Appendix 5.1) this means the following states are merged
together:
\begin{center}
  \begin{tabular}{ |p{3.5cm}|c|p{3.5cm}|c|  }
    \hline
    \multicolumn{2}{|c|}{Democratic Group} &
    \multicolumn{2}{|c|}{Republican Group} \\ \hline
    State & Electoral Votes & State & Electoral Votes\\ \hline
    D.C. & 3 & Wyoming & 3\\
    Vermont & 3 & West Virginia & 5\\
    Massachusetts & 11 & North Dakota & 3\\
    Maryland & 10 & Oklahoma & 7\\
    Hawaii & 4 & Idaho & 4\\ \hline
   \end{tabular}
\end{center}
\noindent\\
The decisiveness of an individual in their state can be calculated
using Laruelle and Valenciano's definition of decisiveness.
Their definition says (for a voter $v$, a
configuration of votes $C$ and the set of
winning configurations of votes $W$):
\begin{align*}
    Prob \{v \text{ is decisive}\} =
    \sum_{C:v \in C \in W \text{ and }
    C \setminus v \not \in W} p(C)
    + \sum_{C:v \not \in C \not \in W \text{ and }
    C \cup v \in W} p(C)
\end{align*}
In this paper a configuration $C$ will be a string of $1$s and $0$s
with length $n$. Further, $n$ denotes the number of voters in a state.
The $i$-th character of a configuration will signify the way
the $i$-th voter voted. $1$ will signify that voter $i$ voted republican,
while $0$ will signify a democratic vote. Hence
\begin{align*}
  C &\in \{0, 1\}^n &
  C_i = 1 &\Leftrightarrow i \text{ voted republican}\\
  C_i &= \text{ voter $i$'s decision} &
  C_i = 0 &\Leftrightarrow i \text{ voted democrat}
\end{align*}
What remains to be defined is a function that assigns a configuration a
probability. For this it is assumed every state $s$ has a fixed number
of republican, democratic and undecided voters. Republican voters
in this model will always vote republican and Democrats will always
vote for democrats. Independent voters will vote for a republican with
the probability
\begin{align*}
  p(\text{Independent votes republican}) &:= \frac{\text{Number of Republicans in }s}{\text{Number of Republicans and Democrats in }s}
\end{align*}
and for a democrat with the probability
\begin{align*}
  p(\text{Independent votes democratic}) &:= \frac{\text{Number of Democrats in }s}{\text{Number of Republicans and Democrats in }s}
\end{align*}
Now let $p_i(C)$ be the probability that voter $i$ votes as configuration
$C$ specifies for them. Further let $r$ be the number of Republicans
in the state and $d$ similarly the number of democrats. $p_i$ is then defined as
\begin{align*}
  p_i(C) &=
  \begin{cases}
    0,  &i \text{ is republican and } C_i = 0\\
    0,  &i \text{ is democratic and } C_i = 1\\
    1,  &i \text{ is republican and } C_i = 1\\
    1,  &i \text{ is democratic and } C_i = 0\\
    \frac{r}{r + d},  &i \text{ is independent and } C_i = 1\\
    \frac{d}{r + d},  &i \text{ is independent and } C_i = 0\\
  \end{cases}
\end{align*}
Finally the probability function $p$ for a configuration
$C$ is the product of the probability functions for every voter
\begin{align*}
  p(C) &= \prod_{i = 1}^{n} p_i(C)
\end{align*}
In this situation
Laruelle and Valenciano's decisiveness definition
\cite{LaruelleAnnick2005Asad} calculates
the probability of a tie (assuming an even number of total voters without our voter $v$).
This is the only time a single voter will be influential in a state.\\ \\
Assuming an even number of total voters this means
\begin{align*}
  Prob\{v \text{ is decisive}\} &=
  \begin{cases}
    0, &\quad \text{if } \frac{n}{2} < r \text{ or } \frac{n}{2} < d\\
    \binom{n-r-d}{\frac{n}{2}-r} \cdot \frac{r}{r+d}^{\frac{n}{2}-r} \cdot \frac{d}{r+d}^{\frac{n}{2}-d} &\quad \text{else}\\
  \end{cases}
\end{align*}
\section{Conclusion}
\noindent
The most notable results are
the following.
\begin{center}
  \begin{tabular}{| l | r ||} 
  \hline
  State & probability of voter decisiveness in election \\ [0.5ex] 
  \hline\hline
  Arizona & 1.5197e-05 \\ \hline 
  Florida & 1.7732e-3204 \\ \hline 
  Georgia & 6.5367e-1632 \\ \hline 
  Iowa & 8.8948e-06 \\ \hline 
  Nebraska & 4.06e-298 \\ \hline 
  North Carolina & 3.8675e-1524 \\ \hline
  Wisconsin & 1.6874e-05 \\ \hline 
  \end{tabular}
\end{center}
\noindent \\
States not listed have probability $0$ or impossible.
Impossible denotes the case where more
than $50\%$ of voters in a state are guaranteed to vote for a specific
political party. These voters should be thought of as having
truly zero power.\\ \\
It should be noted
that a probability of $0$ really means a probability smaller than
$\approx 3.6$e$-4951$ (smallest number the computer used for this project
can represent). A complete list of results can be found in appendix 5.3. \\ \\
\begin{otherlanguage}{australian}
  \printbibliography
\end{otherlanguage}
\section{Appendix}
\subsection{2020 US presidential election Results by State}
Note: The following table is sorted alphabetically and was
obtained by \cite{washpost-election-results}.
  \begin{longtable}{|l|c|r|r|r|}
    \hline
    State & Votes & Democratic & Republican & Win Margin \\ \hline
    \hline
    Alabama & 9 & 36.6\% & 62.0\% & 25.4\% (R) \\ \hline
    Alaska & 3 & 42.8\% & 52.8\% & 10.0\% (R) \\ \hline
    Arizona & 11 & 49.4\% & 49.1\% & 0.3\% (D) \\ \hline
    Arkansas & 6 & 34.8\% & 62.4\% & 27.6\% (R) \\ \hline
    California & 55 & 63.5\% & 34.3\% & 29.2\% (D) \\ \hline
    Colorado & 9 & 55.4\% & 41.9\% & 13.5\% (D) \\ \hline
    Connecticut & 7 & 59.2\% & 39.2\% & 20.0\% (D) \\ \hline
    Delaware & 3 & 58.8\% & 39.8\% & 19.0\% (D) \\ \hline
    District of Columbia & 3 & 92.1\% & 5.4\% & 86.7\% (D) \\ \hline
    Florida & 29 & 47.9\% & 51.2\% & 3.3\% (R) \\ \hline
    Georgia & 16 & 49.5\% & 49.3\% & 0.2\% (D) \\ \hline
    Hawaii & 4 & 63.7\% & 34.3\% & 29.4\% (D) \\ \hline
    Idaho & 4 & 33.1\% & 63.8\% & 30.7\% (R) \\ \hline
    Illinois & 20 & 57.5\% & 40.5\% & 17.0\% (D) \\ \hline
    Indiana & 11 & 41.0\% & 57.0\% & 16.0\% (R) \\ \hline
    Iowa & 6 & 44.9\% & 53.1\% & 8.2\% (R) \\ \hline
    Kansas & 6 & 41.5\% & 56.1\% & 14.6\% (R) \\ \hline
    Kentucky & 8 & 36.2\% & 62.1\% & 25.9\% (R) \\ \hline
    Louisiana & 8 & 39.9\% & 58.5\% & 18.6\% (R) \\ \hline
    Maine & 4 & 53.1\% & 44.0\% & 9.1\% (D) \\ \hline
    Maryland & 10 & 65.4\% & 32.2\% & 33.2\% (D) \\ \hline
    Massachusetts & 11 & 65.6\% & 32.1\% & 33.5\% (D) \\ \hline
    Michigan & 16 & 50.6\% & 47.8\% & 2.8\% (D) \\ \hline
    Minnesota & 10 & 52.4\% & 45.3\% & 7.1\% (D) \\ \hline
    Mississippi & 6 & 41.0\% & 57.5\% & 16.5\% (R) \\ \hline
    Missouri & 10 & 41.4\% & 56.8\% & 15.4\% (R) \\ \hline
    Montana & 3 & 40.4\% & 56.7\% & 16.3\% (R) \\ \hline
    Nebraska & 5 & 39.2\% & 58.2\% & 19.0\% (R) \\ \hline
    Nevada & 6 & 50.1\% & 47.7\% & 2.4\% (D) \\ \hline
    New Hampshire & 4 & 52.8\% & 45.5\% & 7.3\% (D) \\ \hline
    New Jersey & 14 & 57.1\% & 41.3\% & 15.8\% (D) \\ \hline
    New Mexico & 5 & 54.3\% & 43.5\% & 10.8\% (D) \\ \hline
    New York & 29 & 60.9\% & 37.7\% & 23.2\% (D) \\ \hline
    North Carolina & 15 & 48.6\% & 49.9\% & 1.3\% (R) \\ \hline
    North Dakota & 3 & 31.8\% & 65.1\% & 33.3\% (R) \\ \hline
    Ohio & 18 & 45.2\% & 53.3\% & 8.1\% (R) \\ \hline
    Oklahoma & 7 & 32.3\% & 65.4\% & 33.1\% (R) \\ \hline
    Oregon & 7 & 56.5\% & 40.4\% & 16.1\% (D) \\ \hline
    Pennsylvania & 20 & 50.0\% & 48.8\% & 1.2\% (D) \\ \hline
    Rhode Island & 4 & 59.4\% & 38.6\% & 20.8\% (D) \\ \hline
    South Carolina & 9 & 43.4\% & 55.1\% & 11.7\% (R) \\ \hline
    South Dakota & 3 & 35.6\% & 61.8\% & 26.2\% (R) \\ \hline
    Tennessee & 11 & 37.4\% & 60.7\% & 23.3\% (R) \\ \hline
    Texas & 38 & 46.5\% & 52.0\% & 5.5\% (R) \\ \hline
    Utah & 6 & 37.6\% & 58.1\% & 20.5\% (R) \\ \hline
    Vermont & 3 & 66.1\% & 30.7\% & 35.4\% (D) \\ \hline
    Virginia & 13 & 54.1\% & 44.0\% & 10.1\% (D) \\ \hline
    Washington & 12 & 58.0\% & 38.8\% & 19.2\% (D) \\ \hline
    West Virginia & 5 & 29.7\% & 68.6\% & 38.9\% (R) \\ \hline
    Wisconsin & 10 & 49.5\% & 48.8\% & 0.7\% (D) \\ \hline
    Wyoming & 3 & 29.6\% & 69.9\% & 43.3\% (R) \\ \hline
\end{longtable}
\subsection{Party Affiliation By State}
Note: The following table is sorted alphabetically and was
obtained by \cite{gallup-party-affiliation} and for
Washington D.C. \cite{pew-research-party-affiliation} (unfortunately from
2014). Population numbers for residents of
age $18$ or older is from \cite{us-census-bureau-18-over}.
  \begin{longtable}{|l|c|r|r|r|}
    \hline
    State & Democrats & Republicans & Independents \\ \hline
    \hline
    Alabama & 1335208 & 1983737 & 495934\\ \hline
    Alaska & 182015 & 281297 & 88250\\ \hline
    Arizona & 2311777 & 2311777 & 1014927\\ \hline
    Arkansas & 811177 & 1112472 & 394000\\ \hline
    California & 15614967 & 9491450 & 5511165\\ \hline
    Colorado & 2114632 & 1799687 & 584898\\ \hline
    Connecticut & 1475680 & 936490 & 425677\\ \hline
    Delaware & 369692 & 269567 & 130933\\ \hline
    District of Columbia & 421634 & 63534 & 92413\\ \hline
    Florida & 7244079 & 7071601 & 2932127\\ \hline
    Georgia & 3488823 & 3407688 & 1217031\\ \hline
    Hawaii & 602642 & 323641 & 189721\\ \hline
    Idaho & 455214 & 669432 & 214218\\ \hline
    Illinois & 4926973 & 3350342 & 1576631\\ \hline
    Indiana & 1962413 & 2375553 & 826279\\ \hline
    Iowa & 1019856 & 1019856 & 388517\\ \hline
    Kansas & 796703 & 1040140 & 376221\\ \hline
    Kentucky & 1455217 & 1559161 & 450424\\ \hline
    Louisiana & 1317631 & 1602524 & 641010\\ \hline
    Maine & 547685 & 405287 & 142398\\ \hline
    Maryland & 2543936 & 1460408 & 706649\\ \hline
    Massachusetts & 3102234 & 1495720 & 941750\\ \hline
    Michigan & 3529316 & 3058740 & 1254868\\ \hline
    Minnesota & 1994779 & 1647861 & 693836\\ \hline
    Mississippi & 819924 & 1093232 & 364411\\ \hline
    Missouri & 1811400 & 2240416 & 715026\\ \hline
    Montana & 327674 & 386487 & 126029\\ \hline
    Nebraska & 612500 & 627084 & 218750\\ \hline
    Nevada & 1074383 & 907256 & 405878\\ \hline
    New Hampshire & 530140 & 397605 & 176713\\ \hline
    New Jersey & 3471806 & 2430264 & 1041542\\ \hline
    New Mexico & 778076 & 615977 & 226939\\ \hline
    New York & 8175389 & 4627579 & 2622295\\ \hline
    North Carolina & 3356821 & 3438695 & 1391853\\ \hline
    North Dakota & 174567 & 320040 & 87284\\ \hline
    Ohio & 3735543 & 4099986 & 1275551\\ \hline
    Oklahoma & 1141799 & 1382177 & 480757\\ \hline
    Oregon & 1575052 & 1273447 & 502676\\ \hline
    Pennsylvania & 4676993 & 4066950 & 1423433\\ \hline
    Rhode Island & 367592 & 307752 & 179522\\ \hline
    South Carolina & 1493886 & 1897640 & 646005\\ \hline
    South Dakota & 267023 & 340455 & 60080\\ \hline
    Tennessee & 1861693 & 2553179 & 904251\\ \hline
    Texas & 8422468 & 9070350 & 4103253\\ \hline
    Utah & 636937 & 1273873 & 363964\\ \hline
    Vermont & 280491 & 152995 & 76498\\ \hline
    Virginia & 3070349 & 2603122 & 1001201\\ \hline
    Washington & 2975916 & 2083141 & 892775\\ \hline
    West Virginia & 530055 & 701964 & 200561\\ \hline
    Wisconsin & 1959010 & 1959010 & 637817\\ \hline
    Wyoming & 111256 & 262565 & 71204\\ \hline
  \end{longtable}
\subsection{Final Results}
The following table shows the probability of a voter of a certain state
being decisive in the presidential election. The states from
the merged blocks all have the power the entire block
received in their state power column. This does not really matter
however, as they also all have $0$ or impossible for their
p voter decides state column.
\begin{center}
  \begin{longtable}[]{|l|r|r|r|}
    \hline
    State & p voter decides state & p state decisive & p voter decisive \\ \hline \hline
    Alabama & impossible & 0.01029 & impossible \\ \hline 
    Alaska & impossible & 0.00350 & impossible \\ \hline 
    Arizona & 0.000792 & 0.01919 & 1.5197e-05 \\ \hline 
    Arkansas & 0 & 0.00695 & 0 \\ \hline 
    California & impossible & 0.02699 & impossible \\ \hline 
    Colorado & 0 & 0.01029 & 0 \\ \hline 
    Connecticut & impossible & 0.00808 & impossible \\ \hline 
    Delaware & 0 & 0.00350 & 0 \\ \hline 
    District of Columbia & impossible & 0.02558 & impossible \\ \hline 
    Florida & 2.0608e-3203 & 0.08604 & 1.7732e-3204 \\ \hline 
    Georgia & 2.0009e-1630 & 0.03267 & 6.5367e-1632 \\ \hline 
    Hawaii & impossible & 0.02558 & impossible \\ \hline 
    Idaho & 0 & 0.02197 & 0 \\ \hline 
    Illinois & 0 & 0.02063 & 0 \\ \hline 
    Indiana & 0 & 0.01243 & 0 \\ \hline 
    Iowa & 0.0012801 & 0.00695 & 8.8948e-06 \\ \hline 
    Kansas & 0 & 0.00695 & 0 \\ \hline 
    Kentucky & 0 & 0.00919 & 0 \\ \hline 
    Louisiana & 0 & 0.00919 & 0 \\ \hline 
    Maine & 0 & 0.00466 & 0 \\ \hline 
    Maryland & impossible & 0.02558 & impossible \\ \hline 
    Massachusetts & impossible & 0.02558 & impossible \\ \hline 
    Michigan & 3.6452e-4951 & 0.03267 & 0 \\ \hline 
    Minnesota & 0 & 0.01689 & 0 \\ \hline 
    Mississippi & 0 & 0.00695 & 0 \\ \hline 
    Missouri & 0 & 0.01137 & 0 \\ \hline 
    Montana & 0 & 0.00350 & 0 \\ \hline 
    Nebraska & 6.9899e-296 & 0.00581 & 4.06e-298 \\ \hline 
    Nevada & 3.6452e-4951 & 0.00889 & 0 \\ \hline 
    New Hampshire & 0 & 0.00554 & 0 \\ \hline 
    New Jersey & 0 & 0.01547 & 0 \\ \hline 
    New Mexico & 0 & 0.00581 & 0 \\ \hline 
    New York & impossible & 0.02507 & impossible \\ \hline 
    North Carolina & 1.3025e-1522 & 0.02969 & 3.8675e-1524 \\ \hline 
    North Dakota & impossible & 0.02197 & impossible \\ \hline 
    Ohio & 0 & 0.03906 & 0 \\ \hline 
    Oklahoma & 0 & 0.02197 & 0 \\ \hline 
    Oregon & 0 & 0.00808 & 0 \\ \hline 
    Pennsylvania & 3.6452e-4951 & 0.04608 & 0 \\ \hline 
    Rhode Island & 3.6452e-4951 & 0.00466 & 0 \\ \hline 
    South Carolina & 0 & 0.01029 & 0 \\ \hline 
    South Dakota & impossible & 0.00350 & impossible \\ \hline 
    Tennessee & 0 & 0.01243 & 0 \\ \hline 
    Texas & 0 & 0.14121 & 0 \\ \hline 
    Utah & impossible & 0.00695 & impossible \\ \hline 
    Vermont & impossible & 0.02558 & impossible \\ \hline 
    Virginia & 3.6452e-4951 & 0.01448 & 0 \\ \hline 
    Washington & 0 & 0.01347 & 0 \\ \hline 
    West Virginia & 0 & 0.02197 & 0 \\ \hline 
    Wisconsin & 0.00099906 & 0.01689 & 1.6874e-05 \\ \hline 
    Wyoming & impossible & 0.02197 & impossible \\ \hline
  \end{longtable}
\end{center}
\subsection{Bazhaf Calculator}
This program utilizes the highly parallel nature of the Banzhaf Index calculations.
In order to do this MPI, a library that enables communication between
processes \cite{mpi}, was used. The c program can be compiled like this: \verb+mpicc banzhaf.c -o banzhaf+.
It can be run like this: \verb+mpirun -np 8 ./banzhaf+. The following
text is stored in a file named \verb+banzhaf.c+.
\begin{verbatim}
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// specify experiment
int requiredVotes = 270;
int votes[] = {... specify votes here ...};
int noOfVoters = 43;

void saveResults(unsigned long decisiveCases[]) {
    FILE *f = fopen("results.csv", "w");
    char strDecCase[64] = "";
    for(int i = 0; i < noOfVoters; i++) {
        sprintf(strDecCase, "%ld, ", decisiveCases[i]);
        fputs(strDecCase, f);
    }
    fclose(f);
}

int main(int argc, char** argv) {

    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // get MPI config
    int noOfThreads, threadId;
    MPI_Comm_size(MPI_COMM_WORLD, &noOfThreads);
    MPI_Comm_rank(MPI_COMM_WORLD, &threadId);

    // calculate total amount of cases
    unsigned long noOfConfigs = 1;
    for (int i = 0; i < noOfVoters; i++)
    {
        noOfConfigs *= 2;
    }
    
    // declare vars
    unsigned long config = threadId * (noOfConfigs / noOfThreads);
    unsigned long maxConfig = (threadId+1) * (noOfConfigs / noOfThreads);
    if(threadId == noOfThreads-1) { maxConfig = noOfConfigs + 1; }
    unsigned long localDecisiveCases[noOfVoters];
    for(int i = 0; i < noOfVoters; i++) { localDecisiveCases[i] = 0; }
    int yeas = 0;

    // calculate banzhaf
    while (config < maxConfig)
    {
        yeas = 0;
        int selector = config;
        for (int i = 0; i < noOfVoters; i++)
        {
            yeas += votes[i] * (selector & 1);
            selector = selector >> 1;
        }

        if (yeas >= requiredVotes)
        {
            int selector = config;
            int tmp;
            for (int i = 0; i < noOfVoters; i++)
            {
                if (selector & 1)
                {
                    tmp = yeas - votes[i];
                    localDecisiveCases[i] += (tmp < requiredVotes) ? 1 : 0;
                }
                selector = selector >> 1;
            }
        }

        config++;
    }

    // have root process collect results
    if(threadId == 0) {

        unsigned long tmp[noOfVoters];

        // get the results
        for(int i = 1; i < noOfThreads; i++) {
            MPI_Recv(tmp, noOfVoters, MPI_LONG, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            for(int j = 0; j < noOfVoters; j++) {
                localDecisiveCases[j] += tmp[j];
            }
        }

        // printout results
        for (int i = 0; i < noOfVoters; i++)
        {
            printf("%li, ", localDecisiveCases[i]);
        }
        printf("\n");

        saveResults(localDecisiveCases);

    } else {
        // send results to root process
        MPI_Send(localDecisiveCases, noOfVoters, MPI_LONG, 0, 0, MPI_COMM_WORLD);
    }

    // Finalize the MPI environment.
    MPI_Finalize();
}
\end{verbatim}
\subsection{Vote configuration probability function Calculator}
The following c program calculates the probability for
a state election ending in a tie (and hence voter $v$ being decisive).
\begin{verbatim}
#include <stdio.h>
#include <stdlib.h>

long double p(int n, int k, double p_r, double p_d) {

    // handle impossible case
    if(k < 0 || k > n) {
        return 1.0 / 0.0;
    }

    long double result = 1.0;

    int maxK = n-k > k ? n-k : k;
    int minK = n - maxK;

    int rToGo = k;
    int dToGo = n - k;

    for(int i = n; i > maxK; i--) {
        result *= i;
        while(result > 1 && minK > 0) {
            result /= minK;
            minK--;
        }
        while(result > 1 && rToGo > 0) {
            result *= p_r;
            rToGo--;
        }
        while(result > 1 && dToGo > 0) {
            result *= p_d;
            dToGo--;
        }
    }
    while(minK > 0) {
        result /= minK;
        minK--;
    }
    while(rToGo > 0) {
        result *= p_r;
        rToGo--;
    }
    while(dToGo > 0) {
        result *= p_d;
        dToGo--;
    }

    return result;
}

int main() {
    char *states[51] = { ... state names ... };
    double decisiveStateProb[] = { ... banzhaf power for each state ... }
    int republicans[] = { ... number of republicans in states ... };
    int independents[] = {... number of independents in states ...};
    int democrats[] = {... number of democrats in states ...};

    int cases = sizeof(republicans) / sizeof(int);
    double p_r, p_d;
    int k, voters;

    for(int i = 0; i < cases; i++) {

        // calculate total number of voters
        voters = democrats[i] + republicans[i] + independents[i];
        if(voters % 2) {
            independents[i]--;
            voters--;
        }

        // calculate probabilities
        p_r = ((double) republicans[i]) / (republicans[i] + democrats[i]);
        p_d = 1.0 - p_r;

        // calculate needed independents for tie
        k = voters / 2;
        k -= republicans[i];

        // print result
        printf("%s & %.5Lg & %.5f & %.5Lg \\\\ \\hline \n",
        states[i],
        p(independents[i], k, p_r, p_d),
        decisiveStateProb[i],
        p(independents[i], k, p_r, p_d) * decisiveStateProb[i]);
    }
}
\end{verbatim}
\end{document}