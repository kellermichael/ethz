#include <math.h>
#include <stdio.h>

int requiredVotes = 270;
// DON'T FORGET TERMINATING 0!!
int votes[] = {9, 3, 11, 6, 55, 9, 7, 3, 3, 29, 16, 4, 4, 20, 11, 6, 6, 8, 8, 4, 10, 11, 16, 10, 6, 10, 3, 5, 6, 4, 14, 5, 29, 15, 3, 18, 7, 7, 20, 4, 9, 3, 11, 38, 6, 3, 13, 12, 5, 10, 3, 0};
// int votes[] = {44, 32, 20, 8, 4};
int decisiveCases[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
int numberOfVoters = 50;

int main()
{

    // declare vars
    unsigned long config = 0;
    int yeas = 0;

    // calculate total amount of cases
    unsigned long noOfConfigs = 1;
    for (int i = 0; i < numberOfVoters; i++)
    {
        noOfConfigs *= 2;
    }

    while (config <= noOfConfigs)
    {
        if (yeas >= requiredVotes)
        {
            int selector = config;
            for (int i = 0; i < numberOfVoters; i++)
            {
                if (selector & 1)
                {
                    int tmp = yeas - votes[i];
                    decisiveCases[i] += (tmp < requiredVotes) ? 1 : 0;
                }
                selector = selector >> 1;
            }
        }

        config++;
        yeas = 0;
        int selector = config;
        for (int i = 0; i < numberOfVoters; i++)
        {
            yeas += votes[i] * (selector & 1);
            selector = selector >> 1;
        }

        if (config % 100000000 == 0)
        {
            printf("Here\n");
        }
    }

    // printout results
    for (int i = 0; i < numberOfVoters; i++)
    {
        printf("%i, ", decisiveCases[i]);
    }

    return 0;
}
