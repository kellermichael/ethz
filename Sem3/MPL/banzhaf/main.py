# https://www.britannica.com/topic/United-States-Electoral-College-Votes-by-State-1787124

# specify voting game
requiredVotes = 270
# votes = [9, 3, 11, 6, 55, 9, 7, 3, 3, 29, 16, 4, 4, 20, 11, 6, 6, 8, 8, 4, 10, 11, 16, 10, 6, 10, 3, 5, 6, 4, 14, 5, 29, 15, 3, 18, 7, 7, 20, 4, 9, 3, 11, 38, 6, 3, 13, 12, 5, 10, 3]
# DON'T FORGET TERMINATING 0!!
votes = [9, 3, 11, 6, 55, 9, 7, 3, 3, 29, 16, 4, 4, 20, 11, 6, 6, 8, 8, 4, 10, 11, 16, 10, 6, 10, 3, 5, 0]
# result variables
decisiveCases = [0] * len(votes)

# computing variables
noOfVoters = len(votes)

def main():
    yea = 0

    # iterate over all possible configurations
    confNumb = 0
    configuration = [0] * (len(votes) + 1)
    while configuration[noOfVoters] == 0:


        # calculate decisiveness of configuration
        if(yea >= requiredVotes):
            for voter in range(noOfVoters):
                if configuration[voter] == 1:
                    decisiveCases[voter] += 1 if yea - votes[voter] < requiredVotes else 0

        # calculate new configuration
        configuration[0] += 1
        yea += votes[0]
        i = 0
        while(configuration[i] == 2):
            configuration[i] = 0
            yea -= votes[i]
            configuration[i + 1] += 1
            yea += votes[i + 1]
            i += 1
        
        # save when necessary
        if confNumb % 1_000_000 == 0:
            print("+1/32")
            f = open("backup.txt", "a")
            f.write(str(decisiveCases) + "###" + str(configuration) + "\n")
            f.close()
        confNumb += 1

    # save resultsbanzhaf
    print(decisiveCases)

main()