#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// specify experiment
int requiredVotes = 270;
//             Dem Block, Rep Block, California, Arkansas, South Dakota, Kentucky, Alabama, Tennessee, New York, Rhode Island, Utah, Connecticut, Washington, Delaware, Nebraska, Louisiana, Illinois, Mississippi, Montana, Oregon, Indiana, New Jersey, Missouri, Kansas, Colorado, South Carolina, New Mexico, Virginia, Alaska, Maine, Iowa, Ohio, New Hampshire, Minnesota, Texas , Florida, Michigan, Nevada, North Carolina, Pennsylvania, Wisconsin, Arizona, Georgia 
int votes[] = {31       , 22       , 55        , 6       , 3           , 8       , 9      , 11       , 29      , 4           , 6   , 7          , 12        , 3       , 5       , 8        , 20      , 6          , 3      , 7     , 11     , 14        , 10      , 6     , 9       , 9             , 5         , 13      , 3     , 4    , 6   , 18  , 4            , 10       , 38    , 29     , 16      , 6     , 15            , 20          , 10       , 11     , 16     };
int noOfVoters = 43;

// int requiredVotes = 16;
// int votes[] = {9, 9, 7, 3, 1, 1};
// int noOfVoters = 6;

// compile like this: mpicc banzhaf.c -o banzhaf
// run like this: mpirun -np 8 ./banzhaf

void saveResults(unsigned long decisiveCases[]) {
    FILE *f = fopen("results.csv", "w");
    char strDecCase[64] = "";
    for(int i = 0; i < noOfVoters; i++) {
        sprintf(strDecCase, "%ld, ", decisiveCases[i]);
        fputs(strDecCase, f);
    }
    fclose(f);
}

int main(int argc, char** argv) {

    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // get MPI config
    int noOfThreads, threadId;
    MPI_Comm_size(MPI_COMM_WORLD, &noOfThreads);
    MPI_Comm_rank(MPI_COMM_WORLD, &threadId);

    // calculate total amount of cases
    unsigned long noOfConfigs = 1;
    for (int i = 0; i < noOfVoters; i++)
    {
        noOfConfigs *= 2;
    }
    
    // declare vars
    unsigned long config = threadId * (noOfConfigs / noOfThreads);
    unsigned long maxConfig = (threadId+1) * (noOfConfigs / noOfThreads);
    if(threadId == noOfThreads-1) { maxConfig = noOfConfigs + 1; }
    unsigned long localDecisiveCases[noOfVoters];
    for(int i = 0; i < noOfVoters; i++) { localDecisiveCases[i] = 0; }
    int yeas = 0;

    // calculate banzhaf
    while (config < maxConfig)
    {
        yeas = 0;
        int selector = config;
        for (int i = 0; i < noOfVoters; i++)
        {
            yeas += votes[i] * (selector & 1);
            selector = selector >> 1;
        }

        if (config % 10000000000 == 0)
        {
            printf("Here %d\n", threadId);
        }

        if (yeas >= requiredVotes)
        {
            int selector = config;
            int tmp;
            for (int i = 0; i < noOfVoters; i++)
            {
                if (selector & 1)
                {
                    tmp = yeas - votes[i];
                    localDecisiveCases[i] += (tmp < requiredVotes) ? 1 : 0;
                }
                selector = selector >> 1;
            }
        }

        config++;
    }

    // have root process collect results
    if(threadId == 0) {

        unsigned long tmp[noOfVoters];

        // get the results
        for(int i = 1; i < noOfThreads; i++) {
            MPI_Recv(tmp, noOfVoters, MPI_LONG, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            for(int j = 0; j < noOfVoters; j++) {
                localDecisiveCases[j] += tmp[j];
            }
        }

        // printout results
        for (int i = 0; i < noOfVoters; i++)
        {
            printf("%li, ", localDecisiveCases[i]);
        }
        printf("\n");

        saveResults(localDecisiveCases);

    } else {
        // send results to root process
        MPI_Send(localDecisiveCases, noOfVoters, MPI_LONG, 0, 0, MPI_COMM_WORLD);
    }

    // Finalize the MPI environment.
    MPI_Finalize();
}