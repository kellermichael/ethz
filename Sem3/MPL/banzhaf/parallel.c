#include <pthread.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// compile: gcc -O3 ./parallel.c -o parallel -lpthread

struct args
{
  unsigned long noOfConfigs;
  int thread_id;
};

// specify voting game
#define NVOTERS 40
#define NTHREADS 8
#define REQVOTES 6
int votes[] = {4, 3, 2, 1, 0, 0, 0, 0, 0, 0, 4, 3, 2, 1, 0, 0, 0, 0, 0, 0, 4, 3, 2, 1, 0, 0, 0, 0, 0, 0, 4, 3, 2, 1, 0, 0, 0, 0, 0, 0};

void saveResults(int thread_id, unsigned long decisiveCases[], unsigned long config)
{
  // build filename
  char filename[15] = "./backup_";
  char str[12] = "";
  sprintf(str, "%d", thread_id);
  strcat(filename, str);
  strcat(filename, ".txt");

  // write decisive cases
  int written = 0;
  FILE *f = fopen(filename, "a");
  char strDecCase[64] = "";
  for(int i = 0; i < NVOTERS; i++) {
    sprintf(strDecCase, "%ld, ", decisiveCases[i]);
    fputs(strDecCase, f);
  }
  fputs("###", f);
  char strConfig[64] = "";
  sprintf(strConfig, "%ld", config);
  fputs(strConfig, f);
  fputs("\n", f);
  fclose(f);
}

unsigned long getSavedConfig(int thread_id) {
  // build filename
  char filename[15] = "./backup_";
  char str[12] = "";
  sprintf(str, "%d", thread_id);
  strcat(filename, str);
  strcat(filename, ".txt");

  // open file
  FILE *f = fopen(filename, "r");

}


void *compute(void *input)
{
  // get thread arguments
  int thread_id = ((struct args *)input)->thread_id;
  unsigned long noOfConfigs = ((struct args *)input)->noOfConfigs;

  // declare vars
  unsigned long config = thread_id * noOfConfigs;
  int yeas = 0;
  unsigned long decisiveCases[NVOTERS];
  for (int i = 0; i < NVOTERS; i++)
  {
    decisiveCases[i] = 0;
  }

  // restore possible previous state


  // actual calculation
  for (; config < (thread_id + 1) * noOfConfigs; config++)
  {

    // calculate yeas
    yeas = 0;
    int selector = config;
    for (int i = 0; i < NVOTERS; i++)
    {
      yeas += votes[i] * (selector & 1);
      selector = selector >> 1;
    }

    // determine who is decisive
    if (yeas >= REQVOTES)
    {
      int selector = config;
      for (int i = 0; i < NVOTERS; i++)
      {
        if (selector & 1)
        {
          int tmp = yeas - votes[i];
          decisiveCases[i] += (tmp < REQVOTES) ? 1 : 0;
        }
        selector = selector >> 1;
      }
    }

    if(config % 100000000 == 0) {
      saveResults(thread_id, decisiveCases, config);
    }
  }
  saveResults(thread_id, decisiveCases, config);
}

int main()
{
  // calculate total amount of cases
  unsigned long noOfConfigs = 1;
  for (int i = 0; i < NVOTERS; i++)
  {
    noOfConfigs *= 2;
  }
  noOfConfigs /= NTHREADS;

  // create threads
  pthread_t threads[NTHREADS];
  struct args *argArray[NTHREADS];

  for (int i = 0; i < NTHREADS; i++)
  {
    // build arg
    struct args *arg = (struct args *)malloc(sizeof(struct args));
    arg->thread_id = i;
    arg->noOfConfigs = noOfConfigs;

    // start thread
    pthread_create(&threads[i], NULL, compute, (void *)arg);

    // remember malloced stuff
    argArray[i] = arg;
  }

  for (int i = 0; i < NTHREADS; i++)
  {
    pthread_join(threads[i], NULL);
    free(argArray[i]);
  }
  return 0;
}