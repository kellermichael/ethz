#include <stdio.h>
#include <stdlib.h>

double p(int n, int k, double p_r, double p_d) {

    // declare dp array
    double* dp = malloc((n-k+1) * sizeof(double));

    // handle base case
    dp[0] = 1.0;
    for(int i = 1; i < n-k+1; i++) {
        dp[i] = dp[i-1] * p_d;
    }

    // build up solution
    for(int i = 1; i <= k; i++) {
        dp[0] = dp[0] * p_r;
        for(int j = 1; j <= n-k; j++) {
            dp[j] = dp[j] * p_r + dp[j-1] * p_d;
        }
    }

    // memorize result
    double result = dp[n-k];

    // cleanup
    free(dp);

    return result;
}

int main() {
    int republicans[] = {1, 2, 3};
    int independents[] = {2, 2, 2};
    int democrats[] = {3, 2, 1};

    int n = 200000, k = 100000;

    double p_r = 0.5;
    double p_d = 0.5;


    printf("Hello World %.17g\n", p(n, k, p_r, p_d));
}