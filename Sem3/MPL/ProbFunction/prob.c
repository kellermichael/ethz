#include <stdio.h>
#include <stdlib.h>
#include <float.h>

long double p(int n, int k, double p_r, double p_d) {

    // handle impossible case
    if(k < 0 || k > n) {
        return 1.0 / 0.0;
    }

    long double result = 1.0;

    int maxK = n-k > k ? n-k : k;
    int minK = n - maxK;

    int rToGo = k;
    int dToGo = n - k;

    for(int i = n; i > maxK; i--) {
        result *= i;
        while(result > 1 && minK > 0) {
            result /= minK;
            minK--;
        }
        while(result > 1 && rToGo > 0) {
            result *= p_r;
            rToGo--;
        }
        while(result > 1 && dToGo > 0) {
            result *= p_d;
            dToGo--;
        }
    }
    while(minK > 0) {
        result /= minK;
        minK--;
    }
    while(rToGo > 0) {
        result *= p_r;
        rToGo--;
    }
    while(dToGo > 0) {
        result *= p_d;
        dToGo--;
    }

    return result;
}

int main() {
    char *states[51] = {"Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "District of Columbia", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"};
    int republicans[] = {1983737, 281297, 2311777, 1112472, 9491450, 1799687, 936490, 269567, 63534, 7071601, 3407688, 323641, 669432, 3350342, 2375553, 1019856, 1040140, 1559161, 1602524, 405287, 1460408, 1495720, 3058740, 1647861, 1093232, 2240416, 386487, 627084, 907256, 397605, 2430264, 615977, 4627579, 3438695, 320040, 4099986, 1382177, 1273447, 4066950, 307752, 1897640, 340455, 2553179, 9070350, 1273873, 152995, 2603122, 2083141, 701964, 1959010, 262565};
    int independents[] = {495934, 88250, 1014927, 394000, 5511165, 584898, 425677, 130933, 92413, 2932127, 1217031, 189721, 214218, 1576631, 826279, 388517, 376221, 450424, 641010, 142398, 706649, 941750, 1254868, 693836, 364411, 715026, 126029, 218750, 405878, 176713, 1041542, 226939, 2622295, 1391853, 87284, 1275551, 480757, 502676, 1423433, 179522, 646005, 60080, 904251, 4103253, 363964, 76498, 1001201, 892775, 200561, 637817, 71204};
    int democrats[] = {1335208, 182015, 2311777, 811177, 15614967, 2114632, 1475680, 369692, 421634, 7244079, 3488823, 602642, 455214, 4926973, 1962413, 1019856, 796703, 1455217, 1317631, 547685, 2543936, 3102234, 3529316, 1994779, 819924, 1811400, 327674, 612500, 1074383, 530140, 3471806, 778076, 8175389, 3356821, 174567, 3735543, 1141799, 1575052, 4676993, 367592, 1493886, 267023, 1861693, 8422468, 636937, 280491, 3070349, 2975916, 530055, 1959010, 111256};

    double decisiveStateProb[] = {0.010291074920986,0.003500396574264,0.019188276930461,0.006948641103042,0.026993479924974,0.010291074920986,0.00807725113711,0.003500396574264,0.02557562078042,0.086040113592692,0.032668206660584,0.02557562078042,0.021972793611847,0.020630924117694,0.012432757740714,0.006948641103042,0.006948641103042,0.009192131061197,0.009192131061197,0.004658189350069,0.02557562078042,0.02557562078042,0.032668206660584,0.01689011059541,0.006948641103042,0.011372003487198,0.003500396574264,0.005808316043501,0.008887272086041,0.005541889955396,0.015470719798888,0.005808316043501,0.025066846360889,0.029693557838029,0.021972793611847,0.039063101601681,0.021972793611847,0.00807725113711,0.046077244685085,0.004658189350069,0.010291074920986,0.003500396574264,0.012432757740714,0.141205293198958,0.006948641103042,0.02557562078042,0.014484304641302,0.013470958066986,0.021972793611847,0.01689011059541,0.021972793611847};


    int cases = sizeof(republicans) / sizeof(int);
    double p_r, p_d;
    int k, voters;

    for(int i = 0; i < cases; i++) {

        // calculate total number of voters
        voters = democrats[i] + republicans[i] + independents[i];
        if(voters % 2) {
            independents[i]--;
            voters--;
        }

        // calculate probabilities
        p_r = ((double) republicans[i]) / (republicans[i] + democrats[i]);
        p_d = 1.0 - p_r;

        // calculate needed independents for tie
        k = voters / 2;
        k -= republicans[i];

        // print result
        printf("%s & %.5Lg & %.5f & %.5Lg \\\\ \\hline \n", states[i], p(independents[i], k, p_r, p_d),
        decisiveStateProb[i], p(independents[i], k, p_r, p_d) * decisiveStateProb[i]);
    }
}