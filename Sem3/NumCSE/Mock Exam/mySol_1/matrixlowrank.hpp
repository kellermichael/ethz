#include <Eigen/Dense>
#include <iostream>

using namespace Eigen;

/* SAM_LISTING_BEGIN_0 */
class MatrixLowRank {
public:
  MatrixLowRank(unsigned int m, unsigned int n, unsigned int r);
  MatrixLowRank(const Eigen::MatrixXd &A, const Eigen::MatrixXd &B);

  Eigen::Index rows() const { return _m; };
  Eigen::Index cols() const { return _n; };
  Eigen::Index rank() const { return _r; };

  Eigen::MatrixXd operator*(const Eigen::MatrixXd &) const;
  MatrixLowRank &operator*=(const Eigen::MatrixXd &);
  MatrixLowRank &addTo(const MatrixLowRank &, double rtol = 1E-6,
                       double atol = 1E-8);
  Eigen::MatrixXd getA() const { return _A; }
  Eigen::MatrixXd getB() const { return _B; }
  unsigned get_m() const { return _m; }
  unsigned get_n() const { return _n; }
  unsigned get_r() const { return _r; }

private:
  unsigned int _m;    // no. of rows
  unsigned int _n;    // no. of columns
  unsigned int _r;    // maximal rank, =1 for zero matrix
  Eigen::MatrixXd _A; // factor matrix A
  Eigen::MatrixXd _B; // factor matrix B
};

/* SAM_LISTING_END_0 */

/* SAM_LISTING_BEGIN_1 */
MatrixLowRank::MatrixLowRank(unsigned int m, unsigned int n, unsigned int r)
    : _m(m), _n(n), _r(r) {
  // Some abritrary choice: not really meaningful
  _A = Eigen::MatrixXd::Identity(m, r);
  _B = Eigen::MatrixXd::Identity(n, r);
}
/* SAM_LISTING_END_1 */

/* SAM_LISTING_BEGIN_2 */
MatrixLowRank::MatrixLowRank(const Eigen::MatrixXd &A, const Eigen::MatrixXd &B)
    : _m(A.rows()), _n(B.rows()), _r(A.cols()), _A(A), _B(B) {
  assert(_r == B.cols() &&
         "No. of columns in A must be equal to no. of columns in B!");
}
/* SAM_LISTING_END_2 */

/* SAM_LISTING_BEGIN_3 */
Eigen::MatrixXd MatrixLowRank::operator*(const Eigen::MatrixXd &X) const {
  MatrixXd MX; // will contain the result.
  // TO DO: (0-1.b)
  // START
  MX = _A * (_B.transpose() * X); // time complexity is r * k * max(m, n) => k * max(m, n) (as r is constant)
  // END
  return MX;
}
/* SAM_LISTING_END_3 */

/* SAM_LISTING_BEGIN_4 */
MatrixLowRank &MatrixLowRank::operator*=(const Eigen::MatrixXd &X) {
  // TO DO: (0-1.d)
  // START
  _B = X.transpose() * _B;
  _n = _B.rows();
  // END
  return *this;
}
/* SAM_LISTING_END_4 */

/* SAM_LISTING_BEGIN_5 */
MatrixLowRank &MatrixLowRank::addTo(const MatrixLowRank &X, double rtol,
                                    double atol) {
  assert(_m == X._m && _n == X._n && "Matrices must have the same shape!");
  assert((rank() + X.rank() <= std::min(_n, _m)) && "Sum of ranks too large!");
  assert(rtol > 0 && atol > 0 && "Tolerances must be positive!");
  std::cout << "rtol, atol = " << rtol << " ,  " << atol << std::endl;
  // TO DO: (0-1.e)
  // We have this = _A*_B.transpose() and
  // X = X._A*X._B.transpose()
  // START
  
  // compute the precise sum
  MatrixXd preciseSum = _A*_B.transpose() + X._A*X._B.transpose();
  
  // find svd
  Eigen::JacobiSVD<MatrixXd>svd(preciseSum, Eigen::ComputeThinU | Eigen::ComputeThinV);
  
  // singular values
  VectorXd sv = svd.singularValues();
  
  // find rank
  unsigned int r = svd.rank();
  
  // find tolerances
  rtol = rtol * (preciseSum).norm();
  
  // Assign Blocks
  _A = svd.matrixU().leftCols(0);
  _B = svd.matrixV().leftCols(0);
  
  MatrixXd prevSum = _A * _B.transpose();
  
  // multiply singular values to A
  for(int precision = 1; precision < r; precision++) {
    
    // Assign Blocks
    _A.conservativeResize(_A.rows(), _A.cols() + 1);
    _B.conservativeResize(_B.rows(), _B.cols() + 1);
    _A.col(precision - 1) = svd.matrixU().col(precision - 1);
    _B.col(precision - 1) = svd.matrixV().col(precision - 1);
  
    for(int i = 0; i < _m; i++) {
      _A(i, precision - 1) = _A(i, precision - 1) * sv(precision - 1);
    }
    
    // test to see if close enough
    prevSum += _A.col(precision - 1) * _B.col(precision - 1).transpose();
    double score = (preciseSum - (prevSum)).norm();
    if(score <= rtol || score <= atol) {
      // update dimensions
      _m = _A.rows();
      _r = precision;
      _n = _B.rows();
      break;
    }
  }
  
  // END
  return *this;
}
/* SAM_LISTING_END_5 */
