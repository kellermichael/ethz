#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include "matrixlowrank.hpp"
#include <Eigen/Dense>
#include <vector>

TEST_SUITE("Q1") {
  TEST_CASE("operator*()" *
            doctest::description(
                "Testing result of operator* (not testing efficiency)")) {
    unsigned m = 10, n = 12, r = 5, k = 7;
    Eigen::MatrixXd A = Eigen::MatrixXd::Random(m, r);
    Eigen::MatrixXd B = Eigen::MatrixXd::Random(n, r);
    Eigen::MatrixXd C = Eigen::MatrixXd::Random(n, k);
    Eigen::MatrixXd M_full = A * B.transpose();

    MatrixLowRank M(A, B);
    Eigen::MatrixXd MC = M * C;
    Eigen::MatrixXd M_full_C = M_full * C;

    REQUIRE(MC.rows() == m);
    REQUIRE(MC.cols() == k);
    CHECK((MC - M_full_C).norm() / MC.norm() ==
          doctest::Approx(0.).epsilon(1e-10));
  }

  TEST_CASE("operator*=()" *
            doctest::description(
                "Testing result of operator*= (not testing efficiency)")) {
    unsigned m = 10, n = 12, r = 5, k = 7;
    Eigen::MatrixXd A = Eigen::MatrixXd::Random(m, r);
    Eigen::MatrixXd B = Eigen::MatrixXd::Random(n, r);
    Eigen::MatrixXd C = Eigen::MatrixXd::Random(n, k);

    MatrixLowRank M(A, B);
    M *= C;

    unsigned _m = M.get_m();
    unsigned _n = M.get_n();

    Eigen::MatrixXd MC_A = M.getA();
    Eigen::MatrixXd MC_B = M.getB();
    Eigen::MatrixXd sol_MC_B = C.transpose() * B;

    REQUIRE(_m == m);
    REQUIRE(_n == k);
    CHECK((MC_A - A).norm() / A.norm() == doctest::Approx(0.).epsilon(1e-10));
    CHECK((MC_B - sol_MC_B).norm() / A.norm() ==
          doctest::Approx(0.).epsilon(1e-10));
  }

  TEST_CASE("addTo()" *
            doctest::description("Testing result of addTo() for dimensions and "
                                 "tolerances (Not testing efficiency)")) {
    unsigned m = 5;
    Eigen::MatrixXd I = Eigen::MatrixXd::Identity(m, m);
    double atol = 1e-5, rtol = 1e-7;
    Eigen::MatrixXd A = Eigen::MatrixXd::Constant(m, 2, 0);
    A(0, 0) = 1;
    A(1, 1) = sqrt(atol / 2);
    A *= 1 / sqrt(2);

    MatrixLowRank M(A, A);
    MatrixLowRank X(A, A);
    M.addTo(X, atol, rtol);

    Eigen::MatrixXd Y_exact = 2 * A * A.transpose();
    Eigen::MatrixXd R = M.getA() * M.getB().transpose();

    REQUIRE(M.getA().cols() == 1);
    REQUIRE(M.getB().cols() == 1);
    REQUIRE(M.getA().rows() == m);
    REQUIRE(M.getB().rows() == m);
    CHECK(M.get_r() == 1);
    CHECK((Y_exact - R).norm() < atol);
  }
}
