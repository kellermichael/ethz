#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest.h"
#include "symrank1.hpp"
#include <Eigen/Dense>
#include <vector>

TEST_SUITE("Q3") {
  TEST_CASE("symRankOneBestApproxSym" *
            doctest::description("Testing the vector output")) {
    double r = rand();
    unsigned n = 5;
    Eigen::MatrixXd M = r * Eigen::MatrixXd::Identity(n, n);
    Eigen::VectorXd z = symRankOneBestApproxSym(M);
    Eigen::VectorXd z_sol = Eigen::VectorXd::Constant(n, 0);
    z_sol(0) = sqrt(r);

    CHECK((z - z_sol).norm() == doctest::Approx(0.).epsilon(1e-6));
  }

  TEST_CASE("computeKronProdVecMult()" *
            doctest::description("Testing output (Not testing efficiency)")) {
    unsigned n = 5;
    double r = rand() % 10;
    Eigen::VectorXd v = Eigen::VectorXd::Constant(n, r);
    Eigen::VectorXd bvec = Eigen::VectorXd::Random(n);
    Eigen::VectorXd b(n * n);
    for (unsigned i = 0; i < n; ++i)
      b.segment(i * n, n) = bvec;

    Eigen::VectorXd result = computeKronProdVecMult(v, b);
    Eigen::VectorXd sol =
        n * r * bvec + Eigen::VectorXd::Constant(n, v.dot(bvec));

    CHECK((result - sol).norm() == doctest::Approx(0.).epsilon(1e-6));
  }
  
  TEST_CASE("symmRankOneApprox()" *
            doctest::description("Testing output (Not testing efficiency)")) {
    unsigned n = 5;
    double r = rand() % 10;
    Eigen::MatrixXd random = Eigen::MatrixXd::Random(n, n);
    Eigen::MatrixXd lower_triang = random.triangularView<Lower>();

    Eigen::MatrixXd M = Eigen::MatrixXd::Constant(n, n, 0);
    M += lower_triang - lower_triang.transpose();
    Eigen::VectorXd diag = Eigen::VectorXd::Constant(n, r);
    M.diagonal() = diag;

    Eigen::VectorXd result = symmRankOneApprox(M);
    Eigen::VectorXd sol = Eigen::VectorXd::Constant(n, 0);
    sol(0) = sqrt(r);

    CHECK((result - sol).norm() == doctest::Approx(0.).epsilon(1e-6));
  }

}
