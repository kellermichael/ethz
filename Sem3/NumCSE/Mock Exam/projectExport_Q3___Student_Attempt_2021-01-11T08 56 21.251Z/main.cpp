#include "symrank1.hpp"
#include <Eigen/Dense>
#include <iostream>

int main() {

  MatrixXd M(3, 3);
  double eps = 1e-4;
  M << 1 + eps, 2, 1, 2, 1 + eps, 1, 1, 1, 1 + eps;
  std::cout << "Matrix M = \n" << M << std::endl << std::endl;

  // run symRankOneBestApproxSym()
  VectorXd z = symRankOneBestApproxSym(M);
  std::cout << "The output of symRankOneBestApproxSym(M) is:\n"
            << z.transpose() << std::endl
            << std::endl;

  VectorXd v(3);
  v << 2, 0, 1;
  std::cout << "Vector v = \n" << v.transpose() << std::endl << std::endl;

  VectorXd b(9);
  b << 1, 2, 3, 2, 3, 4, 3, 4, 5;
  std::cout << "Vector b = \n" << b.transpose() << std::endl << std::endl;

  // run computeKronProdVecMult()
  VectorXd w = computeKronProdVecMult(v, b);
  std::cout << "The output of computeKronProdVecMult(v,b) is:\n"
            << w.transpose() << std::endl
            << std::endl;

  // run symRankOneApprox()
  MatrixXd MM(3, 3);
  MM << 1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 2.0, 1.0, 0.0;
  std::cout << "Matrix MM = \n" << MM << std::endl << std::endl;

  VectorXd zz = symmRankOneApprox(MM);
  std::cout << "The output of symmRankOneApprox(MM) is:\n"
            << zz.transpose() << std::endl
            << std::endl;

  return 0;
}
