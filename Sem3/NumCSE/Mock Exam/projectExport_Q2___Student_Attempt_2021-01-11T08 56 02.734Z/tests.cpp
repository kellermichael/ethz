#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "contour.hpp"
#include "doctest.h"
#include <Eigen/Dense>
#include <vector>

TEST_SUITE("Q2") {
  TEST_CASE("computeIsolinePoints()" *
            doctest::description("Testing for gradF = x")) {

    auto gradF = [](const Eigen::Vector2d &x) {
      // Gradient of 0.5 * (x(0)^2 + x(1)^2)
      return x;
    };

    double T = M_PI / 2;

    Eigen::Vector2d y0 = Eigen::VectorXd::Random(2);
    y0 /= y0.norm();

    Eigen::MatrixXd States = computeIsolinePoints(gradF, y0, T);
    Eigen::Vector2d y_T = States.col(States.cols() - 1);

    CHECK((y_T - y0).norm() == doctest::Approx(sqrt(2)).epsilon(1e-5));
  }

  TEST_CASE("crookedEgg()" * doctest::description("Testing returned states")) {

    Eigen::MatrixXd states = crookedEgg();
    Eigen::VectorXd y0 = states.col(0);
    Eigen::Vector2d y_T;
    y_T << 0.993671172649357, 0.0568738533072468;

    CHECK((Eigen::Vector2d(1, 0) - y0).norm() ==
          doctest::Approx(0.).epsilon(1e-5));
    CHECK((states.col(states.cols() - 1) - y_T).norm() ==
          doctest::Approx(0.).epsilon(1e-5));
  }

  TEST_CASE("computeIsolinePointsDQ()" *
            doctest::description("Testing for F(x,y) = 0.5 * (x^2+y^2)")) {

    auto F = [](const Eigen::Vector2d &x) {
      // Gradient of 0.5 * (x(0)^2 + x(1)^2)
      return 0.5 * (x(0) * x(0) + x(1) * x(1));
    };

    double T = M_PI / 2;

    Eigen::Vector2d y0 = Eigen::VectorXd::Random(2);
    y0 /= y0.norm();

    Eigen::MatrixXd States = computeIsolinePointsDQ(F, y0, T);
    Eigen::Vector2d y_T = States.col(States.cols() - 1);

    CHECK((y_T - y0).norm() == doctest::Approx(sqrt(2)).epsilon(1e-5));
  }
}
