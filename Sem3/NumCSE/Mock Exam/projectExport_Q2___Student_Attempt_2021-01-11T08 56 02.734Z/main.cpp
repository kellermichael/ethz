#include "contour.hpp"
#include <Eigen/Dense>
#include <iostream>

using namespace Eigen;

int main() {

  double tol = 1e-4;
  // Function for crooked egg:
  auto F = [](const Vector2d &x) {
    double x2 = x(0) * x(0), y2 = x(1) * x(1);
    double x2y2 = x2 + y2;
    return x2y2 * x2y2 - x2 * x(0) - y2 * x(1);
  };
  // Test function:
  auto G = [](const Vector2d &x) {
    return 0.5 * x(0) * x(0) * x(1) - x(1) * x(1) * x(1) / 6;
  };
  // Gradient of test function:
  auto gradG = [](const Vector2d &x) {
    Vector2d grad(x(0) * x(1), 0.5 * x(0) * x(0) - 0.5 * x(1) * x(1));
    return grad;
  };
  Vector2d y0(-0.5, 0.5);
  Vector2d z0(1.0, 0.0);

  // Computing isoline points for test function G
  MatrixXd isoline = computeIsolinePoints(gradG, y0, 5.);
  std::cout << "Output of computeIsolinePoints() is a " << isoline.rows()
            << " by " << isoline.cols() << " matrix";
  if (isoline.cols() > 5)
    std::cout << ", starting with\n" << isoline.leftCols(5) << " ...\n";
  else
    std::cout << ":\n" << isoline << std::endl;

  // Running crookedEggCurve()
  MatrixXd crookedEggCurve = crookedEgg();
  std::cout << "Output of crookedEgg() is a " << crookedEggCurve.rows()
            << " by " << crookedEggCurve.cols() << " matrix";
  if (crookedEggCurve.cols() > 5)
    std::cout << ", starting with\n" << crookedEggCurve.leftCols(5) << " ...\n";
  else
    std::cout << ":\n" << crookedEggCurve << std::endl;

  // Computing isoline points for test function G
  MatrixXd isolineDQ = computeIsolinePointsDQ(G, y0, 5.);
  std::cout << "Output of computeIsolinePointsDQ() is a " << isolineDQ.rows()
            << " by " << isolineDQ.cols() << " matrix";
  if (isolineDQ.cols() > 5)
    std::cout << ", starting with\n" << isolineDQ.leftCols(5) << " ...\n";
  else
    std::cout << ":\n" << isolineDQ << std::endl;

  return 0;
}
