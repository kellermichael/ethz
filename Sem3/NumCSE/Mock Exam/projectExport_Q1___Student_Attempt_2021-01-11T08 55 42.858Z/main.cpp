#include "matrixlowrank.hpp"
#include <Eigen/Dense>
#include <iostream>

using namespace Eigen;

int main() {

  double tol = 1e-4;
  MatrixXd A(4, 2), B(3, 2), C(3, 2), D(4, 2), E(3, 1), F(4, 1), G(3, 4);
  A << 1, 2, 3, 4, 5, 6, 7, 8;
  B << 9, 0, 1, 2, 3, 4;
  C << 5, 6, 7, 8, 9, 0;
  D << 179, 94, 437, 250, 695, 406, 953, 562;
  E << 9. + 1e-9, 1 + 1e-9, 3 + 1e-9;
  F << -1, -3, -5, -7;
  G << 0, 0, 0, 0, 4, 8, 12, 16, 8, 16, 24, 32;
  MatrixLowRank L(B, A);
  MatrixLowRank M(A, B);
  MatrixLowRank N(E, F);

  std::cout << "M*C using exact matrix multiplication \n"
            << D << std::endl
            << std::endl;

  // Using operator*()
  MatrixXd MC = M * C;
  std::cout << "M*C using MatrixLowRank operator* \n"
            << MC << std::endl
            << std::endl;

  // Using operator*=()
  M *= C;
  MatrixXd Me = M.getA() * M.getB().transpose(); // Eigen version of M.
  std::cout << "M after M*=C:\n" << Me << std::endl << std::endl;

  std::cout << "N+L exact \n"
            << N.getA() * N.getB().transpose() + L.getA() * L.getB().transpose()
            << std::endl
            << std::endl;

  // Using addTo()
  N.addTo(L);
  MatrixXd Ne = N.getA() * N.getB().transpose(); // Eigen version of N
  std::cout << "N after N.addTo(L):\n" << Ne << std::endl;

  return 0;
}
