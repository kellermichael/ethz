\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[a4paper,total={6in,9in}]{geometry}
\usepackage[ruled]{algorithm2e}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{physics}
\usepackage{graphicx}
\usepackage{marvosym}
\usepackage{enumitem}
\usepackage{listings}
\graphicspath{ {./imgs/} }

\title{Numerical Methods}
\author{Michael Keller}
\date{February 2021}

% commands
\newcommand{\N}{{\mathbb N}}
\newcommand{\Z}{{\mathbb Z}}
\newcommand{\R}{{\mathbb R}}
\newcommand{\C}{{\mathbb C}}
\newcommand{\X}{{\mathcal X}}
\newcommand{\M}{{\mathcal M}}
\newcommand{\K}{{\mathbb K}}
\newcommand{\ra}{{\rightarrow}}
\newcommand{\Ra}{{\Rightarrow}}
\newcommand{\enum}[1]{\begin{enumerate}[label=(\arabic*)] #1 \end{enumerate}}
\lstnewenvironment{code}{}{}
\lstset{
  basicstyle=\ttfamily,
  mathescape
}

\begin{document}

\maketitle

\section{Basics}
\subsection{The Machine Numbers}
We are given the following:
\enum{
    \item A basis $B \in \N \setminus \{1\}$
    \item An exponent range $\{e_{min}, \dots, e_{max}\} \in \Z$ with $e_{min} < e_{max}$
    \item The number of digits $m$ the mantissa has
}
Then the set $\M$ of machine numbers is
\begin{align*}
    \M := \{i \cdot B^{E-m} \; | \; E \in \{e_{min}, \dots, e_{max}\}, i = B^{m-1}, \dots, B^m - 1\}
\end{align*}
which should seem familiar to people who know the floating point standard.
Here $i$ represents the mantissa and can float between its respective min and max,
while the exponent has a bias of $m$.
\subsection{Absolute and Relative Error}
Let $x' \in \K$ be an approximation of $x \in \K$. Then the absolute error
is defined as
\begin{align*}
    \varepsilon_{abs} := |x - x'|
\end{align*} while the relative error is
\begin{align*}
    \varepsilon_{rel} := \frac{|x - x'|}{|x|}
\end{align*}
Why are we interested in the relative error? Mostly because it puts the absolute error
in perspective. We might be okay with an absolute error of $100$ as long as our number
is very large.\\ \\
The number of correct digits $l$ of an approximation $x'$ is defined such that the equation below is true
\begin{align*}
    \varepsilon_{rel} := \frac{|x - x'|}{|x|} \leq 10^{-l}
\end{align*}
\subsection{EPS}
EPS denotes the largest possible relative error incurred through rounding
\begin{align*}
    \text{EPS} &:= \max_{x \in I} \frac{|rd(x) - x|}{|x|}\\
\end{align*}
Or in the machine numbers $M$
\begin{align*}
    \text{EPS} &:= \frac{B^{1-m}}{2}
\end{align*}
In this course we will assume that EPS is the machine precision. This means basic
arithmetic operations like $+, -, \cdot, /$ and standard functions
like $\exp, \sin, \log$ have the properties
\begin{align*}
    \tilde{f}(x) &= f(x) \cdot (1 + \delta)\\
    \tilde{f}(x, y) &= f(x, y) \cdot (1 + \delta)\\
    |\delta| &\leq \text{EPS}
\end{align*}
for all $x, y \in \M$. Here $\tilde{f}$ denotes the computer implementation
of one of the elementary functions mentioned above, while $f$ is the analogous
"pure" function.\\ \\
An important note is that the axiom of roundoff analysis does not hold once
non normalized numbers are encountered. Here an example where a non normalized
numbers leads to imprecision.
\begin{code}
    double min = numeric_limits<double>::min();
    double res1 = $\pi$ * min / 123456789101112;
    double res2 = res1 / min * 123456789101112;
\end{code}
Here \verb res2  should equal $\pi$ but ends up being $3.1524..$.
\subsection{Cancellation}
The extreme amplification of $\textbf{relative}$ errors. This occurs
when subtracting two almost equal numbers with small relative errors:
\begin{center}
    \includegraphics[scale=0.5]{roundoff.png}
\end{center}
This is most notably an issue when approximating derivatives, as
$\lim_{h \rightarrow 0} \frac{f(x + h) - f(x)}{h}$ contains a subtraction
of numbers that will get very close to each other.\\ \\
If inevitable, perform problematic subtractions
$\textbf{as early as possible!}$ The reason is that input
data and and initial intermediate results are
usually not as much tainted by roundoff
errors as numbers computed after many steps.\\ \\
Sometimes roundoff errors can be avoided entirely by performing
algebraically equivalent operations that don't involve a subtraction.
Here are some examples:
\enum{
    \item When determining the real roots of a quadratic polynomial
    we only use the standard method s.t. $-b$ and $\pm \sqrt{b^2 - 4ac}$
    have the same sign. For the other root we use Vieta's formula
    which states: $r_1 \cdot r_2 = \frac{c}{a}$
    \item Trigonometric identities can sometimes be used: $1 - \cos(x) = 2 \sin^2(\frac{x}{2})$
}
\subsection{Stable Algorithms}
A stable algorithm $\tilde{F}$ is defined as an algorithm that when given a slightly
imprecise input produces a result with an error of the same magnitude as
the input received. Formally:
\begin{align*}
    \exists C \;: \; \forall x \in \X \;: \; \exists \tilde{x} \in \X \;: \;
    \norm*{x - \tilde{x}}_\X \leq C \cdot w(x) \cdot \text{EPS} \cdot \norm*{x}_\X \land \tilde{F}(x) = F(\tilde{x})
\end{align*}
where $w(x)$ denotes the number of steps the algorithm performs on input $x$.
\end{document}