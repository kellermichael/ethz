n = 100;
t = transpose(linspace(0, 1, n));
y = t + 0.3*(rand(size(t)) - 0.5*ones(size(t)));
d = 50;
A = ones(n, d);
t2 = t;
for i = 2:d
    A(:, i) = t2;
    t2 = t2 .* t;
end
x = A\y;
x = flip(x);
y2 = polyval(x, t);
figure(1);
plot(t, y, "b-", t, y2, "g-");