function fib=fibonacci(n)
%% Berechnet die Fibonacci-Zahlen.
% Input: Natuerliche Zahl n.
% Output: Fibonacci-Zahlen f_0, ..., f_n. 

% Initialisiere Null-Vektor, der die Fibonacci-Zahlen
% enthalten soll.
fib=zeros(1,n+1);

% Anfangswerte

%%%%%%%%%%% hier der Loesungsvorschlag  %%%%%%%%%%%%

% Iteration

%%%%%%%%%%% hier der Loesungsvorschlag  %%%%%%%%%%%%

return
