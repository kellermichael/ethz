function x = gauss( A, b )

% A is a n*n matrix and b a n*1 vector

    % get length of vector b
	n = ???
	
	% If there is no unique solution then "false" is returned
	x = false;
	
	% We use the Gauss algorithm to transform B into 
	% row echelon form
	B = [A, b];	
	
	% Transform B into upper triangular form
	for k = 1:n
        
		% Check for Zero column
		???
		
		% Find pivot element
		[pivot, pivotInd] = max(abs(B(k:n, k)));
        
		% Index of pivot element
		pivotInd = pivotInd + (k-1);
		pivot = B(pivotInd, k);
		
		% Swap pivot row
		temp = B(k, :);
		B(k, :) = ???
		B(pivotInd, :) = ???
		
		% Normalize row
		B(k, :) = B(k, :) / pivot;
		
		% Eliminate rest of column
		for l = k+1:n
			B(l,:) = ???
		end
	end
	
	% Back substitution to solve for x
	% Transform B into the form [I, x]
	for k = (n-1):-1:1
		for l = k:-1:1
			B(l,:) = ???
		end
	end	

	% x is now the last column of B
	x = B(:, n+1);
end