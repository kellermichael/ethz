function x = gaussZ( A, b )
%GAUSS   Solve the linear system A*x = b
%   A is an n*n matrix and b an n*1 vector

	n = length(b);
	% If there is no unique solution then "false" is returned
	x = false;
	% We use the Gauss algorithm to transform B into the form [I, x]
	B = [A, b];	
	
	% Transform B into upper triangular form
	for k = 1:n
		% Zero entry?
		if (abs(B(k,k)) == 0)
			% zero pivot; return with x=false
			return;
		end

		% Normalize the row
		B(k,:) = B(k,:) / B(k,k);
		
		% Eliminate the rest of the column
		for l = k+1:n
			B(l,:) = B(l,:) - B(k,:)*B(l,k);
		end
	end
	
	% Transform B into the form [I, x]
	for k = (n-1):-1:1
		for l = k:-1:1
			B(l,:) = B(l,:) - B(k+1,:)*B(l,k+1);
		end
	end	

	% x is now the last column of B
	x = B(:, n+1);
end