% (c) Olga Sorkine, 2012
% ls_smoothing.m

% path = 'curves/bird.txt';
function ls_smoothing(path)

xy_0 = load(path);

% we assume x and y are prepared as column-vectors
% compute the number of vertices
n=length(xy_0);

% geometry dimensions needed for the plot
xmin = min(xy_0(:,1));
xmax = max(xy_0(:,1));
ymin = min(xy_0(:,2));
ymax = max(xy_0(:,2));

% plot the original curve
close;
plot(xy_0(:,1), xy_0(:,2), '.-');
axis([xmin xmax ymin ymax]);
axis equal; 
axis off;
hold on;

% Write your code here



% plot the smoothed curve
plot(xy(:,1), xy(:,2), '.-');
title(sprintf('Smoothed curve'));

