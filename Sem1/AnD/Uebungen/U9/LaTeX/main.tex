\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[a4paper,total={6in,9in}]{geometry}
\usepackage[ruled]{algorithm2e}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{tikz}
\newtheorem{theorem}{Theorem}
\newcommand{\floor}[1]{\left\lfloor #1 \right\rfloor}
\newcommand{\ceil}[1]{\left\lceil #1 \right\rceil}

\title{AnD U9}
\author{Robin Hänni, Michael Keller}
\date{November 2019}

\begin{document}

\maketitle

\section*{Task 9.1}
\subsection*{a)}
We offer a proof by contradiction. Our assumption is as follows: Some longest path in a nonempty forest doesn't connect two different leaves of the forest. It therefore follows that at least one end can't terminate in a leaf but must rather terminate at a vertex. This means the end vertex has at least one neighbour, which was already marked and thus there exists a cycle within our graph; namely the cycle from the end vertex to (one of) its neighbour(s) back to itself. As forests are by definition acyclic, this represents a contradiction to our assumption. Thus, the converse must be true: Any longest path in a nonempty forest connects two different leaves of the forest.\hfill$\square$
\subsection*{b)}
We offer a proof by algorithm. Consider first that there exist k vertices of odd degree. Now consider the following algorithm:
\begin{enumerate}
	\item Identify a longest path in the forest (i.e. a path where both beginning and end are a leaf)
	\item Remove all edges contained in the aforementioned path.
	\item Repeat until there are no more edges left.
\end{enumerate}
The second step in the algorithm has the following effect:
\begin{itemize}
    \item The degree of all vertices (except start and end vertex) in the aforementioned path decreases by two, whereby the amount of vertices with odd degree doesn't change.
    \item The degree of the start and end vertex decrease by one.
\end{itemize}
As we are in a forest, there must always be at least two leaves present, otherwise our forest would need to be cyclic, which would be a contradiction to the definition of a forest. As we saw in subtask a), if there are two leaves present, we can form a longest path. Thus: our algorithm can always be executed on any nonempty forest.\newline Every round of the algorithm sets the degree of two vertices (the start and end vertex) to zero, thereby decreasing the amount of odd degree vertices by two. Thus: the algorithm can only be executed k/2 times before the graph is empty. Hence, if this algorithm removes k/2 paths from the graph and arrives at an empty graph, there exists a union of the k/2 paths it removed that must generate the original graph.\hfill$\square$
\section*{Task 9.3}
\subsection*{a)}

Consider some longest path in G. The path is of the form $v_0,v_1,\ldots,v_{x-1},v_x$. Let's focus on the last vertex $v_x$. By our assumption, it has degree at least $m$ i.e. it has at least $m$ neighbours. One of them necessarily is $v_{x-1}$ since we came from there. The other $m-1$ neighbours, must appear somewhere in our path (otherwise we could add them to the path to make it longer and we assumed a longest path). Therefore, if we choose the neighbor which is the farthest away from $v_x$ (in terms of how far back in the path it appears), let's call that one $v_y$, and add the edge $\{v_x,v_y\}$ we just formed a cycle $v_x,v_y,\ldots,v_x$. This cycle must contain at least all neighbours of $v_x$ and $v_x$ itself and is therefore of length at least $m+1$ \hfill$\square$

\subsection*{b)}
To satisfy the condition we propose a graph with $m + 1$ vertices where every vertex is connected to every other vertex (i.e. the complete graph). Thus the maximum cycle length is the number of vertices in the graph: $m + 1$. Here are some examples:
\begin{figure}[hbp]
  \includegraphics[width=300pt]{examples.jpeg}\centering
  \caption{Examples of graphs with cycles of max length m + 1 and min vertex degree m}
  \label{fig:example1}
\end{figure}

\section*{Task 9.4}
\subsection*{a)}
Assume that it is possible to form a line under the described conditions. The line has the form seen in figure \ref{fig:domino}.
\begin{figure}[h]
    \centering
    \begin{tikzpicture}
\draw[step=0.5] (0,0) grid (3,0.5);
\node at (0.25,0.25) {X};
\node at (0.75,0.25) {A};
\node at (1.25,0.25) {A};
\node at (1.75,0.25) {B};
\node at (2.25,0.25) {B};
\node at (2.75,0.25) {C};
\node at (3.25,0.25) {...};
\draw[step=0.5] (3.49,0) grid (5.5,0.5);
\node at (3.75,0.25) {D};
\node at (4.25,0.25) {E};
\node at (4.75,0.25) {E};
\node at (5.25,0.25) {Y};
\end{tikzpicture}
\caption{A line of dominoes}
\label{fig:domino}
\end{figure}
The crucial observation here is that for a line of $m$ dominoes, there are $m-1$ pairs of equal numbers, and the start and end do not matter (X and Y in the figure). So in our example, we need a total of 20 pairs of equal numbers to form a line of 21 dominoes. Every number (1 to 6) appears exactly seven times; twice on the stone with both numbers equal and 5 times on the stones with another number (e.g. [1,1] [1,2] [1,3] [1,4] [1,5] [1,6]). This means, for any number we can form $\floor{\frac{7}{2}} = 3$ pairs with said number. There are 6 numbers available so the total number of pairs possible is $3\cdot6=18$ which is of course less than 20 and thus it is not possible to form a line of 21 dominoes.
\subsection*{b)}
The above statement (that one needs $m-1$ pairs to form a line of $m$ dominoes) naturally holds for any number of dominoes but in our case, we're only interested in numbers of the form $m = \binom{n}{2} + n$. The amount each number appears with $n$ possible numbers is $n+1$ and thus the total amount of possible pairs is $\floor{\frac{n+1}{2}}\cdot n$. For it to be possible to form a line with all dominoes, the amount of possible pairs must be greater or equal to the amount of required pairs i.e.:
\begin{align}
    \floor{\frac{n+1}{2}}\cdot n &\geq m-1 = \binom{n}{2} + n - 1\\
    &= \frac{n!}{(n-2)!\cdot 2!} + n - 1\\
    &= \frac{n \cdot (n-1)}{2!} + n - 1\\
    &= \frac{n^2 - n}{2} + n - 1\\
    &= \frac{n^2 + n}{2} - 1\\
    \floor{\frac{n+1}{2}}\cdot n &\geq \frac{n^2 + n}{2} - 1
\end{align}
There are two cases: $n$ is even and $n$ is odd. If $n$ is odd, the floor function has no effect so the equation yields:
\begin{align}
    \frac{n+1}{2}\cdot n &\geq \frac{n^2 + n}{2} - 1\\
    \frac{n^2+n}{2} &\geq \frac{n^2 + n}{2} - 1
\end{align}
which is of course true for all $n\geq2$. Thus, all $n\geq2$ with $n$ odd, lead to a combination of dominoes, which can be lined up in a single line.
If $n$ on the other hand is even, the floor function takes away 0.5 (i.e. the plus one doesn't have an effect). Which leads to:
\begin{align}
    \frac{n}{2}\cdot n &\geq \frac{n^2 + n}{2} - 1\\
    \frac{n^2}{2} &\geq \frac{n^2 + n}{2} - 1\\
    0 &\geq \frac{n}{2} - 1\\
    1 &\geq \frac{n}{2}\\
    2 &\geq n\\
\end{align}
and therefore all even $n$ greater than 2 will not lead to a combination of dominoes, which can be lined up in a single line.
\newline\newline
All in all, it is possible to form a line with all dominoes for $n = 2$ and all $n > 2$ with $n$ odd.
\end{document}
