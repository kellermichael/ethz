/**
 * Using this template, your task is to implement functions denoted by TODO in the comments.
 * 
 * You do not have to understand the whole source code to finish this task.
 * It is necessary to take a look on the attributes of the class Main
 * (e.g., <code>array</code>, the second argument is not important for this task).
 * 
 * The Java project also contains tests, in Eclipse you can run them by left-clicking:
 * test/(default package)/JUnitTest.java and selecting Run as -> JUnitTest.java.
 * 
 * The tests will show you if they passed/failed. In case of fail, you can exceptions
 * if the code did not finished, or the difference between your output and expected output.
 * The test names should help you to explain what is being tested. You can also see the content
 * of the tests, for the format of the input, see <code>read</code> method.
 * 
 * The output format is described in the comment of <code>output</code> method.
 */


//You are forbidden to add any other imports
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;


class Silhouette {
	/*
	 * For efficiency reasons, silhouettes are represented by ArrayList,
	 * a data structure with in average constant append operation 
	 * (adding an element to the end of array). Unfortunately, in Java it
	 * has a different API from a normal array.
	 * 
	 * Instead of access by <code>array[index]</code>, you have to use 
	 * <code>array.get(index)</code> method.
	 * 
	 * To append a new item, you can use <code>array.add(number)</code>
	 * to insert it to the end (in average constant time complexity),
	 * or <code>array.add(index, number)</code> which inserts to given index,
	 * at the cost of moving all succeeding elements, therefore linear complexity.
	 * 
	 * To change item at given position, use <code>array.set(index, number)</code>.
	 * 
	 * And to get the length of the array, instead of <code>array.length</code>,
	 * use <code>array.size()</code>.
	 * 
	 * The last drawback of ArrayList is that it stores object of Integer class.
	 * You can create these objects simply by passing int. Comparing two integers
	 * can be done by <, <=, >=, > operators, or for equality you have to use
	 * method equals. For example for testing if the beginning and end of a building
	 * are the same, you have to write
	 * <code>silhouette.get(0).equals(silhouette.get(2))</code> instead of a simple
	 * <code>silhouette.get(0) == silhouette.get(2)</code>.
	 */
	public ArrayList<Integer> silhouette;
	
	public Silhouette(int[] arr) {
		silhouette = new ArrayList<Integer>();
		for(int x : arr) {
	         silhouette.add(x);
	    }
	}
	
	public Silhouette(ArrayList<Integer> arr) {
		silhouette = arr;
	}
}

class Main {
	private static Silhouette[] array; // array of building silhouette triples as input
	

	/**
	 * TODO: Implement an algorithm to compute the silhouette of a given set of rectangular buildings
	 * in <code>array</code>. Every building is represented as triple (beginning, height, end).
	 * See the visual explanation in the assignment pdf.
	 * 
	 * If you wish every input silhouette in <code>array</code> be terminated with final 0, 
	 * thus having form (beginning, height, end, 0), please search for the following comment in read method:
	 * "// uncomment this if you want trailing 0".
	 * 
	 * The output format is an array consists of pairs of positions where the height of silhouette
	 * changes and the height itself. Every silhouette is therefore terminated by height 0.
	 * 
	 */
	private static Silhouette evaluate() {
		
		// init data
		ArrayList<ArrayList<int[]>> input = new ArrayList<ArrayList<int[]>>();
		for(int a = 0; a < array.length; a++) {
			ArrayList<int[]> house = new ArrayList<int[]>();
			house.add(new int[] {array[a].silhouette.get(0), array[a].silhouette.get(1)});
			house.add(new int[] {array[a].silhouette.get(2), 0});
			input.add(house);
		}
		
		// merge that shit!
		ArrayList<ArrayList<int[]>> result = new ArrayList<ArrayList<int[]>>();
		while(input.size() > 1) {
			result = new ArrayList<ArrayList<int[]>>();
			for(int a = 0; a < (int)(input.size() / 2); a++) {
				ArrayList<int[]> tmp = merge(input.get(2*a), input.get(2*a+1));
				result.add(tmp);
			}
			if(input.size() % 2 == 1) {
				result.add(input.get(input.size()-1));
			}
			input = result;
		}
		
		// construct result result
		ArrayList<Integer> output = new ArrayList<Integer>();
		for(int b = 0; b < input.get(0).size(); b++) {
			for(int c = 0; c < input.get(0).get(b).length; c++) {
				output.add(input.get(0).get(b)[c]);
			}
		}
		
		
		return new Silhouette(output); // substitute this with valid solution
	}
	
	private static ArrayList<int[]> merge(ArrayList<int[]> sceneA, ArrayList<int[]> sceneB) {
		ArrayList <int[]> result = new ArrayList<int[]>();
		int a = 0;
		int b = 0;
		
		int heightA = 0;
		int heightB = 0;
		
		int prevHeight = -1;
		
		while(a < sceneA.size() && b < sceneB.size()) {
			
			int posA = sceneA.get(a)[0];
			int posB = sceneB.get(b)[0];
			
			if(posA < posB) {
				heightA = sceneA.get(a)[1];
				int[] elem = {sceneA.get(a)[0], heightA > heightB ? heightA : heightB};
				if(prevHeight != elem[1]) {
					result.add(elem);
					prevHeight = elem[1];
				}
				a++;
			} else if(posA == posB) {
				heightA = sceneA.get(a)[1];
				heightB = sceneB.get(b)[1];
				int[] elem = {sceneA.get(a)[0], heightA > heightB ? heightA : heightB};
				if(prevHeight != elem[1]) {
					result.add(elem);
					prevHeight = elem[1];
				}
				a++;
				b++;
			} else {
				heightB = sceneB.get(b)[1];
				int[] elem = {sceneB.get(b)[0], heightA > heightB ? heightA : heightB};
				if(prevHeight != elem[1]) {
					result.add(elem);
					prevHeight = elem[1];
				}
				b++;
			}
		}
		while(b < sceneB.size()) {
			result.add(sceneB.get(b));
			b++;
		}
		while(a < sceneA.size()) {
			result.add(sceneA.get(a));
			a++;
		}
		
		return result;
	}
	
	///////////////////////////////////////////////////////////////////////
	// DO NOT MODIFY THE FOLLOWING CODE, YOU CAN COMPLETELY IGNORE IT
	///////////////////////////////////////////////////////////////////////
	
	/**
	 * Parses input in form:
	 * <length of input array = array_len>
	 * <array_len Silhouette elements of the array>
	 */
	private static void read(InputStream in) {
		FastReader s = new FastReader(in);
		int array_len = s.nextInt();
		array = new Silhouette[array_len];

		for (int idx = 0; idx < array_len; idx++) {
			int from = s.nextInt();
			int height = s.nextInt();
			int to = s.nextInt();
			array[idx] = new Silhouette(new int[] {from, height, to}); // comment this out if you want trailing 0
			//array[idx] = new Silhouette(new int[] {from, height, to, 0}); // uncomment this if you want trailing 0
		}
	}

	/**
	 * Output file has format:
	 * [<comma separated array of resulting silhouette>]
	 */
	private static void output(PrintStream out, Silhouette silhouette) {
		out.println(silhouette.silhouette.toString());
	}

	/**
	 * Parse the input and for each value, call the <code>evaluate</code>.
	 */
	public static void read_and_solve(InputStream in, PrintStream out) {
		read(in);

		output(out, evaluate());
	}

	public static void main(String[] args) {
		read_and_solve(System.in, System.out);
	}
	
	/**
	 * Ignore this class please. It is used for input parsing. Source:
	 * https://www.geeksforgeeks.org/fast-io-in-java-in-competitive-programming/
	 */
	static class FastReader {
		BufferedReader br;
		StringTokenizer st;

		public FastReader(InputStream in) {
			br = new BufferedReader(new InputStreamReader(in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}

		long nextLong() {
			return Long.parseLong(next());
		}

		double nextDouble() {
			return Double.parseDouble(next());
		}

		String nextLine() {
			String str = "";
			try {
				str = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return str;
		}
	}
}
