//NB: Fügen Sie keine Pakete hinzu.

//NB: Das Importieren von anderen Klassen ist NICHT ERLAUBT.
//NB: Das Benützen anderer Klassen, die nicht zu java.lang.* gehören, ist NICHT ERLAUBT.
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Scanner;

// BITTE VERÄNDERN SIE DEN OBENSTEHENDEN CODE NICHT.

class Vertex
{
	public int key;
	public Vertex leftChild = null;
	public Vertex rightChild = null;
		
	public Vertex(int key)
	{
		this.key=key;
	}
}

//NB: Verändern Sie die untenstehende Deklaration der Klasse Main und der Methode main() NICHT, 
//sonst kann der Judge das Programm nicht laufen lassen. 
//Die Klasse MUSS als "class Main { ... }" deklariert werden, 
//die Methode als "public static void main(String[] args) { ... }".
class Main
{			

	//n bezeichnet die Anzahl Knoten des Binären Suchbaumes aus dem Input
	//Das i-te Element im Array keysPreorder (hat Index i-1 und) ist der Schlüssel (engl. key) des i-ten Knotens,
	//der durch eine Preorder Traversierung des Binären Suchbaumes aus dem Input besucht wird.
	public static int[] solve(int n, int[] keysPreorder)
	{	
		//TODO: Geben Sie einen Array aus n Ganzzahlen (engl. integers) zurück.
		//Das i-te Element dieses Arrays (hat Index i-1 und) soll gleich dem Schlüssel des des i-ten Knotens sein,
		//der durch eine Breitensuche auf dem Binären Suchbaum aus dem Input besucht wird.
		
		// init vars
		int[] solution = new int[n];
		solution[0] = keysPreorder[0];
		
		// build BST
		Vertex root = new Vertex(keysPreorder[0]);
		Vertex cursor = root;
		for(int i = 1; i < n; i++) {
			int toBeInserted = keysPreorder[i];
			while((cursor.key > toBeInserted && cursor.leftChild != null) || (cursor.key < toBeInserted && cursor.rightChild != null)) {
				if(cursor.key > toBeInserted) {
					cursor = cursor.leftChild;
				} else {
					cursor = cursor.rightChild;
				}
			}
			if(cursor.key > toBeInserted) {
				cursor.leftChild = new Vertex(toBeInserted);
			} else {
				cursor.rightChild = new Vertex(toBeInserted);
			}
			cursor = root;
		}
		
		// get BFS
		int filledUpToHere = 1;
		int pos = 0;
		while(filledUpToHere < n) {
			// find elem in BST
			while(cursor.key != solution[pos]) {
				if(cursor.key > solution[pos]) {
					cursor = cursor.leftChild;
				} else {
					cursor = cursor.rightChild;
				}
				// System.out.println(cursor.key);
			}
			
			// add its children
			if(cursor.leftChild != null) {
				solution[filledUpToHere] = cursor.leftChild.key;
				filledUpToHere++;
			}
			if(cursor.rightChild != null) {
				solution[filledUpToHere] = cursor.rightChild.key;
				filledUpToHere++;
			}
			// System.out.println(Arrays.toString(solution));
			pos++;
			cursor = root;
		}
		
		// System.out.println(Arrays.toString(solution));
		return solution;
	}
	
	public static void main(String[] args)
	{
		read_and_solve(System.in, System.out);
	}

	public static void read_and_solve(InputStream istream, PrintStream output) 
	{
		Scanner scanner = new Scanner(istream);
				
		int ntestcases = scanner.nextInt();
		for(int t=0; t<ntestcases; t++)
		{
			int n = scanner.nextInt();
			int[] keysPreorder = new int[n];

			for(int i=0; i<n; i++)
				keysPreorder[i] = scanner.nextInt();

			int[] keysBFS = solve(n, keysPreorder);

			for(int i=0; i<n; i++)
			{
				if(i!=0)
					output.print(" ");

				output.print(keysBFS[i]);
			}
			output.println();
		}
		
		scanner.close();
	}
}
