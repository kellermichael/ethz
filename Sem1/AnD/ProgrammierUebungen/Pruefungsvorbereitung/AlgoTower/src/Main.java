//NB: Fügen Sie keine Pakete hinzu.

//NB: Das Importieren von anderen Klassen ist NICHT ERLAUBT.
//NB: Das Benützen anderer Klassen, die nicht zu java.lang.* gehören, ist NICHT ERLAUBT.
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Scanner;

// BITTE VERÄNDERN SIE DEN OBENSTEHENDEN CODE NICHT.

//NB: Verändern Sie die untenstehende Deklaration der Klasse Main und der Methode main() NICHT, 
//sonst kann der Judge das Programm nicht laufen lassen. 
//Die Klasse MUSS als "class Main { ... }" deklariert werden, 
//die Methode als "public static void main(String[] args) { ... }".
class Main
{		

	//n bezeichnet die Anzahl der ALGO Steine.
	//l[i] enthält die Länge des i-ten ALGO Steins, für i=1,...,n
	//b[i] enthält die Breite des i-ten ALGO Steins, für i=1,...,n
	//h[i] enthält die Höhe des i-ten ALGO Steins, für i=1,...,n
	static int solve(int n, int[] l, int[] b, int[] h)
	{
		//TODO: Geben Sie die Höhe des höchsten ALGO Turms, der aus den vorhandenen Steinen
		//gebaut werden kann, zurück.
		
		int[] dp = new int[n+1];
		// init dp array
		for(int a = 0; a < h.length; a++) {
			dp[a] = h[a];
		}
		
		/*
		System.out.println(Arrays.toString(l));
		System.out.println(Arrays.toString(b));
		System.out.println(Arrays.toString(h));
		System.out.println();
		*/
		
		for(int base = n; base > 0; base--) {
			for(int a = n; base < a; a--) {
				// if dimensions fit
				if((l[base] >= l[a] && b[base] >= b[a]) || (l[base] >= b[a] && b[base] >= l[a])) {
					if(dp[base] < (h[base] + dp[a])) {
						dp[base] = h[base] + dp[a];
						// System.out.println(Arrays.toString(dp));
					}
				}
			}
		}
		
		/*
		System.out.println(Arrays.toString(dp));
		System.out.println("----------------------");
		*/
		
		int maxHeight = 0;
		for(int a = 0; a < n; a++) {
			if(maxHeight < dp[a]) {
				maxHeight = dp[a];
			}
		}
		
		return maxHeight;
	}

	public static void main(String[] args)
	{
		read_and_solve(System.in, System.out);
	}

	public static void read_and_solve(InputStream istream, PrintStream output) 
	{
		Scanner scanner = new Scanner(istream);
				
		int ntestcases = scanner.nextInt();
		for(int t=0; t<ntestcases; t++)
		{
			int n = scanner.nextInt();

			int[] l = new int[n+1];
			int[] b = new int[n+1];
			int[] h = new int[n+1];

			for(int i=1; i<=n; i++)
				l[i] = scanner.nextInt();

			for(int i=1; i<=n; i++)
				b[i] = scanner.nextInt();

			for(int i=1; i<=n; i++)
				h[i] = scanner.nextInt();

			output.println(solve(n, l, b, h));
		}
		
		scanner.close();
	}
}
