// =======================================================================================================
// Test Exam    : WindT18.WindTurbines
// Author       :  
// =======================================================================================================

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Scanner;

class Main {
	
	//
	// Parameters include:
	//
	// n    - number of possible positions
	// D    - the minimal distance between two turbines
	// d[i] - position of the i-th turbine
	// e[i] - energy of the it-ht turbine
	//
	static int solve(int n, int D, int[] d, int[] e){
		// init dp table
		int[] dp = new int[n+1];
		for(int a = 0; a < n+1; a++) {dp[a] = e[a];}
		
		// do dp
		int closestTurbine = 0;
		for(int a = 1; a < n+1; a++) {
			// push up closestTurbine if possible
			while(closestTurbine < n && (d[a] - d[closestTurbine+1]) >= D) {closestTurbine++;}
			
			// dp stuff
			if(d[a] - d[closestTurbine] >= D) {dp[a] = dp[a] + dp[closestTurbine];}
			if(a > 0 && dp[a-1] > dp[a]) {dp[a] = dp[a-1];}
		}
		return dp[n];
	}

	public static void read_and_solve(InputStream in, PrintStream out) 
	{
		Scanner scanner = new Scanner(in);

		int cases = scanner.nextInt();
		for (int t = 0; t < cases; t += 1) {
			//
			// Scan the input sizes i.e number of possible positions
			// as well as the minimal distance between two turbines
			//
			int n = scanner.nextInt();
			int D = scanner.nextInt();
			//
			// Allocate space for position & energy
			//
			int d[] = new int[n+1];
			int e[] = new int[n+1];
			//
			// Perform the scans for position and energy
			//
			for (int i = 1; i <= n; i += 1) d[i] = scanner.nextInt();
			for (int i = 1; i <= n; i += 1) e[i] = scanner.nextInt();

			out.println(solve(n, D, d, e));
		}

		scanner.close();
	}

	//
	// Do not modify the main method, and keep the method read_and_solve
	// 
	public static void main(String[] args) {	
		read_and_solve(System.in, System.out);		
	}
}