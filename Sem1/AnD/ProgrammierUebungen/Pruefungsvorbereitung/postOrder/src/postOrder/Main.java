package postOrder;

import java.util.*;

public class Main {
	
	public static class Vertex {
		
		String Name;
		int pre = 0;
		int post = 0;
		LinkedList<Vertex> edgesFrom = new LinkedList<Vertex>();
		LinkedList<Vertex> edgesTo = new LinkedList<Vertex>();
		
		public Vertex(String name) {
			Name = name;
		}
		
		// heisst es git e directed Edge vo dere vertex zu ere andere
		public void to(Vertex v) {
			edgesFrom.add(v);
		}
		
		// e hilf dass mir wüssed vo wo mir cho sind
		public void from(Vertex v) {
			edgesTo.add(v);
		}
	}
	
	public static void determinePostOrder() {
		// setup example
		Vertex A = new Vertex("A");
		Vertex B = new Vertex("B");
		Vertex C = new Vertex("C");
		Vertex D = new Vertex("D");
		// edge One
		A.to(B);
		B.from(A);
		// edge Two
		A.to(C);
		C.from(A);
		// edge three
		C.to(D);
		D.from(C);
		// edge four
		A.to(D);
		D.from(A);
		
		// determine pre and post order
		int counter = 1;
		Vertex currentVertex = A;
		while(currentVertex.edgesFrom.size() != 0 || currentVertex.edgesTo.size() != 0) {
			// if unvisited mark with pre
			if(currentVertex.pre == 0) {currentVertex.pre = counter;counter++;}
			
			// if can go down further
			if(currentVertex.edgesFrom.size() > 0) {
				currentVertex = currentVertex.edgesFrom.remove(0);
			} else {
				Vertex tmp = currentVertex.edgesTo.remove(0);
				if(currentVertex.edgesFrom.size() == 0 && currentVertex.edgesTo.size() == 0) {
					currentVertex.post = counter;
					counter++;
				}
				currentVertex = tmp;
			}
		}
		currentVertex.post = counter;
		System.out.println("A.pre = " + A.pre + ", A.post = " + A.post);
		System.out.println("B.pre = " + B.pre + ", B.post = " + B.post);
		System.out.println("C.pre = " + C.pre + ", C.post = " + C.post);
		System.out.println("D.pre = " + D.pre + ", D.post = " + D.post);
				
	}
	
	public static void main(String[] args) {
		determinePostOrder();
	}

}
