// ==================================================================================================================
// Exercise   : AD19E02.Grid
// Submission : https://judge.inf.ethz.ch/team/websubmit.php?cid=28781&problem=AD19E02
// Author     : 
// ==================================================================================================================

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

class Main {	
	//
	// Provide the solution of the grid problem in this function. 
	// Feel free to provide additional fields and methods if 
	// necessary.
	//
	public static int solveGrid (int [][] grid) {
		int[][] dp = new int[grid.length][grid[0].length];
		
		// init dp table
		dp[0] = grid[0];
		
		// solve
		for(int a = 1; a < dp.length; a++) {
			for(int b = 0; b < dp[0].length; b++) {
				int minTop = Integer.MAX_VALUE;
				if(b - 1 >= 0 && minTop > dp[a-1][b-1]) {minTop = dp[a-1][b-1];}
				if(minTop > dp[a-1][b]) {minTop = dp[a-1][b];}
				if(b + 1 < dp.length && minTop > dp[a-1][b+1]) {minTop = dp[a-1][b+1];}
				dp[a][b] = grid[a][b] + minTop;
			}
		}
		
		// read out result
		int result = Integer.MAX_VALUE;
		for(int a = 0; a < dp.length; a++) {
			if(result > dp[dp.length-1][a]) {
				result = dp[dp.length-1][a];
			}
		}
		return result;
	}
	
	public static int min(int a, int b , int c) {
		if(a <= b && a <= c) {return a;}
		if(b <= c) {return b;}
		return c;
	}

	//
	// Please, do not modify the read_and_solve method, as well as the main method
	//
	public static void read_and_solve(InputStream in, PrintStream out) {
		//
		// Define a scanner that will read the input
		//
		Scanner scanner = new Scanner(in);
		//
		// Read the number of test cases, and start executing
		//
		int T = scanner.nextInt();
		for (int test = 0; test < T; test += 1) {
			//
			// Read the size of the array and create a new one
			//
			int N = scanner.nextInt();
			int[][] grid = new int[N][];			
			for (int i = 0; i < N; i += 1) {
				grid[i] = new int[N];
				for (int j = 0; j < N; j += 1) {
					grid[i][j] = scanner.nextInt();
				}
			}
			out.println(solveGrid(grid));
		}		
		scanner.close();
	}

	//
	// Do not modify the main method, and keep the method read_and_solve
	// 
	public static void main(String[] args) {	
		read_and_solve(System.in, System.out);		
	}
}