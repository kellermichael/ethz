// ==================================================================================================================
// Exercise   : AD18E02.Square
// Submission : https://judge.inf.ethz.ch/team/websubmit.php?cid=28777&problem=AD18E02
// Author     : 
// ==================================================================================================================

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

class Main {		
	//
	// Add additional methods, classes, fields, etc, to fit your needs.
	//
	// int someField = ...
	
	
	public static int solve(int[][] matrix) {
		if(matrix.length == 0) {return 0;}
		int result = 0;
		int[][] dp = new int[matrix.length][matrix[0].length];
		
		// do dp
		for(int a = 0; a < matrix.length; a++) {
			for(int b = 0; b < matrix[a].length; b++) {
				if(matrix[a][b] == 1) {
					int optL = b-1 >= 0 ? dp[a][b-1] : 0;
					int optT = a-1 >= 0 ? dp[a-1][b] : 0;
					int optTL = a-1 >= 0 && b-1 >= 0 ? dp[a-1][b-1] : 0;
					if(!(optL == 0 && optT == 0 && optTL == 0)) {
						dp[a][b] = min(optL, optT, optTL) + 1;
					} else {
						dp[a][b] = 1;
					}
				}
			}
		}
		
		// extract solution
		for(int a = 0; a < dp.length; a++) {
			for(int b = 0; b < dp[a].length; b++) {
				if(dp[a][b]*dp[a][b] > result) {result = dp[a][b]*dp[a][b];}
			}
		}
		return result;
	}
	
	public static int min(int a, int b, int c) {
		if(a <= b && a <= c) {return a;}
		if(b <= c) {return b;}
		return c;
	}
	
	//
	// Feel free to modify the template, but do not rename the 
	// read_and_solve method, nor change its arguments.
	//
	public static void read_and_solve(InputStream in, PrintStream out) {
		//
		// Define a scanner that will read the input
		//
		Scanner scanner = new Scanner(in);
		//
		// Read the number of test cases
		//
		int T = scanner.nextInt();
		//
		// Now solve each case
		//		
		for (int test = 0; test < T; test += 1) {
			//
			// Read the number of rows of the matrix
			//
			int M = scanner.nextInt(); 
			//
			// Read the number of columns of the matrix
			//
			int N = scanner.nextInt(); 
			//
			// Allocate the matrix
			//
			int B[][] = new int[M][N];			
			//
			// Read the matrix
			//
			for (int i = 0; i < M; i += 1) {
				for (int j = 0; j < N; j += 1) {                					
					B[i][j] = scanner.nextInt();            						
				}
			}				
			//
			// Provide your solution here. Feel free to modify the template adding or 
			// removing variables to fit your needs. Find the largest square and store
			// it's area in the maxArea variable:
			//			
			int maxArea = solve(B);
			//
			// Once done, output the result:
			//
			out.println(maxArea);
		}
		//
		// Finally, close the scanner once done
		//
		scanner.close();
	}

	//
	// Do not modify the main method, and keep the method read_and_solve
	// 
	public static void main(String[] args) {	
		read_and_solve(System.in, System.out);		
	}
}