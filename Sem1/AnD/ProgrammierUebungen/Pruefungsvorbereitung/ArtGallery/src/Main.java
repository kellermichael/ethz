//NB: KEIN Paket hinzufügen!
//NB: Es ist NICHT ERLAUBT, andere Klassen zu importieren. 
//NB: Es ist NICHT ERLAUBT, andere Klassen die nicht zu java.lang.* gehören zu verwenden. 

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Scanner;

// BITTE VERÄNDERN SIE DIE ZEILEN OBERHALB NICHT! 

// NB: Bitte verändern Sie die untenstehenden Deklarationen der Klasse Main und der Methode main() NICHT, um das Programm auf dem Judge laufen zu lassen.
// Die Klasse muss als "class Main { ... }" deklariert sein. 
// Die Methode muss als "public static void main(String[] args) { ... }" deklariert sein.

class Main
{		
	//n ist die Anzahl Skulpturen in der Kunstgalerie 
	//V ist die Kapazität des Lastwagens 
	//T ist die verbleibende Zeit bis zum eintreffen der Polizei 
	//Für i=1,...,n gilt: Die Elemente volume[i], time[i] und price[i] sind die ganzen Zahlen (Integer) v_i, t_i und p_i der Aufgabenstellung.
	//volume[0], time[0] und price[0] werden nicht benutzt und sind gleich 0.
	static int[][][] dp;
	static int solve(int n, int V, int T, int[] volume, int[] time, int[] price)
	{
		dp = new  int[n+1][V+1][T+1];
		return recursiveSolve(n, V, T, volume, time, price);
	}
	
	static int recursiveSolve(int n, int V, int T, int[] volume, int[] time, int[] price)
	{
		if(n == 0) {return 0;}
		if(dp[n][V][T] != 0) {return dp[n][V][T];}
		int takeIt = 0;
		if(V - volume[n] >= 0 && T - time[n] >= 0) {
			takeIt = recursiveSolve(n-1, V - volume[n], T - time[n], volume, time, price) + price[n];
		}
		int bePoor = recursiveSolve(n-1, V, T, volume, time, price);
		dp[n][V][T] = takeIt > bePoor ? takeIt : bePoor;
		return dp[n][V][T];
	}
		
	public static void main(String[] args)
	{
		read_and_solve(System.in, System.out);
	}

	public static void read_and_solve(InputStream istream, PrintStream output) 
	{
		Scanner scanner = new Scanner(istream);
				
		int ntestcases = scanner.nextInt();
		for(int t=0; t<ntestcases; t++)
		{
			int n = scanner.nextInt();
			int V = scanner.nextInt();
			int T = scanner.nextInt();

			int[] volume = new int[n+1];
			int[] time = new int[n+1];
			int[] price = new int[n+1];

			for(int i=1; i<=n; i++)
			{
				volume[i] = scanner.nextInt();
				time[i] = scanner.nextInt();
				price[i] = scanner.nextInt();
			}
			
			output.println(solve(n, V, T, volume, time, price));
		}
		
		scanner.close();
	}
}