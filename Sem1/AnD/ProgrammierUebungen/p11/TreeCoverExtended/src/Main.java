/**
 * Using this template, your task is to implement functions denoted by TODO in the comments.
 * 
 * In this exercise, you are given a binary tree that stores BigInteger (it is not a search tree,
 * so the values stored in the tree and branches' height have no special property).
 * 
 * Your task is to implement method <code>getMaximum</code> that returns the maximum product
 * of nodes (by nodes we mean their values) that satisfy the following property:
 * No two adjacent nodes are included in the product.
 * 
 * The Java project also contains tests, in Eclipse you can run them by left-clicking:
 * test/(default package)/JUnitTest.java and selecting Run as -> JUnitTest.java.
 * 
 * The tests will show you if they passed/failed. In case of fail, you can see an exception
 * if the code did not finished, or the difference between your output and expected output.
 * The test names should help you to explain what is being tested. You can also see the content
 * of the tests, for the format of the input, see <code>read</code> method.
 * 
 * The output format is described in the comment of <code>output</code> method.
 */

/**
 * This imports policy applies to all programming assignments and the exam.
 * 
 * You are forbidden to add any other import statements. Make sure that you do not leave any
 * imports commented out, or that your IDE does not add any automatically. You can recognize
 * allowed imports by the <code>// allowed import</code> comment.
 * Calls by the fully qualified name are treated as imports as well. Those are, for example,
 * <code>java.util.Arrays.sort</code>, or <code>java.util.Arrays.binarySearch</code>.
 * You can use only data structures we provide to you by imports, by our definitions (even
 * implicitly used classes of packages, such as methods of Object those are inherited with
 * every class declaration, Array, or String), or data structures that you write on your own.
 * Usage of common arrays (<code>type[] variable</code>) is not restricted.
 * 
 * Note that Judge is not enforcing this policy. So if you violate this policy, you may lose the
 * bonus points even if Judge accepts your submission.
 * 
 * The general exceptions out of this policy is the package Math. The exceptions that are specific
 * to a given template are written down by its imports.
 */
import java.util.StringTokenizer; // allowed import
import java.io.BufferedReader; // allowed import
import java.io.IOException; // allowed import
import java.io.InputStream; // allowed import
import java.io.InputStreamReader; // allowed import
import java.io.PrintStream; // allowed import
import java.lang.Math; // allowed import
import java.math.BigInteger; // allowed import

class Main {
	public static void main(String[] args) {
		ReadAndWrite rw = new ReadAndWrite();
		rw.readAndSolve(System.in, System.out);
	}
}

class Node {
	Node left; // left child
	Node right; // right child
	int id; // node's id, that you should return <code>getNodes</code>
	BigInteger value; // stored value
	
	public Node(Node l, Node r, int i, int v) {
		left = l;
		right = r;
		id = i;
		value = BigInteger.valueOf(v);
	}
}

// you can create new classes for return value

class BinaryTree {
	Node root; // root of the binary tree
	
	/**
	 * Constructor creates empty tree, we use <code>deserialize</code>
	 * to load the tree.
	 * 
	 * If you need, you can store additional attributes and initialize them here.
	 */
	BinaryTree() {
		root = null;
	}
	
	/**
	 * TODO: Implement this method that finds the maximal product of node values,
	 * such that no two adjacent nodes are included in the sum.
	 * 
	 * Tested by tests "max"
	 */
	BigInteger getMaximum() {
		return new BigInteger("-1");
	}
	
	/**
	 * TODO: Implement this method that returns the nodes that contribute to the
	 * maximal product, such that no two adjacent nodes are included in the sum.
	 * 
	 * Tested by tests "nodes"
	 */
	int[] getNodes() {
		return new int[] {};
	}
	
	int getMaximum() {
		if(root != null) {
			performRecursion(root);
			// System.out.println(max(root.maxSumIfNotUsed, root.maxSumIfUsed));
			return max(root.maxSumIfNotUsed, root.maxSumIfUsed);
		} else {
			return 0;
		}
	}
	
	private void performRecursion(Node node) {
		if(node.left != null || node.right != null) {
			if(node.left != null && node.right != null) {
				performRecursion(node.left);
				performRecursion(node.right);
				node.maxSumIfUsed = node.left.maxSumIfNotUsed + node.right.maxSumIfNotUsed + node.value;
				node.maxSumIfNotUsed = max(node.left.maxSumIfUsed, node.left.maxSumIfNotUsed, 0) + max(node.right.maxSumIfUsed, node.right.maxSumIfNotUsed, 0);
			} else {
				if(node.left != null) {
					performRecursion(node.left);
					node.maxSumIfUsed = node.left.maxSumIfNotUsed + node.value;
					node.maxSumIfNotUsed = max(node.left.maxSumIfUsed, node.left.maxSumIfNotUsed, 0);
				} else {
					performRecursion(node.right);
					node.maxSumIfUsed = node.right.maxSumIfNotUsed + node.value;
					node.maxSumIfNotUsed = max(node.right.maxSumIfUsed, node.right.maxSumIfNotUsed, 0);
				}	
			}
		} else {
			node.maxSumIfUsed = node.value;
			node.maxSumIfNotUsed = 0;
		}
	}
	
	/**
	 * If you wish, you can use the following functions for getting the maximum
	 * of all arguments.
	 * 
	 * For multiplication or multiple arguments, you can use functions <code>op</code>
	 */
	BigInteger max(BigInteger a, BigInteger b) {
		int x = a.compareTo(b);
		return x == 1 ? a : b;
	}

	BigInteger max(BigInteger a, BigInteger b, BigInteger c) {
		return max(max(a, b), c);
	}

	BigInteger max(BigInteger a, BigInteger b, BigInteger c, BigInteger d) {
		return max(max(a, b), max(c, d));
	}
	
	BigInteger op(BigInteger a, BigInteger b) {
		return a.multiply(b);
	}
	
	BigInteger op(BigInteger a, BigInteger b, BigInteger c) {
		return a.multiply(b).multiply(c);
	}
	
	void deserialize(int[] serializedTree) {
		int counter = 0;
		
		Node[] stack = new Node[(serializedTree.length / 2) + 1];
		int top = -1;
		for (int num : serializedTree) {
			if (num == -1) {
				stack[++top] = null;
			} else {
				Node r = stack[top--];
				Node l = stack[top--];
				stack[++top] = new Node(l, r, counter++, num);
			}
		}
		
		root = stack[top];
	}
}

///////////////////////////////////////////////////////////////////////
// DO NOT MODIFY THE FOLLOWING CODE, YOU CAN COMPLETELY IGNORE IT
// WE MAY USE LANGUAGE CONSTRUCTS THAT YOU MAY HAVE NOT SEEN SO FAR
///////////////////////////////////////////////////////////////////////

class ReadAndWrite {
	/**
	 * Parses input in form: <number of instances> 
	 * <post-fix traversal of tree of n nodes,
	 * where -1 marks nil node (leaf), positive
	 * number marks normal node with the given value>
	 * 
	 * If you need to debug the code and look on the input,
	 * take a look on <code>testSingle</code> method in
	 * <code>JUnitTest</code>.
	 */
	void readAndSolve(InputStream in, PrintStream out) {
		FastReader s = new FastReader(in);

		int instances = s.nextInt();
		
		for (int i = 0; i < instances; i++) {
			int version = s.nextInt();
			int n = s.nextInt();
			int[] array = new int[n];
			
			for (int j = 0; j < n; j++) {
				array[j] = s.nextInt();
			}

			BinaryTree bt = new BinaryTree();
			bt.deserialize(array);
			if (version == 0) {
				out.println(bt.getMaximum());
			} else {
				int[] sol = bt.getNodes();
				java.util.Arrays.sort(sol);
				out.println(java.util.Arrays.toString(sol));
			}
		}
	}
}

/**
 * Ignore this class please. It is used for input parsing. Source:
 * https://www.geeksforgeeks.org/fast-io-in-java-in-competitive-programming/
 */
class FastReader {
	BufferedReader br;
	StringTokenizer st;

	FastReader(InputStream in) {
		br = new BufferedReader(new InputStreamReader(in));
	}

	String next() {
		while (st == null || !st.hasMoreElements()) {
			try {
				st = new StringTokenizer(br.readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return st.nextToken();
	}

	int nextInt() {
		return Integer.parseInt(next());
	}

	String nextLine() {
		String str = "";
		try {
			str = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return str;
	}
}
