
public class Omega extends Beta {
	
	String Name = "Omega";

	public String name() {
		return Name;
	}
	
	public Lambda create() {
		Lambda myLambda = new Lambda();
		myLambda.setName("Kappa");
		return myLambda;
	}
}
