
public class Lambda extends Iota {
	
	public Lambda() {
		Name = "Lambda";
		new Kappa();
	}
	
	public Beta choose(Alpha a, Sigma s, Iota i) {
		return a;
	}
	
	public void setName(String newName) {
		Name = newName;
	}
}
