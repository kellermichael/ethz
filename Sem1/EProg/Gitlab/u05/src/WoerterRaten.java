import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class WoerterRaten {
    
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("woerter.txt"));
        String[] woerter = liesWoerter(scanner);
        scanner.close();
        
        rateSpiel(woerter);
    }
    
    /**
     * Liest die Wörter von dem gegebenen Scanner ein und gibt sie als Array zurück.
     */
    static String[] liesWoerter(Scanner scanner) {
        String[] woerter = new String[scanner.nextInt()];
        for(int i = 0; i < woerter.length; i++) {
            woerter[i] = scanner.next();
        }
        return woerter;
    }
    
    /**
     * Führt das Rate-Spiel einmal durch.
     */
    static void rateSpiel(String[] woerter) {
    	String geheimWort = zufallsWort(woerter);
    	String tipp = "";
    	int noOfTries = 0;
    	Scanner myScanner = new Scanner(System.in);
    	while(!geheimWort.equals(tipp)) {
    		if(tipp != "") {
        		System.out.println("Das Wort " + hinweis(geheimWort, tipp) + " \"" + tipp + "\"");	
    		}
    		System.out.print("Tipp? ");
    		tipp = myScanner.nextLine();
    		noOfTries++;
    	}
    	myScanner.close();
    	System.out.println("Glückwunsch, du hast nur " + noOfTries + " Versuche benötigt!");
    }
    
    /**
     * Wählt zufällig ein Wort aus dem "woerter"-Array aus und gibt es zurück.
     */
    static String zufallsWort(String[] woerter) {
        return woerter[(int) (Math.ceil(Math.random()*woerter.length)) - 1];
    }
    
    /**
     * Vergleicht das gegebene "wort" und die "tipp"-Zeichenkette und gibt einen Hinweis zurück.
     * Folgende Hinweise sind möglich: "ist", "beginnt mit", "endet mit", "beginnt mit und endet
     * mit", "enthält" oder "enthält nicht".
     */
    static String hinweis(String wort, String tipp) {
        if(wort.contains(tipp)) {
        	if(wort.equals(tipp)) {
        		return "ist";
        	} else if(wort.startsWith(tipp) && wort.endsWith(tipp)) {
        		return "beginnt mit und endet mit";
        	} else if(wort.startsWith(tipp)) {
        		return "beginnt mit";
        	} else if(wort.endsWith(tipp)) {
        		return "endet mit";
        	} else {
        		return "enthält";
        	}
        } else {
        	return "enthält nicht";
        }
    }
}