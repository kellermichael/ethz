import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

public class Hotellerie {

	public static void main(String[] args) throws FileNotFoundException {
		
		// Sie koennen den Inhalt von main aendern
		
		Scanner scanner = new Scanner(new File("hotelDaten1.txt"));
		scanner.useLocale(java.util.Locale.forLanguageTag("ch-de"));
		PrintStream output = new PrintStream(System.out);
		
		analyse(scanner, output);
	}
	
	public static void analyse(Scanner input, PrintStream output) throws FileNotFoundException {
		
		// daten einlesen
		int noOfBookings = input.nextInt();
		input.nextLine();
		stay[] stays = new stay[noOfBookings];
		for(int a = 0; a < noOfBookings; a++) {
			Scanner line = new Scanner(input.nextLine());
			int roomNumb = line.nextInt();
			int start = line.nextInt();
			int end = line.nextInt();
			double money = line.nextDouble();
			double disc = (double) line.nextInt();
			stays[a] = new stay(roomNumb, start, end, money, disc);
			line.close();
		}
		
		// init vars
		frequencyObject[] roomFreq = new frequencyObject[noOfBookings];
		frequencyObject[] roomUsage = new frequencyObject[noOfBookings];
		frequencyObject[] roomProfit = new frequencyObject[noOfBookings];
		double hotelProfit = 0.0;
		
		for(int b = 0; b < noOfBookings; b++) {
			
			// init vars
			int c = 0;
			boolean found = false;
			
			// handle roomFreq
			while(roomFreq[c] != null && !found) {
				if(roomFreq[c].title == (double) stays[b].roomNumber) {
					roomFreq[c].occurrencce++;
					found = true;
				}
				c++;
			}
			if(!found) {
				roomFreq[c] = new frequencyObject((double) stays[b].roomNumber, 1.0);
			}
			
			// handle roomUsage
			c = 0;
			found = false;
			while(roomUsage[c] != null && !found) {
				if(roomUsage[c].title == (double) stays[b].roomNumber) {
					roomUsage[c].occurrencce += stays[b].calculateStayDuration();
					found = true;
				}
				c++;
			}
			if(!found) {
				roomUsage[c] = new frequencyObject((double) stays[b].roomNumber, (double) stays[b].calculateStayDuration());
			}
			
			// handle roomProfit
			c = 0;
			found = false;
			while(roomProfit[c] != null && !found) {
				if(roomProfit[c].title == (double) stays[b].roomNumber) {
					roomProfit[c].occurrencce += stays[b].calculateCost();
					found = true;
				}
				c++;
			}
			if(!found) {
				roomProfit[c] = new frequencyObject((double) stays[b].roomNumber, (double) stays[b].calculateCost());
			}
			
			// handle hotel profit
			hotelProfit += stays[b].calculateCost();
		}
		
		
		// Ersetzen Sie null im unteren Code mit Ihren entsprechenden Loesungen
		output.println("Am haeufigsten gebucht: " + (int) findMaxValue(roomFreq));
		output.println("Am meisten besetzt: " + (int) findMaxValue(roomUsage));
		output.println("Groessten Betrag eingebracht: " + (int) findMaxValue(roomProfit));
		output.println("Gesamteinnahmen des Hotels: " + hotelProfit); 
	}
	
	public static double findMaxValue(frequencyObject[] data) {
		int index = 0;
		double maxValue = 0.0;
		for(int a = 0; a < data.length; a++) {
			if(data[a] != null && data[a].occurrencce > maxValue) {
				index = a;
				maxValue = data[a].occurrencce;
			}
		}
		return data[index].title;
		
	}

	
	public static class stay{
		int roomNumber;
		int startDate;
		int endDate;
		double price;
		double discount;
		
		public stay(int roomNumb, int start, int end, double money, double disc) {
			roomNumber = roomNumb;
			startDate = start;
			endDate = end;
			price = money;
			discount = disc;
		}
		
		public double calculateCost() {
			return calculateStayDuration() * price * (100.0-discount) / 100.0;
		}
		
		public int calculateStayDuration() {
			return (endDate - startDate + 1);
		}
	}
	
	
	
	public static class frequencyObject{
		double title;
		double occurrencce;
		
		public frequencyObject(double name, double occur) {
			title = name;
			occurrencce = occur;
		}
	}
	
	

}
