import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;
import gui.Window; // dieser Import wird benoetigt, falls das Histogramm in einem Fenster gezeichnet wird und die Window-Klasse verwendet wird

public class DatenAnalyse {
    
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("groessen.txt"));
        int[] groessen = liesGroessen(scanner);
        scanner.close();
        
        einfacheAnalyse(groessen);

        int histMin = 140; //  Wir nehmen an, dass alle Groessen >= 1.40m sind
        int histMax = 200; //  Wir nehmen an, dass alle Groessen < 2.00m sind
        histogrammAnalyse(groessen, histMin, histMax);
    }
    
    /**
     * Liest die Körpergrössen von dem gegebenen Scanner ein und gibt sie als Array zurück.
     */
    static int[] liesGroessen(Scanner scanner) {
    	int noOfParticipants = scanner.nextInt();
    	int[] groessen = new int[noOfParticipants];
    	for(int a = 0; a < noOfParticipants; a++) {
    		groessen[a] = scanner.nextInt();
    	}
        return groessen;
    }
    
    /**
     * Macht eine einfache Datenanalyse: Gibt die Anzahl Daten, das Minimum, das Maximum und den
     * Durchschnitt der Körpergrössen aus.
     */
    static void einfacheAnalyse(int[] groessen) {
    	// sort the groessen array
    	Arrays.sort(groessen);
    	
    	// determine min, max values
    	int minHeight = groessen[0];
    	int maxHeight = groessen[groessen.length -1];
    	
    	// determine average and numb of different values
    	double sum = 0;
    	double noOfParticipants = (double) groessen.length;
    	
    	boolean[] valueMarker = new boolean[maxHeight + 1];
    	int numbOfUniqueValues = 0;
    	
    	for(int a = 0; a < groessen.length; a++) {
    		sum += groessen[a];
    		if(!valueMarker[groessen[a]]) {
    			valueMarker[groessen[a]] = true;
    			numbOfUniqueValues++;
    		}
    	}
    	
    	// actually calculate avg
    	double avg = sum/noOfParticipants;
    	
    	// create new array with the unique height values
    	int uniqueHeightId = 0;
    	int[] heightValues = new int[numbOfUniqueValues];
    	for(int b = 0; b < valueMarker.length; b++) {
    		if(valueMarker[b]) {
    			heightValues[uniqueHeightId] = b;
    			uniqueHeightId++;
    		}
    	}
    	
    	// System.out.println(Arrays.toString(heightValues));
    	// System.out.println(maxHeight);
    	
    	
    	
    }
    
    /**
     * Erstellt ein Histogramm der Körpergrössen und gibt es aus. Zuerst wird der Benutzer nach der
     * Anzahl Histogramm-Klassen gefragt.
     */
    static void histogrammAnalyse(int[] groessen, int histMin, int histMax) {
    	
    	// ask the user how many classes they want
    	System.out.print("Wieviele Histogramm Klassen möchten Sie? ");
    	Scanner scanner = new Scanner(System.in);
    	int noOfClasses = scanner.nextInt();
    	scanner.close();
    	
    	// generate datastructure for histogramm
    	int[] histogramm = erstelleHistogramm(groessen, histMin, histMax, noOfClasses);
    	
    	// determine historgamm intervalls
    	int intervall = klassenBreite(histMin, histMax, noOfClasses);
    	
    	for(int a = 0; a < noOfClasses; a++) {
    		System.out.print("[" + (histMin + a*intervall));
    		System.out.print(" " + (histMin + (a+1)*intervall) + ") ");
    		for(int b = 0; b < histogramm[a]; b++) {
    			System.out.print(" |");
    		}
    		System.out.println();
    	}
    	
    	
    	
    	
    }
    
    /**
     * Erstellt von den Körpergrössen in dem "groessen"-Array ein Histogramm mit den gegebenen
     * Grössen Minimum, Maximum und Anzahl Klassen.
     * 
     * @return das Histogramm, als Array. Jedes Element entspricht einer Klasse im Histogramm und
     *         enthält die Anzahl Körpergrössen in dieser Klasse.
     */
    static int[] erstelleHistogramm(int[] groessen, int histMin, int histMax, int klassen) {
    	
    	int[] histogramm = new int[klassen];
    	
    	// determine historgamm intervalls
    	int intervall = klassenBreite(histMin, histMax, klassen);
    	
        for(int a = 0; a < groessen.length; a++) {
        	int position = (int)Math.floor(((double)groessen[a] - (double)histMin) / (double)intervall);
        	histogramm[position]++;
        }
        
        return histogramm;
    }
    
    /**
     * Gibt die Klassenbreite in einem Histogramm mit den gegebenen Grössen Minimum, Maximum und
     * Anzahl Klassen zurück. Falls sich die gesamte Breite des Histogramms nicht restlos auf die
     * Klassen aufteilen lässt, wird die Klassenbreite aufgerundet. Das heisst, dass das Histogramm
     * "histMax" überschreiten kann.
     */
    static int klassenBreite(int histMin, int histMax, int klassen) {
    	double min = (double) histMin;
    	double max = (double) histMax;
    	double classNumb = (double) klassen;
        return (int) Math.ceil((max-min)/classNumb);
    }
}