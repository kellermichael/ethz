import gui.Window;

public class ChaosGame {

    private static final int WIDTH = 800;
    private static final int HEIGHT = 600;

	public static void main(String[] args) {
		Window window = new Window("Hello", WIDTH, HEIGHT);
		window.open();
		
		int cornerAX = (int) (WIDTH * Math.random());
		int cornerAY = (int) (HEIGHT * Math.random());
		int cornerBX = (int) (WIDTH * Math.random());
		int cornerBY = (int) (HEIGHT * Math.random());
		int cornerCX = (int) (WIDTH * Math.random());
		int cornerCY = (int) (HEIGHT * Math.random());
		
		int startX = (int) (WIDTH * Math.random());
		int startY = (int) (HEIGHT * Math.random());

        while(window.isOpen()) {
        	
        	int random = (int)(Math.random()*3);
        	
        	if(random == 0) {
        		int x = (startX + cornerAX)/2;
        		int y = (startY + cornerAY)/2;
        		window.fillRect(x, y, 1, 1);
        		startX = x;
        		startY = y;
        	}
        	if(random == 1) {
        		int x = (startX + cornerBX)/2;
        		int y = (startY + cornerBY)/2;
        		window.fillRect(x, y, 1, 1);
        		startX = x;
        		startY = y;
        	}
        	if(random == 2) {
        		int x = (startX + cornerCX)/2;
        		int y = (startY + cornerCY)/2;
        		window.fillRect(x, y, 1, 1);
        		startX = x;
        		startY = y;
        	}
        	
        	window.refresh(5);
        }
	}
}
