import java.util.*;

public class Adder {
	public static void main(String[] args) {
		Scanner console = new Scanner(System.in);
		System.out.print("Geben Sie Zahl 1 ein: ");
		int numbOne = console.nextInt();
		System.out.print("Geben Sie Zahl 2 ein: ");
		int numbTwo = console.nextInt();
		System.out.println(numbOne + " + " + numbTwo + " = " + (numbOne+ numbTwo));
		console.close();
	}
}
