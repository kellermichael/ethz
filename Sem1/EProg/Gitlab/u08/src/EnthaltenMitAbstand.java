import java.util.Arrays;

public class EnthaltenMitAbstand {
	
	static int permutations = 0;
	static boolean works = false;
	
	/*
	 * 
	 * Bitte entschuldigt diesen grässlichen Code, ich war in eile und wollte einfach etwas funktionierendes
	 * 
	 * */

	public static void main(String[] args) {
		
		// Sie koennen die main Methode beliebig aendern
		
		String s = "cbbbbba";
		String t = "cabb";
		int abstand = 1;
		
		boolean istEnthalten = enthalten(s, t, abstand);

		System.out.println(istEnthalten);
	}

	public static boolean enthalten(String s, String t, int abstand) {
		
		if(t.length() == 0) {
			return true;
		}
		
		// generate all possible orders of t
		
		return generateOptions(t.toCharArray(), new char[] {}, s, abstand);
	}
	
	public static boolean generateOptions(char[] input, char[] previous, String s, int abstand) {
		for(int a = 0; a < input.length; a++) {
			// System.out.print(input[a] + ", ");
			
			// new input generation
			char[] newInput = new char[input.length -1];
			int cursor = 0;
			for(int b = 0; b < input.length; b++) {
				if(a != b) {
					newInput[cursor] = input[b];
					cursor++;
				}
			}
			
			// new previous generation
			char[] newPrevious = new char[previous.length +1];
			for(int b = 0; b < previous.length; b++) {
				newPrevious[b] = previous[b];
			}
			newPrevious[newPrevious.length -1] = input[a];
			
			// if the permutation is done, check if it is in the string s
			if(newInput.length == 0) {
				// System.out.println(Arrays.toString(newPrevious));
				permutations++;
				if(testPermutation(newPrevious, s, abstand)) {
					return true;
				}
				// System.out.println("Works: " + testPermutation(newPrevious, s, abstand));
			}
			if(generateOptions(newInput, newPrevious, s, abstand)) {
				return true;
			}
			
		}
		// System.out.println();
		return false;
	}
	
	public static boolean testPermutation(char[] input, String s, int abstand) {
		
		// generate all space permutations (by counting up in base abstand)
		int[] spaces = new int[input.length-1];
		// System.out.println("Count up to: " +  Math.pow((double) abstand+1.0, (double) input.length-1));
		for(int a = 0; a < Math.pow((double) abstand+1.0, (double) input.length-1); a++) {
			int spaceSum = 0;
			for(int b = 0; b < input.length-1; b++) {
				spaces[b] = (int) ((double) a / Math.pow((double) abstand+1.0, (double) b));
				spaces[b] = spaces[b] % (abstand+1);
				spaceSum += spaces[b];
			}
			// System.out.println(Arrays.toString(spaces));
			// Actually Test if the case works
			char[] sSplitUp = s.toCharArray();
			int fitsInWord = (spaceSum + input.length) <= s.length() ? s.length() - (spaceSum + input.length) +1 : 0;
			// System.out.println("Fits in word: " + fitsInWord);
			for(int b = 0; b < fitsInWord; b++) {
				boolean works = true;
				int cursor = b;
				for(int c = 0; c < input.length; c++) {
					if(sSplitUp[cursor] != input[c]) {
						works = false;
					}
					if( c != input.length-1) {
						cursor += spaces[c];	
					}
					cursor++;
				}
				if(works) {
					return true;
					//System.out.println("It Works! Spaces: " + Arrays.toString(spaces) + ", Letters: " + Arrays.toString(input));
				}
			}
		}
		if(input.length == 1) {
			if(s.indexOf(input[0]) > -1) {
				return true;
			}
		}
		return false;
	}
	
}

