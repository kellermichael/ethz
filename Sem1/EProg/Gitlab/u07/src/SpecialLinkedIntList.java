
/**
 * A singly-linked list of integer values with fast addFirst and addLast methods
 */
public class SpecialLinkedIntList {
    
    SpecialIntNode first;
    SpecialIntNode last;
    
    /**
     * Appends 'value' at the end of the list.
     */
    void addLast(int value) {
    	
    	// Aendern Sie diese Methode nicht.
    
        SpecialIntNode newNode = new SpecialIntNode(value);
        if(first == null) {
        	first = newNode;
        } else {
        	last.next = newNode;
        }
        last = newNode;
    }
    
    
    SpecialLinkedIntList split(int zahl) {
    	// init vars
    	SpecialIntNode cursor = first;
    	SpecialLinkedIntList result = new SpecialLinkedIntList();
    	result.first = null;
    	result.last = null;
    	first = null;
    	last = null;
    	while(cursor != null) {
    		cursor.oldNext = cursor.next;
    		if(cursor.value > zahl) {
    			if(result.first == null) {result.first = cursor; result.last = cursor;}
    			else {result.last.next = cursor; result.last = cursor;}
    		} else {
    			if(first == null) {first = cursor; last = cursor;}
    			else {last.next = cursor; last = cursor;}
    		}
    		cursor = cursor.next;
    	}
    	if(last != null) {last.next = null;}
    	if(result.last != null) {result.last.next = null;}
    	return result;
    }
    
    /** Printet die Liste wenn man den 'next' Feldern folgt. */
    void printList() {
    	
    	// Sie koennen diese Methode beliebig aendern.
    	
    	SpecialIntNode n = first;
    	System.out.print("[");
    	if (n != null) {
    		System.out.print(n.value);
    		n = n.next;
    		while(n != null) {
    			System.out.print(", " + n.value);
    			n = n.next;
    		}
    	}
    	System.out.print("]");
    	System.out.println();
    }
    
    /** Printet die Liste wenn man den 'oldNext' Feldern folgt. */
    void printOldList() {
    	
    	// Sie koennen diese Methode beliebig aendern.
    	
    	SpecialIntNode n = first;
    	System.out.print("[");
    	if (n != null) {
    		System.out.print(n.value);
    		n = n.oldNext;
    		while(n != null) {
    			System.out.print(", " + n.value);
    			n = n.oldNext;
    		}
    	}
    	System.out.print("]");
    	System.out.println();
    }
    
}
