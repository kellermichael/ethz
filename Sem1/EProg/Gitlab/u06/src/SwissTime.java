public class SwissTime {

    public static void main(String[] args) {
        // Die Beispiele vom Übungsblatt:
        System.out.println("00:00 -> " + toSwissGerman("00:00"));
        System.out.println("01:45 -> " + toSwissGerman("01:45"));
        System.out.println("09:25 -> " + toSwissGerman("09:25"));
        System.out.println("12:01 -> " + toSwissGerman("12:01"));
        System.out.println("16:46 -> " + toSwissGerman("16:46"));
        System.out.println("21:51 -> " + toSwissGerman("21:51"));
        System.out.println("22:37 -> " + toSwissGerman("22:37"));
    }

    public static String toSwissGerman(String time) {
        int hours = Integer.valueOf(time.split(":")[0]);
        int minutes = Integer.valueOf(time.split(":")[1]);
        
        String SwissTime = "";
        
        // determine minutes expression
        if(minutes > 0) {
        	if(minutes == 15 || minutes == 30 || minutes == 45) {
        		switch(minutes) {
	        		case 15:
	        			SwissTime += "viertel ab";
	        			break;
	        		
		        	case 30:
		    			SwissTime += "halbi";
		    			hours++;
		    			break;
		    		
			        case 45:
						SwissTime += "viertel vor";
						hours++;
						break;
        		}
        	} else if(minutes <= 24) {
        		SwissTime += String.valueOf(minutes) + " ab";
        	} else if(minutes > 24 && minutes < 30) {
        		SwissTime += String.valueOf(5-(minutes-25)) + " vor halbi";
        		hours++;
        	} else if(minutes > 30 && minutes < 40) {
        		SwissTime += String.valueOf(minutes-30) + " ab halbi";
        		hours++;
        	} else {
        		SwissTime += String.valueOf(20-(minutes-40)) + " vor";
        		hours++;
        	}
        }
        
        // determine hours expression
        switch(hours % 12) {
        case 0:
        	SwissTime += " 12i";
        	break;
        case 1: 
        	SwissTime += " 1";
        	break;
        case 2: 
        	SwissTime += " 2";
        	break;
        default:
        	SwissTime += " " + String.valueOf(hours%12) + "i";
        	break;
        }
        
        // determine daytime expression
        if(hours < 5 || hours >= 23) {
        	SwissTime += " znacht";
        } else if(hours >= 5 && hours < 12) {
        	SwissTime += " am morge";
        } else if(hours == 12) {
        	SwissTime += " am mittag";
        } else if(hours > 12 && hours <= 17) {
        	SwissTime += " am namittag";
        } else {
        	SwissTime += " am abig";
        }
        
        // remove leading spaces
        SwissTime = SwissTime.trim();
        return SwissTime;
    }
}
