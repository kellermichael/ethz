import gui.Window;
import gui.Color;

public class Uhr {
    private static final int SIZE = 400;

    public static void main(String[] args) {
        Window window = new Window("Uhr", SIZE, SIZE);
        window.open();
        
        while(window.isOpen()) {
            // TODO: draw clock
        	double center = SIZE/2.0;
        	double time = System.currentTimeMillis();
        	
        	// Prettify
        	window.setRoundStroke(true);
        	window.setStrokeWidth(7);
        	window.setColor(new Color(0, 0, 0));
        	window.drawCircle(center, center, center-20);
        	window.setStrokeWidth(10);
        	window.drawCircle(center, center, 5);
        	
        	// calculate hours hand
        	double hours = Math.floor(((time/1000.0) % 86400.0)/3600.0);
        	// add +1h for utc
        	hours++;
        	hours = hours % 12.0;
        	double gamma = (hours/12)*2.0*Math.PI;
        	double lengthHours = SIZE/2.0 - 100.0;
        	double x2Hours = Math.sin(gamma)*lengthHours;
        	double y2Hours =  -Math.cos(gamma)*lengthHours;
        	window.setColor(new Color(0, 0, 0));
        	window.setStrokeWidth(15);
        	window.drawLine(center, center, center+x2Hours, center+y2Hours);
        	
        	
        	// calculate minutes hand
        	double minutes = Math.floor(((time/1000.0) % 3600.0)/60.0);
        	double beta = (minutes/60)*2.0*Math.PI;
        	double lengthMinutes = SIZE/2.0 - 60.0;
        	double x2Minutes = Math.sin(beta)*lengthMinutes;
        	double y2Minutes =  -Math.cos(beta)*lengthMinutes;
        	window.setColor(new Color(0, 0, 0));
        	window.setStrokeWidth(10);
        	window.drawLine(center, center, center+x2Minutes, center+y2Minutes);
        	
        	
        	
        	// calculate seconds hand
        	double seconds = (time/1000.0) % 60.0;
        	double alpha = (seconds/60)*2.0*Math.PI;
        	double lengthSeconds = SIZE/2.0 - 40.0;
        	double x2Seconds = Math.sin(alpha)*lengthSeconds;
        	double y2Seconds =  -Math.cos(alpha)*lengthSeconds;
        	window.setColor(new Color(209, 10, 6));
        	window.setStrokeWidth(7);
        	window.drawLine(center, center, center+x2Seconds, center+y2Seconds);
        	window.setStrokeWidth(15.0);
        	window.drawOval(center+x2Seconds-7.5, center+y2Seconds-7.5, 15.0, 15.0);
        	
        	
        	
        	
            
            // display everything and then fill the canvas with white:
            window.refreshAndClear(20);
        }
    }
}
