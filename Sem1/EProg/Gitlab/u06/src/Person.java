/**
 * Eine Person mit diversen Eigenschaften, inkl. Alter, Gewichts, Grösse, Geschlecht und einigen
 * spezielleren Körper-Eigenschaften.
 */
public class Person {
    
    int Age;
    double Weight;
    double Height;
    boolean IsMale;
    double ShoulderWidth;
    double ChestDepth;
    double ChestWidth;
    Person(int alter, double gewicht, double groesse, boolean istMaennlich,
            double schulterBreite, double brustTiefe, double brustBreite) {
    	Age = alter;
        Weight = gewicht;
        Height = groesse;
        IsMale = istMaennlich;
        ShoulderWidth = schulterBreite;
        ChestDepth = brustTiefe;
        ChestWidth = brustBreite;
    }
    
    String beschreibung() {
    	String sex = IsMale ? "m" : "f";
        return "Person (" + sex + ", " + Integer.toString(Age) + " Jahre, " 
    			+ Double.toString(Height) + " cm, " + Double.toString(Weight) + " kg)";
    }
    
    double bodyMassIndex() {
        return (Weight / Math.pow(Height/100, 2));
    }
}
