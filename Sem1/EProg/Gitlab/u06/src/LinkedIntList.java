public class LinkedIntList {
	private IntNode First;
	private IntNode Last;
	int Size;
	
	// Frage: Weshalb ist das Last Element Notwendig?
	public LinkedIntList() {
		First = null;
		Last = null;
		Size = 0;
	}
	
	public void addFirst(int a) {
		IntNode newNode = new IntNode(a, First);
		First = newNode;
		Size++;
	}
	
	public int removeFirst() {
		if(Size > 0) {
			int firstValue = First.Value;
			First = First.Next;
			Size--;
			return firstValue;
		} else {
			Errors.error("Can't remove the first Element because it does not exist.");
			return 0;
		}
	}
	
	public int removeLast() {
		if(Size > 0) {
			int lastValue = Last.Value;
			IntNode currentNode = First;
			while(currentNode.Next != null) {
				currentNode = currentNode.Next;
			}
			Last = currentNode;
			Size--;
			return lastValue;
		} else {
			Errors.error("Can't remove the last Element because it does not exist.");
			return 0;
		}
	}
	
	public void clear() {
		First = null;
		Last = null;
		Size = 0;
	}
	
	public boolean isEmpty() {
		if(First == null) {
			return true;
		} else {
			return false;
		}
	}
	
	public int get(int position) {
		if(position < Size) {
			IntNode cursor = First;
			for(int a = 0; a < position; a++) {
				cursor = cursor.Next;
			}
			return cursor.Value;
		} else {
			Errors.error("Can't get this Element because it does not exist.");
			return 0;
		}
	}
	
	
	public void set(int position, int value) {
		if(position < Size) {
			IntNode cursor = First;
			for(int a = 0; a < position; a++) {
				cursor = cursor.Next;
			}
			cursor.Value = value;
		} else {
			Errors.error("Can't set this Element because it does not exist.");
		}
	}
	
	
	public void printList() {
		String result = "[ ";
		System.out.print("Current List State: ");
		for(IntNode currentElement = First; currentElement != null; currentElement = currentElement.Next) {
			result += currentElement.Value;
			if(currentElement.Next != null) {
				result += ", ";
			} else {
				result += " ";
			}
		}
		result += "]";
		System.out.println(result);
	}
	
	// Demonstrate the functionality Of the List
	public static void main(String[] args) {
		LinkedIntList myLinkedList = new LinkedIntList();
		
		// Initialize List
		System.out.println("Initialize this List");
		myLinkedList.addFirst(1);
		myLinkedList.addFirst(2);
		myLinkedList.addFirst(3);
		myLinkedList.addFirst(4);
		myLinkedList.addFirst(5);
		myLinkedList.printList();
		
		// Remove an Element
		System.out.println("Remove the first element");
		System.out.println(myLinkedList.removeFirst());
		myLinkedList.printList();
		
		// Clear the list
		System.out.println("Clear the list");
		myLinkedList.clear();
		myLinkedList.printList();
		
		// Check if the List is empty
		System.out.println("Is the List Empty? " + myLinkedList.isEmpty());
		
		// Initialize List
		System.out.println("Initialize this List");
		myLinkedList.addFirst(1);
		myLinkedList.addFirst(2);
		myLinkedList.addFirst(3);
		myLinkedList.addFirst(4);
		myLinkedList.addFirst(5);
		myLinkedList.printList();
		
		// Get An Element
		System.out.println("Get the second Element");
		myLinkedList.printList();
		System.out.println("Second Element: " + myLinkedList.get(1));
		
		// Set An Element
		System.out.println("Set the fourth Element to 9");
		myLinkedList.set(3, 9);
		myLinkedList.printList();
		
		
	}
}
