import java.util.Scanner;

public class Echo {
	public static void main(String[] args) {
		LinkedIntList ll = new LinkedIntList();
		Scanner scanner = new Scanner(System.in);
		System.out.print("Geben Sie eine Zahl ein: ");
		while(scanner.hasNextInt()) {
			ll.addFirst(scanner.nextInt());
			System.out.print("Geben Sie eine Zahl ein: ");
		}
		System.out.println("Ungültige Eingabe");
		ll.printList();
		scanner.close();
	}
}
