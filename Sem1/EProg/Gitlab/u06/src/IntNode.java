
public class IntNode {
	int Value;
	IntNode Next;
	public IntNode(int value, IntNode next) {
		Value = value;
		Next = next;
	}
}
