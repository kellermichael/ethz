import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

/* 
 * Author: Maximiliana Muster
 * für Einführung in die Programmierung
 * 
 * Analysiert einen Datensatz von Personen-Eigenschaften. Findet "ungesunde" Personen und teilt
 * alle Personen in Trainingspartner-Paare ein.
 */
public class PersonenAnalyse {
    
    public static void main(String[] args) throws FileNotFoundException {
    	Scanner scanner = new Scanner(new File("body.dat.txt"));
    	Person[] people = liesPersonen(scanner);
    	PrintStream printstream = new PrintStream(new File("ungesund.txt"));
    	druckeUngesunde(people, printstream);
    	druckeGuteTrainingsPartner(people);
    }

    /**
     * Liest die Personen-Daten von dem gegebenen Scanner ein und gibt sie als Person[] zurück.
     */
    static Person[] liesPersonen(Scanner scanner) {
        int noOfPeople = scanner.nextInt();
        scanner.nextLine();
        Person[] people = new Person[noOfPeople];
        for(int a = 0; a < noOfPeople; a++) {
        	double schulterBreite = scanner.nextDouble();
            double brustTiefe = scanner.nextDouble();
            double brustBreite = scanner.nextDouble();
        	int alter = scanner.nextInt();
        	double gewicht = scanner.nextDouble();
        	double groesse = scanner.nextDouble();
        	boolean istMaennlich = scanner.nextInt() == 1 ? true : false;
        	people[a] = new Person(alter, gewicht, groesse, istMaennlich, schulterBreite, brustTiefe, brustBreite);
        }
        return people;
    }
    
    /**
     * Findet alle Personen, die nicht normalgewichtig sind und gibt deren Beschreibungen, sowie die
     * entsprechende Gewichts-Klassen aus.
     */
    static void druckeUngesunde(Person[] personen, PrintStream ausgabe) {
        for(int a = 0; a < personen.length; a++) {
        	double bmi = personen[a].bodyMassIndex();
        	if(bmi < 18.5 || bmi >= 25) {
        		String unhealthyPerson = personen[a].beschreibung();
        		String classification = "";
        		if(bmi < 18.5) { classification = "ist untergewichtig";}
        		if(bmi >= 25.0 && bmi < 30.0) { classification = "ist übergewichtig";}
        		if(bmi >= 30.0) { classification = "ist fettleibig";}
        		// System.out.println(unhealthyPerson + " " + classification);
        		ausgabe.append(unhealthyPerson + " " + classification + System.lineSeparator());
        	}
        }
    }

    /**
     * Gibt die Partner-Qualität von zwei Trainingspartner p1 und p2 zurück. Wert liegt zwischen 0
     * (schlechtester Wert) und 1 (bester Wert).
     */
    static double partnerQualitaet(Person p1, Person p2) {
        double sizeDiff = p1.Height - p2.Height;
        double chestDiff = p1.ChestDepth*p1.ChestWidth - p2.ChestDepth*p2.ChestWidth;
        double shoulderDiff = p1.ShoulderWidth - p2.ShoulderWidth;
        double quality = 1.0 / (1.0 +Math.pow(sizeDiff, 2.0) + Math.abs(chestDiff)/5.0 + Math.pow(shoulderDiff, 2.0)/2.0);
        return quality;
    }
    
    // druckt alle guten trainingspartner aus
    static void druckeGuteTrainingsPartner(Person[] personen) {
    	for(int a = 0; a < personen.length; a++) {
    		for(int b = a+1; b < personen.length; b++) {
    			double quality = partnerQualitaet(personen[a], personen[b]);
    			if(quality > 0.8) {
    				System.out.println(personen[a].beschreibung());
    				System.out.println(personen[b].beschreibung());
    				System.out.println("Qualität: " + quality);
    				System.out.println();
    			}
    		}
    	}
    }
    
    
    
    
    
    
}