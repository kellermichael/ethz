 import static org.junit.Assert.*;

import org.junit.Test;

public class BlackBoxTest {

	@Test
	public void testRotateArray() {
		// Basic
	    int[] values = new int[] { 1, 2 };
		int[] expected = new int[] { 2, 1 };
		BlackBox.rotateArray(values, 1);
		assertArrayEquals(expected, values);
	}
	
	@Test
	public void testRotateNoElements() {
	    int[] values = new int[] {};
		int[] expected = new int[] {};
		BlackBox.rotateArray(values, 1);
		assertArrayEquals(expected, values);
	}
	
	@Test
	public void testRotateSimple() {
	    int[] values = new int[] { 1, 2, 3 };
		int[] expected = new int[] { 3, 1, 2 };
		BlackBox.rotateArray(values, 1);
		assertArrayEquals(expected, values);
	}
	
	@Test
	public void testRotateSame() {
	    int[] values = new int[] { 1, 2, 3 };
		int[] expected = new int[] { 1, 2, 3 };
		BlackBox.rotateArray(values, 3);
		assertArrayEquals(expected, values);
	}
	
	@Test
	public void testRotateSameElements() {
	    int[] values = new int[] { 1, 2, 3, 3, 3 };
		int[] expected = new int[] { 3, 3, 1, 2, 3 };
		BlackBox.rotateArray(values, 2);
		assertArrayEquals(expected, values);
	}
	
	@Test
	public void testRotateMultipleTimes() {
	    int[] values = new int[] { 1, 2, 3, 4 };
		int[] expected = new int[] { 3, 4, 1, 2 };
		BlackBox.rotateArray(values, 10);
		assertArrayEquals(expected, values);
	}
	
	@Test
	public void testRotateZero() {
	    int[] values = new int[] { 1, 2, 3, 3, 3 };
		int[] expected = new int[] { 1, 2, 3, 3, 3 };
		BlackBox.rotateArray(values, 0);
		assertArrayEquals(expected, values);
	}
	
	@Test
	public void testRotateNegative() {
	    int[] values = new int[] { 1, 2, 3, 3, 3 };
		int[] expected = new int[] { 2, 3, 3, 3, 1 };
		BlackBox.rotateArray(values, -1);
		assertArrayEquals(expected, values);
	}
	
	@Test
	public void testRotateSameBack() {
	    int[] values = new int[] { 1, 2, 3, 3, 3 };
		int[] expected = new int[] { 1, 2, 3, 3, 3 };
		BlackBox.rotateArray(values, -5);
		assertArrayEquals(expected, values);
	}
	
	@Test
	public void testRotateBackLarge() {
	    int[] values = new int[] { 1, 2, 3, 3, 3 };
		int[] expected = new int[] { 3, 3, 3, 1, 2 };
		BlackBox.rotateArray(values, -7);
		assertArrayEquals(expected, values);
	}
}
