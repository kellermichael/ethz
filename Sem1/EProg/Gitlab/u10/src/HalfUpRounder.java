
public class HalfUpRounder implements RundungStrategy {
	public int round(double a) {
		if(a > 0.0) {
			double tmp = a - (double) (int) a;
			if(tmp >= 0.5) {
				return (int) a + 1;
			} else {
				return (int) a;
			}
		} else {
			double tmp = Math.abs(a + (double) (int) Math.abs(a));
			if(tmp <= 0.5) {
				return (int) a;
			} else {
				return (int) a-1;
			}
		}
	}
}
