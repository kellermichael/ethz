
// Veraendern Sie keine der vorhandenen Deklarationen.
// Sie koennen und sollen die Implementationen hinzufuegen.

interface RundungStrategy {
	// TODO: Definieren Sie die noetigen Methoden
	public int round(double a);
}

class RundungStrategyFactory {
	
	public RundungStrategyFactory() {
		// Hier koennen Sie ebenfalls Code hinzufuegen
	}
	
	RundungStrategy getHalfUpRunder() {
		// TODO: Geben Sie einen Runder zurueck, welcher ab 0.5 hoch rundet
		return new HalfUpRounder(); // solution
	}
	
	RundungStrategy getDownRunder() {
		// TODO: Geben Sie einen Runder zurueck, welcher runter rundet.
		return new DownRounder(); // solution
	}
}

