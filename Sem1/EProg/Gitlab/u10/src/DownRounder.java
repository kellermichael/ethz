
public class DownRounder implements RundungStrategy {
	public int round(double a) {
		if(a > 0.0) {
			return (int) a;
		} else {
			return (int) a - 1;
		}
	}
}
