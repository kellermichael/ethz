
public class SainteLague implements SitzzuteilungStrategy {
	double Start = 0.5;
	double Distance = 1.0;
	public double getDivisor(int a) {
		double tmp = Start;
		for(int b = 0; b < a; b++) {
			tmp += Distance;
		}
		return tmp;
	}
}
