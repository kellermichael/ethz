import java.util.Arrays;

class Oberzuteilung implements Zuteilung {
	
	private WahlDaten daten;
	private int[] sitzeProPartei;
	private boolean istFertig;
	
	private SitzzuteilungStrategy verteilung;
	
	public Oberzuteilung(WahlDaten daten, SitzzuteilungStrategy verteilung) {
		this.daten = daten;
		this.verteilung = verteilung;
		this.sitzeProPartei = null;
		this.istFertig = false;
	}
	
	// Returned die Oberzuteilung, heisst die verfuegbaren Sitze pro Partei
	public int[] getSitzeProPartei() {
		if (!istKorrekt()) {
			anpassen();
		}
		return sitzeProPartei;
	}
	
	public WahlDaten getWahlDaten() {
		return daten;
	}

	@Override
	public boolean istKorrekt() {
		return istFertig;
	}
	
	@Override
	public void anpassen() {
		if (!istKorrekt()) {
			// TODO: Berechnen Sie die Sitze pro Partei
			// Tipp: Verwenden Sie 'Hoechstzahlverfahren.berechneParteiSitze'
			// init vars
			int[] stimmenProPartei = new int[daten.getAnzahlParteien()];
			int totaleSitze = 0;
			sitzeProPartei = new int[daten.getAnzahlParteien()];
			
			// calculate no of votes every party got
			for(int a = 0; a < daten.getAnzahlWahlkreise(); a++) {
				for(int b = 0; b < daten.getAnzahlParteien(); b++) {
					stimmenProPartei[b] += daten.getStimmen(a, b);
				}
				totaleSitze += daten.getSitzeProWahlkreis(a);
			}
			sitzeProPartei = new Hoechstzahlverfahren().berechneParteiSitze(totaleSitze, stimmenProPartei, verteilung);
		}
	}
}
