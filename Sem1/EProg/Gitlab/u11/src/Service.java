import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Service {
	
	ArrayList<double[]> data;

    public Service(Scanner scanner) {
        // TODO
    	data = new ArrayList<double[]>();
    	
    	
    	while(scanner.hasNext()) {
    		// String[] tmp = scanner.nextLine().split("\\s+");
    		double[] student = new double[4];
    		student[0] = scanner.nextDouble();
    		student[1] = scanner.nextDouble();
    		student[2] = scanner.nextDouble();
    		student[3] = scanner.nextDouble();
    		System.out.println(Arrays.toString(student));
    		data.add(student);
    	}
    }

    public List<Integer> critical(double bound1, double bound2) {
        List<Integer> criticalStudents = new ArrayList<Integer>();
        for(double[] student: data) {
        	if(student[1] <= bound1 && (student[2] + student[3]) < bound2) {
        		criticalStudents.add((int)student[0]);
        	}
        }
        return criticalStudents;
    }

    public List<Integer> top(int limit) {
    	List<double[]> avgs = new ArrayList<double[]>();
    	List<Integer> result = new ArrayList<Integer>();
        for(double[] student: data) {
        	avgs.add(new double[] {student[0], (student[1] + student[2] + student[3])});
        }
        Collections.sort(avgs, new Comparator<double[]>() {

			@Override
			public int compare(double[] o1, double[] o2) {
				// TODO Auto-generated method stub
				return Double.compare(o2[1], o1[1]);
			}
        });
        for(int a = 0; a < (limit < data.size() ? limit: data.size()); a++) {
        	result.add((int) avgs.get(a)[0]);
        }
        return result;
    }
}