package wahlen;

// Veraendern Sie keine der vorhandenen Deklarationen.
// Sie koennen und sollen die Implementationen hinzufuegen.

interface RundungStrategy {
	// TODO: Definieren Sie die noetigen Methoden
	int round(double x); // solution
}

class RundungStrategyFactory {
	
	public RundungStrategyFactory() {
		// Hier koennen Sie ebenfalls Code hinzufuegen
	}
	
	RundungStrategy getHalfUpRunder() {
		// TODO: Geben Sie einen Runder zurueck, welcher ab 0.5 hoch rundet
		return new HalfUpRunder(); // solution
	}
	
	RundungStrategy getDownRunder() {
		// TODO: Geben Sie einen Runder zurueck, welcher runter rundet.
		return new FloorRunder(); // solution
	}
}

//solution
class HalfUpRunder implements RundungStrategy {
	public int round(double x) {
		return (int) Math.round(x);
	}
}

//solution
class FloorRunder implements RundungStrategy {
	public int round(double x) {
		return (int) Math.floor(x);
	}
}
