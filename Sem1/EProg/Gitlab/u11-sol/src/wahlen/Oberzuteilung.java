package wahlen;

class Oberzuteilung implements Zuteilung {
	
	private WahlDaten daten;
	private int[] sitzeProPartei;
	private boolean istFertig;
	
	private SitzzuteilungStrategy verteilung;
	
	public Oberzuteilung(WahlDaten daten, SitzzuteilungStrategy verteilung) {
		this.daten = daten;
		this.verteilung = verteilung;
		this.sitzeProPartei = null;
		this.istFertig = false;
	}
	
	// Returned die Oberzuteilung, heisst die verfuegbaren Sitze pro Partei
	public int[] getSitzeProPartei() {
		if (!istKorrekt()) {
			anpassen();
		}
		return sitzeProPartei;
	}
	
	public WahlDaten getWahlDaten() {
		return daten;
	}

	@Override
	public boolean istKorrekt() {
		return istFertig;
	}
	
	@Override
	public void anpassen() {
		if (!istKorrekt()) {
			// TODO: Berechnen Sie die Sitze pro Partei
			// Tipp: Verwenden Sie 'Hoechstzahlverfahren.berechneParteiSitze' 
			// solution
			int[] stimmenProPartei = new int[daten.getAnzahlParteien()];
			for (int i = 0; i < daten.getAnzahlParteien(); i += 1) {
				for (int j = 0; j < daten.getAnzahlWahlkreise(); j += 1) {
					stimmenProPartei[i] += daten.getStimmen(j, i);
				}
			}
			
			int anzahlSitze = 0;
			for (int i = 0; i < daten.getAnzahlWahlkreise(); i += 1) {
				anzahlSitze += daten.getSitzeProWahlkreis(i);
			}
			
			sitzeProPartei = Hoechstzahlverfahren.berechneParteiSitze(anzahlSitze, stimmenProPartei, verteilung);
			istFertig = true;
		}
	}
}
