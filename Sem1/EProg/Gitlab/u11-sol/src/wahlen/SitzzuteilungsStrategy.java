package wahlen;

// Veraendern Sie keine der vorhandenen Deklarationen.
// Sie koennen und sollen die Implementationen hinzufuegen.

interface SitzzuteilungStrategy /* solution */ extends HoechstzahlStrategy {
	// TODO: Definieren Sie die noetigen Methoden
}

class SitzzuteilungStrategyFactory {
	
	public SitzzuteilungStrategyFactory() {
		// Hier koennen Sie ebenfalls Code hinzufuegen
	}
	
	SitzzuteilungStrategy getSainteLague() {
		// TODO: Geben Sie hier eine Instanz der Sainte-Lague Implementation zurueck.
		// Zur Erinnerung, Sainte-Lague ist ein Hoechstzahl-Verfahren mit der Divisorfolge 0.5, 1.5, 2.5, ...
		return new SainteLague(); // solution
	}
	
	SitzzuteilungStrategy getDHondt() {
		// TODO: Geben Sie hier eine Instanz der D'Hondt Implementation zurueck.
		// Zur Erinnerung, D'Hondt ist ein Hoechstzahl-Verfahren mit der Divisorfolge 1, 2, 3, ...
		return new DHondt(); // solution
	}
}

//solution
class SainteLague implements SitzzuteilungStrategy {
	public double  getDivisor(int ith) { return 0.5 + ith; }
}

//solution
class DHondt implements SitzzuteilungStrategy {
	public double  getDivisor(int ith) { return ith + 1; }
}


