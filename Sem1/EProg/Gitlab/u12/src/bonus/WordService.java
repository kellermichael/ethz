package bonus;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class WordService {
	
	LinkedList<MyWord> words = new LinkedList<MyWord>();

	public static void main(String[] args) throws FileNotFoundException {
		WordScanner input = new WordScanner(new File("loremIpsum.txt"));
		
		WordService service = new WordService(input);
		
		for (MyWord word : service.getWords()) {
			System.out.println(word);
		}
		
		System.out.println("----------------------------------");
		
	    List<MyWord> topWords = service.top(3);
	    
	    for (MyWord word : topWords) {
			System.out.println(word);
		}
	}
	
	
	public WordService(WordScanner scanner) {
		// TODO: lesen Sie den Text von dem Scanner und initialisieren Sie das Objekt
		try {
			int position = 0;
			while(scanner.hasNext()) {
				String nextWord = scanner.next();
				boolean alreadyIndexed = false;
				for(int a = 0; a < words.size(); a++) {
					if(words.get(a).getWord().equals(nextWord)) {
						words.get(a).updatePositions(position);
						alreadyIndexed = true;
						break;
					}
				}
				if(!alreadyIndexed) {
					Set<Integer> init = new HashSet<Integer>();
					init.add(position);
					words.add(new MyWord(nextWord, init));
				}
				position++;
			}
		} catch(Exception e) {
			throw new IllegalArgumentException();
		}
	}
	
	public List<MyWord> getWords() {
		// TODO: geben Sie eine Liste mit den Woertern des Textes aus$
		/*
		LinkedList<String> allWords = new LinkedList<String>();
		for(int a = 0; a < words.size(); a++) {
			allWords.add(words.get(a).getWord());
		}
		*/
		return words;
	}
	
	public List<MyWord> top(int number) {
		// TODO: geben Sie die number (Anzahl) groessten Woerter zurueck. 
		//       Wobei Groesse durch die definierte Ordnung aus dem Aufgabenblatt bestimmt ist.
		LinkedList<MyWord> result = new LinkedList<MyWord>();
		try {
		MyWord[] tmp = words.toArray(new MyWord[words.size()]);
		// System.out.println(Arrays.toString(words.toArray()));
		System.out.println(Arrays.toString(tmp));
		Arrays.sort(tmp);
		for(int a = 0; a < (number < words.size() ? number: words.size()); a++) {
			result.add(tmp[a]);
		}
		System.out.println(Arrays.toString(tmp));
		} catch(Exception e) {
			throw new IllegalArgumentException();
		}
		return result;
	}
}
