package bonus;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class MyWord implements Comparable<MyWord> {
	
	private String word;
	private Set<Integer> positions;
	private int size;
	
	public MyWord(String word, Set<Integer> positions) {
		this.word = word;
		this.positions = positions;
		this.size = 0;
	}
	
	public String getWord() {  
		return word;
	}
	
	public Set<Integer> getPositions() {
		return new HashSet<Integer>(positions);
	}
	
	public void updatePositions(int newPos) {
		positions.add(newPos);
		Integer[] posA = positions.toArray(new Integer[positions.size()]);
		Arrays.sort(posA);
		size = posA[posA.length-1] - posA[0];
	}
	
	@Override
	public String toString() {
		return word + ": " + positions.toString();
	}

	@Override
	public int compareTo(MyWord o) {
		// TODO: implementieren Sie die Ordnung wie auf dem Aufgabenblatt beschrieben
		if(size == o.size) {
			return 0;
		}
		return size > o.size ? -1 : 1;
	}
}
