import java.util.InputMismatchException;
import java.util.Scanner;
/*
 * Author: 
 * 
 * 
 * Dieses Programm berechnet die Reihe 1/1^2 + 1/2^2 + ... + 1/N^2
 */
public class Reihe {

	public static void main(String[] args) {
		// Get number from user
		Scanner scanner = new Scanner(System.in);
		int n = -1;
		while(n < 0) {
			System.out.print("Gib eine Zahl ein welche Element von N0 ist: ");
			try {
				n = scanner.nextInt();
			} catch (InputMismatchException e) {
				scanner.nextLine();
			}
		}
		scanner.close();
		
		// convert n to double
		double N = n/1.0;
		
		// do the math
		double sum = 1;
		for(double i = 1; i < N; i++) {
			sum += 1.0/((i+1.0)*(i+1.0));
		}
		
		// print result
		System.out.println("Das Resultat ist: " + sum);
	}

}
