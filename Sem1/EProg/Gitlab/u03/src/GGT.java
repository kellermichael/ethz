import java.util.InputMismatchException;
import java.util.Scanner;
/*
 * Author: 
 * 
 * 
 * Dieses Programm berechnet den GGT von zwei ganzen Zahlen.
 */
public class GGT {

	public static void main(String[] args) {
		// Get numbers X and Y from user
		Scanner scanner = new Scanner(System.in);
		int x = -1;
		int y = -1;
		while(x <= 0) {
			System.out.print("Gib eine Zahl X ein welche Element von N ist: ");
			try {
				x = scanner.nextInt();
			} catch (InputMismatchException e) {
				scanner.nextLine();
			}
		}
		while(y <= 0) {
			System.out.print("Gib eine Zahl Y ein welche Element von N ist: ");
			try {
				y = scanner.nextInt();
			} catch (InputMismatchException e) {
				scanner.nextLine();
			}
		}
		scanner.close();
		
		while(x % y != 0) {
			if(x > y) {
				int tmp = y;
				y = x % y;
				x = tmp;
			} else {
				int tmp = x;
				x = y;
				y = tmp;
			}
		}
		System.out.println("Der GGT ist: " + y);

	}

}
