import java.util.InputMismatchException;
import java.math.*;
import java.util.Scanner;
/*
 * Author: 
 * 
 * 
 * Dieses Programm konvertiert base10  zahlen zu base2 zahlen
 */
public class Binaer {

	public static void main(String[] args) {
		// Get number from user
		Scanner scanner = new Scanner(System.in);
		int n = -1;
		while(n < 0) {
			System.out.print("Gib eine Zahl ein welche Element von N ist: ");
			try {
				n = scanner.nextInt();
			} catch (InputMismatchException e) {
				scanner.nextLine();
			}
		}
		scanner.close();
		
		// get the length of the binary number
		int binLen = 1;
		while(n >= Math.pow(2, binLen)) {
			binLen++;
		}
		
		
		// determine digits
		for(int a = 0; a < binLen; a++) {
			if(Math.pow(2, (binLen-a-1)) <= n) {
				System.out.print("1");
				n -= Math.pow(2, (binLen-a-1));
			} else {
				System.out.print("0");
			}
		}
		
		// print a new line
		System.out.println();
	}

}
