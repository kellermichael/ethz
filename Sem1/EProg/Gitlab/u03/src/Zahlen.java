import java.util.InputMismatchException;
import java.util.Scanner;
/*
 * Author: 
 * 
 * 
 * Dieses Programm liest einen String ein, der eine Siebensegmentanzeige kodiert, und gibt die kodierte Zahl als Integer aus.
 */
public class Zahlen {

	public static void main(String[] args) {
		
		// Get string from user
		Scanner scanner = new Scanner(System.in);
		String number = "";
		System.out.print("Gib eine kodierte Zahl ein: ");
		try {
			number = scanner.nextLine();
		} catch (InputMismatchException e) {
			System.out.print("Gib eine kodierte Zahl ein: ");
			scanner.nextLine();
		}
		scanner.close();
		
		// make string lowercase just in case
		number = number.toLowerCase();
		
		// figure out number
		int i = 0;
		if(number.contains("e")) {
			if(number.length() == 6) {
				if(number.contains("b")) {
					i = 0;
				} else {
					i = 6;
				}
			} else {
				if(number.contains("c")) {
					i = 8;
				} else {
					i = 2;
				}
			}
		} else {
			if(number.contains("d")) {
				if(number.contains("b")) {
					if(number.contains("f")) {
						i = 9;
					} else {
						i = 3;
					}
				} else {
					i = 5;
				}
			} else {
				if(number.contains("a")) {
					i = 7;
				} else if(number.contains("f")) {
					i = 4;
				} else {
					i = 1;
				}
			}
		}
		
		System.out.println(i);

	}

}
