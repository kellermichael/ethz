public class Newton {
	public static void main(String[] args) {
		double c = 2.0;
		double t = 1.0;
		double eps = 0.000000000001;
		while(Math.abs(t*t - c) >= eps) {
			t = ((c/t) + t)/2;
		}
		String formattedSqrt = String.format("%.12f", t);
		System.out.println("Die Wurzel von " + c + " ist " + formattedSqrt + " mit Fehlertoleranz " + eps);
	}
}
