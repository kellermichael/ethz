import java.util.Arrays;

public class Sieb {
	public static void main(String[] args) {
		int limit = 1000;
		Boolean[] primes = generatePrimes(limit);
		int numbOfPrimes = 0;
		for(int c = 0; c <= limit; c++) {
			if(primes[c]) { numbOfPrimes++;}
		}
		System.out.println("Die Anzahl von Primzahlen kleiner gleich dem Limit ist: " + numbOfPrimes);
	}
	
	public static Boolean[] generatePrimes(int limit) {
		// initialize array
		Boolean[] primes = new Boolean[limit +1];
		Arrays.fill(primes, Boolean.TRUE);
		
		// set 0 and 1 to not a prime number
		primes[0] = false;
		primes[1] = false;
		
		// mark all multiples of numbers
		for(int a = 2; a <= limit; a++) {
			if(primes[a]) {
				for(int b = 2*a; b <= limit; b += a) {
					primes[b] = false;
				}
			}
		}
		
		// return primes array
		return primes;
	}
}
