import gui.Window;

public class RealSwissFlag {
	public static void main(String[] args) {
		Window window = new Window("Fahne", 400, 400);
		
		// set Dimensions
		int squareLength = 400;
		int barWidth = 75;
		int borderDistance = 50;
		
		// Set background red
		window.setColor(255, 0, 0);
		window.fillRect(0, 0, squareLength, squareLength);
		
		// Draw first
		window.setColor(255, 255, 255);
		window.fillRect(borderDistance, (squareLength-barWidth)/2, (squareLength-2*borderDistance), barWidth);
		
		
		// Draw first
		window.setColor(255, 255, 255);
		window.fillRect((squareLength-barWidth)/2, borderDistance, barWidth, (squareLength-2*borderDistance));
		
		// open window
        window.open();
        window.waitUntilClosed();
	}
}
