import java.util.HashSet;
import java.util.LinkedList;

public class Labyrinth {
	
	static int maxNumbOfColorsSeen = 0;
	static boolean twoColorIsPossible = false;
	static boolean foundLoop = true;

	public static void main(String[] args) {
		Room a1 = new Room(1);
		Room a2 = new Room(3);
		Room a3 = new Room(4);
		Room a4 = new Room(2);
		Room a5 = new Room(1);
		Room a6 = new Room(5);
		a1.doorsTo.add(a2);
		a1.doorsTo.add(a4);
		a2.doorsTo.add(a3);
		a3.doorsTo.add(a1);
		a4.doorsTo.add(a5);
		a4.doorsTo.add(a6);

		System.out.println(Labyrinth.colorNotSuccessively(a1));
	}

	public static boolean colorExactlyOnce(Room room) {
		// TODO: Geben Sie true zurueck genau dann wenn
		// das Labyrint vom Raum room verlassen werden kann,
		// sodass ein Raum jeder Farbe exakt einmal verwendet wird
		maxNumbOfColorsSeen = 1;
		HashSet<Integer> colorsSeen = new HashSet<Integer>();
		colorsSeen.add(room.getColor());
		for(int a = 0; a < room.doorsTo.size(); a++) {
			recursiveSolve(room.doorsTo.get(a), colorsSeen, 1);
		}
		if(maxNumbOfColorsSeen == 10) {
			return true;
		}
		return false;
	}
	
	public static void recursiveSolve(Room room, HashSet<Integer> colorsSeen, int beenInNumbOfRooms) {
		colorsSeen.add(room.getColor());
		beenInNumbOfRooms += 1;
		if(colorsSeen.size() > maxNumbOfColorsSeen && beenInNumbOfRooms == colorsSeen.size()) {
			maxNumbOfColorsSeen = colorsSeen.size();
		}
		if(beenInNumbOfRooms == colorsSeen.size() && !room.isExit()) {
			for(int a = 0; a < room.doorsTo.size(); a++) {
				recursiveSolve(room.doorsTo.get(a), colorsSeen, beenInNumbOfRooms);
			}
		}
	}

	public static boolean colorNotSuccessively(Room room) {
		twoColorIsPossible = false;
		HashSet<Room> seenRooms = new HashSet<Room>();
		seenRooms.add(room);
		if(room.isExit()) {twoColorIsPossible = true;}
		for(int a = 0; a < room.doorsTo.size(); a++) {
			prevColorRec(room.doorsTo.get(a), room.getColor(), seenRooms);
		}
		return twoColorIsPossible;
	}
	
	public static void prevColorRec(Room room, int prevColor, HashSet<Room> seenRooms) {
		if(twoColorIsPossible) {return;}
		if(prevColor != room.getColor()) {
			if(seenRooms.contains(room)) {return;}
			seenRooms.add(room);
			if(room.isExit()) {
				twoColorIsPossible = true;
				return;
			}
			for(int a = 0; a < room.doorsTo.size(); a++) {
				prevColorRec(room.doorsTo.get(a), room.getColor(), seenRooms);
			}
		}
	}

	public static void removeCycle(Room room) {
		// TODO: Falls Sie von dem Raum room eine Schleife
		// erreichen koennen, dann entfernen Sie
		// genau alle diese Verbindungen, welche für
		// die Schleife benoetigt werden.
		LinkedList<Room> seenRooms = new LinkedList<Room>();
		seenRooms.add(room);
		while(foundLoop) {
			foundLoop = false;
			for(int a = 0; a < room.doorsTo.size(); a++) {
				infSolverRec(room.doorsTo.get(a), seenRooms);
			}
		}
	}
	
	public static void infSolverRec(Room room, LinkedList<Room> seenRooms) {
		
		if(foundLoop || room.isExit()) {
			return;
		} else {
			/*
			for(int a = 0; a < room.doorsTo.size(); a++) {
				if(seenRooms.contains(room.doorsTo.get(a))) {
					foundLoop = true;
					// find the start of the loop
					int loopStart = 0;
					for(int b = 0; b < seenRooms.size(); b++) {
						if(room.doorsTo.get(a) == seenRooms.get(b)) {
							loopStart = b;
						}
					}
					System.out.println(loopStart);
					// remove loop
					Room cursor = seenRooms.get(loopStart);
					for(int b = loopStart; b < seenRooms.size()-1; b++) {
						Room next = seenRooms.get(b+1);
						cursor.doorsTo.remove(b);
						cursor = next;
					}
					return;
				} else {
					seenRooms.add(room);
					infSolverRec(room.doorsTo.get(a), seenRooms);
					
				}
			}
			*/
		}
	}
}
