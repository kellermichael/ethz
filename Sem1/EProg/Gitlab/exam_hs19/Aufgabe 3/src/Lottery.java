import java.util.*;

public class Lottery {
	
	LinkedList<Ticket> tickets = new LinkedList<Ticket>();
	HashSet<Integer> winningNumbers;
	int MaxNumber;

	public Lottery(int maxNumber) {
		MaxNumber = maxNumber;
	}

	public Ticket buyTicket(int[] numbers) {
		Ticket newTicket = new Ticket();
		HashSet<Integer> set = new HashSet<Integer>();
		for(int a = 0; a < numbers.length; a++) {
			if(numbers[a] <= MaxNumber && numbers[a] > 0) {
				set.add(numbers[a]);
			}
		}
		if(set.size() != 6) {
			throw new IllegalArgumentException();
		}
		newTicket.setNumbers(set);
		newTicket.setId(tickets.size() + 1);
		tickets.add(newTicket);
		return newTicket;
	}

	public int soldTickets() {
		return tickets.size();
	}

	public void draw() throws IllegalStateException {
		HashSet<Integer> drawn = new HashSet<Integer>();
		int[] drawnArray = new int[6];
		int pos = 0;
		LinkedList<Integer> possibleNumbs = new LinkedList<Integer>();
		for(int a = 1; a <= MaxNumber; a++) {
			possibleNumbs.add(a);
		}
		for(int a = 0; a < 6; a++) {
			int newNumb = (int) Math.random() * possibleNumbs.size();
			if(newNumb == possibleNumbs.size()) {
				newNumb = 0;
			}
			drawn.add(possibleNumbs.get(newNumb));
			drawnArray[pos] = possibleNumbs.get(newNumb);
			pos++;
			possibleNumbs.remove(newNumb);
		}
		winningNumbers = drawn;
		for(int a = 0; a < tickets.size(); a++) {
			tickets.get(a).setCorrectNumbs(drawnArray);
		}
	}

	public Set<Integer> getWinning() {
		return winningNumbers;
	}
}
