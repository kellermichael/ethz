import java.util.*;

public class Ticket {
	
	HashSet<Integer> numbs = new HashSet<Integer>();
	int[] winningNumbs;
	int id;

	public int getNumber() {
		return id;
	}
	
	public void setId(int a) {
		id = a;
	}

	public Set<Integer> getNumbers() {
		return numbs;
	}
	
	public void setNumbers(HashSet<Integer> a) {
		numbs = a;
	}

	public Set<Integer> getCorrectNumbers() {
		HashSet<Integer> correctNumbs = new HashSet<Integer>();
		for(int a = 0; a < winningNumbs.length; a++) {
			if(numbs.contains(winningNumbs[a])) {
				correctNumbs.add(winningNumbs[a]);
			}
		}
		return correctNumbs;
	}
	
	public void setCorrectNumbs(int[] correct) {
		winningNumbs = correct;
	}

	public int getPrize() {
		HashSet<Integer> tmp = (HashSet<Integer>) getCorrectNumbers();
		if(tmp.size() == 0) {return 0;} else { return (int) ((int) 5 * Math.pow(20, tmp.size()-1)); }
	}
}
