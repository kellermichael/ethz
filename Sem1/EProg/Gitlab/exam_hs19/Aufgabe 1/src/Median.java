import java.util.*;

public class Median {
	public static double median(Scanner scanner) {
		// TODO
		LinkedList<Integer> list = new LinkedList<Integer>();
		while(scanner.hasNext()) {
			list.add(scanner.nextInt());
		}
		if(list.size() == 0) {return 0.0;}
		double[] nums = new double[list.size()];
		for(int a = 0; a < list.size(); a++) {
			nums[a] = (double) list.get(a);
		}
		Arrays.sort(nums);
		if(nums.length % 2 == 1) {
			return nums[nums.length / 2];
		} else {
			double tmp = nums[(nums.length-1) / 2] + nums[(nums.length+1) / 2];
			tmp = tmp / 2.0;
			return tmp;
		}
	}
}
