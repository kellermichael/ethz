import java.util.Arrays;

public class BlockIntList {
	private static final int MAX_BLOCKS = 50;
	private static final int FIRST_BLOCK_SIZE = 2;

	private int[][] blocks;
	private int lastBlock;
	private int elemsInLast;
	private int size;

	public BlockIntList() {
		blocks = new int[MAX_BLOCKS][];
		blocks[0] = new int[FIRST_BLOCK_SIZE];
		lastBlock = elemsInLast = size = 0;
	}

	public void add(int value) {
		ensureSpaceAdd();
		blocks[lastBlock][elemsInLast] = value;
		elemsInLast++;
		size++;
	}

	private void ensureSpaceAdd() {
		if (elemsInLast == blocks[lastBlock].length) {
			// add a new block
			int newSize = 2 * blocks[lastBlock].length;
			lastBlock++;
			blocks[lastBlock] = new int[newSize];
			elemsInLast = 0;
		}
	}

	public int size() {
		return size;
	}

	public String toString() {
		String result = "";
		for (int b = 0; b < lastBlock; b++) {
			for (int i = 0; i < blocks[b].length; i++) {
				result += blocks[b][i] + ", ";
			}
		}
		for (int i = 0; i < elemsInLast; i++) {
			result += blocks[lastBlock][i] + ", ";
		}
		if (!result.isEmpty()) {
			result = result.substring(0, result.length() - 2);
		}
		return "[" + result + "]";
	}

	public int get(int index) {
		double block = 1;
		double pos = (double) index;
		// calculate block
		while((pos - Math.pow(2.0, block)) >= 0) {
			pos = pos - Math.pow(2, block);
			block++;
		}
		block = block -1;
		
		// System.out.println(block + ", " + pos + ", " + toString());
		return blocks[(int) block][(int) pos];
	}

	public void addFirst(int value) {
		if(size == 0) {
			add(value);
			return;
		}
		int[] numbs = new int[size];
		int pos = 0;
		for(int a = 0; a < blocks.length; a++) {
			if(blocks[a] != null) {
				for(int b = 0; b < blocks[a].length; b++) {
					if(pos < size) {
						numbs[pos] = blocks[a][b];
						pos++;
					}
				}
			}
		}
		// System.out.println(Arrays.toString(numbs));
		// System.out.println(toString());
		blocks = new int[MAX_BLOCKS][];
		blocks[0] = new int[FIRST_BLOCK_SIZE];
		lastBlock = elemsInLast = size = 0;
		add(value);
		for(int a = 0; a < numbs.length; a++) {
			add(numbs[a]);
		}
	}
}
