public class LinkedRingBuffer {

    Node first;
    Node last;

    public LinkedRingBuffer(int size) throws IllegalArgumentException {
        if(size == 1) {
        	first = new Node();
        	last = null;
        	first.next = first;
        } else if(size > 1) {
        	first = new Node();
        	Node current = first;
        	for(int a = 1; a < size; a++) {
        		current.next = new Node();
        		current = current.next;
        	}
        	current.next = first;
        	last = null;
        } else {
        	throw new IllegalArgumentException();
        }
    }

    public int capacity() {
        int capacity = 1;
        Node current = first;
        while(current.next != first) {capacity = capacity + 1; current = current.next;}
        // System.out.println("Capacity: " +capacity);
        return capacity;
    }

    public int length() {
    	if(last == null) {
    		return 0;
    	} else {
    		int length = 1;
            Node current = first;
            while(current != last) {
            	length = length + 1;
            	current = current.next;
            }
            // System.out.println("Length: " + length);
            return length;
    	}
    }

    public void enqueue(int value) throws IllegalStateException {
        if(length() < capacity()) {
        	if(length() == 0) {
        		last = first;
        		last.value = value;
        	} else {
        		last = last.next;
        		last.value = value;
        	}
        } else {
        	throw new IllegalStateException();
        }
    }

    public int dequeue() throws IllegalStateException{
        int result = 0;
        if(length() != 0) {
        	if(length() == 1) {
        		last = null;
        		return first.value;
        	} else {
        		result = first.value;
            	Node current = first;
            	while(current.next != last) {
            		current.value = current.next.value;
            		current = current.next;
            	}
            	last = current;
            	last.value = current.next.value;
        	}
        } else {
        	throw new IllegalStateException();
        }
        return result;
    }
}

class Node {
    Node next;
    int value;
}
