import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Service {
	
	LinkedList<Integer> students = new LinkedList<Integer>();
	LinkedList<Double> markOne = new LinkedList<Double>();
	LinkedList<Double> markTwo = new LinkedList<Double>();
	LinkedList<Double> markThree = new LinkedList<Double>();

    public Service(Scanner scanner) {
        while(scanner.hasNextLine()) {
        	String line = scanner.nextLine();
        	if(line.strip() != "") {
        		Scanner lineScanner = new Scanner(line);
            	students.add(lineScanner.nextInt());
            	markOne.add(lineScanner.nextDouble());
            	markTwo.add(lineScanner.nextDouble());
            	markThree.add(lineScanner.nextDouble());
            	lineScanner.close();
        	}
        }
    }

    public List<Integer> critical(double bound1, double bound2) {
        LinkedList<Integer> result = new LinkedList<Integer>();
        for(int a = 0; a < students.size(); a++) {
        	if(markOne.get(a) <= bound1 && (markTwo.get(a) + markThree.get(a)) < bound2) {
        		result.add(students.get(a));
        	}
        }
        return result;
    }

    public List<Integer> top(int limit) {
    	LinkedList<Integer> result = new LinkedList<Integer>();
    	double[] sums = new double[students.size()];
        for(int a = 0; a < students.size(); a++) {
        	sums[a] = markOne.get(a) + markTwo.get(a) + markThree.get(a);
        }
        Arrays.sort(sums);
        for(int a = sums.length - 1; a >= 0; a--) {
        	for(int b = 0; b < students.size(); b++) {
        		if(result.size() < limit && sums[a] == (markOne.get(b) + markTwo.get(b) + markThree.get(b))) {
        			result.add(students.get(b));
        		}
        	}
        }
        return result;
    }
}