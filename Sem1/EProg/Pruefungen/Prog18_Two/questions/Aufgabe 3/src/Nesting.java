public class Nesting {

    public static boolean isCorrectlyNested(String s) {
        boolean wasAbleToUpdate = true;
        while(wasAbleToUpdate) {
        	wasAbleToUpdate = false;
        	String tmpString = "";
        	for(int a = 0; a < s.length(); a++) {
        		if(!pairFound(a, s)) {
        			tmpString = tmpString + s.charAt(a);
        		} else {
        			wasAbleToUpdate = true;
        			a = a + 1;
        		}
        	}
        	s = tmpString;
        }
        if(s == "") {
        	return true;
        }
        return false;
    }
    
    public static boolean pairFound(int a, String s) {
    	if(a >= s.length()-1) {return false;}
    	if(s.charAt(a) == '(' && s.charAt(a+1) == ')') {
    		return true;
    	}
    	if(s.charAt(a) == '[' && s.charAt(a+1) == ']') {
    		return true;
    	}
    	if(s.charAt(a) == '<' && s.charAt(a+1) == '>') {
    		return true;
    	}
    	return false;
    }
}
