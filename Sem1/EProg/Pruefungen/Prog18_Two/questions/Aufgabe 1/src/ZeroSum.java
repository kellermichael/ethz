import java.util.Arrays;
import java.util.Scanner;

public class ZeroSum {

    public static Pair zeroSum(Scanner scanner) {
    	String line = scanner.nextLine();
    	String[] stringElems = line.split(" ");
    	double[] elems = new double[stringElems.length];
    	// make nuber array
        for(int a = 0; a < stringElems.length; a++) {
        	elems[a] = Double.parseDouble(stringElems[a]);
        }
        
        for(int a = 0; a < elems.length; a++) {
        	for(int b = a+1; b < elems.length; b++) {
        		if((elems[b] + elems[a]) == 0.0) {
        			return new Pair(a, b);
        		}
        	}
        }
        return null;
    }
}

class Pair {
    int i, j;
    Pair(int i, int j) {
        this.i = i;
        this.j = j;
    }
}
