import java.util.Arrays;
import java.util.Scanner;

public class Blocks {

    public static int largestBlock(String string) {
    	// read entries into matrix
    	Scanner scanner = new Scanner(string);
    	int size = (int) scanner.nextDouble();
    	scanner.nextLine();
    	double[][] matrix = new double[size][size];
    	int row = 0;
    	while(scanner.hasNextLine() && row < size) {
    		int column = 0;
    		Scanner lineScanner = new Scanner(scanner.nextLine());
    		while(lineScanner.hasNext() && row < size) {
    			matrix[row][column] = lineScanner.nextDouble();
    			column = column + 1;
    		}
    		lineScanner.close();
    		row = row + 1;
    	}
    	scanner.close();
    	
    	// System.out.println(checkField(0, 4, 3, matrix));
    	
    	// do the problem
    	for(int a = size; a > 0; a--) {
    		for(int x = 0; x < size; x++) {
    			for(int y = 0; y < size; y++) {
    				if(checkField(x, y, a, matrix)) {
    					// System.out.println(x + ", " + y + ", " + a);
    					return a;
    				}
    			}
    		}
    	}
    	
        // TODO
        return 0;
    }
    
    public static boolean checkField(int x, int y, int edgeSize, double[][] matrix) {
    	
    	// check dims
    	if((x + edgeSize - 1) < matrix.length && (y + edgeSize - 1) < matrix.length) {
    		// check if all same
    		for(int a = 0; a < edgeSize; a++) {
    			for(int b = 0; b < edgeSize; b++) {
    				if(matrix[x][y] != matrix[x+a][y+b]) {
    					return false;
    				}
    			}
    		}
    		return true;
    	}
    	return false;
    }
}