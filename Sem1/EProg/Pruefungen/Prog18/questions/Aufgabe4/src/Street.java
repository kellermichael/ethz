public class Street {
	
	Intersection from;
	Intersection to;
	int length;
	
	Street() {}
	
	Street(int l, Intersection f, Intersection t) {
		length = l;
		to = t;
		from = f;
	}
	
	@Override
	public String toString() {
		String line = "";
		for (int i = 0; i < length; i++) {
			line += "-";
		}
		return from + " " + line + " " + to;
	}
}
