public class CityBuilder {
	
	static City build() {
		City zuerich = new City();
		
		// add squares
		Square Polyterrasse = new Square("Polyterrasse");
		Square Central = new Square("Central");
		Square Stampfenbachplatz = new Square("Stampfen-bachplatz");
		Square Bahnhofplatz = new Square("Bahnhofplatz");
		Square Loewenplatz = new Square("Loewenplatz");
		Square Sihlpost = new Square("Sihlpost");
		zuerich.addIntersection(Polyterrasse);
		zuerich.addIntersection(Central);
		zuerich.addIntersection(Stampfenbachplatz);
		zuerich.addIntersection(Bahnhofplatz);
		zuerich.addIntersection(Loewenplatz);
		zuerich.addIntersection(Sihlpost);
		
		// add intersections
		Intersection a = new Intersection();
		Intersection b = new Intersection();
		Intersection c = new Intersection();
		zuerich.addIntersection(a);
		zuerich.addIntersection(b);
		zuerich.addIntersection(c);
		
		// add streets
		Street A = new Street(3, Polyterrasse, a);
		Street B = new Street(8, b, Polyterrasse);
		Street C = new Street(8, Polyterrasse, b);
		Street D = new Street(7, b, Central);
		Street E = new Street(7, Central, b);
		Street F = new Street(9, Central, Stampfenbachplatz);
		Street G = new Street(2, Central, Bahnhofplatz);
		Street H = new Street(2, Bahnhofplatz, Central);
		Street I = new Street(10, Bahnhofplatz, Stampfenbachplatz);
		Street J = new Street(10, Stampfenbachplatz, Bahnhofplatz);
		Street K = new Street(6, Loewenplatz, Bahnhofplatz);
		Street L = new Street(5, Bahnhofplatz, c);
		Street M = new Street(6, c, Sihlpost);
		zuerich.addStreet(A);
		zuerich.addStreet(B);
		zuerich.addStreet(C);
		zuerich.addStreet(D);
		zuerich.addStreet(E);
		zuerich.addStreet(F);
		zuerich.addStreet(G);
		zuerich.addStreet(H);
		zuerich.addStreet(I);
		zuerich.addStreet(J);
		zuerich.addStreet(K);
		zuerich.addStreet(L);
		zuerich.addStreet(M);
		
		// add out streets to Intersections
		Polyterrasse.addStreet(A);
		Polyterrasse.addStreet(C);
		Central.addStreet(E);
		Central.addStreet(F);
		Central.addStreet(G);
		Stampfenbachplatz.addStreet(J);
		Bahnhofplatz.addStreet(H);
		Bahnhofplatz.addStreet(I);
		Bahnhofplatz.addStreet(L);
		Loewenplatz.addStreet(K);
		b.addStreet(B);
		b.addStreet(D);
		c.addStreet(M);
		
		return zuerich;
	}
}
