import java.util.HashSet;
import java.util.Set;

public class Intersection {
	
	Set<Street> outgoingStreets = new HashSet<>();
	
	Intersection() {}
	
	void addStreet(Street rd) {
		outgoingStreets.add(rd);
	}
	
	@Override
	public String toString() {
		return "X";
	}
}
