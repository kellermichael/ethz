public class Square extends Intersection {
	
	String name;
	
	Square() {}
	
	Square(String initName) {
		name = initName;
	}
	
	@Override
	public String toString() {
		return "[" + name + "]";
	}
}
