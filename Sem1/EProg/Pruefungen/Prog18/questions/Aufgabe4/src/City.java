import java.util.HashSet;
import java.util.Set;

public class City {
	
	Set<Street> streets = new HashSet<>();
	Set<Intersection> intersections = new HashSet<>();
	
	City() {}
	
	void addStreet(Street rd) {
		streets.add(rd);
	}
	
	void addIntersection(Intersection intSec) {
		intersections.add(intSec);
	}
	
	Set<Street> deadEnds() {
		Set<Street> sackGassen = new HashSet<Street>();
		Intersection[] gassen = new Intersection[intersections.size()];
		intersections.toArray(gassen);
		Street[] rds = new Street[streets.size()];
		streets.toArray(rds);
		boolean notDone = true;
		
		// determine og sackgassen
		for(int i = 0; i < gassen.length; i++) {
			if(gassen[i].outgoingStreets.isEmpty()) {
				for(int j = 0; j < rds.length; j++) {
					if(rds[j].to == gassen[i]) {
						sackGassen.add(rds[j]);
					}
				}
			}
		}
		
		while(notDone) {
			notDone = false;
			Street[] dead = new Street[sackGassen.size()];
			sackGassen.toArray(dead);
			for(int i = 0; i < rds.length; i++) {
				for(int j = 0; j < dead.length; j++) {
					if(!sackGassen.contains(rds[i]) && rds[i].to == dead[j].from) {
						Set<Street> tmp = new HashSet<Street>();
						tmp.addAll(sackGassen);
						tmp.addAll(rds[i].to.outgoingStreets);
						if(tmp.size() == sackGassen.size()) {
							notDone = true;
							sackGassen.add(rds[i]);
						}
					}
				}
			}
		}
		return sackGassen;
	}
	
	Set<Square> reachableSquares(String squareName, int distance) {
		// TODO
		return null;
	}
}
