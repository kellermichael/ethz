import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Warenanalyse {
	
	static void analyse(File input, File output) throws IOException {
		// beste transaktion
		double maxTransak = 0.0;
		double maxPreis = 0.0;
		String produkt = "";
		
		// bester kunde
		String[] Kunden = new String[(int) input.length()];
		double[] kundenZahlungen = new double[(int) input.length()];
		
		// spanne produkt dings
		String[] Produkte = new String[(int) input.length()];
		double[][] preise = new double[2][(int) input.length()];
		
		Scanner scanner = new Scanner(input);
		int lastOccupied = 0;
		int lastOccupiedTwo = 0;
		while(scanner.hasNextLine()) {
			String[] line = scanner.nextLine().split(" ");
			
			// beste transaktion
			if(Double.valueOf(line[2]) * Double.valueOf(line[3]) > maxTransak) {
				maxTransak = Double.valueOf(line[2]) * Double.valueOf(line[3]);
				maxPreis = Double.valueOf(line[3]);
				produkt = line[0];
			}
			
			// bester kunde
			boolean found = false;
			for(int i = 0; i < Kunden.length; i++) {
				if(Kunden[i] != null && Kunden[i].equals(line[1])) {
					kundenZahlungen[i] += Double.valueOf(line[2]) * Double.valueOf(line[3]);
					found = true;
				}
			}
			if(!found) {
				kundenZahlungen[lastOccupied] = Double.valueOf(line[2]) * Double.valueOf(line[3]);
				Kunden[lastOccupied] = line[1];
				lastOccupied = lastOccupied + 1;
		
			}
			
			// min max preis ding
			found = false;
			for(int i = 0; i < Produkte.length; i++) {
				if(Produkte[i] != null && Produkte[i].equals(line[0])) {
					if(preise[0][i] < Double.valueOf(line[3])) {
						preise[0][i] = Double.valueOf(line[3]);
						found = true;
					}
					if(preise[1][i] > Double.valueOf(line[3])) {
						preise[1][i] = Double.valueOf(line[3]);
						found = true;
					}
				}
			}
			if(!found) {
				Produkte[lastOccupiedTwo] = line[0];
				preise[0][lastOccupiedTwo] = Double.valueOf(line[3]);
				preise[1][lastOccupiedTwo] = Double.valueOf(line[3]);
				lastOccupiedTwo = lastOccupiedTwo + 1;
			}
			
		}
		
		// beste transaktion
		// System.out.println(produkt + ", " + maxPreis);
		

		// bester Kunde
		double maxSpent = 0.0;
		String besterKunde = "";
		for(int i = 0; i < kundenZahlungen.length; i++) {
			if(kundenZahlungen[i] > maxSpent) {
				maxSpent = kundenZahlungen[i];
				besterKunde = Kunden[i];
			}
		}
		// System.out.println(besterKunde + ", " + maxSpent);
		
		// min max preis ding
		double maxDiff = 0.0;
		String maxDiffProd = "";
		// System.out.println(Arrays.toString(Produkte));
		// System.out.println(Arrays.deepToString(preise));
		for(int i = 0; i < Produkte.length; i++) {
			if(Produkte[i] != null && (preise[0][i] - preise[1][i] >= maxDiff)) {
				maxDiff = preise[0][i] - preise[1][i];
				maxDiffProd = Produkte[i];
			}
			// System.out.println(maxDiff);
		}
		// System.out.println(maxDiffProd + ", " + maxDiff);
		
		FileWriter myWriter = new FileWriter(output);
	    myWriter.write(produkt + " " + maxPreis + "\n");
	    myWriter.write(besterKunde + " " + maxSpent + "\n");
	    myWriter.write(maxDiffProd + "\n");
	    myWriter.close();
	}
}
