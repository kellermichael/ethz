public class Abstand {
	
	public static int abstand(int[] ns, int[] ms) {
		// TODO
		int abstand = 0;
		int length = ns.length > ms.length ? ns.length : ms.length;
		for(int i = 0; i < length; i++) {
			int a = i < ns.length ? ns[i] : 0;
			int b = i < ms.length ? ms[i] : 0;
			
			abstand = abstand + Math.abs(a - b);
		}
		return abstand;
	}
}
