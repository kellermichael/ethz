public class Kombinationen {
	
	public static String generate(String input) {
		String result = "\"";
		int permutations = (int) Math.pow(2, input.length());
		System.out.println(input);
		for(int i = 0; i < permutations; i++) {
			String binTmp = Integer.toBinaryString(i);
			String bin = "";
			for(int a = binTmp.length() - 1; a >= 0; a--) {
				bin = bin + binTmp.charAt(a);
			}
			for(int j = 0; j < bin.length(); j++) {
				if(bin.charAt(j) == '1') {
					result = result + input.charAt(j);
				}
			}
			if(i + 1 < permutations) {
				result = result + "\" \"";
			}
		}
		
		// result = generateResultRecursive(input, 0);
		result = result + "\"";
		return result;
	}
	
	public String generateResultRecursive(String s, int pos) {
		if(s.length() == 0) {
			return s;
		} else {
			return s;
		}
	}
}
