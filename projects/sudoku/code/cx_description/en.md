# Magician and the Coin

There is a new magician in town with a magic (of course, what else?) coin.

The magician stays in town for $n$ days. On each day $i$, the magician flips his
coin *once*, and you may choose how much money $b_i$ you want to bet on the
outcome of the coin flip. If the coin comes up heads you win and your wealth
increases by $b_i$. If the coin comes up tails you lose and your wealth
decreases by $b_i$.

The magic lies in the fact that on every given day $i$, the coin has probability
exactly $p_i$ of coming up heads, and $(1 - p_i)$ of coming up tails. A friend
of yours knows the magician, and he tells you the probabilities $p_i$ for all
$n$ days in advance. So you decide to test your luck. You start with a wealth of
$k$, and your goal is to have a wealth of at least $m$ in the end. Now, you
wonder how you can maximise the probability of this outcome (to have at least a
wealth of $m$ in the end). *With an optimal strategy, what is the
probability of you having wealth at least $m$ after $n$ days?*

## Additional Information

  - For your strategy, on the $i$-th day ($1 \leq i \leq n$) you should
      decide on some bet $b_i$ for this day. Your choice may depend on the
      outcome of the coin flips on days $1, \ldots, i - 1$.
  - The magician only accepts non-negative integer bets, so you are
      restricted to $b_i \in N_0$. In particular, it is allowed to bet $b_i =
      0$.
  - You can never bet more money than you have. For example, on the first
      day you can choose any $b_1 \in \{0, \ldots, k\}$, but it is not possible
      to bet $k + 1$ or more.
  - You only care whether or not you have wealth at least $m$ in the end.
      It does not matter whether you end up with a wealth of $0$ or of $m - 1$;
      both are failures.


## Input
  The first line of the input contains the number $t \leq 30$ of test cases.
  Each of the $t$ test cases is described as follows.

  - It starts with a line that consists of three integers \verb|n k m|,
      separated by a space. They denote

    - $n$, the number of days the magician stays in town ($1 \leq n \leq
          10$);
    - $k$, your wealth on the first day ($0 \leq k \leq 10^2$);
    - $m$, the wealth you want to have by the time the magician leaves
          town ($1 \leq m \leq 5 \cdot 10^2$).
  - The following line contains $n$ real numbers $\verb|p|_1\ \ldots\
      \verb|p|_n$, separated by a space, and such that $0 \leq p_i \leq 1$. Each
      $p_i$ denotes the probability of winning on day $i$.

## Output
  For each test case output a single line with the maximum probability of you
  having wealth at least $m$ after $n$ days. Each output value should be a real
  number between $0$ and $1$. Your solution is going to be accepted if it has an
  absolute or relative error of at most $10^{-3}$.

**Hint:** We strongly advise you to use $\texttt{double}$ throughout the
algorithm for representing real numbers.

## Points
  This exercise gives no bonus points.

  There are three groups of test sets, worth $100$ non-bonus-points in total.

  1. For the first group of test sets, worth $20$ points, you may assume
      that $n \leq 5$ and $m \leq 10$. In addition, the probability of winning
      on any given day is the same and at most $1/2$, that is $p_1 = \ldots =
      p_n$ and $0 \leq p_i \leq 1/2$, for all $i \in [n]$.
  2. For the second group of test sets, worth $30$ points, you may assume
      that the probability of winning on any given day is the same and at most
      $1/2$, that is $p_1 = \ldots = p_n$ and $0 \leq p_i \leq 1/2$, for all $i
      \in [n]$.
  3. For the third group of test sets, worth $50$ points, there are no
      additional assumptions.


### Explanation for testcases in sample.in (in order of appearance)
    
  1. The only way to reach $8$ is to bet all you have on each day.
  2. There is no way to reach $33$ in only four days.
  3. An optimal strategy is to bet $2$ on the first day, $1$ on the second
      day (unless you are broke). If you win on the second day, you bet $0$ on
      the third day, and bet all you have on the remaining two days. If your
      lose on the second day, you bet all you have on the remaining three days.
