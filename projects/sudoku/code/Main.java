import java.util.*;

class Main {
    public static void main(String[] args) {
        // Uncomment this line if you want to read from a file
        In.open("public/sample.in");
        Out.compareTo("public/sample.out");
        
        int t = In.readInt();
        for (int i = 0; i < t; i++) {
            testCase();
        }
        
        // Uncomment this line if you want to read from a file
        In.close();
    }

    public static void testCase() {
        // Input using In.java class
        int n = In.readInt();
        int k = In.readInt();
        int m = In.readInt();
        double[] winProb = new double[n];
        for(int i = 0; i < n; i++) {winProb[i] = In.readDouble();}
        
        double[][][] dp = new double[n+1][m+1][2];
        
        System.out.println(recursiveSolve(dp, n, k, m, winProb));
        
    }
    
    public static double recursiveSolve(double[][][] dp, int days, int curWealth, int goalWealth, double[] winProb) {
      // base case
      if(days == 0) {
        return curWealth == goalWealth ? 1.0 : 0.0;
      }
      
      // test if case has been solved
      if(dp[days][curWealth][1] == 1.0) {
        return dp[days][curWealth][0];
      }
      
      // general case
      double maxProb = 0.0;
      for(int bet = 0; bet <= curWealth; bet++) {
        double winP = winProb[winProb.length - days];
        double win = recursiveSolve(dp, days - 1, Math.min(curWealth + bet, goalWealth), goalWealth, winProb) * winP;
        double loose = recursiveSolve(dp, days - 1, curWealth - bet, goalWealth, winProb) * (1 - winP);
        if(maxProb < (win + loose)) {
          maxProb = win + loose;
        }
      }
      
      // momoize
      dp[days][curWealth][0] = maxProb;
      dp[days][curWealth][1] = 1.0;
      return maxProb;
    }
}