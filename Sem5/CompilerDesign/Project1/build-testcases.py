import random

def buildcase(h):
    if h == 0:
        return "Const " + str(random.randint(0, 1000)) + "L"
    else:
        choice = random.randint(0, 4)
        if choice == 0:
            choice2 = choice = random.randint(0, 1)
            if choice2 == 0:
                return 'Var "x"'
            else:
                return 'Var "y"'
        elif choice == 1:
            return "Const " + str(random.randint(0, 1000)) + "L"
        elif choice == 2:
            return "Neg(" + buildcase(h-1) + ")"
        elif choice == 3:
            return "Add(" + buildcase(h-1) + ", " + buildcase(h-1) + ")"
        elif choice == 4:
            return "Mult(" + buildcase(h-1) + ", " + buildcase(h-1) + ")"

def main():
    maxHeight = 10
    f = open("dump.txt", "a")
    for i in range(2000):
        case = buildcase(maxHeight)
        f.write('("optimize' + str(i) + '", assert_eqf (fun () -> interpret ctxt2 (' + case + ')) (run ctxt2 (compile (' + case + '))));\n')
    f.close()
main()