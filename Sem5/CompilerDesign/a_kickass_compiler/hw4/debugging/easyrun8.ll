define i64 @program(i64 %argc, { i64, [0 x i8*] }* %argv) {
  %_tmp2 = icmp ne i64 6, 5
  br i1 %_tmp2, label %_if5, label %_els6
_if5:
  %_tmp18 = sub i64 0, 6
  %_tmp16 = lshr i64 5, %_tmp18
  %_tmp15 = shl i64 %_tmp16, 9
  %_tmp14 = ashr i64 %_tmp15, 10
  %_tmp13 = sub i64 0, %_tmp23
  %_tmp23 = add i64 %_tmp14, 1
  %_tmp12 = mul i64 %_tmp13, 2
  %_tmp11 = sub i64 %_tmp12, 100
  %_tmp10 = add i64 %_tmp11, 6
  ret i64 %_tmp10
_els6:
  ret i64 2
}


declare i64* @oat_alloc_array(i64)
declare { i64, [0 x i64] }* @array_of_string(i8*)
declare i8* @string_of_array({ i64, [0 x i64] }*)
declare i64 @length_of_string(i8*)
declare i8* @string_of_int(i64)
declare i8* @string_cat(i8*, i8*)
declare void @print_string(i8*)
declare void @print_int(i64)
declare void @print_bool(i1)