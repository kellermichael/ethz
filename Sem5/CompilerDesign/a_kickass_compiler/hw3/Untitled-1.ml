(*
      begin match op with
       | Null  -> [restoreRBP]
       | Const -> [restoreRBP]
       | Gid   -> [restoreRBP] (* see llvm lite specifications *)
       | Uid   -> [restoreRBP]
      end
      
let restoreRBP = Movq [Rsp; Rbp] in
    begin match t with
    | Ret (ty, op)         -> failwith "fail"
    | Br lbl               -> failwith "[compile_terminator] case Br not handeled"
    | Cbr (op, lbl1, lbl2) -> failwith "[compile_terminator] case Cbr not handeled"
    end
    *)