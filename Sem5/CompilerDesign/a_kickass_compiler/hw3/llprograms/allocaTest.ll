define i64 @main(i64 %argc, i8** %arcv) {
  %0 = alloca i64
  %2 = alloca i64
  %3 = alloca i64
  %4 = alloca i64
  store i64 %0, i64* %2
  %5 = load i64, i64* %2
  store i64 %5, i64* %3
  %6 = load i64, i64* %3
  store i64 %6, i64* %4
  %7 = load i64, i64* %3
  %8 = load i64, i64* %4
  ret i64 %8
}