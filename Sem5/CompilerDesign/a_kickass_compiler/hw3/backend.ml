(*

    1.  First get a minimal implementation of compile_fdecl working so that you can compile
        functions with empty bodies but varying numbers of input parameters. To do this, you'll
        need to understand the System V AMD64 ABI calling conventions (see the lecture slides
        and Wikipedia for an explanation), then understand the notion of a layout and complete
        the arg_loc function. At this point, the X86 code you generate won't be able to run because
        the code for the compiled function does not exit propertly. (But you can still look at the
        generated assembly code to see whether it looks reasonable.)
    2.  Next implement enough of the compile_terminator function to handle (void) functions that
        return no results. Similarly, implement enough of compile_block to handle blocks with no
        instructions. At this point, your compiler should be able to generate working code for an
        LLVM function like that found in returnvoid.ll:

            define void @main(i64 %argc, i8** %argv) {
              ret void
            }
            

        (Note, this isn't part of the test suite, since the value "returned" to the shell when this
        program runs isn't well defined.)
    3.  Understand the notion of the ctxt type and develop a strategy for storing uid locals. See
        the comments in the backend.ml file. Implement the compile_operand function.
    4.  Implement the Binop case for compile_insn (which, if you follow the suggested
        method of compiling locals, will use compile_operand).
    5.  At this point, you probably want to revisit compile_fdecl and compile_block to adjust them
        to deal properly with contexts and non-empty control-flow graphs / blocks.
    6.  Next go back and implement the rest of the cases for compile_terminator. At this point,
        your compiler should be able to handle functions that return i64 values and that contain simple arithmetic and direct jumps.
    7.  Implement the translation of Icmp in compile_insn, followed by Alloca, Load, and Store.
    8.  Next tackle the Call instruction. The code you generate must properly handle the System V AMD64
        ABI calling conventions (but note that we care only about 64-bit values). After successfully completing
        this step, your compiler should be able to handle the recursive factorial function definition.
    9.  Breathe a sigh of relief at how easy it is to implement Bitcast, because the target language is untyped.
    10. Finally, gather your courage, and implement the Gep (getelementptr) instruction.

 *)


(* ll ir compilation -------------------------------------------------------- *)

open Ll
open X86

(* Overview ----------------------------------------------------------------- *)

(* We suggest that you spend some time understanding this entire file and
   how it fits with the compiler pipeline before making changes.  The suggested
   plan for implementing the compiler is provided on the project web page.
*)


(* helpers ------------------------------------------------------------------ *)

(* Map LL comparison operations to X86 condition codes *)
let compile_cnd = function
  | Ll.Eq  -> X86.Eq
  | Ll.Ne  -> X86.Neq
  | Ll.Slt -> X86.Lt
  | Ll.Sle -> X86.Le
  | Ll.Sgt -> X86.Gt
  | Ll.Sge -> X86.Ge



(* locals and layout -------------------------------------------------------- *)

(* One key problem in compiling the LLVM IR is how to map its local
   identifiers to X86 abstractions.  For the best performance, one
   would want to use an X86 register for each LLVM %uid.  However,
   since there are an unlimited number of %uids and only 16 registers,
   doing so effectively is quite difficult.  We will see later in the
   course how _register allocation_ algorithms can do a good job at
   this.

   A simpler, but less performant, implementation is to map each %uid
   in the LLVM source to a _stack slot_ (i.e. a region of memory in
   the stack).  Since LLVMlite, unlike real LLVM, permits %uid locals
   to store only 64-bit data, each stack slot is an 8-byte value.

   [ NOTE: For compiling LLVMlite, even i1 data values should be
   represented as a 8-byte quad. This greatly simplifies code
   generation. ]

   We call the datastructure that maps each %uid to its stack slot a
   'stack layout'.  A stack layout maps a uid to an X86 operand for
   accessing its contents.  For this compilation strategy, the operand
   is always an offset from %rbp (in bytes) that represents a storage slot in
   the stack.
*)

type layout = (uid * X86.operand) list

(* A context contains the global type declarations (needed for getelementptr
   calculations) and a stack layout. *)
type ctxt = { tdecls : (tid * ty) list
            ; layout : layout
            }

(* useful for looking up items in tdecls or layouts *)
let lookup m x = List.assoc x m


(* compiling operands  ------------------------------------------------------ *)

(* LLVM IR instructions support several kinds of operands.

   LL local %uids live in stack slots, whereas global ids live at
   global addresses that must be computed from a label.  Constants are
   immediately available, and the operand Null is the 64-bit 0 value.

     NOTE: two important facts about global identifiers:

     (1) You should use (Platform.mangle gid) to obtain a string
     suitable for naming a global label on your platform (OS X expects
     "_main" while linux expects "main").

     (2) 64-bit assembly labels are not allowed as immediate operands.
     That is, the X86 code: movq _gid %rax which looks like it should
     put the address denoted by _gid into %rax is not allowed.
     Instead, you need to compute an %rip-relative address using the
     leaq instruction:   leaq _gid(%rip).

   One strategy for compiling instruction operands is to use a
   designated register (or registers) for holding the values being
   manipulated by the LLVM IR instruction. You might find it useful to
   implement the following helper function, whose job is to generate
   the X86 instruction that moves an LLVM operand into a designated
   destination (usually a register).
*)
let compile_operand (ctxt:ctxt) (dest:X86.operand) : Ll.operand -> ins =
let ripRelative gid = Ind3 (Lbl (Platform.mangle gid), Rip) in
  fun operand -> match operand with
  | Null -> Movq, [Imm (Lit 0L); dest]
  | Const c -> Movq, [Imm (Lit c); dest]
  | Gid gid -> Leaq, [ripRelative gid; dest]
  | Id uid -> Movq, [lookup ctxt.layout uid; dest]



(* compiling call  ---------------------------------------------------------- *)

(* You will probably find it helpful to implement a helper function that
   generates code for the LLVM IR call instruction.

   The code you generate should follow the x64 System V AMD64 ABI
   calling conventions, which places the first six 64-bit (or smaller)
   values in registers and pushes the rest onto the stack.  Note that,
   since all LLVM IR operands are 64-bit values, the first six
   operands will always be placed in registers.  (See the notes about
   compiling fdecl below.)

   [ NOTE: It is the caller's responsibility to clean up arguments
   pushed onto the stack, so you must free the stack space after the
   call returns. ]

   [ NOTE: Don't forget to preserve caller-save registers (only if
   needed). ]
*)




(* compiling getelementptr (gep)  ------------------------------------------- *)

(* The getelementptr instruction computes an address by indexing into
   a datastructure, following a path of offsets.  It computes the
   address based on the size of the data, which is dictated by the
   data's type.

   To compile getelementptr, you must generate x86 code that performs
   the appropriate arithmetic calculations.
*)

(* [size_ty] maps an LLVMlite type to a size in bytes.
    (needed for getelementptr)

   - the size of a struct is the sum of the sizes of each component
   - the size of an array of t's with n elements is n * the size of t
   - all pointers, I1, and I64 are 8 bytes
   - the size of a named type is the size of its definition

   - Void, i8, and functions have undefined sizes according to LLVMlite.
     Your function should simply return 0 in those cases
*)
let rec size_ty (tdecls:(tid * ty) list) (t:Ll.ty) : int =
  match t with
    | Void          -> 0
    | I1            -> 8
    | I8            -> 0
    | I64           -> 8
    | Ptr t         -> 8
    | Struct lst    -> List.fold_right (fun x b -> b + (size_ty tdecls x)) lst 0
    | Array (n,t)   -> n * (size_ty tdecls t)
    | Fun (lst,l)   -> 0
    | Namedt t      -> size_ty tdecls (lookup tdecls t)




(* Generates code that computes a pointer value.

   1. op must be of pointer type: t*

   2. the value of op is the base address of the calculation

   3. the first index in the path is treated as the index into an array
     of elements of type t located at the base address

   4. subsequent indices are interpreted according to the type t:

     - if t is a struct, the index must be a constant n and it
       picks out the n'th element of the struct. [ NOTE: the offset
       within the struct of the n'th element is determined by the
       sizes of the types of the previous elements ]

     - if t is an array, the index can be any operand, and its
       value determines the offset within the array.

     - if t is any other type, the path is invalid

   5. if the index is valid, the remainder of the path is computed as
      in (4), but relative to the type f the sub-element picked out
      by the path so far
*)
let getConst (op: Ll.operand) : int64 = 
  begin match op with
    | Const x -> x
    | _ -> 0L
  end

let rec resolve (ctxt:ctxt) (a : Ll.ty) (path: Ll.operand list) : int64 = 
  if path!=[] then begin
  let mul a b = Int64.mul a b in
  let add a b = Int64.add a b in
  let sizeType a = Int64.of_int (size_ty ctxt.tdecls a) in
  let index = getConst (List.hd path) in
  let nthTy lst = List.nth lst (Int64.to_int index) in
  let firstN lst = List.flatten (List.mapi (fun i t -> if i<(Int64.to_int index) then [t] else []) lst) in
  let tlPath = List.tl path in
  begin match a with
    | Void            -> 0L
    | I1              -> 0L
    | I8              -> 0L
    | I64             -> 0L
    | Ptr p           -> resolve ctxt p path
    | Struct tyLst    -> add (List.fold_left (fun i a -> add (sizeType a) i) 0L (firstN tyLst)) (resolve ctxt (nthTy tyLst) tlPath)
    | Array (i,ty)    -> add (mul (sizeType ty) index) (resolve ctxt ty tlPath)
    | Fun (tyLst, ty) -> failwith "unimplemented"
    | Namedt tid      -> resolve ctxt (lookup ctxt.tdecls tid) path
  end
  end else 0L

(*
let rec buildTree (ctxt: ctxt) (a : Ll.ty) : ins list = 
  begin match a with
    | Void            -> []
    | I1              -> []
    | I8              -> []
    | I64             -> []
    | Ptr p           -> buildTree ctxt p
    | Struct tyLst    -> List.flatten (List.mapi (fun i b -> [Cmpq, [Imm (Lit (Int64.of_int i)); Reg R14]; (J Eq), [Imm (Lbl("label"))]] @ (buildTree ctxt b)) tyLst)
    | Array (i,ty)    -> []
    | Fun (tyLst, ty) -> failwith "not valid"
    | Namedt tid      -> buildTree ctxt (lookup ctxt.tdecls tid)
  end


let rec getLocation (ctxt: ctxt) (i:int) ((l::lst):ty list): int64 =
  let sizeType a = Int64.of_int (size_ty ctxt.tdecls a) in
  if i<=0 then
    0L
  else
    Int64.add (sizeType l) (getLocation ctxt (i-1) lst)

let rec createTable (ctxt: ctxt) (lst: ty list) (n:int) : ins list = 
  if n>=0 then
    [Movq, [Imm (Lit (getLocation ctxt n lst)); Ind3(Lit (Int64.of_int (-8 * n)), Rsp)]]
  else
    []

let rec resolve (ctxt:ctxt) (a : Ll.ty) (path: Ll.operand list) : ins list = 
  if path!=[] then begin
  let mul a b = Int64.mul a b in
  let add a b = Int64.add a b in
  let sizeType a = Int64.of_int (size_ty ctxt.tdecls a) in
  let index = getConst (List.hd path) in
  let indexReg = compile_operand ctxt (Reg R14) (List.hd path) in (* index is saved to R14 *)
  let nthTy lst = List.nth lst (Int64.to_int index) in
  let firstN lst = List.flatten (List.mapi (fun i t -> if i<(Int64.to_int index) then [t] else []) lst) in
  let tlPath = List.tl path in
  begin match a with
    | Void            -> []
    | I1              -> []
    | I8              -> []
    | I64             -> []
    | Ptr p           -> resolve ctxt p path
                         (*createTable ctxt tyList (List.length tyList) @ indexReg @ [Imulq, [Lit (8L); Reg R14]] @ [Addq, [Lit (); Reg Rsp]] @ [Addq, [Ind2(Rsp); Reg R15]] (* @ (resolve ctxt ty tlPath)*)*)
    | Struct tyLst    -> List.buildTree ctxt (List.hd tyLst)(*[Addq, [Imm Lit (List.fold_left (fun i a -> add (sizeType a) i) 0L (firstN tyLst)); Reg R15]] @ (resolve ctxt (nthTy tyLst) tlPath)*)
    | Array (i,ty)    -> [indexReg; Imulq, [Imm (Lit (sizeType ty)); Reg R14]; Addq, [Reg R14; Reg R15]] @ (resolve ctxt ty tlPath)
    | Fun (tyLst, ty) -> failwith "unimplemented"
    | Namedt tid      -> resolve ctxt (lookup ctxt.tdecls tid) path
  end
  end else []
*)  

(* TODO: ADD baseTysize to baseAddr *)
let compile_gep (ctxt:ctxt) (op : Ll.ty * Ll.operand) (path: Ll.operand list) : ins list =
  let baseAddr op = compile_operand ctxt (Reg R15) (snd op) in
  let baseTySize = Int64.of_int (size_ty ctxt.tdecls (fst op)) in
  [baseAddr op]
  @ [Movq, [Imm(Lit baseTySize); Reg R14]]
  @ [Imulq, [Imm (Lit (getConst (List.hd path))); Reg R14]]
  @ [Addq, [Reg R14; Reg R15]]
  @ [Addq, [Imm (Lit (resolve ctxt (fst op) (List.tl path))); Reg R15]]



(* compiling instructions  -------------------------------------------------- *)

(* The result of compiling a single LLVM instruction might be many x86
   instructions.  We have not determined the structure of this code
   for you. Some of the instructions require only a couple of assembly
   instructions, while others require more.  We have suggested that
   you need at least compile_operand, compile_call, and compile_gep
   helpers; you may introduce more as you see fit.

   Here are a few notes:

   - Icmp:  the Setb instruction may be of use.  Depending on how you
     compile Cbr, you may want to ensure that the value produced by
     Icmp is exactly 0 or 1.

   - Load & Store: these need to dereference the pointers. Const and
     Null operands aren't valid pointers.  Don't forget to
     Platform.mangle the global identifier.

   - Alloca: needs to return a pointer into the stack

   - Bitcast: does nothing interesting at the assembly level
*)
let x86opcode (op:bop) : X86.opcode =
  match op with
   | Add  -> Addq
   | Sub  -> Subq
   | Mul  -> Imulq
   | Shl  -> Shlq
   | Lshr -> Shrq
   | Ashr -> Sarq
   | And  -> Andq
   | Or   -> Orq
   | Xor  -> Xorq
let x86operand (op:Ll.bop) (d:Ll.operand) : (X86.operand list) =
  begin match (op,d) with
    | (Shl, Const x)  -> [Imm (Lit x); Reg R14]
    | (Lshr, Const x) -> [Imm (Lit x); Reg R14]
    | (Ashr, Const x) -> [Imm (Lit x); Reg R14]
    | (y,z)           -> [Reg R15; Reg R14]
  end

(* This helper function computes the location of the nth incoming
   function argument: either in a register or relative to %rbp,
   according to the calling conventions.  You might find it useful for
   compile_fdecl.

   [ NOTE: the first six arguments are numbered 0 .. 5 ]
*)
let arg_loc (n : int) : operand =
  match n with
  | 0 -> Reg Rdi
  | 1 -> Reg Rsi
  | 2 -> Reg Rdx
  | 3 -> Reg Rcx
  | 4 -> Reg R08
  | 5 -> Reg R09
  | 6 -> Reg R10
  | 7 -> Reg R11
  | 8 -> Reg R12
  | 9 -> Reg R13
  | _ -> failwith "arg_loc not complete"

(*List.mapi (fun i x -> Movq, [arg_loc i; lookup layout x]) in_args *)
let setupCallVars (ctxt:ctxt) (l: (ty * Ll.operand) list) : (X86.ins list) =
  let setVal op = compile_operand ctxt (Reg R15) op in
  List.flatten (List.mapi (fun i x -> [setVal x] @ [Movq, [Reg R15; arg_loc i]]) (List.map snd l))

let compile_insn (ctxt:ctxt) ((uid:uid), (i:Ll.insn)) : X86.ins list =
  let setValA op = compile_operand ctxt (Reg R14) op in
  let setValB op = compile_operand ctxt (Reg R15) op in
  let opSize a = Imm (Lit (Int64.of_int (size_ty ctxt.tdecls a))) in
  match i with
  | Binop (a, b, c, d) -> [setValA c] @ [setValB d] @ [x86opcode a, (x86operand a d); Movq, [Reg R14; lookup ctxt.layout uid]]
  | Alloca a ->  [Movq, [Reg Rsp; lookup ctxt.layout uid]] @ [Subq, [opSize a; Reg Rsp]]
  | Load (a, b) -> [setValA b] @ [Movq, [Ind2 R14; Reg R15]] @ [Movq, [Reg R15; lookup ctxt.layout uid]]
  | Store (a, b, c) -> [setValA b] @ [setValB c] @ [Movq, [Reg R14; Ind2 R15]]
  | Icmp (a, b, c, d) ->  [setValA c] @ [setValB d] @ [Cmpq, [Reg R15; Reg R14]; Set (compile_cnd a), [lookup ctxt.layout uid]]
  | Call (a, b, c) -> [setValA b] @ (setupCallVars ctxt c) @ [Pushq, [Reg Rbp]; Callq, [Reg R14]; Popq, [Reg Rbp]; Movq, [Reg Rax; lookup ctxt.layout uid]]
  | Bitcast (a, b, c) -> []
  | Gep (a, b, c) -> (compile_gep ctxt (a,b) c) @ [Movq, [Reg R15; lookup ctxt.layout uid]]



(* compiling terminators  --------------------------------------------------- *)

(* prefix the function name [fn] to a label to ensure that the X86 labels are 
   globally unique . *)
let mk_lbl (fn:string) (l:string) = fn ^ "." ^ l

(* Compile block terminators is not too difficult:

   - Ret should properly exit the function: freeing stack space,
     restoring the value of %rbp, and putting the return value (if
     any) in %rax.

   - Br should jump

   - Cbr branch should treat its operand as a boolean conditional

   [fn] - the name of the function containing this terminator
*)

(*type ctxt = { tdecls : (tid * ty) list
            ; layout : layout
            } *)

let compile_terminator (fn:string) (ctxt:ctxt) (t:Ll.terminator) : ins list =
  let restoreRBP = [Popq, [Reg Rbp]; Retq, []] in
  let setVal op = compile_operand ctxt (Reg R15) op in
  match t with
  | Ret (ty, None) -> restoreRBP
  | Ret (ty, Some op) -> [setVal op] @ [Movq, [Reg R15; Reg Rax]] @ restoreRBP
  | Br (label) -> [Jmp, [Imm (Lbl (mk_lbl fn label))]]
  | Cbr (op, lbl1, lbl2) -> [setVal op] @ [Cmpq, [Imm (Lit 0L); Reg R15]; J Neq, [Imm (Lbl (mk_lbl fn lbl1))]; Jmp, [Imm (Lbl (mk_lbl fn lbl2))]]


(* compiling blocks --------------------------------------------------------- *)

(* We have left this helper function here for you to complete. 
   [fn] - the name of the function containing this block
   [ctxt] - the current context
   [blk]  - LLVM IR code for the block
*)
(* helper function that calculates total amount that was subtracted from Rsp by all allocas *)
let rec addRsp (ctxt:ctxt) (lst : ((uid * insn) list))  : int64 =
  let sz l = 
    begin match (snd l) with
    | Alloca a -> Int64.of_int (size_ty ctxt.tdecls a)
    | _ -> 0L
    end
  in
    begin match lst with
    | [] -> 0L
    | l::lt -> Int64.add (sz l) (addRsp ctxt lt)
    end
    
let compile_block (fn:string) (ctxt:ctxt) (blk:Ll.block) : ins list =
  (List.flatten (List.map (fun x -> compile_insn ctxt x) blk.insns)) @
  [Addq, [Imm (Lit (addRsp ctxt blk.insns)); Reg Rsp]] @
  compile_terminator fn ctxt (snd blk.term)

let compile_lbl_block fn lbl ctxt blk : elem =
  Asm.text (mk_lbl fn lbl) (compile_block fn ctxt blk)



(* compile_fdecl ------------------------------------------------------------ *)


(* We suggest that you create a helper function that computes the
   stack layout for a given function declaration.

   - each function argument should be copied into a stack slot
   - in this (inefficient) compilation strategy, each local id
     is also stored as a stack slot.
   - see the discussion about locals
*)

let rec remove_duplicates (l:'a list) : ('a list) =
  match l with
  | [] -> []
  | x::xs -> if List.exists (fun a -> x = a) xs then remove_duplicates xs else x::(remove_duplicates xs)

(*type layout = (uid * X86.operand) list *)

let stack_layout (args : uid list) ((block, lbled_blocks):cfg) : layout =
  let blocks = [block] @ (List.map snd lbled_blocks) in
  let internal_vars = List.flatten (List.map (fun x -> List.map fst x.insns) blocks) in
  let combined = remove_duplicates (args @ internal_vars) in
  let number i = (List.nth combined i, Ind3 (Lit (Int64.mul (Int64.of_int (i+1)) (-8L)), Rbp)) in
  List.map number (List.init (List.length combined) (fun x -> x)) (* Can't be number, has to be proper name! *)

(* The code for the entry-point of a function must do several things:

   - since our simple compiler maps local %uids to stack slots,
     compiling the control-flow-graph body of an fdecl requires us to
     compute the layout (see the discussion of locals and layout)

   - the function code should also comply with the calling
     conventions, typically by moving arguments out of the parameter
     registers (or stack slots) into local storage space.  For our
     simple compilation strategy, that local storage space should be
     in the stack. (So the function parameters can also be accounted
     for in the layout.)

   - the function entry code should allocate the stack storage needed
     to hold all of the local stack slots.
*)

(*type ctxt = { tdecls : (tid * ty) list
            ; layout : layout
            } *)

let compile_fdecl (tdecls:(tid * ty) list) (name:string) ({ f_ty; f_param; f_cfg }:fdecl) : prog =
  let locals = (List.map fst tdecls) @ f_param in
  let layout = stack_layout locals f_cfg in
  let prolog = [Pushq, [Reg Rbp]; Movq, [Reg Rsp; Reg Rbp]] @ List.mapi (fun i x -> Movq, [arg_loc i; lookup layout x]) locals in
  let program = compile_block name { tdecls; layout } (fst f_cfg) in
  [{
    lbl = name;
    global = true;
    asm = Text (prolog @ program);
  }] @ List.map (fun x -> compile_lbl_block name (fst x) { tdecls; layout } (snd x)) (snd f_cfg)



(* compile_gdecl ------------------------------------------------------------ *)
(* Compile a global value into an X86 global data declaration and map
   a global uid to its associated X86 label.
*)
let rec compile_ginit : ginit -> X86.data list = function
  | GNull     -> [Quad (Lit 0L)]
  | GGid gid  -> [Quad (Lbl (Platform.mangle gid))]
  | GInt c    -> [Quad (Lit c)]
  | GString s -> [Asciz s]
  | GArray gs | GStruct gs -> List.map compile_gdecl gs |> List.flatten
  | GBitcast (t1,g,t2) -> compile_ginit g

and compile_gdecl (_, g) = compile_ginit g


(* compile_prog ------------------------------------------------------------- *)
let compile_prog {tdecls; gdecls; fdecls} : X86.prog =
  let g = fun (lbl, gdecl) -> Asm.data (Platform.mangle lbl) (compile_gdecl gdecl) in
  let f = fun (name, fdecl) -> compile_fdecl tdecls name fdecl in
  (List.map g gdecls) @ (List.map f fdecls |> List.flatten)
