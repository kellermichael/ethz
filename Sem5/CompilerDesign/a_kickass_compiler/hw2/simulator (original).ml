
(* X86lite Simulator *)

(* See the documentation in the X86lite specification, available on the 
   course web pages, for a detailed explanation of the instruction
   semantics.
*)

open X86

(* simulator machine state -------------------------------------------------- *)

let mem_bot = 0x400000L          (* lowest valid address *)
let mem_top = 0x410000L          (* one past the last byte in memory *)
let mem_size = Int64.to_int (Int64.sub mem_top mem_bot)
let nregs = 17                   (* including Rip *)
let ins_size = 8L                (* assume we have a 8-byte encoding *)
let exit_addr = 0xfdeadL         (* halt when m.regs(%rip) = exit_addr *)

(* Your simulator should raise this exception if it tries to read from or
   store to an address not within the valid address space. *)
exception X86lite_segfault

(* The simulator memory maps addresses to symbolic bytes.  Symbolic
   bytes are either actual data indicated by the Byte constructor or
   'symbolic instructions' that take up eight bytes for the purposes of
   layout.
   The symbolic bytes abstract away from the details of how
   instructions are represented in memory.  Each instruction takes
   exactly eight consecutive bytes, where the first byte InsB0 stores
   the actual instruction, and the next sevent bytes are InsFrag
   elements, which aren't valid data.
   For example, the two-instruction sequence:
        at&t syntax             ocaml syntax
      movq %rdi, (%rsp)       Movq,  [~%Rdi; Ind2 Rsp]
      decq %rdi               Decq,  [~%Rdi]
   is represented by the following elements of the mem array (starting
   at address 0x400000):
       0x400000 :  InsB0 (Movq,  [~%Rdi; Ind2 Rsp])
       0x400001 :  InsFrag
       0x400002 :  InsFrag
       0x400003 :  InsFrag
       0x400004 :  InsFrag
       0x400005 :  InsFrag
       0x400006 :  InsFrag
       0x400007 :  InsFrag
       0x400008 :  InsB0 (Decq,  [~%Rdi])
       0x40000A :  InsFrag
       0x40000B :  InsFrag
       0x40000C :  InsFrag
       0x40000D :  InsFrag
       0x40000E :  InsFrag
       0x40000F :  InsFrag
       0x400010 :  InsFrag
*)
type sbyte = InsB0 of ins       (* 1st byte of an instruction *)
           | InsFrag            (* 2nd - 8th bytes of an instruction *)
           | Byte of char       (* non-instruction byte *)

(* memory maps addresses to symbolic bytes *)
type mem = sbyte array

(* Flags for condition codes *)
type flags = { mutable fo : bool
             ; mutable fs : bool
             ; mutable fz : bool
             }

(* Register files *)
type regs = int64 array

(* Complete machine state *)
type mach = { flags : flags
            ; regs : regs
            ; mem : mem
            }

(* simulator helper functions ----------------------------------------------- *)

(* The index of a register in the regs array *)
let rind : reg -> int = function
  | Rip -> 16
  | Rax -> 0  | Rbx -> 1  | Rcx -> 2  | Rdx -> 3
  | Rsi -> 4  | Rdi -> 5  | Rbp -> 6  | Rsp -> 7
  | R08 -> 8  | R09 -> 9  | R10 -> 10 | R11 -> 11
  | R12 -> 12 | R13 -> 13 | R14 -> 14 | R15 -> 15

(* Helper functions for reading/writing sbytes *)

(* Convert an int64 to its sbyte representation *)
let sbytes_of_int64 (i:int64) : sbyte list =
  let open Char in 
  let open Int64 in
  List.map (fun n -> Byte (shift_right i n |> logand 0xffL |> to_int |> chr))
           [0; 8; 16; 24; 32; 40; 48; 56]

(* Convert an sbyte representation to an int64 *)
let int64_of_sbytes (bs:sbyte list) : int64 =
  let open Char in
  let open Int64 in
  let f b i = match b with
    | Byte c -> logor (shift_left i 8) (c |> code |> of_int)
    | _ -> 0L
  in
  List.fold_right f bs 0L

(* Convert a string to its sbyte representation *)
let sbytes_of_string (s:string) : sbyte list =
  let rec loop acc = function
    | i when i < 0 -> acc
    | i -> loop (Byte s.[i]::acc) (pred i)
  in
  loop [Byte '\x00'] @@ String.length s - 1

(* Serialize an instruction to sbytes *)
let sbytes_of_ins (op, args:ins) : sbyte list =
  let check = function
    | Imm (Lbl _) | Ind1 (Lbl _) | Ind3 (Lbl _, _) -> 
      invalid_arg "sbytes_of_ins: tried to serialize a label!"
    | o -> ()
  in
  List.iter check args;
  [InsB0 (op, args); InsFrag; InsFrag; InsFrag;
   InsFrag; InsFrag; InsFrag; InsFrag]

(* Serialize a data element to sbytes *)
let sbytes_of_data : data -> sbyte list = function
  | Quad (Lit i) -> sbytes_of_int64 i
  | Asciz s -> sbytes_of_string s
  | Quad (Lbl _) -> invalid_arg "sbytes_of_data: tried to serialize a label!"


(* Code to print machine *)
(* ------------------------------------------------------------------------ *)
let print_sbyte (s: sbyte) : string = 
begin
  begin match s with
    | InsB0 i-> string_of_ins i
    | InsFrag -> "<- Frag ->"
    | Byte c  -> String.make 1 c
  end
end

let printMachine (m:mach) (l:int) : unit = 
begin
  (* Print the condition flags *)
  Printf.printf "Flags: o:%b s:%b f:%b\n" (m.flags.fo) (m.flags.fs) (m.flags.fz);

  (* there are exactly 17 registers in our machine *)
  (*Printf.printf "Rax" addr (print_sbyte (m.mem.(x)));*)
  Printf.printf "Rip: %Lx \n" (m.regs.(16));
  Printf.printf "Rax: %Lx \n" (m.regs.(0));
  Printf.printf "Rbx: %Lx \n" (m.regs.(1));
  Printf.printf "Rcx: %Lx \n" (m.regs.(2));
  Printf.printf "Rdx: %Lx \n" (m.regs.(3));
  Printf.printf "Rsi: %Lx \n" (m.regs.(4));
  Printf.printf "Rdi: %Lx \n" (m.regs.(5));
  Printf.printf "Rbp: %Lx \n" (m.regs.(6));
  Printf.printf "Rsp: %Lx \n" (m.regs.(7));
  for x = 8 to 15 do
    Printf.printf "R%d: %Lx \n" x (m.regs.(x));
  done;

  (* Loop over mem array and print its sbytes *)
  for x = 0 to (Array.length m.mem) - 1 do
    let addr= (Int64.add (Int64.of_int x) 0x400000L) in
      if (l>x) then begin
        Printf.printf "%Lx %s\n" addr (print_sbyte (m.mem.(x)));
        end
  done;
  ();
end
(* ------------------------------------------------------------------------ *)

(* It might be useful to toggle printing of intermediate states of your 
   simulator. Our implementation uses this mutable flag to turn on/off
   printing.  For instance, you might write something like:
     [if !debug_simulator then print_endline @@ string_of_ins u; ...]
*)
let debug_simulator = ref true

(* Interpret a condition code with respect to the given flags. *)
let interp_cnd {fo; fs; fz} : cnd -> bool = function 
  | Eq -> fz
  | Neq -> not fz
  | Lt -> fs != fo
  | Le -> (fs != fo) || fz
  | Gt -> not ((fs != fo) || fz)
  | Ge -> not (fs != fo)
  
(* Maps an X86lite address into Some OCaml array index,
   or None if the address is not within the legal address space. *)
let map_addr (addr:quad) : int option =
  begin if mem_bot <= addr && addr < mem_top then
    Some (Int64.to_int (Int64.sub addr mem_bot))
  else
    None
  end

let get_addr (addr:quad) : int =
  match map_addr addr with
  | Some i -> i
  | None -> raise X86lite_segfault

let get_imm (i:imm) : int64 = match i with
  | Lit l -> l
  | Lbl l -> int64_of_sbytes (sbytes_of_string (string_of_lbl l))

let updateRip (m:mach) : unit = m.regs.(rind Rip) <- Int64.add m.regs.(rind Rip) 8L

let read ((m,src):(mach * operand)) : int64 = match src with
  | Imm i -> get_imm i
  | Reg r -> m.regs.(rind r)
  | Ind1 i -> get_imm i
  | Ind2 i -> int64_of_sbytes [m.mem.(get_addr m.regs.(rind i))]
  | Ind3 (d, i) -> int64_of_sbytes [m.mem.(get_addr (Int64.add  m.regs.(rind i) (get_imm d)))]

let write ((m,dest,v):(mach * operand * int64)) : unit = match dest with
  | Reg r -> m.regs.(rind r) <- v
  | Ind2 i -> m.mem.(get_addr m.regs.(rind i)) <- List.nth (sbytes_of_int64 v) 0
  | Ind3 (d, i) -> m.mem.(get_addr (Int64.add  m.regs.(rind i) (get_imm d))) <- List.nth (sbytes_of_int64 v) 0
  | _ -> failwith "Write Error, Location wrong?"


let handle ((m,i):(mach * ins)) : unit = match i with
  | Movq, src::dest::[] -> write (m, dest, (read (m, src))); ()
  | _ -> ()
(* Simulates one step of the machine:
    - fetch the instruction at %rip
    - compute the source and/or destination information from the operands
    - simulate the instruction semantics
    - update the registers and/or memory appropriately
    - set the condition flags
*)

let step (m:mach) : unit = 
  let instr_sbyte = m.mem.(get_addr m.regs.(rind Rip)) in
  let instr = match instr_sbyte with
  | InsB0 i -> i
  | InsFrag -> failwith "Not an instruction"
  | Byte b -> failwith "Not an instruction"
  in
  updateRip m;
  handle (m, instr); ()
  


(* Runs the machine until the rip register reaches a designated
   memory address. Returns the contents of %rax when the 
   machine halts. *)
let run (m:mach) : int64 = 
  while m.regs.(rind Rip) <> exit_addr do step m done;
  m.regs.(rind Rax)

(* assembling and linking --------------------------------------------------- *)

(* A representation of the executable *)
type exec = { entry    : quad              (* address of the entry point *)
            ; text_pos : quad              (* starting address of the code *)
            ; data_pos : quad              (* starting address of the data *)
            ; text_seg : sbyte list        (* contents of the text segment *)
            ; data_seg : sbyte list        (* contents of the data segment *)
            }

(* Assemble should raise this when a label is used but not defined *)
exception Undefined_sym of lbl

(* Assemble should raise this when a label is defined more than once *)
exception Redefined_sym of lbl

(* Convert an X86 program into an object file:
   - separate the text and data segments
   - compute the size of each segment
      Note: the size of an Asciz string section is (1 + the string length)
            due to the null terminator
   - resolve the labels to concrete addresses and 'patch' the instructions to 
     replace Lbl values with the corresponding Imm values.
   - the text segment starts at the lowest address
   - the data segment starts after the text segment
  HINT: List.fold_left and List.fold_right are your friends.
 *)

 (*
 type elem = { lbl: lbl; global: bool; asm: asm }
 type prog = elem list

 type asm = Text of ins list    (* code *)
         | Data of data list   (* data *)

 type ins = opcode * operand list     

 type sbyte = InsB0 of ins       (* 1st byte of an instruction *)
           | InsFrag            (* 2nd - 8th bytes of an instruction *)
           | Byte of char       (* non-instruction byte *)
 *)

(* TODO: Redefined symbol error *)
(* TODO: Filter only Data segments *)

type translation = {
  mutable label: string;
  mutable address: int64
  }

type assembler = {
  mutable text: sbyte list;
  mutable data: sbyte list; 
  mutable text_length: int64;
  mutable text_labels: string list;
  mutable data_labels: translation list;
  mutable tmp: int64;
}

let print_assembler (a:assembler) : unit =
  Printf.printf "text: %s\n data: %s\n text-length: %Ld\n text-labels: %s\n data-labels: %s\n" 
    (List.fold_right (fun x b -> b ^ ", " ^(print_sbyte x)) a.text "")
    (List.fold_right (fun x b -> b ^ ", " ^(print_sbyte x)) a.data "")
    (a.text_length)
    (List.fold_right (fun x b -> b ^ ", " ^ x) a.text_labels "")
    (List.fold_right (fun x b -> b ^ ", (" ^ Int64.to_string (x.address) ^ "," ^ x.label ^ ")") a.data_labels "")

let contains (ls: translation list) (l:lbl) : bool = 
  List.fold_right (fun x b -> b || ((String.compare (x.label) l)==0)) ls false

let labelCheck (a:assembler) (l:lbl): unit =
  let defined =  contains a.data_labels l in
      if defined then 
        ()
      else
        raise (Undefined_sym l)


let resolveAddr (a:assembler) (l:lbl): int64 = 
  let base = Int64.add mem_bot a.text_length in
    labelCheck a l;
    Int64.add (List.fold_right (fun x b -> if ((String.compare x.label l)==0) then x.address else b) a.data_labels 0L) base

let linker (a:assembler) : assembler = begin
  a.text <- List.fold_right (fun x b -> 
  begin match x with
    | InsB0 (cd,ls) -> [InsB0 (cd, List.map (fun c ->
      begin match c with                      
        | Imm (Lbl l)      -> Imm (Lit (resolveAddr a l))
        | Ind1 (Lbl l)     -> Ind1 (Lit (resolveAddr a l))
        | Ind3 ((Lbl l),r) -> Ind3 ((Lit (resolveAddr a l)),r)
        | _ -> c
      end) ls)] @ b
    | _ -> [x] @ b
  end) a.text [];
  a
end

let staticCheck (a:assembler) : unit =
  let main = List.exists (fun x -> (String.compare "main" x)==0) a.text_labels in
    if main then
      (List.fold_right (fun x b ->
      if ((List.length (List.filter (fun e -> (String.compare e.label x.label)==0) a.data_labels))>=2)
      then raise (Redefined_sym x.label)
      else ())
      a.data_labels ();)
    else
      raise (Undefined_sym "main")


let build (p: elem list) : assembler =
begin
  let res = (List.fold_left (fun b e ->
    let frag = Array.to_list (Array.make 7 InsFrag) in
      begin match e.asm with
        | Text lst -> b.text <- b.text @ (List.fold_right (fun x b -> ([InsB0 x] @ frag) @ b) lst []); (* sbytes_of_ins x *)
                      b.text_labels <- b.text_labels @ [e.lbl];
                      b 
        | Data lst -> let var = (List.fold_right (fun x b -> (sbytes_of_data x) @ b) lst []) in
                        b.data <- b.data @ var;
                        b.data_labels <- b.data_labels @ [{label=e.lbl; address=b.tmp}];
                        b.tmp <- Int64.add b.tmp (Int64.of_int (List.length var));
                        b
      end ) {text=[]; data=[]; text_length=0L; text_labels=[]; data_labels=[]; tmp=0L} p)
      in res.text_length <- Int64.of_int (List.length res.text); res
  end

let assemble (p:prog) : exec =
  let bld = build p in
      staticCheck bld;
      let assem = (linker bld) in 
        {entry = Int64.add mem_bot 8L;
        text_pos = mem_bot;
        data_pos = Int64.add mem_bot assem.text_length;
        text_seg = assem.text;
        data_seg = assem.data}

(*
type exec = { entry    : quad              (* address of the entry point *)
            ; text_pos : quad              (* starting address of the code *)
            ; data_pos : quad              (* starting address of the data *)
            ; text_seg : sbyte list        (* contents of the text segment *)
            ; data_seg : sbyte list        (* contents of the data segment *)
            }
*)

(* Convert an object file into an executable machine state. 
    - allocate the mem array
    - set up the memory state by writing the symbolic bytes to the 
      appropriate locations 
    - create the inital register state
      - initialize rip to the entry point address
      - initializes rsp to the last word in memory 
      - the other registers are initialized to 0
    - the condition code flags start as 'false'
  Hint: The Array.make, Array.blit, and Array.of_list library functions 
  may be of use.
*)
let load {entry; text_pos; data_pos; text_seg; data_seg} : mach = begin
  let mem = Array.make mem_size (Byte '\x00') in
    let regs = Array.make nregs 0L in
      regs.(rind Rip) <- entry;
      regs.(rind Rsp) <- Int64.sub mem_top 8L;

      (* Add text section *)
      Array.blit (Array.of_list text_seg) 0 mem (get_addr text_pos) (List.length text_seg); 

      (* Add data section *)
      Array.blit (Array.of_list data_seg) 0 mem (get_addr data_pos) (List.length data_seg);

      (* Add Exit Address *)
      Array.blit (Array.of_list (sbytes_of_int64 exit_addr)) 0 mem (get_addr (Int64.sub mem_top 8L)) 8;

      {flags={fo=false; fs=false; fz=false}; regs=regs; mem=mem}
end