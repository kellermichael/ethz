(* X86lite Simulator *)

(* See the documentation in the X86lite specification, available on the 
   course web pages, for a detailed explanation of the instruction
   semantics.
*)

open X86

(* simulator machine state -------------------------------------------------- *)

let mem_bot = 0x400000L          (* lowest valid address *)
let mem_top = 0x410000L          (* one past the last byte in memory *)
let mem_size = Int64.to_int (Int64.sub mem_top mem_bot)
let nregs = 17                   (* including Rip *)
let ins_size = 8L                (* assume we have a 8-byte encoding *)
let exit_addr = 0xfdeadL         (* halt when m.regs(%rip) = exit_addr *)

(* Your simulator should raise this exception if it tries to read from or
   store to an address not within the valid address space. *)
exception X86lite_segfault

(* The simulator memory maps addresses to symbolic bytes.  Symbolic
   bytes are either actual data indicated by the Byte constructor or
   'symbolic instructions' that take up eight bytes for the purposes of
   layout.

   The symbolic bytes abstract away from the details of how
   instructions are represented in memory.  Each instruction takes
   exactly eight consecutive bytes, where the first byte InsB0 stores
   the actual instruction, and the next sevent bytes are InsFrag
   elements, which aren't valid data.

   For example, the two-instruction sequence:
        at&t syntax             ocaml syntax
      movq %rdi, (%rsp)       Movq,  [~%Rdi; Ind2 Rsp]
      decq %rdi               Decq,  [~%Rdi]

   is represented by the following elements of the mem array (starting
   at address 0x400000):

       0x400000 :  InsB0 (Movq,  [~%Rdi; Ind2 Rsp])
       0x400001 :  InsFrag
       0x400002 :  InsFrag
       0x400003 :  InsFrag
       0x400004 :  InsFrag
       0x400005 :  InsFrag
       0x400006 :  InsFrag
       0x400007 :  InsFrag
       0x400008 :  InsB0 (Decq,  [~%Rdi])
       0x40000A :  InsFrag
       0x40000B :  InsFrag
       0x40000C :  InsFrag
       0x40000D :  InsFrag
       0x40000E :  InsFrag
       0x40000F :  InsFrag
       0x400010 :  InsFrag
*)
type sbyte = InsB0 of ins       (* 1st byte of an instruction *)
           | InsFrag            (* 2nd - 8th bytes of an instruction *)
           | Byte of char       (* non-instruction byte *)

(* memory maps addresses to symbolic bytes *)
type mem = sbyte array

(* Flags for condition codes *)
type flags = { mutable fo : bool
             ; mutable fs : bool
             ; mutable fz : bool
             }

(* Register files *)
type regs = int64 array

(* Complete machine state *)
type mach = { flags : flags
            ; regs : regs
            ; mem : mem
            }

(* Code to print machine *)
(* ------------------------------------------------------------------------ *)
let print_sbyte (s: sbyte) : string = 
begin
  begin match s with
    | InsB0 i-> string_of_ins i
    | InsFrag -> "<- Frag ->"
    | Byte c  -> String.make 1 c
  end
end

let printMachine (m:mach) (l:int) : unit = 
begin
  (* Print the condition flags *)
  Printf.printf "Flags: o:%b s:%b f:%b\n" (m.flags.fo) (m.flags.fs) (m.flags.fz);

  (* there are exactly 17 registers in our machine *)
  (*Printf.printf "Rax" addr (print_sbyte (m.mem.(x)));*)
  Printf.printf "Rip: %Lx \n" (m.regs.(16));
  Printf.printf "Rax: %Lx \n" (m.regs.(0));
  Printf.printf "Rbx: %Lx \n" (m.regs.(1));
  Printf.printf "Rcx: %Lx \n" (m.regs.(2));
  Printf.printf "Rdx: %Lx \n" (m.regs.(3));
  Printf.printf "Rsi: %Lx \n" (m.regs.(4));
  Printf.printf "Rdi: %Lx \n" (m.regs.(5));
  Printf.printf "Rbp: %Lx \n" (m.regs.(6));
  Printf.printf "Rsp: %Lx \n" (m.regs.(7));
  for x = 8 to 15 do
    Printf.printf "R%d: %Lx \n" x (m.regs.(x));
  done;

  (* Loop over mem array and print its sbytes *)
  for x = 0 to (Array.length m.mem) - 1 do
    let addr= (Int64.add (Int64.of_int x) 0x400000L) in
      if (l>x) then begin
        Printf.printf "%Lx %s\n" addr (print_sbyte (m.mem.(x)));
        end
  done;
  ();
end
(* ------------------------------------------------------------------------ *)

(* simulator helper functions ----------------------------------------------- *)

(* The index of a register in the regs array *)
let rind : reg -> int = function
  | Rip -> 16
  | Rax -> 0  | Rbx -> 1  | Rcx -> 2  | Rdx -> 3
  | Rsi -> 4  | Rdi -> 5  | Rbp -> 6  | Rsp -> 7
  | R08 -> 8  | R09 -> 9  | R10 -> 10 | R11 -> 11
  | R12 -> 12 | R13 -> 13 | R14 -> 14 | R15 -> 15

(* Helper functions for reading/writing sbytes *)

(* Convert an int64 to its sbyte representation *)
let sbytes_of_int64 (i:int64) : sbyte list =
  let open Char in 
  let open Int64 in
  List.map (fun n -> Byte (shift_right i n |> logand 0xffL |> to_int |> chr))
           [0; 8; 16; 24; 32; 40; 48; 56]

(* Convert an sbyte representation to an int64 *)
let int64_of_sbytes (bs:sbyte list) : int64 =
  let open Char in
  let open Int64 in
  let f b i = match b with
    | Byte c -> logor (shift_left i 8) (c |> code |> of_int)
    | _ -> 0L
  in
  List.fold_right f bs 0L

(* Convert a string to its sbyte representation *)
let sbytes_of_string (s:string) : sbyte list =
  let rec loop acc = function
    | i when i < 0 -> acc
    | i -> loop (Byte s.[i]::acc) (pred i)
  in
  loop [Byte '\x00'] @@ String.length s - 1

(* Serialize an instruction to sbytes *)
let sbytes_of_ins (op, args:ins) : sbyte list =
  let check = function
    | Imm (Lbl _) | Ind1 (Lbl _) | Ind3 (Lbl _, _) -> 
      invalid_arg "sbytes_of_ins: tried to serialize a label!"
    | o -> ()
  in
  List.iter check args;
  [InsB0 (op, args); InsFrag; InsFrag; InsFrag;
   InsFrag; InsFrag; InsFrag; InsFrag]

(* Serialize a data element to sbytes *)
let sbytes_of_data : data -> sbyte list = function
  | Quad (Lit i) -> sbytes_of_int64 i
  | Asciz s -> sbytes_of_string s
  | Quad (Lbl _) -> invalid_arg "sbytes_of_data: tried to serialize a label!"


(* It might be useful to toggle printing of intermediate states of your 
   simulator. Our implementation uses this mutable flag to turn on/off
   printing.  For instance, you might write something like:

     [if !debug_simulator then print_endline @@ string_of_ins u; ...]

*)
let debug_simulator = ref false

(* Interpret a condition code with respect to the given flags. *)
let interp_cnd {fo; fs; fz} : cnd -> bool = fun x ->
begin match x with
   | Eq -> fz
   | Neq -> fz==false
   | Gt -> (fz==false) && (fs==fo)
   | Ge -> fs==fo
   | Lt -> fs!=fo
   | Le -> (fz)||(fs!=fo)
end

(* Maps an X86lite address into Some OCaml array index,
   or None if the address is not within the legal address space. *)
(* WARNING: (int64.compare index mem_size)<=0 *)
let map_addr (addr:quad) : int option =
  let (index:int64) = Int64.sub addr mem_bot in
    begin
      if (index>=0L && (Int64.of_int mem_size)>index) then
        Some (Int64.to_int index)
      else
        None
    end

let map_addr_int (addr:quad) : int =
  let (index:int64) = Int64.sub addr mem_bot in
    begin
      if (index>=0L && (Int64.of_int mem_size)>index) then
        Int64.to_int index
      else
        raise X86lite_segfault;
    end


let get_imm (a: imm) : int64 =
  begin match a with
    | Lit i -> i
    | Lbl l -> int64_of_sbytes (sbytes_of_string (string_of_lbl l))
  end

let updateRip (m:mach) : unit = m.regs.(rind Rip) <- Int64.add m.regs.(rind Rip) 8L

(* converts an sbyte to an int, only using a base address *)
(* bs is a machine base address *)
let getMemoryValue (m:mach) (bs:int) : int64 =
  begin
    let arr = List.map (fun i -> m.mem.(bs + i)) [0;1;2;3;4;5;6;7] in     (* take the subsequent 7 bytes after regAddress *)
      begin
        int64_of_sbytes arr                                               (* It is now possible to convert sbytes to int64 *)
      end
  end


(* Function that transforms an operand to an int64 value *)
let opValue (m:mach) (op:operand) : int64 =
begin
  begin match op with
    |Imm i       -> get_imm i                                                     (* immediate value (WARNING: Imm might be a label) *)
    |Reg r       -> m.regs.(rind r)                                               (* register value *)
    |Ind1 i      -> int64_of_sbytes [m.mem.(map_addr_int (get_imm i))]
    |Ind2 r      -> let regAddress = m.regs.(rind r) in                           (* treats value in register as address WARNING: map_addr??*)
                    begin
                      int64_of_sbytes [m.mem.(map_addr_int regAddress)]
                    end
    |Ind3 ((Lit o),r)  -> let baseAddr = m.regs.(rind r) in begin                 (* treats value in register as base address *)
                            let addr = Int64.add baseAddr o in                    (* Look up address (baseAddr+offset) in memory and convert it from sbyte to int64 *)
                              begin
                                int64_of_sbytes [m.mem.(map_addr_int addr)];
                              end
                          end
    |_           -> failwith "cant resolve operand to int64";
  end
end

let write (m:mach) (vl:int64) (ds:operand) : unit =
  begin
    begin match ds with
      | Reg r  -> m.regs.(rind r) <- vl; ();
      | Ind2 i -> m.mem.(map_addr_int m.regs.(rind i)) <- List.nth (sbytes_of_int64 vl) 0
      | Ind3 (d, i) -> m.mem.(map_addr_int (Int64.add  m.regs.(rind i) (get_imm d))) <- List.nth (sbytes_of_int64 vl) 0
      |_       -> failwith "invalid destination operand"
    end
  end

(* Simulates one step of the machine:
    - fetch the instruction at %rip
    - compute the source and/or destination information from the operands
    - simulate the instruction semantics
    - update the registers and/or memory appropriately
    - set the condition flags
*)
  

let step (m:mach) : unit =
   (* mem array index of instruction to fetch *)
   let instrIndex=  m.regs.(rind Rip) in begin
      (* get sbyte at instruction index *)
      let (instr: sbyte) = m.mem.(map_addr_int instrIndex) in begin
        (* Check if instruction pointer points to an instruction *)
        begin match instr with
          |InsB0 (Movq, [src; dst]) -> write m (opValue m src) dst; updateRip m;
          |InsB0 (Addq, [src; dst])    -> write m (Int64.add (opValue m src) (opValue m dst)) dst; updateRip m;
          |InsB0 (Pushq, [src])        -> let newRsp = Int64.sub (m.regs.(rind Rsp)) 8L in
                                            m.regs.(rind Rsp)<-newRsp; write m (opValue m src) (Ind2 Rsp); updateRip m;
          |InsB0 (Leaq, [src; dst])    -> ()
          |InsB0 (Incq, [src; dst])    -> ()
          |InsB0 (Decq, [src; dst])    -> ()
          |InsB0 (Negq, [dst])         -> write m (Int64.neg (opValue m dst)) dst; updateRip m;
          |InsB0 (Notq, [src; dst])    -> ()
          |InsB0 (Subq, [src; dst])    -> write m (Int64.sub (opValue m src) (opValue m dst)) dst; updateRip m;
          |InsB0 (Imulq, [src; dst])   -> write m (Int64.mul (opValue m src) (opValue m dst)) dst; updateRip m;
          |InsB0 (Xorq, [src; dst])    -> ()
          |InsB0 (Orq, [src; dst])     -> ()
          |InsB0 (Andq, [src; dst])    -> write m (Int64.logand (opValue m src) (opValue m dst)) dst; updateRip m;
          |InsB0 (Shlq, [src; dst])    -> ()
          |InsB0 (Sarq, [src; dst])    -> ()
          |InsB0 (Jmp, [src; dst])     -> ()
          |InsB0 (J cnd, [src; dst])   -> ()
          |InsB0 (Cmpq, [src; dst])    -> ()
          |InsB0 (Set cnd, [src; dst]) -> ()
          |InsB0 (Callq, [src; dst])   -> ()
          |InsB0 (Retq, [src; dst])    -> ()
          |_ -> failwith "invalid machine state"
        end
      end
    end

    let handle 

(* Runs the machine until the rip register reaches a designated
   memory address. Returns the contents of %rax when the 
   machine halts. *)
let run (m:mach) : int64 = 
  while m.regs.(rind Rip) <> exit_addr do step m done;
  m.regs.(rind Rax)

(* assembling and linking --------------------------------------------------- *)

(* A representation of the executable *)
type exec = { entry    : quad              (* address of the entry point *)
            ; text_pos : quad              (* starting address of the code *)
            ; data_pos : quad              (* starting address of the data *)
            ; text_seg : sbyte list        (* contents of the text segment *)
            ; data_seg : sbyte list        (* contents of the data segment *)
            }

(* Assemble should raise this when a label is used but not defined *)
exception Undefined_sym of lbl

(* Assemble should raise this when a label is defined more than once *)
exception Redefined_sym of lbl

(* Convert an X86 program into an object file:
   - separate the text and data segments
   - compute the size of each segment
      Note: the size of an Asciz string section is (1 + the string length)
            due to the null terminator

   - resolve the labels to concrete addresses and 'patch' the instructions to 
     replace Lbl values with the corresponding Imm values.

   - the text segment starts at the lowest address
   - the data segment starts after the text segment

  HINT: List.fold_left and List.fold_right are your friends.
 *)
let assemble (p:prog) : exec =
failwith "assemble unimplemented"

(* Convert an object file into an executable machine state. 
    - allocate the mem array
    - set up the memory state by writing the symbolic bytes to the 
      appropriate locations 
    - create the inital register state
      - initialize rip to the entry point address
      - initializes rsp to the last word in memory 
      - the other registers are initialized to 0
    - the condition code flags start as 'false'

  Hint: The Array.make, Array.blit, and Array.of_list library functions 
  may be of use.
*)
let load {entry; text_pos; data_pos; text_seg; data_seg} : mach = begin
  {{false;false;false}; Array.make 17 Int64.zero; Array.make mem_size sbyte}
  end

  (*
  type mem = sbyte array

(* Flags for condition codes *)
type flags = { mutable fo : bool
             ; mutable fs : bool
             ; mutable fz : bool
             }

(* Register files *)
type regs = int64 array

  type mach = { flags : flags
            ; regs : regs
            ; mem : mem
            }

  let test_exec: exec =
  { entry = 0x400008L
  ; text_pos = 0x400000L
  ; data_pos = 0x400064L
  ; text_seg = [] 
  ; data_seg = []
  }

  GradedTest("Easy Load Tests", 5,[
    ("load_flags", assert_eqf (fun () -> (load test_exec).flags)
                              {fo = false; fs = false; fz = false});
    ("load_rip", assert_eqf (fun () -> (load test_exec).regs.(rind Rip))
                             0x400008L);
    ("load_rsp", assert_eqf (fun () -> (load test_exec).regs.(rind Rsp))
                             0x40FFF8L);
  ]);
  *)
