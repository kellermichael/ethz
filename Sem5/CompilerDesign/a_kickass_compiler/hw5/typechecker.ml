open Ast
open Astlib
open Tctxt

(* Error Reporting ---------------------------------------------------------- *)
(* NOTE: Use type_error to report error messages for ill-typed programs. *)

exception TypeError of string

let type_error (l : 'a node) err = 
  let (_, (s, e), _) = l.loc in
  raise (TypeError (Printf.sprintf "[%d, %d] %s" s e err))

let t1 (a,_,_) = a
let t2 (_,a,_) = a
let t3 (_,_,a) = a

let d1 (a,_) = a
let d2 (_,a) = a

(* initial context: G0 ------------------------------------------------------ *)
(* The Oat types of the Oat built-in functions *)
let builtins =
  [ "array_of_string",  ([TRef RString],  RetVal (TRef(RArray TInt)))
  ; "string_of_array",  ([TRef(RArray TInt)], RetVal (TRef RString))
  ; "length_of_string", ([TRef RString],  RetVal TInt)
  ; "string_of_int",    ([TInt], RetVal (TRef RString))
  ; "string_cat",       ([TRef RString; TRef RString], RetVal (TRef RString))
  ; "print_string",     ([TRef RString],  RetVoid)
  ; "print_int",        ([TInt], RetVoid)
  ; "print_bool",       ([TBool], RetVoid)
  ]

(* binary operation types --------------------------------------------------- *)
let typ_of_binop : Ast.binop -> Ast.ty * Ast.ty * Ast.ty = function
  | Add | Mul | Sub | Shl | Shr | Sar | IAnd | IOr -> (TInt, TInt, TInt)
  | Lt | Lte | Gt | Gte -> (TInt, TInt, TBool)
  | And | Or -> (TBool, TBool, TBool)
  | Eq | Neq -> failwith "typ_of_binop called on polymorphic == or !="

(* unary operation types ---------------------------------------------------- *)
let typ_of_unop : Ast.unop -> Ast.ty * Ast.ty = function
  | Neg | Bitnot -> (TInt, TInt)
  | Lognot       -> (TBool, TBool)

(* subtyping ---------------------------------------------------------------- *)
(* Decides whether H |- t1 <: t2 
    - assumes that H contains the declarations of all the possible struct types

    - you will want to introduce addition (possibly mutually recursive) 
      helper functions to implement the different judgments of the subtyping
      relation. We have included a template for subtype_ref to get you started.
      (Don't forget about OCaml's 'and' keyword.)
*)
let rec subtype (c : Tctxt.t) (t1 : Ast.ty) (t2 : Ast.ty) : bool =
  begin match (t1,t2) with
    | (TBool,TBool)              -> true
    | (TInt, TInt)               -> true
    | (TRef r1, TRef r2)         -> subtype_ref c r1 r2
    | (TNullRef r1, TNullRef r2) -> subtype_ref c r1 r2
    | (TRef r1, TNullRef r2)     -> subtype_ref c r1 r2
    | _                          -> false
  end

(* Decides whether H |-r ref1 <: ref2 *)
and subtype_ref (c : Tctxt.t) (t1 : Ast.rty) (t2 : Ast.rty) : bool =
let len x = List.length x in
  begin match (t1, t2) with
  | (RString, RString)                    -> true
  | (RArray t1, RArray t2)                -> subtype c t2 t1
  | (RStruct id1, RStruct id2)            -> let str1 = Tctxt.lookup_struct_option id1 c in
                                             let str2 = Tctxt.lookup_struct_option id2 c in
                                              begin match (str1, str2) with
                                                | (Some s1,Some s2) ->  if (len s1) >= (len s2) then
                                                                          let look a b = Tctxt.lookup_field_option a b c in
                                                                          let fldValid st_name f_name = begin match look st_name f_name with
                                                                                                          | Some e -> true
                                                                                                          | None   -> false
                                                                                                        end in
                                                                          let fldTy st_name f_name = begin match look st_name f_name with
                                                                                                          | Some e -> e
                                                                                                          | None   -> failwith "this case should never happen"
                                                                                                        end in
                                                                          let validTyp = List.mapi (fun i x -> (fldValid id1 x.fieldName) && (x.ftyp==(fldTy id1 x.fieldName))) s2 in
                                                                          let validLbl = List.mapi (fun i x -> (fldValid id1 x.fieldName)) s2 in
                                                                          List.fold_left (fun b x -> x && b) true (validTyp @ validLbl)
                                                                        else
                                                                          false (*struct S2 has to be a subset of struct S1*)
                                                | _                 -> false (* at least one of the structs is undefined *)
                                              end
                                              
    | (RFun (lst1,ty1), RFun (lst2,ty2))  ->  if (len lst1)==(len lst2) then
                                                let combined = List.combine lst1 lst2 in
                                                let args = List.fold_left (fun b x -> (subtype c (snd x) (fst x)) && b) true combined in
                                                let ret = subtype_ret_ty c ty1 ty2 in
                                                  ret && args
                                              else
                                                false
                                                (* Both functions must have an equal number of arguments *)
    | _                                   -> false
  end

and subtype_ret_ty (c: Tctxt.t) (t1 : Ast.ret_ty) (t2 : Ast.ret_ty) : bool =
  begin match (t1,t2) with
    | (RetVoid, RetVoid)      -> true
    | (RetVal t1, RetVal t2)  -> subtype c t1 t2
    | _                       -> false
  end


(* well-formed types -------------------------------------------------------- *)
(* Implement a (set of) functions that check that types are well formed according
   to the H |- t and related inference rules

    - the function should succeed by returning () if the type is well-formed
      according to the rules

    - the function should fail using the "type_error" helper function if the 
      type is 

    - l is just an ast node that provides source location information for
      generating error messages (it's only needed for the type_error generation)

    - tc contains the structure definition context
 *)
let rec typecheck_ty (l : 'a Ast.node) (tc : Tctxt.t) (t : Ast.ty) : unit =
  begin match t with
    | TBool        -> ()
    | TInt         -> ()
    | TRef rty     -> typecheck_rty l tc rty
    | TNullRef rty -> typecheck_rty l tc rty
  end

and typecheck_rty (l : 'a Ast.node) (tc : Tctxt.t) (t : Ast.rty) : unit =
  begin match t with
    | RString     -> ()
    | RArray t    -> typecheck_ty l tc t
    | RStruct id  -> begin match Tctxt.lookup_struct_option id tc with
                      | None     -> type_error l "struct definition not found [typecheck_rty]"
                      | Some s   -> let allWellFormed = true (*List.fold_left (fun b x -> ((typecheck_ty l tc x.ftyp)==()) && b) true s*) in
                                  begin match allWellFormed with
                                    | true  -> ()
                                    | false -> type_error l "not all struct components are well defined [typecheck_rty]"
                                  end
                     end
    | RFun (lst,rt) -> let allWellFormed = List.fold_left (fun b x -> ((typecheck_ty l tc x)==()) && b) true lst in
                       let retTypOk = typecheck_ret_ty l tc rt in
                          begin match allWellFormed with
                            | true  -> retTypOk
                            | false -> type_error l "not all function arguments well defined [typecheck_rty]"
                          end
  end

and typecheck_ret_ty (l : 'a Ast.node) (tc : Tctxt.t) (t : Ast.ret_ty) : unit =
 begin match t with
   | RetVoid   -> ()
   | RetVal ty -> typecheck_ty l tc ty
 end

(* typechecking expressions ------------------------------------------------- *)
(* Typechecks an expression in the typing context c, returns the type of the
   expression.  This function should implement the inference rules given in the
   oad.pdf specification.  There, they are written:

       H; G; L |- exp : t

   See tctxt.ml for the implementation of the context c, which represents the
   four typing contexts: H - for structure definitions G - for global
   identifiers L - for local identifiers

   Returns the (most precise) type for the expression, if it is type correct
   according to the inference rules.

   Uses the type_error function to indicate a (useful!) error message if the
   expression is not type correct.  The exact wording of the error message is
   not important, but the fact that the error is raised, is important.  (Our
   tests also do not check the location information associated with the error.)

   Notes: - Structure values permit the programmer to write the fields in any
   order (compared with the structure definition).  This means that, given the
   declaration struct T { a:int; b:int; c:int } The expression new T {b=3; c=4;
   a=1} is well typed.  (You should sort the fields to compare them.)

*)
let duplicates_in_list (lst: 'a list) : bool = 
let num_dup = List.map (fun x -> List.length (List.filter (fun y -> x==y) lst)) lst in
List.fold_right (fun x b -> b && x==1) num_dup true

let rec typecheck_exp (c : Tctxt.t) (e : Ast.exp node) : Ast.ty =
  begin match e.elt with
    | CNull rty          -> let check = typecheck_rty e c rty in
                              check; TNullRef rty
    | CBool b            -> TBool
    | CInt i             -> TInt
    | CStr str           -> TRef RString
    | Id i               -> let defined = Tctxt.lookup_option i c in
                              begin match defined with
                              | Some s  -> s
                              | None    -> type_error e "variable undefined"
                              end
    | CArr (t,lst)       -> let types = List.map (fun x -> typecheck_exp c x) lst in
                            let valid = List.fold_right (fun x b -> b && subtype c x t) types true in
                            let wellFormed = typecheck_ty e c t in
                              begin match valid with
                              | true  -> wellFormed; (TRef (RArray t))
                              | false -> type_error e "t is not a subtype of every array element type"
                              end
    | NewArr (t,e1,x,e2) -> let wellFormed = typecheck_ty e c t in (* Type t should be well typed *)
                            let e1Ty = typecheck_exp c e1 in       (* should be of Type TInt *)
                            let lookupX = lookup_local_option x c in
                            begin match (lookupX,e1Ty) with
                            | (Some c,_)   -> type_error e "free variable condition violation in NewArr"
                            | (None, TInt) ->
                              let c' = Tctxt.add_local c x TInt in
                              let e2Ty = typecheck_exp c' e2 in
                              let subtype = subtype c e2Ty t in
                              begin match subtype with
                              | true  -> wellFormed; TRef (RArray t)
                              | false -> type_error e "subtype condition violated"
                              end
                            | _            -> type_error e "first expression must be of type TInt"
                            end
    | Index (e1,e2)      -> let arr = typecheck_exp c e1 in
                            let index = typecheck_exp c e2 in
                              begin match (arr,index) with
                              (*| (TRef (RArray (TNullRef rty)), TInt) -> type_error e "Can't index array which might be null"*)
                              | (TRef (RArray ty), TInt) -> ty
                              | (TRef (RArray ty), _)    -> type_error e "second index expression must be of type TInt"
                              | (_, TInt)                -> type_error e "first index expression must be of type t[]"
                              | (_,_)                    -> type_error e "both index expressions are of wrong type"
                              end
    | Length e           -> let arr = typecheck_exp c e in
                              begin match arr with
                                | TRef (RArray ty) -> TInt
                                | _                -> type_error e "expression within length must be of type t[]"
                              end
    | CStruct (i,lst)    -> begin match lookup_struct_option i c with
                             | None   -> type_error e "Struct undefined"
                             | Some s -> let fields' = List.map (fun x -> d1 x) lst in
                                         let types' = List.map (fun x -> typecheck_exp c (d2 x)) lst in
                                         (*let fields = List.map (fun x -> x.fieldName) s in*)
                                         (*let types = List.map (fun x -> x.ftyp) s in*)
                                         let length = (List.length s) == (List.length lst) in
                                         let duplicates = duplicates_in_list lst in
                                         begin match length&&duplicates with
                                          | true -> let valid l = begin match (lookup_field_option i (List.nth fields' l) c) with
                                                        | None   -> type_error e "Error 404, field not found"
                                                        | Some r -> r
                                                       end in
                                                    let check = List.mapi (fun j x -> subtype c x (valid j)) types' in
                                                     check; TRef (RStruct i)
                                          | false -> type_error e "struct initialisation failed"
                                         end
                                         
                            end
    | Proj (e1,fld)       -> let eTy = typecheck_exp c e1 in
                            begin match eTy with
                              | TRef (RStruct id) -> 
                                  begin match (lookup_field_option id fld c) with 
                                   | Some s -> s
                                   | None   -> type_error e "Field not found"
                                  end
                              | _ -> type_error e "projection type must be a struct"
                            end
    | Call (e1,lst)      -> let fstTy = typecheck_exp c e1 in
                            let types' = List.map (fun x -> typecheck_exp c x) lst in
                            let funDef = begin match fstTy with
                                          | TRef (RFun (lst, ret)) -> (lst,ret)
                                          | _                      -> type_error e "Function type expected"
                                         end in
                            let types = d1 funDef in
                            let retTy = begin match d2 funDef with
                                         | RetVoid  -> type_error e "Function has return type void"
                                         | RetVal t -> t
                                        end in
                            let len_check = List.length types == List.length lst in
                            begin match len_check with
                             | true  -> let ty_check = List.mapi (fun j x -> subtype c x (List.nth types j)) types' in
                                        let valid = List.fold_left (fun x b -> b && x) true ty_check in
                                          if valid then retTy else type_error e "subtype violation in Call"
                             | false -> type_error e "number of arguments do not match"
                            end            
    | Bop (Neq,e1,e2)    -> let e1Ty = typecheck_exp c e1 in
                            let e2Ty = typecheck_exp c e2 in
                            let subTy a b = subtype c a b in
                            let valid = (subTy e1Ty e2Ty) && (subTy e2Ty e1Ty) in
                              if valid then TBool else type_error e "involved types not equal"
    | Bop (Eq,e1,e2)     -> let e1Ty = typecheck_exp c e1 in
                            let e2Ty = typecheck_exp c e2 in
                            let subTy a b = subtype c a b in
                            let valid = (subTy e1Ty e2Ty) && (subTy e2Ty e1Ty) in
                              if valid then TBool else type_error e"involved types not equal"
    | Bop (op,e1,e2)     -> let e1Ty = typecheck_exp c e1 in
                            let e2Ty = typecheck_exp c e2 in
                            let opTy = (typ_of_binop op) in
                            let valid = (e1Ty==(t1 opTy))&&(e2Ty==(t2 opTy)) in
                              if valid then t3 opTy else type_error e "binary operation types not matching"
    | Uop (op,e1)        -> let e1Ty = typecheck_exp c e1 in
                            let opTy = (typ_of_unop op) in
                            let valid = e1Ty==(d1 opTy) in
                              if valid then d2 opTy else type_error e "unary operation type not matching"
  end

let rec typecheck_gexp (c : Tctxt.t) (e : Ast.exp node) : Ast.ty =
begin match e.elt with
  | CNull rty          -> typecheck_exp c e
  | CBool b            -> typecheck_exp c e
  | CInt i             -> typecheck_exp c e
  | CStr str           -> typecheck_exp c e
  | CArr (t,lst)       -> typecheck_exp c e
  | CStruct (i,lst)    -> typecheck_exp c e
  | Id i               -> typecheck_exp c e
  | _                  -> type_error e "global expression not allowed"
end

(* statements --------------------------------------------------------------- *)

(* Typecheck a statement 
   This function should implement the statment typechecking rules from oat.pdf.  
   Inputs:
    - tc: the type context
    - s: the statement node
    - to_ret: the desired return type (from the function declaration)
   Returns:
     - the new type context (which includes newly declared variables in scope
       after this statement
     - A boolean indicating the return behavior of a statement:
        false:  might not return
        true: definitely returns 
        in the branching statements, both branches must definitely return
        Intuitively: if one of the two branches of a conditional does not 
        contain a return statement, then the entier conditional statement might 
        not return.
  
        looping constructs never definitely return 
   Uses the type_error function to indicate a (useful!) error message if the
   statement is not type correct.  The exact wording of the error message is
   not important, but the fact that the error is raised, is important.  (Our
   tests also do not check the location information associated with the error.)
   - You will probably find it convenient to add a helper function that implements the 
     block typecheck rules.
*)

let rec typecheck_stmt (tc : Tctxt.t) (s:Ast.stmt node) (to_ret:ret_ty) : Tctxt.t * bool =
  begin match s.elt with
  | Assn (e1, e2) ->  
                    let assignError  e a b = type_error e ("Can't assign type "
                    ^ (string_of_ty a)
                    ^ " to variable of type "
                    ^ (string_of_ty b)) in
                    let tc2 = typecheck_exp tc e2 in
                    let check t =  begin match (subtype tc tc2 t) with
                                    | true  -> (tc, false)
                                    | false -> assignError e2 t tc2
                                   end in
                    begin match e1.elt with
                      | Id id          -> let loc = Tctxt.lookup_local_option id tc in
                                          let glb = Tctxt.lookup_global_option id tc in
                                          begin match (loc,glb) with
                                           | (Some x, _)    -> check x
                                           | (None, Some (TRef (RFun (a,b)))) -> type_error e1 "global function assignment not permitted"
                                           | (None, Some (TNullRef x)) -> type_error e1 "possibly null array"
                                           | (None, Some x) -> check x
                                           | (None,None)    -> type_error e1 "Variable not defined"
                                          end               
                      | Index (a,b)    -> check (typecheck_exp tc e1)         
                      | Proj (exp1,id) -> check (typecheck_exp tc e1)
                      | _ -> failwith "Bruh, don't assign weird stuff"
                      end
  | Decl (id, e) -> let tc1 = typecheck_exp tc e in
                    let lookup = Tctxt.lookup_local_option id tc in   (* TODO: Should it be only local or local and global? *)
                    begin match lookup with
                     | Some x -> type_error e "variable already defined"
                     | None   -> (Tctxt.add_local tc id tc1, false)
                    end
  | Ret e -> begin match (e,to_ret) with
              | (Some e, RetVal t) -> let exp1 = typecheck_exp tc e in
                                      let check = subtype tc exp1 t in
                                        (tc,check)
              | (None, RetVoid)    -> (tc,true)
              | _                  -> (tc,false)
              end
  | SCall (e, eL) ->  begin match e.elt with
                      | Id id ->  let f_typ = Tctxt.lookup_global id tc in
                                  begin match f_typ with
                                  | TRef (RFun (args, RetVoid)) ->  let list_input_tc = List.mapi (fun i exp -> subtype tc (typecheck_exp tc exp) (List.nth args i)) eL in
                                                                    let correct_args = List.fold_left (fun x y -> x && y) true list_input_tc in
                                                                    if correct_args then (tc, false) else (type_error e "function call has wrong input types")
                                  | _ -> type_error e "Function with return type RetVoid expected"
                                  end
                      | _ -> failwith "Bruh, don't assign weird stuff"
                      end
  | If (e, aL, bL) -> let boolcheck = subtype tc (typecheck_exp tc e) TBool in
                      let tc_aL = typecheck_blk tc aL to_ret in
                      let tc_bL = typecheck_blk tc bL to_ret in
                      let ret = (d2 tc_aL) && (d2 tc_bL) in
                      begin match boolcheck with
                       | true  -> (tc,ret)
                       | false -> type_error e "if condition ins't a bool"
                      end
  | Cast (retTyp, id, e, sL1, sL2) -> (tc, false)
  | For (vL, Some e, s, sL) -> let c = List.fold_left (fun c' decl -> Tctxt.add_local c' (fst decl) (typecheck_exp c' (snd decl))) tc vL in
                          let boolcheck = subtype c (typecheck_exp c e) TBool in
                          if boolcheck then (tc, false) else type_error e "for condition ins't a bool"
  | For (vL, None, s, sL) -> let c = List.fold_left (fun c' decl -> Tctxt.add_local c' (fst decl) (typecheck_exp c' (snd decl))) tc vL in
                                    (tc, false)
  | While (e, sL) -> let boolcheck = subtype tc (typecheck_exp tc e) TBool in if boolcheck then (tc, false) else type_error e "while condition ins't a bool"
end
and typecheck_blk (tc : Tctxt.t) (b:(Ast.stmt node) list) (to_ret : ret_ty) : Tctxt.t * bool =
  let evalC c x = d1 (typecheck_stmt (d1 c) x to_ret) in
  let evalB c x = d2 (typecheck_stmt (d1 c) x to_ret) in
  List.fold_left (fun c x -> (
    begin match (x.elt,d2 c) with
     | (Ret e,true) -> type_error x "early return error"
     | _            -> evalC c x
    end,
    (d2 c) || (evalB c x))) (tc,false) b


(*
begin match x with
     | (Ret e,true) -> type_error x "early return error"
     | _            -> evalC c x
    end
*)

(* struct type declarations ------------------------------------------------- *)
(* Here is an example of how to implement the TYP_TDECLOK rule, which is 
   is needed elswhere in the type system.
 *)

(* Helper function to look for duplicate field names *)
let rec check_dups fs =
  match fs with
  | [] -> false
  | h :: t -> (List.exists (fun x -> x.fieldName = h.fieldName) t) || check_dups t

let typecheck_tdecl (tc : Tctxt.t) id fs  (l : 'a Ast.node) : unit =
  if check_dups fs
  then type_error l ("Repeated fields in " ^ id) 
  else List.iter (fun f -> typecheck_ty l tc f.ftyp) fs

(* function declarations ---------------------------------------------------- *)
(* typecheck a function declaration 
    - extends the local context with the types of the formal parameters to the 
      function
    - typechecks the body of the function (passing in the expected return type
    - checks that the function actually returns
*)
let typecheck_fdecl (tc : Tctxt.t) (f : Ast.fdecl) (l : 'a Ast.node) : unit =
  let c = List.fold_left (fun c' x -> Tctxt.add_local c' (snd x) (fst x) ) tc f.args in
  let block_check = typecheck_blk c f.body f.frtyp in
  begin match (snd block_check) with
   | true  -> ()
   | false -> type_error l "Function doesn't return"
  end
(* creating the typchecking context ----------------------------------------- *)

(* The following functions correspond to the
   judgments that create the global typechecking context.
   create_struct_ctxt: - adds all the struct types to the struct 'S'
   context (checking to see that there are no duplicate fields
     H |-s prog ==> H'
   create_function_ctxt: - adds the the function identifiers and their
   types to the 'F' context (ensuring that there are no redeclared
   function identifiers)
     H ; G1 |-f prog ==> G2
   create_global_ctxt: - typechecks the global initializers and adds
   their identifiers to the 'G' global context
     H ; G1 |-g prog ==> G2    
   NOTE: global initializers may mention function identifiers as
   constants, but can't mention other global values *)

let anyEq (i: Ast.id) (ids: Ast.id list) : bool =
List.fold_right (fun x b -> (String.equal x i) || b) ids false

let rec recursiveStruct (tc : Tctxt.t) (baseId : Ast.id) (ids : Ast.id list) : bool =
let lookup = lookup_struct_option baseId tc in
begin match lookup with
  | Some flst -> List.fold_right (fun x b ->
    begin match x.ftyp with
     | TRef (RStruct i) -> if anyEq i ids then true else b || (recursiveStruct tc i ([i]@ids))
     | _                -> b
    end
  ) flst false
  | None      -> false
end

let checkStructs (tc:Tctxt.t) (p:Ast.prog) : unit =
  List.fold_left (fun c x ->
  begin match x with
  | Gtdecl y -> let id = d1 (y.elt) in
                begin match recursiveStruct tc id [id] with
                 | true  -> type_error y "struct may not be recursively defined"
                 | false -> ()
                end
  | _ -> ()
  end
  ) () p

let create_struct_ctxt (tc:Tctxt.t) (p:Ast.prog) : Tctxt.t =
  List.fold_left (fun c x ->
  begin match x with
  | Gtdecl y -> 
    let id = d1 (y.elt) in
    let fields = d2 (y.elt) in
    let lookup = Tctxt.lookup_struct_option id c in
    let duplicates = check_dups fields in
    begin match (lookup,duplicates) with
     | (Some x,_)    -> type_error y "Struct already defined"
     | (_,true)      -> type_error y "duplicate field definitions in struct"
     | (None,false)  -> Tctxt.add_struct c (fst y.elt) (snd y.elt)
    end
  | _ -> c
  end
  ) tc p

let create_function_ctxt (tc:Tctxt.t) (p:Ast.prog) : Tctxt.t =
  List.fold_left (fun c x ->
  begin match x with
  | Gfdecl y ->
    let f = y.elt in
    let lookup = Tctxt.lookup_global_option f.fname c in
    begin match lookup with
     | Some x -> type_error y "function name already defined"
     | None   -> Tctxt.add_global c f.fname (TRef (RFun (List.map fst f.args, f.frtyp)))
    end
  | _ -> c
  end
  ) tc p

let names (p:Ast.prog) : (Ast.id list) = 
List.flatten (List.map
(fun x -> begin match x with
           | Gvdecl y -> [y.elt.name]
           | _        -> []
          end
) p)

let create_global_ctxt (tc:Tctxt.t) (p:Ast.prog) : Tctxt.t =
  List.fold_left (fun c x ->
  begin match x with
  | Gvdecl y ->
    let g = y.elt in
    let lookup = Tctxt.lookup_global_option g.name c in
    begin match lookup with
     | Some x -> type_error y "global variable already defined"
     | None   -> let add = Tctxt.add_global c g.name (typecheck_gexp c g.init) in
                 begin match g.init.elt with
                  | Id i -> if anyEq i (names p) then type_error y "gexp may not be another global variable" else add
                  | _    -> add
                 end
    end
  | _ -> c
  end
  ) tc p

let create_initial_ctxt (p:Ast.prog) : Tctxt.t =
  List.fold_left (fun c x ->
    let name = d1 x in
    let args =  d1 (d2 x) in
    let retTy = d2 (d2 x) in
    Tctxt.add_global c name (TRef (RFun (args, retTy)))
  ) Tctxt.empty builtins

(* This function implements the |- prog and the H ; G |- prog 
   rules of the oat.pdf specification.   
*)
let typecheck_program (p:Ast.prog) : unit =
  let init = create_initial_ctxt p in
  let sc = create_struct_ctxt init p in
  let fc = create_function_ctxt sc p in
  let gc = create_global_ctxt fc p in
  let tc = gc in
  (*checkStructs tc p;*)
  List.iter (fun p ->
    match p with
    | Gfdecl ({elt=f} as l) -> typecheck_fdecl tc f l
    | Gtdecl ({elt=(id, fs)} as l) -> typecheck_tdecl tc id fs l 
    | _ -> ()) p