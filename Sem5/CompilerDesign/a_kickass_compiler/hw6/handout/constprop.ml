open Ll
open Datastructures

(* The lattice of symbolic constants ---------------------------------------- *)
module SymConst =
  struct
    type t = NonConst           (* Uid may take on multiple values at runtime *)
           | Const of int64     (* Uid will always evaluate to const i64 or i1 *)
           | UndefConst         (* Uid is not defined at the point *)

    let compare s t =
      match (s, t) with
      | (Const i, Const j) -> Int64.compare i j
      | (NonConst, NonConst) | (UndefConst, UndefConst) -> 0
      | (NonConst, _) | (_, UndefConst) -> 1
      | (UndefConst, _) | (_, NonConst) -> -1

    let to_string : t -> string = function
      | NonConst -> "NonConst"
      | Const i -> Printf.sprintf "Const (%LdL)" i
      | UndefConst -> "UndefConst"

    
  end

(* The analysis computes, at each program point, which UIDs in scope will evaluate 
   to integer constants *)
type fact = SymConst.t UidM.t



(* flow function across Ll instructions ------------------------------------- *)
(* - Uid of a binop or icmp with const arguments is constant-out
   - Uid of a binop or icmp with an UndefConst argument is UndefConst-out
   - Uid of a binop or icmp with an NonConst argument is NonConst-out
   - Uid of stores and void calls are UndefConst-out
   - Uid of all other instructions are NonConst-out
 *)
type s = Nc           (* Uid may take on multiple values at runtime *)
       | C of int64   (* Uid will always evaluate to const i64 or i1 *)
       | Uc           (* Uid is not defined at the point *)

let to_int (s:Llinterp.sval) : int64 =
begin match s with
 | VInt x -> x
 | _-> failwith "error in conversion"
end

let handleOp (op:Ll.operand) (d:fact): SymConst.t =
  begin match op with
   | Id i    -> let res = UidM.find_opt i d in
                begin match res with
                 | Some x -> x
                 | None   -> SymConst.NonConst
                end
   | Gid g   -> let res = UidM.find_opt g d in
                begin match res with
                 | Some x -> x
                 | None   -> SymConst.NonConst
                end
   | Const x -> SymConst.Const x 
   | Null    -> SymConst.UndefConst (* WARNING: NOT sure wether UndefConst or NonConst *)
  end

let handle (op1:Ll.operand) (op2:Ll.operand) (d:fact) (u:uid) (f: Llinterp.sval -> Llinterp.sval -> Llinterp.sval): fact =
  let uid1 = handleOp op1 d in
  let uid2 = handleOp op2 d in
  begin match (uid1,uid2) with 
   | (SymConst.UndefConst, _)             -> UidM.add u (SymConst.UndefConst) d
   | (_ ,SymConst.UndefConst)             -> UidM.add u (SymConst.UndefConst) d
   | (SymConst.NonConst, _ )              -> UidM.add u (SymConst.NonConst) d
   | (_ ,SymConst.NonConst)               -> UidM.add u (SymConst.NonConst) d
   | (SymConst.Const x, SymConst.Const y) -> UidM.add u (SymConst.Const (to_int (f (VInt x) (VInt y)))) d
  end

let insn_flow ((u,i):uid * insn) (d:fact) : fact =
  begin match i with
  | Binop (bop,ty,op1,op2)     -> handle op1 op2 d u (Llinterp.interp_bop bop)
  | Icmp (cnd,ty,op1,op2)      -> handle op1 op2 d u (Llinterp.interp_cnd cnd)
  | Store (Void,op1,Id (junk)) -> UidM.add junk (SymConst.UndefConst) d
  | Store (v,op1,Id (junk))    -> UidM.add junk (SymConst.NonConst) d
  | Call (Void,op,lst)         -> UidM.add u (SymConst.UndefConst) d
  | _                          -> UidM.add u (SymConst.NonConst) d
  end
(* The flow function across terminators is trivial: they never change const info *)
let terminator_flow (t:terminator) (d:fact) : fact = d

(* module for instantiating the generic framework --------------------------- *)
module Fact =
  struct
    type t = fact
    let forwards = true

    let insn_flow = insn_flow
    let terminator_flow = terminator_flow
    
    let normalize : fact -> fact = 
      UidM.filter (fun _ v -> v != SymConst.UndefConst)

    let compare (d:fact) (e:fact) : int  = 
      UidM.compare SymConst.compare (normalize d) (normalize e)

    let to_string : fact -> string =
      UidM.to_string (fun _ v -> SymConst.to_string v)

    (* The constprop analysis should take the meet over predecessors to compute the
       flow into a node. You may find the UidM.merge function useful *)

    let combine (ds:fact list) : fact = 
      List.fold_right (fun d b->
        UidM.merge (fun u a b ->
          begin match (a,b) with
           | (None, None)                               -> None
           | (Some x, None)                             -> Some x
           | (None, Some x)                             -> Some x
           | (Some(SymConst.UndefConst), _ )            -> Some(SymConst.UndefConst)
           | (_, Some(SymConst.UndefConst) )            -> Some(SymConst.UndefConst)
           | (Some(SymConst.NonConst), _ )              -> Some(SymConst.NonConst)
           | (_, Some(SymConst.NonConst) )              -> Some(SymConst.NonConst)
           | (Some(SymConst.Const i), Some(SymConst.Const j)) ->
                                                           begin match Int64.equal i j with
                                                            | true  -> Some(SymConst.Const i)
                                                            | false -> Some(SymConst.NonConst)
                                                           end
          end
        ) d b
      ) ds UidM.empty
  end

(* instantiate the general framework ---------------------------------------- *)
module Graph = Cfg.AsGraph (Fact)
module Solver = Solver.Make (Fact) (Graph)

(* expose a top-level analysis operation ------------------------------------ *)
let analyze (g:Cfg.t) : Graph.t =
  (* the analysis starts with every node set to bottom (the map of every uid 
     in the function to UndefConst *)
  let init l = UidM.empty in

  (* the flow into the entry node should indicate that any parameter to the
     function is not a constant *)
  let cp_in = List.fold_right 
    (fun (u,_) -> UidM.add u SymConst.NonConst)
    g.Cfg.args UidM.empty 
  in
  let fg = Graph.of_cfg init cp_in g in
  Solver.solve fg


(* run constant propagation on a cfg given analysis results ----------------- *)
(* HINT: your cp_block implementation will probably rely on several helper 
   functions.      
*)

(* TODO: Check all predecessor blocks and replace values there aswell *)
(* TODO: Do constant propagation in terminators (e.g. return) *)
let search_opt (id: Ll.uid) (fn: Ll.uid -> Fact.t) : Fact.t option =
try Some (fn id)
with Not_found -> None

let replace (op: Ll.operand) (f : Fact.t) : Ll.operand =
begin match op with
 | Id id -> begin match (UidM.find_opt id f) with
              | Some(SymConst.Const i) -> Const i
              | _                      -> Id id
            end
 | _     -> op
end

let replace_term ((uid,term) : (Ll.uid * Ll.terminator)) (fn : Ll.uid -> Fact.t) : (Ll.uid * Ll.terminator)=
let fact = fn uid in
begin match term with
 | Ret (ty, Some (op))  -> (uid,Ret(ty, Some (replace op fact)))
 | Cbr (op, lbl1, lbl2) -> (uid,Cbr (replace op fact, lbl1, lbl2))
 | _                    -> (uid,term)
end

let remove_block (l:Ll.lbl) (g:Cfg.cfg) : Cfg.cfg =
  {g with blocks=LblM.remove l g.blocks }


let run (cg:Graph.t) (cfg:Cfg.t) : Cfg.t =
  let open SymConst in

  let cp_block (l:Ll.lbl) (cfg:Cfg.t) : Cfg.t =
    let ll_block = Cfg.block cfg l in
    let fn  = Graph.uid_out cg l in
    let terminator = replace_term ll_block.term fn in
    let fact x = fn (fst x)in
    let block_insns = List.map (
      fun x ->
      begin match snd x with
        | Binop (bop,ty,op1,op2) -> let rep1 = replace op1 (fact x) in
                                    let rep2 = replace op2 (fact x) in
                                    (fst x,Binop (bop,ty,rep1,rep2))
        | Alloca ty              -> x
        | Load (ty,op1)          -> let rep = replace op1 (fact x) in
                                    (fst x,Load (ty,rep))
        | Store (ty,op1,op2)     -> let rep = replace op1 (fact x) in
                                    (fst x,Store (ty,rep,op2))
        | Icmp (cnd,ty,op1,op2)  -> let rep1 = replace op1 (fact x) in
                                    let rep2 = replace op2 (fact x) in
                                    (fst x,Icmp (cnd,ty,rep1,rep2))
        | Call (ty,op,lst)       -> let rep = replace op (fact x) in
                                    let repLst = List.map (fun a -> (fst a,replace (snd a) (fact x))) lst in
                                    (fst x,Call (ty,rep,repLst))
        | Bitcast (ty1,op,ty2)   -> x
        | Gep (ty,op,lst)        -> let rep = replace op (fact x) in
                                    (fst x,Gep (ty,rep,lst))
      end
    ) ll_block.insns in
    let succs = Cfg.block_succs ll_block in
    let cfg' = remove_block l cfg in
    Cfg.add_block l {insns = block_insns; term = terminator} cfg'

  in
  LblS.fold cp_block (Cfg.nodes cfg) cfg