(** Alias Analysis *)

open Ll
open Datastructures

(* The lattice of abstract pointers ----------------------------------------- *)
module SymPtr =
  struct
    type t = MayAlias           (* uid names a pointer that may be aliased *)
           | Unique             (* uid is the unique name for a pointer *)
           | UndefAlias         (* uid is not in scope or not a pointer *)

    let compare : t -> t -> int = Pervasives.compare

    let to_string = function
      | MayAlias -> "MayAlias"
      | Unique -> "Unique"
      | UndefAlias -> "UndefAlias"

  end

(* The analysis computes, at each program point, which UIDs in scope are a unique name
   for a stack slot and which may have aliases *)
type fact = SymPtr.t UidM.t

(* flow function across Ll instructions ------------------------------------- *)
(* TASK: complete the flow function for alias analysis. 

   - After an alloca, the defined UID is the unique name for a stack slot
   - A pointer returned by a load, call, bitcast, or GEP may be aliased
   - A pointer passed as an argument to a call, bitcast, GEP, or store
     may be aliased
   - Other instructions do not define pointers

 *)
let insn_flow ((u,i):uid * insn) (d:fact) : fact =
  let rec helper (ul:uid list) (d:fact) : fact =
    begin match ul with
    | x::xs -> helper xs ((UidM.add x SymPtr.MayAlias) d)
    | _ -> d
    end
  in
  let getUids (ops:operand list) : uid list =
    List.filter (fun x -> (String.compare x "") != 0) 
      (List.map (fun x ->   begin match x with
                            | Id y -> y
                            | _ -> ""
                            end) ops)
  in
  let getPointers (ops:(ty * operand) list) : (operand list) =
    List.map snd (List.filter (fun x -> begin match x with
                          | (Ptr _, _) -> true
                          | _ -> false
                          end) ops)
  in
  let isPointer (t:ty) : bool =
    begin match t with
    | Ptr _ -> true
    | _ -> false
    end
  in
  begin match i with
  | Alloca _ -> (UidM.add u SymPtr.Unique) d
  | Load (Ptr Ptr t, op) -> helper [u] d
  | Call (t, op, l) ->  let l' = getUids (getPointers l) in
                        let l'' = if isPointer t then u::l' else l' in
                        helper l'' d
  | Bitcast (t1, op, t2) -> let l' = if isPointer t1 then getUids [op] else [] in
                            let l'' = if isPointer t2 then u::l' else l' in
                            helper l'' d
  | Gep (t, op, l) -> let l' = getUids (op::l) in (* TODO: This is kinda sus *)
                      let l'' = if isPointer t then u::l' else l' in
                      helper l'' d
  | Store (Ptr t, op1, op2) -> let l' = getUids [op1] in helper l' d
  | _ -> d
  end


(* The flow function across terminators is trivial: they never change alias info *)
let terminator_flow t (d:fact) : fact = d

(* module for instantiating the generic framework --------------------------- *)
module Fact =
  struct
    type t = fact
    let forwards = true

    let insn_flow = insn_flow
    let terminator_flow = terminator_flow
    
    (* UndefAlias is logically the same as not having a mapping in the fact. To
       compare dataflow facts, we first remove all of these *)
    let normalize : fact -> fact = 
      UidM.filter (fun _ v -> v != SymPtr.UndefAlias)

    let compare (d:fact) (e:fact) : int = 
      UidM.compare SymPtr.compare (normalize d) (normalize e)

    let to_string : fact -> string =
      UidM.to_string (fun _ v -> SymPtr.to_string v)

    (* TASK: complete the "combine" operation for alias analysis.

       The alias analysis should take the join over predecessors to compute the
       flow into a node. You may find the UidM.merge function useful.

       It may be useful to define a helper function that knows how to take the
       join of two SymPtr.t facts.
    *)
    let combine (ds:fact list) : fact =
      let merge (x:fact) (y:fact) : fact = UidM.merge (fun k a b ->
        (* begin match (a, b) with
        | (Some c, Some d) -> begin match (c, d) with
                              | (Some SymPtr.Unique, Some SymPtr.Unique) -> Some SymPtr.Unique
                              | (None, Some SymPtr.MayAlias) -> Some SymPtr.MayAlias
                              | (Some SymPtr.MayAlias, None) -> Some SymPtr.MayAlias
                              | _ -> Some SymPtr.UndefAlias
                              end
        | (Some c, None) -> Some c
        | (None, Some c) -> Some c
        | _ -> Some SymPtr.UndefAlias
        end *)
        begin match (a, b) with
        | (Some c, Some d) -> begin match (c, d) with
                              | (SymPtr.Unique, SymPtr.Unique) -> Some SymPtr.Unique
                              | (SymPtr.MayAlias, _) -> Some SymPtr.MayAlias
                              | (_, SymPtr.MayAlias) -> Some SymPtr.MayAlias
                              | _ -> Some SymPtr.UndefAlias
                              end
        | (Some c, _) -> Some c
        | (_, Some c) -> Some c
        | _ -> None
        end
      ) x y
      in
      let rec helper (ds:fact list) : fact =
        begin match ds with
        | x::y::xs -> helper ((merge x y)::xs)
        | x::[] -> x
        | [] -> UidM.empty
        | _ -> failwith "Weird Stuff at Alias Combine"
        end
      in helper ds
  end

(* instantiate the general framework ---------------------------------------- *)
module Graph = Cfg.AsGraph (Fact)
module Solver = Solver.Make (Fact) (Graph)

(* expose a top-level analysis operation ------------------------------------ *)
let analyze (g:Cfg.t) : Graph.t =
  (* the analysis starts with every node set to bottom (the map of every uid 
     in the function to UndefAlias *)
  let init l = UidM.empty in

  (* the flow into the entry node should indicate that any pointer parameter 
     to the function may be aliased *)
  let alias_in = 
    List.fold_right 
      (fun (u,t) -> match t with
                    | Ptr _ -> UidM.add u SymPtr.MayAlias
                    | _ -> fun m -> m) 
      g.Cfg.args UidM.empty 
  in
  let fg = Graph.of_cfg init alias_in g in
  Solver.solve fg

