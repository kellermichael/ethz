function plot_all()

cd all4

thrp_SA2_DA1_AC1_ID_30_saturation__End;
thrp2=result_thrp_SA2_DA1_AC1_ID_30_saturation__End;
thrp_SA3_DA1_AC1_ID_44_saturation__End;
thrp3=result_thrp_SA3_DA1_AC1_ID_44_saturation__End;
thrp_SA4_DA1_AC1_ID_58_saturation__End;
thrp4=result_thrp_SA4_DA1_AC1_ID_58_saturation__End;
thrp_SA5_DA1_AC1_ID_72_saturation__End;
thrp5=result_thrp_SA5_DA1_AC1_ID_72_saturation__End;
thrp_SA6_DA1_AC1_ID_86_saturation__End;
thrp6=result_thrp_SA6_DA1_AC1_ID_86_saturation__End;
thrp_SA7_DA1_AC1_ID_100_saturation__End;
thrp7=result_thrp_SA7_DA1_AC1_ID_100_saturation__End;
thrp_SA9_DA1_AC1_ID_128_saturation__End;
thrp9=result_thrp_SA9_DA1_AC1_ID_128_saturation__End;
thrp_SA8_DA1_AC1_ID_114_saturation__End;
thrp8=result_thrp_SA8_DA1_AC1_ID_114_saturation__End;
thrp_SA10_DA1_AC1_ID_142_saturation__End;
thrp10=result_thrp_SA10_DA1_AC1_ID_142_saturation__End;
thrp_SA11_DA1_AC1_ID_156_saturation__End;
thrp11=result_thrp_SA11_DA1_AC1_ID_156_saturation__End;
thrp_SA12_DA1_AC1_ID_170_saturation__End;
thrp12=result_thrp_SA12_DA1_AC1_ID_170_saturation__End;
thrp_SA13_DA1_AC1_ID_184_saturation__End;
thrp13=result_thrp_SA13_DA1_AC1_ID_184_saturation__End;
thrp_SA14_DA1_AC1_ID_198_saturation__End;
thrp14=result_thrp_SA14_DA1_AC1_ID_198_saturation__End;


fig_handle = figure('NumberTitle', 'on', 'Name', 'all','PaperPositionMode', 'auto','Pointer', 'arrow');

set(gcf, 'color', 1/255 * [214 221 225]);
set(gcf, 'InvertHardCopy', 'off');
set(gcf,'Renderer','painters');

set(fig_handle,'Position', [200 20 1000 600]);
set(gca,'Position',[0.08,0.1,0.9,0.85]);
set(gca,'FontSize',20);
set(gca,'FontName','Georgia');
box on;
axis on;
set(gca,'ytick',[0, 0.02  0.13  0.18 0.20  0.24 0.27 0.52 .6 .7]);
set(gca,'xtick',[0:100:1000]);
%set(gca,'ytick',[]);

xlim([0 1300]);
ylim([0 0.71]);
ylabel('throughput [Mb/s]');
xlabel('time [s]');
grid on;
hold on;
plot(thrp2(:,1)/1000,thrp9(:,6),'-','Color',[.6 .6 .6],'LineWidth',6);
plot(thrp2(:,1)/1000,thrp2(:,6),'k--','LineWidth',3);
plot(thrp2(:,1)/1000,thrp3(:,6),'b--','LineWidth',6);
plot(thrp2(:,1)/1000,thrp4(:,6),'b-','LineWidth',3);
plot(thrp2(:,1)/1000,thrp5(:,6),'g-','LineWidth',3);
plot(thrp2(:,1)/1000,thrp6(:,6),'y-','LineWidth',3);
plot(thrp2(:,1)/1000,thrp7(:,6),'r-','LineWidth',3);
plot(thrp2(:,1)/1000,thrp8(:,6),'c-','LineWidth',5);
plot(thrp2(:,1)/1000,thrp10(:,6),'m--','LineWidth',3);
plot(thrp2(:,1)/1000,thrp11(:,6),'m-','LineWidth',3);
plot(thrp2(:,1)/1000,thrp12(:,6),'y--','LineWidth',3);
plot(thrp2(:,1)/1000,thrp13(:,6),'r--','LineWidth',3);
plot(thrp2(:,1)/1000,thrp14(:,6),'c--','LineWidth',3);
legend("(None)", " Algo6", " BlueSky", " BroFi", " Classic2", " FairShare", " FindingNash", " Freeze", " PFA", " Sneaky", " StarFighter", " Vincent", " Yacmoca");

cd ..
