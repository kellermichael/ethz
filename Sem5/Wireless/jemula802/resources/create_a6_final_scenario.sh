# Algo6
echo  Algo6
sed -e 's/ALGa/Algo6/g' template_ALGn.xml > a6/tttAlgo6.xml
# BlueSky
echo  BlueSky
sed -e 's/ALGb/BlueSky/g' a6/tttAlgo6.xml > a6/tttBlueSky.xml
# BroFi
echo  BroFi
sed -e 's/ALGc/BroFi/g' a6/tttBlueSky.xml > a6/tttBroFi.xml
# Classic2
echo  Classic2
sed -e 's/ALGd/Classic2/g' a6/tttBroFi.xml > a6/tttClassic2.xml
# FairShare
echo  FairShare
sed -e 's/ALGe/FairShare/g' a6/tttClassic2.xml > a6/tttFairShare.xml
# FindingNash
echo  FindingNash
sed -e 's/ALGf/FindingNash/g' a6/tttFairShare.xml > a6/tttFindingNash.xml
# Freeze
echo  Freeze
sed -e 's/ALGg/Freeze/g' a6/tttFindingNash.xml > a6/tttFreeze.xml
# None
echo  None
sed -e 's/ALGh/None/g' a6/tttFreeze.xml > a6/tttNone.xml
# PFA
echo  PFA
sed -e 's/ALGi/PFA/g' a6/tttNone.xml > a6/tttPFA.xml
# Sneaky
echo  Sneaky
sed -e 's/ALGj/Sneaky/g' a6/tttPFA.xml > a6/tttSneaky.xml
# StarFighter
echo  StarFighter
sed -e 's/ALGk/StarFighter/g' a6/tttSneaky.xml > a6/tttStarFighter.xml
# Vincent
echo  Vincent
sed -e 's/ALGl/Vincent/g' a6/tttStarFighter.xml > a6/tttVincent.xml
# Yacmoca
echo  Yacmoca
sed -e 's/ALGm/Yacmoca/g' a6/tttVincent.xml > a6/tttYacmoca.xml
# Karma
echo  Karma
sed -e 's/ALGn/Karma/g' a6/tttYacmoca.xml > a6/all.xml

rm -f a6/ttt*
