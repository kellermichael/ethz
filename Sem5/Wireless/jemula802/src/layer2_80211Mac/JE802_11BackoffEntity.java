/*
 * 
 * This is Jemula.
 *
 *    Copyright (c) 2019 Stefan Mangold
 *    All rights reserved. Urheberrechtlich geschuetzt.
 *    
 *    Redistribution and use in source and binary forms, with or without modification,
 *    are permitted provided that the following conditions are met:
 *    
 *      Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer. 
 *    
 *      Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution. 
 *    
 *      Neither the name of any affiliation of Stefan Mangold nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission. 
 *    
 *    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *    IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 *    OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 *    OF SUCH DAMAGE.
 *    
 */

package layer2_80211Mac;

import gui.JE802Gui;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Vector;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import kernel.JEEvent;
import kernel.JEEventHandler;
import kernel.JEEventScheduler;
import kernel.JETime;
import kernel.JETimer;
import layer1_802Phy.JE802PhyMode;
import layer2_80211Mac.JE802_11Mpdu.JE80211MpduType;
import layer3_network.JE802IPPacket;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

public final class JE802_11BackoffEntity extends JEEventHandler {

	private final JE802_11Mac theMac;

	private final JE802_11Vch theVch;

	private final JE802Gui theUniqueGui;

	private int theCW;

	private int theAC;

	private int dot11EDCACWmin;

	private int dot11EDCACWmax;

	private double dot11EDCAPF;

	private int dot11EDCAAIFSN;

	private JETime dot11EDCAMMSDULifeTime;

	private int dot11EDCATXOPLimit;

	private int theShortRetryCnt;

	private int theLongRetryCnt;

	private long theDiscardCnt = 0;

	private long theTxCnt = 0;

	private long theTxByteCnt = 0;

	private long theAckCnt = 0;

	public long getTheTxCnt() {
		return theTxCnt;
	}

	public long getTheTxByteCnt() {
		return theTxByteCnt;
	}

	public long getTheAckCnt() {
		return theAckCnt;
	}

	private int maxRetryCountShort;

	private int maxRetryCountLong;

	private Vector < JE802_11Mpdu > theQueue;

	private int theQueueSize;

	private JE802_11TimerNav theNavTimer;

	private JETimer theTxTimeoutTimer;

	private JETimer theInterFrameSpaceTimer;

	private JE802_11TimerBackoff theBackoffTimer;

	private JE802_11Mpdu theMpduRx;

	private JE802_11Mpdu theMpduData;

	private JE802_11Mpdu theMpduRts;

	private JE802_11Mpdu theMpduCtrl;

	private JETime theSlotTime;

	private JETime theSIFS;

	private JETime theAIFS;

	private HashSet<Integer> theMacAddresses;

	private int theCollisionCnt = 0;

	int debugCount = 0;

	private enum txState {
		idle, txRts, txAck, txCts, txData
	}

	private txState theTxState;

	private long previousDiscardedPacket = -1;
	private long previousReceivedAck = -1;
	private int successfullyTxedCount;
	private boolean update_was_scheduled;

	public JE802_11BackoffEntity(JEEventScheduler aScheduler,
			Random aGenerator, JE802Gui aGui, Node aTopLevelNode,
			JE802_11Mac aMac, JE802_11Vch aVch) throws XPathExpressionException {

		super(aScheduler, aGenerator);
		theUniqueGui = aGui;
		theMac = aMac;
		theVch = aVch;

		theMacAddresses = new HashSet<>();
		theMacAddresses.add(aMac.getMacAddress());

		if(parseConfiguration(aTopLevelNode)){
			theSIFS = theMac.getPhy().getSIFS();
			theSlotTime = theMac.getPhy().getSlotTime();
			theAIFS = theSIFS.plus(theSlotTime.times(dot11EDCAAIFSN));

			theCW = dot11EDCACWmin;			// contention window

			theShortRetryCnt = 0;
			theLongRetryCnt = 0;

			createTimers(aGui);

			update_was_scheduled = false;

			theTxState = txState.idle;
			updateBackoffTimer();

			theMpduRx = null;
			theMpduData = null;
			theMpduRts = null;
			theMpduCtrl = null;

		} else {
			warning("Station " + theMac.getMacAddress() + " messed up xml, dude.");
		}
	}

	private boolean parseConfiguration(Node aTopLevelNode) throws XPathExpressionException {
		Element backoffElem = (Element) aTopLevelNode;
		if (backoffElem.getNodeName().equals("JE802BackoffEntity")) {
			theAC = Integer.valueOf(backoffElem.getAttribute("AC"));
			theQueue = new Vector < JE802_11Mpdu > ();
			theQueueSize = Integer.valueOf(
					backoffElem.getAttribute("queuesize"));

			XPath xpath = XPathFactory.newInstance().newXPath();
			Element mibElem = (Element) xpath.evaluate("MIB802.11e",
					backoffElem, XPathConstants.NODE);
			if (mibElem != null) {
				dot11EDCACWmin = Integer.valueOf(mibElem.getAttribute("dot11EDCACWmin"));
				dot11EDCACWmax = Integer.valueOf(mibElem.getAttribute("dot11EDCACWmax"));
				dot11EDCAPF = Double.valueOf(mibElem.getAttribute("dot11EDCAPF"));
				dot11EDCAAIFSN = Integer.valueOf(mibElem.getAttribute("dot11EDCAAIFSN"));
				dot11EDCAMMSDULifeTime = new JETime(Double.valueOf(mibElem.getAttribute("dot11EDCAMSDULifetime")));
				dot11EDCATXOPLimit = Integer.valueOf(mibElem.getAttribute("dot11EDCATXOPLimit"));
			} else {
				error("Station " + theMac.getMacAddress() + " no MIB parameters found.");
			}
			return true;
		}
		return false;
	}

	private void createTimers(JE802Gui aGui){
		theNavTimer = new JE802_11TimerNav(theUniqueEventScheduler, theUniqueRandomGenerator, aGui, "nav_expired_ind", getHandlerId());
		theTxTimeoutTimer = new JETimer(theUniqueEventScheduler, theUniqueRandomGenerator, "tx_timeout_ind", getHandlerId());
		theInterFrameSpaceTimer = new JETimer(theUniqueEventScheduler, theUniqueRandomGenerator, "interframespace_expired_ind", getHandlerId());
		theBackoffTimer = new JE802_11TimerBackoff(
				theUniqueEventScheduler, theUniqueRandomGenerator, theUniqueGui, theMac.getMacAddress(), theAC,
				"backoff_expired_ind", getHandlerId(), theSlotTime, this);
	}

	@Override
	public void event_handler(JEEvent anEvent) {
		String anEventName = anEvent.getName();
		theLastRxEvent = anEvent;

		message("Station " + theMac.getMacAddress() + " on Channel " +
				theMac.getPhy().getCurrentChannelId() + " AC " +
				theAC + " received event '" + anEventName + "'", 30);

		if (anEventName.contains("update_backoff_timer_req")) {
			update_was_scheduled = false; // we received the scheduled update, now reset the status
			updateBackoffTimer();
		} else if (anEventName.contains("MLME")) {
			event_MlmeRequests(anEvent);
		} else if (anEventName.equals("MSDUDeliv_req")) {
			event_MSDUDeliv_req(anEvent);
		} else if (anEventName.equals("MPDUDeliv_req")) {
			event_MPDUDeliv_req(anEvent);
		} else if (anEventName.equals("backoff_expired_ind")) {
			event_backoff_expired_ind(anEvent);
		} else if (anEventName.equals("interframespace_expired_ind")) {
			event_InterframeSpace_expired_ind(anEvent);
		} else if (anEventName.equals("nav_expired_ind")) {
			event_nav_expired_ind(anEvent);
		} else if (anEventName.equals("tx_timeout_ind")) {
			event_tx_timeout_ind(anEvent);
		} else if (anEventName.equals("PHY_RxEnd_ind")) {
			event_PHY_RxEnd_ind(anEvent);
		} else if (anEventName.equals("PHY_SyncStart_ind")) {
			event_PHY_SyncStart_ind(anEvent);
		} else if (anEventName.equals("MPDUReceive_ind")) {
			event_MPDUReceive_ind(anEvent);
		} else if (anEventName.equals("virtual_collision_ind")) {
			event_virtual_collision_ind(anEvent);
		} else if (anEventName.equals("PHY_TxEnd_ind")) {
			event_PHY_TxEnd_ind(anEvent);
		} else {
			error("Station " + theMac.getMacAddress() +
					" undefined event '" + anEventName + "'");
		}
	}

	private void event_virtual_collision_ind(JEEvent anEvent) {
		theTxState = txState.idle;
		updateBackoffTimer();
	}

	private void event_PHY_TxEnd_ind(JEEvent anEvent) {
		if (theTxState.equals(txState.txAck) || theTxState.equals(txState.txCts)) {
			theTxState = txState.idle;
			theMpduCtrl = null;
			keep_going(); // now let us continue our own backoff, because the backoff timer was paused before.
			// we do this even after CTS, so in case data is not following, we just go ahead with our own stuff.
		} else if (theMpduData != null && theMpduData.getDA() == theMac.getDot11BroadcastAddress()) {
			theMpduData = null;
			theTxState = txState.idle;
			theMpduCtrl = null;
			send(new JEEvent("broadcast_sent", theMac.getHandlerId(), anEvent.getScheduledTime()));
			keep_going();
		} else {
			// nothing
		}
	}

	private void increaseCw() {
		theCW = (int) Math.round((theCW + 1) * dot11EDCAPF) - 1;
		if (theCW < 0) {
			theCW = 0;
		} else if (theCW > dot11EDCACWmax) {
			theCW = dot11EDCACWmax;
		}
	}

	private void event_tx_timeout_ind(JEEvent anEvent) {
		if (theTxState == txState.txRts) { // RTS was sent, but station did not receive CTS frame
			theTxState = txState.idle;
			theCollisionCnt++;
			retryRts();
		} else if (theTxState == txState.txData) { // data was sent, but station did not receive an ack frame
			theTxState = txState.idle;
			theCollisionCnt++;
			retryData();

		} else if (theTxState == txState.txCts) {
			theTxState = txState.idle;
			theMpduCtrl = null;
		} else if (theTxState == txState.txAck) {
			theTxState = txState.idle;
			theMpduCtrl = null;
		}
		send("tx_timeout_ind", theVch);
		keep_going();
	}

	private void retryData() {

		if (theMpduData == null) {
			error("Station " + theMac.getMacAddress() + " retryData: no pending RTS/DATA frame to transmit");
		}

		if (theMpduData.getFrameBodySize() < theMac.getDot11RTSThreshold()) {
			theShortRetryCnt++; // station Short Retry Count

			if (theShortRetryCnt > theMac.getDot11ShortRetryLimit()) {
				JE802_11Mpdu data = theMpduData;
				theMpduData = null;
				theCW = dot11EDCACWmin; // reset contention window
				parameterlist.clear();
				parameterlist.add(data);
				parameterlist.add(false);
				parameterlist.add(theShortRetryCnt);
				setMaxRetryCountShort(Math.max(getMaxRetryCountShort(), theShortRetryCnt));
				theDiscardCnt++;
				theShortRetryCnt = 0;
				send(new JEEvent("MSDU_discarded_ind", theMac, theUniqueEventScheduler.now(), parameterlist));
			} else {
				increaseCw(); // increase the CW and retry after the backoff
				deferForIFS();
			}
		} else {
			theLongRetryCnt++; // station Long Retry Count
			if (theLongRetryCnt > theMac.getDot11LongRetryLimit()) {
				JE802_11Mpdu data = theMpduData;
				theMpduData = null;
				theCW = dot11EDCACWmin; // reset contention window
				parameterlist.clear();
				parameterlist.add(data);
				parameterlist.add(false);
				parameterlist.add(theLongRetryCnt);
				maxRetryCountLong = Math.max(maxRetryCountLong, theLongRetryCnt);
				theDiscardCnt++;
				theLongRetryCnt = 0;
				send(new JEEvent("MSDU_discarded_ind", theMac, theUniqueEventScheduler.now(), parameterlist));
			} else {
				increaseCw(); // increase the CW and retry after the backoff
				deferForIFS();
			}

		}
	}

	private void retryRts() {
		if ((theMpduData == null) | (theMpduRts == null)) {
			error("Station " + theMac.getMacAddress() + " retryRTS: no pending RTS/DATA frame to transmit");
		}
		theShortRetryCnt++; // station Short Retry Count
		if (theShortRetryCnt > theMac.getDot11ShortRetryLimit()) {
			JE802_11Mpdu data = theMpduData;
			theMpduRts = null;
			theMpduData = null;
			maxRetryCountLong = Math.max(maxRetryCountLong, theLongRetryCnt);
			setMaxRetryCountShort(Math.max(getMaxRetryCountShort(), theShortRetryCnt));
			theShortRetryCnt = 0;
			theLongRetryCnt = 0;
			theDiscardCnt++;
			theCW = dot11EDCACWmin; // reset contention window
			parameterlist.clear();
			parameterlist.add(data);
			parameterlist.add(false);
			send(new JEEvent("MSDU_discarded_ind", theMac, theUniqueEventScheduler.now(), parameterlist));
		} else {
			increaseCw(); // increase the CW and retry after the backoff
			deferForIFS();
		}
	}

	private void event_backoff_expired_ind(JEEvent anEvent) {
		if (theMpduCtrl != null) {
			if (theBackoffTimer.is_idle() & theInterFrameSpaceTimer.is_idle()) {
				message("Station " + theMac.getMacAddress() + " backoff error, possibly due to hidden stations. Discarding Ctrl PDU", 10);
				theMpduCtrl = null; // discarding this ctrl pdu. Might happen in hidden station scenarios.
				return;
			}
		}
		if (!checkAndTxRTS(false)) {
			if (!checkAndTxDATA(false)) {
				// no more rts and no more data.
				keep_going();
			}
		}
	}

	private void event_InterframeSpace_expired_ind(JEEvent anEvent) {
		//this.theUniqueGui.addLine(this.theUniqueEventScheduler.now(), this.theMac.getMacAddress(), this.theAC+2, "blue", 1);
		if (!checkAndTxCTRL()) {
			if (theBackoffTimer.is_active()) {
				warning("Station " + theMac.getMacAddress() + ": problem (possibly due to hidden station): backoff active while AIFS or any other IFS");
			} else {
				if (!checkAndTxRTS(true)) {
					if (!checkAndTxDATA(true)) {
						/* nothing was forwarded to this.tx() */
					} else { /* DATA was forwarded to this.tx() */
					};
				} else { /* RTS was forwarded to this.tx() */}
			}
		} else {/* control frame was forwarded to this.tx() */};
	}

	private void event_nav_expired_ind(JEEvent anEvent) {
		keep_going();
	}

	private void keep_going() {
		//deferForIFS();
		if (theBackoffTimer.is_paused() && !cca_busy()) { // now let us continue our own backoff
			deferForIFS();
		} else { // or check next Mpdu
			nextMpdu();
		}
	}

	private void event_PHY_RxEnd_ind(JEEvent anEvent) {
		// received packet completely or collision occurred
		parameterlist = anEvent.getParameterList();
		if (parameterlist.elementAt(0) == null) { // bad luck: no packet at all, just garbage
			theMpduRx = null;
			keep_going();
		} else { // we successfully received a packet, which is now given to us as event parameter.
			theMpduRx = (JE802_11Mpdu) parameterlist.elementAt(0);

			// extract the found source MAC addresses
			theMacAddresses.add(theMpduRx.getSA());

			if (theMpduRx.getDA() != theMac.getDot11BroadcastAddress()) {
				// extract the found destination MAC addresses
				theMacAddresses.add(theMpduRx.getDA());

				if ((theMpduRx.getDA() != theMac.getMacAddress()) || (theMpduRx.getAC() != theAC)) {					
					theMpduRx = null;
					return; // this is all for this backoff entity. The frame is not for us.
				}
			}
			JE80211MpduType type = theMpduRx.getType();
			switch (type) {
			case data:
				receiveData();
				break;
			case ack:
				receiveAck();
				break;
			case rts:
				receiveRts();
				break;
			case cts:
				receiveCts();
				break;
			default:
				error("Undefined MpduType");
			}
			theMpduRx = null;
		}
	}

	private void event_PHY_SyncStart_ind(JEEvent anEvent) {
		updateBackoffTimer();
	}

	private void updateBackoffTimer() {
		if (theBackoffTimer.is_paused()) {
			if (cca_busy()) {
				if (!update_was_scheduled) {
					update_was_scheduled = true;
					send(new JEEvent("update_backoff_timer_req", getHandlerId(), theUniqueEventScheduler.now().plus(theSlotTime)));
				}
			} else {
				// do nothing here, especially not any resume!
				//theBackoffTimer.resume(cca_busy()); 
			}
		} else if (theBackoffTimer.is_active() && cca_busy()) {
			theBackoffTimer.pause();
		}
	}

	private void receiveRts() {
		if (theMpduRx.getDA() - theMac.getMacAddress() == 0) {
			if ((theMpduRx.getAC() - theAC) != 0) {
				error("Station " + theMac.getMacAddress() +
						" generating CTS for wrong AC: AC[theMpduRx]=" +
						theMpduRx.getAC() + " and AC[this]=" +
						theAC);
			}
			generateCts();
			if (theInterFrameSpaceTimer.is_active()) {
				theInterFrameSpaceTimer.stop();
			}
			deferForIFS();

		} else {
			// the received RTS-frame is for another station
		}
	}

	private void receiveAck() {

		theAckCnt++;

		if (theTxState == txState.txData) {
			if (theMpduData == null) {
				error("Station " + theMac.getMacAddress() + " receiveAck: station has no data frame");
			}
			JE802_11Mpdu receivedData = theMpduData; // store for forwarding
			parameterlist.clear();
			parameterlist.add(receivedData);

			if (getMac().getMlme().isARFEnabled()) {
				if (receivedData.getSeqNo() == previousReceivedAck + 1) {
					successfullyTxedCount++;
					previousReceivedAck = receivedData.getSeqNo();

					if (successfullyTxedCount == 10) {
						successfullyTxedCount = 0;
					}
				}
			}

			if (theMpduData.getFrameBodySize() < theMac.getDot11RTSThreshold()) {
				parameterlist.add(theShortRetryCnt);
				setMaxRetryCountShort(Math.max(getMaxRetryCountShort(), theShortRetryCnt));
				theShortRetryCnt = 0;
			} else {
				parameterlist.add(theLongRetryCnt);
				maxRetryCountLong = Math.max(maxRetryCountLong, theLongRetryCnt);
				theLongRetryCnt = 0;
			}

			theMpduData = null;
			theNavTimer.stop();
			theCW = dot11EDCACWmin; // reset contention window
			theTxState = txState.idle;
			theTxTimeoutTimer.stop();
			send(new JEEvent("MSDU_delivered_ind", theMac.getHandlerId(),
					theUniqueEventScheduler.now(), parameterlist));
			deferForIFS();
		} else {
			keep_going();
		}
	}

	private void receiveCts() {
		if (theTxState == txState.txRts) {
			if (theMpduRts == null) {
				message("Station " + theMac.getMacAddress()
				+ " receiveCts: station has no pending rts frame", 30);
			}
			theMpduRts = null; // we sent the rts, and received cts. So let's assume rts has done its job.
			if (theMpduData == null) {
				message("Station " + theMac.getMacAddress() +
						" receiveCts: station has no pending data frame", 30);
			}
			theTxTimeoutTimer.stop();
			theShortRetryCnt = 0;
			setMaxRetryCountShort(Math.max(theShortRetryCnt, getMaxRetryCountShort()));
			theCW = dot11EDCACWmin; // reset contention window
			deferForIFS();
		} else {
			// do nothing in case the station did not transmit rts frame
		}
	}

	private void receiveData() {

		if (theMpduRx.getDA() == theMac.getMacAddress()) {
			if (theMpduRx.getAC() != theAC) {
				error("Station " + theMac.getMacAddress() + " this backoff entity generates an ACK for a data frame of different AC.");
			}
			theMpduCtrl = null;
			theTxTimeoutTimer.stop();
			// send received packet back to its source, for evaluation:
			parameterlist.clear();
			parameterlist.add(theMpduRx.clone());
			// make a copy since arrival time will
			// be changed until mpdu gets evaluated
			theMpduRx.setLastArrivalTime(theUniqueEventScheduler.now());
			parameterlist.add(theUniqueEventScheduler.now());
			parameterlist.add(theAC);
			if (theMpduRx.getHopAddresses() != null && !theMpduRx.getHopAddresses().isEmpty()) {
				send(new JEEvent("packet_forward", theMac, theUniqueEventScheduler.now(), parameterlist));
			} else {
				send(new JEEvent("packet_exiting_system_ind", theMac
						.getHandlerId(), theUniqueEventScheduler.now(), parameterlist));
			}
			theMpduRx.setLastArrivalTime(theUniqueEventScheduler.now());
			this.generateAck(); // generate an ACK
			this.deferForIFS();
		} else if (theMpduRx.getDA() == theMac.getDot11BroadcastAddress()) {
			// the packet is a broadcast packet
			theMpduCtrl = null;
			theTxTimeoutTimer.stop();
			// packet was sent by ourselves
			if (theMpduRx.getSA() != theMac.getMacAddress()) {
				parameterlist.clear();
				parameterlist.add(theMpduRx.clone());
				parameterlist.add(theUniqueEventScheduler.now());
				parameterlist.add(theAC);
				send(new JEEvent("packet_exiting_system_ind", theMac
						.getHandlerId(), theUniqueEventScheduler.now(), parameterlist));
			}
			deferForIFS();
		} else {
			error("Station " + theMac.getMacAddress()
			+ " the received MPDU should be ours, but it is for another station. Not good.");
		}
	}

	private void event_MPDUReceive_ind(JEEvent anEvent) {
		parameterlist = anEvent.getParameterList();
		int aMacId = (int)parameterlist.get(0);
		int anAc = (int)parameterlist.get(1);
		JETime aNav = new JETime((JETime) parameterlist.elementAt(2));

		if (theBackoffTimer.is_active()) { // the classical case: while down-counting, another station initiated a frame exchange
			theBackoffTimer.pause();
		}
		theMpduRx = new JE802_11Mpdu();
		theMpduRx.setDA(aMacId); // destination MAC address
		theMpduRx.setAC(anAc); // access category
		theMpduRx.setNAV(aNav); // NAV value
		if (!(aMacId == theMac.getMacAddress() && anAc == theAC) || aMacId == theMac.getDot11BroadcastAddress()) {
			theNavTimer.start(aNav, theMac.getMacAddress(), theAC, theMac.getPhy().getCurrentChannelId());
		}
	}

	@SuppressWarnings("unchecked")
	private void event_MSDUDeliv_req(JEEvent anEvent) {
		parameterlist = anEvent.getParameterList();
		JE802IPPacket packet = (JE802IPPacket) parameterlist.elementAt(3);
		int size = packet.getLength();
		long seqno = (Long) parameterlist.get(4); // MAC Seq No, not TCP
		int DA = ((JE802HopInfo) parameterlist.elementAt(0)).getAddress();
		ArrayList<JE802HopInfo> hopAdresses = (ArrayList<JE802HopInfo>) parameterlist.elementAt(2);
		int sourceHandler = (Integer) anEvent.getParameterList().get(5);
		int headerSize = theMac.getDot11MacHeaderDATA_byte();
		JE802_11Mpdu aMpdu = new JE802_11Mpdu(theMac.getMacAddress(), DA,
				JE80211MpduType.data, seqno, size, headerSize, theAC,
				sourceHandler, hopAdresses, theUniqueEventScheduler.now(), packet);

		// no queue handling needed here: this is done in event_MPDUDeliv_req.
		// Just request MPDUDeliv_req and thats it.
		parameterlist.clear();
		parameterlist.add(aMpdu);
		send(new JEEvent("MPDUDeliv_req", this, theUniqueEventScheduler.now(), parameterlist));
	}

	private void event_MPDUDeliv_req(JEEvent anEvent) {

		parameterlist = anEvent.getParameterList();
		JE802_11Mpdu aMpdu = (JE802_11Mpdu) parameterlist.elementAt(0);
		aMpdu.setPhyMode(theMac.getPhy().getCurrentPhyMode());
		aMpdu.setTxTime(calcTxTime(aMpdu)); // here we actually
		// store the txtime in the Mpdu as well now set the NAV to
		// FRAMEDURATION(the tx time)-SYNCDUR (usually 20us)+SIFS+ACK frame:
		aMpdu.setNAV(this
				.calcTxTime(aMpdu)
				.minus(theMac.getPhy().getPLCPHeaderDuration())
				.plus(theSIFS.plus(calcTxTime(0, theMac
						.getDot11MacHeaderACK_byte(), theMac.getPhy()
						.getBasicPhyMode(aMpdu.getPhyMode())))));
		if ((theMpduData == null) && (!theInterFrameSpaceTimer.is_active())) {
			theMpduData = aMpdu;
			theMpduData.setType(JE80211MpduType.data);
			theMpduData.setSA(theMac.getMacAddress());
			theMpduData.setTxTime(calcTxTime(theMpduData));
			theMpduData.setNAV(this
					.calcTxTime(aMpdu)
					.minus(theMac.getPhy().getPLCPHeaderDuration())
					.plus(theSIFS.plus(calcTxTime(0,
							theMac.getDot11MacHeaderACK_byte(),
							theMac.getPhy().getBasicPhyMode(
									aMpdu.getPhyMode())))));
			generateRts();
			boolean busy = cca_busy();
			if (!theBackoffTimer.is_active()) {
				if (!busy) {
					if (!theInterFrameSpaceTimer.is_active()) {
						deferForIFS();
					} else {
						error("Station " + theMac.getMacAddress() + " already deferring");
					}
				} else {
					if (theBackoffTimer.is_idle()) {
						theBackoffTimer.start(theCW, busy); // new backoff
					}
				}
			} else {
				// we have to wait until backoff expires. Happens often during post backoff.
			}
		} else { // queue the MPDU or discard if queue full - in this case inform TrafficGen
			if (theQueue.size() >= theQueueSize) {
				parameterlist.clear();
				parameterlist.add(theMpduData);
				parameterlist.add(true);
				send(new JEEvent("MSDU_discarded_ind", theMac, theUniqueEventScheduler.now(), parameterlist));
				this.keep_going(); // watchdog-like reminder. Attempt to wake up the backoff entity, if required.
			} else {
				theQueue.add(aMpdu);
			}
		}
	}

	private void nextMpdu() {
		if (!theQueue.isEmpty()) {
			if (theMpduData == null) {
				theLongRetryCnt = 0;
				theShortRetryCnt = 0;
				JE802_11Mpdu aMpdu = theQueue.remove(0); // we pull out a new MPDU from the queue
				parameterlist.clear();
				parameterlist.add(aMpdu);
				send(new JEEvent("MPDUDeliv_req", this, theUniqueEventScheduler.now(), parameterlist));
			} else {
				// backoff was stopped before. Resume it by starting IFS defer.
				deferForIFS();
			}
		} else {
			if (theMpduData != null) {
				deferForIFS();
			}
			// the queue is empty. So we don't have anything to do - well this is not entirely true:
			// If we are using the "saturation" traffic model, now is the time to ask the traffic
			// gen for another MPDU. Why? Because we ALWAYS have to pump data.
			// So let us ask for another MPDU:
			parameterlist.clear();
			parameterlist.add(theAC);
			send(new JEEvent("empty_queue_ind", theMac, theUniqueEventScheduler.now(), parameterlist));
		}
	}

	private void event_MlmeRequests(JEEvent anEvent) {
		String anEventName = anEvent.getName();
		if (anEventName.equals("undefinded")) { // nothing defined in MLME so far
		} else {
			error("Station " + theMac.getMacAddress() + " undefined MLME request event '" + anEventName + "'");
		}
	}

	private void tx(JE802_11Mpdu aTxPdu, JETime aTimeout) {

		if (theMac.getMlme().isARFEnabled()) {  // TODO - This is algorithm proprietary. Might be better to take it out later, here and in MLME
			aTxPdu.setPhyMode(theMac.getPhy().getCurrentPhyMode());
			if (aTxPdu.getSeqNo() == previousDiscardedPacket) {} else {
				previousDiscardedPacket = aTxPdu.getSeqNo();
			}
		}

		parameterlist.clear();
		parameterlist.add(aTxPdu);
		send(new JEEvent("txattempt_req", theVch, theUniqueEventScheduler.now(), parameterlist));
		if (aTxPdu.isData() || aTxPdu.isRts()) {
			// only RTS and DATA know the timeout, because they expect something back: CTS or ACK, resp.
			theTxTimeoutTimer.start(aTimeout);
		}
	}

	private void deferForIFS() {
		if (true) {//!theInterFrameSpaceTimer.is_active()) { // ignore during interframe space defering interval
			if (theMpduCtrl != null) { // CTS or ACK required
				theInterFrameSpaceTimer.start(theSIFS);
			} else if (theMpduRts != null) {
				if (!theBackoffTimer.is_active()) {
					theInterFrameSpaceTimer.start(theAIFS);
				}
			} else if (theMpduData != null) {
				if (!theBackoffTimer.is_active()) {
					if (theTxState == txState.txRts) {
						theInterFrameSpaceTimer.start(theSIFS); // we sent RTS before and just received CTS. Now SIFS is used before DATA, not AIFS
					} else {
						theInterFrameSpaceTimer.start(theAIFS);
					}
				} else { // backoff is busy. Do nothing though data is pending.
					message("Station " + theMac.getMacAddress() + " deferForIFS: doing nothing though data is pending: backoff timer already busy.", 10);
				}
			} else { // the transmission was successful.
				theTxState = txState.idle;
				nextMpdu(); // now check for next MPDU
			}
		}
	}

	private boolean cca_busy() { // returns true if NAV or medium is busy, or
		// interframe space is ongoing (SIFS, AIFS)
		return (theNavTimer.getExpiryTime().isLaterThan(theUniqueEventScheduler.now())
				|| theMac.getPhy().isCcaBusy()
				|| theInterFrameSpaceTimer.is_active());
	}

	private void generateRts() {
		if (theMpduData != null) {
			if (theMpduRts == null) {
				if (!(theMpduData.getFrameBodySize() < theMac.getDot11RTSThreshold())) {
					int headerSize = theMac.getDot11MacHeaderRTS_byte();
					theMpduRts = new JE802_11Mpdu(theMpduData.getSA(), theMpduData.getDA(),
							JE80211MpduType.rts, 0, 0, headerSize, theAC,
							-1, null, null, null);

					theMpduRts.setPhyMode(theMac.getPhy().getBasicPhyMode(theMpduData.getPhyMode()));


					// calculate transmission time, with RTS framebody size 0
					theMpduRts.setTxTime(calcTxTime(0, theMac.getDot11MacHeaderRTS_byte(),
							theMac.getPhy().getBasicPhyMode(theMpduData.getPhyMode())));

					// set seqno
					theMpduRts.setSeqNo(theMpduData.getSeqNo());
					// calculate RTS NAV duration field
					JETime nav = theMpduRts.getTxTime();
					nav = nav.minus(theMac.getPhy().getPLCPHeaderDuration());
					nav = nav.plus(theSIFS);
					nav = nav.plus(this.calcTxTime(0,theMac.getDot11MacHeaderCTS_byte(),
							theMac.getPhy().getBasicPhyMode(theMpduData.getPhyMode())));
					nav = nav.plus(theSIFS);
					nav = nav.plus(theMpduData.getTxTime());
					nav = nav.plus(theSIFS);
					nav = nav.plus(this.calcTxTime(0,theMac.getDot11MacHeaderACK_byte(),
							theMac.getPhy().getBasicPhyMode(theMpduData.getPhyMode())));
					theMpduRts.setNAV(nav);
				}
			} else {
				error("Station " + theMac.getMacAddress() +
						" backoff entity has ongoing RTS frame.");
			}
		} else {
			error("Station " + theMac.getMacAddress() +
					" backoff entity has ongoing MPDU.");
		}
	}

	private void generateCts() {
		int aDA = theMpduRx.getSA();
		JETime anRtsNav = theMpduRx.getNav();
		if (theMpduCtrl == null) {
			int headersize = theMac.getDot11MacHeaderCTS_byte();
			theMpduCtrl = new JE802_11Mpdu(0, aDA, JE80211MpduType.cts, 0,
					0, headersize, theAC, -1, null, null, null);
			theMpduCtrl.setSA(theMac.getMacAddress());
			// calculate CTS transmission time
			theMpduCtrl.setPhyMode(theMac.getPhy().getCurrentPhyMode());
			theMpduCtrl.setTxTime(calcTxTime(0, theMac .getDot11MacHeaderCTS_byte(),
					theMac.getPhy().getCurrentPhyMode()));
			// set seqno
			theMpduCtrl.setSeqNo(theMpduRx.getSeqNo());
			// calculate CTS's nav value:
			theMpduCtrl.setNAV(anRtsNav.minus(theSIFS).minus(theMpduCtrl.getTxTime())
					.minus(theMac.getPhy().getPLCPHeaderDuration()));
		} else {
			message("Station " + theMac.getMacAddress() +
					"generateCTS: backoff entity has pending CTS/ACK frame",
					10);
		}
	}

	private void generateAck() {
		if (theMpduCtrl == null) {
			int headerSize = theMac.getDot11MacHeaderACK_byte();
			theMpduCtrl = new JE802_11Mpdu(0, theMpduRx.getSA(), JE80211MpduType.ack, 0,
					0, headerSize, theAC, -1, null, null, null);
			theMpduCtrl.setTxTime(calcTxTime(0, theMac.getDot11MacHeaderACK_byte(),
					theMac.getPhy().getBasicPhyMode(theMpduRx.getPhyMode())));
			theMpduCtrl.setNAV(theMpduCtrl.getTxTime().minus(theMac.getPhy().getPLCPHeaderDuration()));
			theMpduCtrl.setSA(theMac.getMacAddress());
			theMpduCtrl.setPhyMode(theMac.getPhy().getBasicPhyMode(theMpduRx.getPhyMode()));
			// set seqno
			theMpduCtrl.setSeqNo(theMpduRx.getSeqNo());
			message("Station " + theMac.getMacAddress() + " sending ACK " + theMpduCtrl.getSeqNo() + " to Station "
					+ theMpduCtrl.getDA() + " on channel " + theMac.getPhy().getCurrentChannelId(), 10);
		} else {
			error("Station " + theMac.getMacAddress() + " generateACK: backoff entity has pending CTS/ACK frame");
		}
	}

	private boolean checkAndTxRTS(boolean IfsExpired) {
		if (theBackoffTimer.is_active()) {
			warning("Station " + theMac.getMacAddress() + " backoff still busy");
			return false;
		}
		if (theMpduRts == null) {
			return false; // no RTS to send
		}
		if (theMpduRts.isRts()) {
			if (IfsExpired) { // we are now at the end of the IFS
				boolean busy = cca_busy();
				if (theBackoffTimer.is_paused() && !busy) {
					theBackoffTimer.resume(cca_busy()); // continue downcounting
				}
				if (theBackoffTimer.is_idle()) {
					theBackoffTimer.start(theCW, busy); // new backoff
				}
				return true;
			} // we are now at the end of the backoff
			if (!cca_busy()) {
				theTxState = txState.txRts;
				updateBackoffTimer();
				JETime aTimeout = new JETime(calcTxTime(0,
						theMac.getDot11MacHeaderRTS_byte(), theMpduRts.getPhyMode()));
				aTimeout = aTimeout.plus(theSIFS);
				aTimeout = aTimeout.plus(calcTxTime(0,
						theMac.getDot11MacHeaderCTS_byte(), theMpduRts.getPhyMode()));
				aTimeout = aTimeout.plus(theSlotTime);
				tx(theMpduRts, aTimeout);
				return true; // RTS was sent (if no error occurred. But then we are in trouble anyway.)
			} else {
				return false;
			}
		} else {
			error("Station " + theMac.getMacAddress() + " has Mpdu of wrong subtype, expected "
					+ JE80211MpduType.rts);
			return false;
		}
	}

	private boolean checkAndTxCTRL() {

		if (theMpduCtrl == null) {
			return false; // no CTS or ACK to send
		}

		if (this.theTxState == txState.txData) {
			//			this.theUniqueGui.addLine(this.theUniqueEventScheduler.now(), this.theMac.getMacAddress(), this.theAC, "magenta", 1);
			//warning("Station " + theMac.getMacAddress() + " tx(): attempt to transmit ACK while already transmitting data frame");
			theMpduCtrl = null;
			return true; // currently transmitting already (can happen in hidden scenarios)
		}

		if (theMpduCtrl.isCts()) {
			theTxState = txState.txCts;
		} else if (theMpduCtrl.isAck()) {
			theTxState = txState.txAck;
		} else {
			error("Station " + theMac.getMacAddress() + " has Mpdu of wrong subtype, expected " + JE80211MpduType.ack + " or " + JE80211MpduType.cts);
		}
		tx(theMpduCtrl, /* no timeout for CTS or ACK: */ null);
		theMpduCtrl = null;
		return true;
	}

	private boolean checkAndTxDATA(boolean IfsExpired) {
		if (theBackoffTimer.is_active()) {
			warning("Station " + theMac.getMacAddress() + " backoff still busy");
			return false;
		}
		if (theMpduData == null) {
			return false; // no DATA to send
		}
		boolean busy = cca_busy();
		if (theMpduData.isData()) {
			if (IfsExpired && !theTxState.equals(txState.txRts)) {
				if (theBackoffTimer.is_paused() && !busy) {
					theBackoffTimer.resume(cca_busy()); // continue down counting
					//					this.theUniqueGui.addLine(this.theUniqueEventScheduler.now(), this.theMac.getMacAddress(), this.theAC-1, "green", 1);
				}
				if (theBackoffTimer.is_idle()) {
					theBackoffTimer.start(theCW, busy); // new backoff
					this.updateBackoffTimer();
					//					this.theUniqueGui.addLine(this.theUniqueEventScheduler.now(), this.theMac.getMacAddress(), this.theAC, "green", 1);
				}
				return true;
			}
			if (!busy) {
				theTxState = txState.txData;
				updateBackoffTimer();
				JETime aTimeout = new JETime(theMpduData.getTxTime());
				aTimeout = aTimeout.plus(theSIFS);
				aTimeout = aTimeout.plus(calcTxTime(0, theMac
						.getDot11MacHeaderACK_byte(), theMac.getPhy()
						.getBasicPhyMode(theMpduData.getPhyMode())));
				aTimeout = aTimeout.plus(theSlotTime);
				theTxCnt++;
				theTxByteCnt = theTxByteCnt + theMpduData.getFrameBodySize();
				this.tx(theMpduData, aTimeout);
				return true;
			} else {
				return false;
			}
		} else {
			error("Station " + theMac.getMacAddress() + " has Mpdu of wrong subtype, expected " + JE80211MpduType.data);
			return false;
		}
	}


	private JETime calcTxTime(int framebodysize, int headersize, JE802PhyMode aPhyMode) {

		// first the preamble:
		JETime aTxTime = theMac.getPhy().getPLCPPreamble();
		// now add the phy header:
		aTxTime = aTxTime.plus(theMac.getPhy().getPLCPHeaderWithoutServiceField());
		// now calc the payload duration incl MAC Header in byte:
		int framesize = framebodysize + headersize + theMac.getDot11MacFCS_byte();
		if (theMac.isDot11WepEncr()) {
			framesize = framesize + 8 * 32; // theMac.get("MACCONFIG.WEP_byte");
		}
		// now calc the duration of this payload:
		int aNumOfPayload_bit = theMac.getPhy().getPLCPServiceField_bit() + framesize * 8;
		int aNumOfSymbols = (aNumOfPayload_bit + aPhyMode.getBitsPerSymbol() - 1) / aPhyMode.getBitsPerSymbol();
		JETime aPayloadDuration = theMac.getPhy().getSymbolDuration().times(aNumOfSymbols);
		// now calculate the final duration and return it:
		aTxTime = aTxTime.plus(aPayloadDuration);
		return aTxTime;
	}

	private JETime calcTxTime(JE802_11Mpdu aMpdu) {
		int framebodysize = aMpdu.getFrameBodySize();
		int headersize = aMpdu.getHeaderSize();
		JE802PhyMode aPhyMode = aMpdu.getPhyMode();
		return calcTxTime(framebodysize, headersize, aPhyMode);
	}

	public Integer getAC() {
		return theAC;
	}

	public JETime getAIFS() {
		return theAIFS;
	}

	public int getDot11EDCAAIFSN() {
		return dot11EDCAAIFSN;
	}

	public int getDot11EDCACWmax() {
		return dot11EDCACWmax;
	}

	public int getDot11EDCACWmin() {
		return dot11EDCACWmin;
	}

	public JETime getDot11EDCAMMSDULifeTime() {
		return dot11EDCAMMSDULifeTime;
	}

	public int getDot11EDCATXOPLimit() {
		return dot11EDCATXOPLimit;
	}

	public double getDot11EDCAPF() {
		return dot11EDCAPF;
	}

	public int getTheCollisionCnt() {
		return theCollisionCnt;
	}

	public int getQueueSize() {
		return theQueueSize;
	}

	public int getCurrentQueueSize() {
		return theQueue.size();
	}

	public JE802_11Mac getMac() {
		return theMac;
	}

	public void setMaxRetryCountShort(int aMaxRetryCountShort) {
		maxRetryCountShort = aMaxRetryCountShort;
	}

	public int getMaxRetryCountShort() {
		return maxRetryCountShort;
	}

	public void setMaxRetryCountLong(int aMaxRetryCountLong) {
		maxRetryCountLong = aMaxRetryCountLong;
	}

	public int getMaxRetryCountLong() {
		return maxRetryCountLong;
	}

	public void setAC(Integer anAC) {
		theAC = anAC;
	}

	public HashSet<Integer> getMacAddresses() {
		return this.theMacAddresses;
	}

	public void setDot11EDCAAIFSN(Integer dot11edcaaifsn) {
		if (dot11edcaaifsn < 1) {
			warning("Station " + theMac.getMacAddress() + " AIFSN < 1: " + dot11edcaaifsn);
			dot11edcaaifsn = 1;
		}
		dot11EDCAAIFSN = dot11edcaaifsn;
		theAIFS = theSIFS.plus(theSlotTime.times(dot11EDCAAIFSN));
	}

	public void setDot11EDCACWmax(Integer dot11edcacWmax) {
		dot11EDCACWmax = dot11edcacWmax;
	}

	public void setDot11EDCACWmin(Integer dot11edcacWmin) {
		if (dot11edcacWmin < 1) {
			warning("Station " + theMac.getMacAddress() + " CWmin < 1: " + dot11edcacWmin);
			dot11edcacWmin = 1;
		}
		dot11EDCACWmin = dot11edcacWmin;
	}

	public void setDot11EDCAPF(Double dot11edcapf) {
		dot11EDCAPF = dot11edcapf;
	}

	public void setDot11EDCATXOPLimit(Integer dot11edcatxopLimit) {
		dot11EDCATXOPLimit = dot11edcatxopLimit;
	}

	public void setDot11EDCAMMSDULifeTime(JETime dot11edcammsduLifeTime) {
		dot11EDCAMMSDULifeTime = dot11edcammsduLifeTime;
	}

	@Override
	public String toString() {
		return "BE in Station " + theMac.getMacAddress() + " AC: " + theAC;
	}

	public void discardQueue() {
		theDiscardCnt += theQueue.size();
		for (int i = theQueue.size() - 1; i >= 0; i--) {
			Vector < Object > params = new Vector < Object > ();
			params.add(theQueue.get(i));
			params.add(theMac.getPhy().getCurrentChannelId());
			//send(new JEEvent("push_back_packet", theMac.getHandlerId(), theUniqueEventScheduler.now(), params)); // TODO.... This does something weird in the IP layer, might be required for multi-hop
		}
		theQueue.clear();
		parameterlist.clear();
		parameterlist.add(theAC);
		send(new JEEvent("empty_queue_ind", theMac, theUniqueEventScheduler.now(), parameterlist));
	}

	public void setTheDiscardCnt(int aDiscardedCounter) {
		theDiscardCnt = aDiscardedCounter;
	}

	public long getTheDiscardCnt() {
		return theDiscardCnt;
	}

	public JETime getTime() {
		return theUniqueEventScheduler.now();
	}

	public JEEventScheduler getTheUniqueEventScheduler() {
		return theUniqueEventScheduler;
	}

	public Integer getLongRetryCount() {
		return theLongRetryCnt;
	}

	public Integer getShortRetryCount() {
		return theShortRetryCnt;
	}

}
