package layer2_802Algorithms;

import plot.JEMultiPlotter;
import layer2_80211Mac.JE802_11BackoffEntity;
import layer2_80211Mac.JE802_11Mac;
import layer2_80211Mac.JE802_11MacAlgorithm;

public class TutorialVerbose extends JE802_11MacAlgorithm {

	private JE802_11BackoffEntity theBackoffEntityAC01;
	private boolean flag_undefined = false;

	public TutorialVerbose(String name, JE802_11Mac mac) {
		super(name, mac);
		this.theBackoffEntityAC01 = this.mac.getBackoffEntity(1);
	}

	@Override
	public void compute() {
		message("---------------------------", 10);
		message("I am station " + this.dot11MACAddress.toString() + ". My algorithm is called '" + this.algorithmName
				+ "'.", 10);

		// observe outcome:
		Integer AIFSN_AC01 = theBackoffEntityAC01.getDot11EDCAAIFSN();
		Integer CWmin_AC01 = theBackoffEntityAC01.getDot11EDCACWmin();

		theBackoffEntityAC01.getQueueSize();
		theBackoffEntityAC01.getCurrentQueueSize();

		message("with the following contention window parameters ...", 10);
		message("    AIFSN[AC01] = " + AIFSN_AC01.toString(), 10);
		message("    CWmin[AC01] = " + CWmin_AC01.toString(), 10);
		message("... the backoff entity queues perform like this:", 10);

		// infer decision: (note, we just change the values arbitrarily
		if (flag_undefined) { // we should increase AIFSN
			AIFSN_AC01 = AIFSN_AC01 + 1;
		} else { // we should decrease AIFSN
			AIFSN_AC01 = AIFSN_AC01 - 1;
		}
		if (AIFSN_AC01 >= 20)
			flag_undefined = false;
		if (AIFSN_AC01 <= 2)
			flag_undefined = true;

		// AIFSN_AC02 = 10;
		// CWmin_AC02 = 5;

		// act:
		theBackoffEntityAC01.setDot11EDCAAIFSN(AIFSN_AC01);
		theBackoffEntityAC01.setDot11EDCACWmin(CWmin_AC01);
	}

	@Override
	public void plot() {
		if (plotter == null) {
			plotter = new JEMultiPlotter("", "AIFSN(AC01)", "time [ms]", this.algorithmName + "", 10000, true);
			plotter.addSeries("CWmin(AC01)");
			plotter.addSeries("queue(AC01)");
			plotter.display();
		}
		plotter.plot(((Double) theUniqueEventScheduler.now().getTimeMs()).doubleValue(),
				theBackoffEntityAC01.getDot11EDCAAIFSN(), 0);
		plotter.plot(((Double) theUniqueEventScheduler.now().getTimeMs()).doubleValue(),
				theBackoffEntityAC01.getDot11EDCACWmin(), 1);
		plotter.plot(((Double) theUniqueEventScheduler.now().getTimeMs()).doubleValue(),
				theBackoffEntityAC01.getCurrentQueueSize(), 2);
	}

}
