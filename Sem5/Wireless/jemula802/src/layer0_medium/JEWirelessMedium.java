package layer0_medium;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Vector;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import kernel.JEEvent;
import kernel.JEEventHandler;
import kernel.JEEventScheduler;
import kernel.JETime;
import kernel.JEmula;
import layer1_802Phy.JE802Phy;
import layer1_802Phy.JE802Ppdu;

import util.Vector3d;
import visualization.JE802KmlGenerator;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class JEWirelessMedium extends JEEventHandler {

  private final Map<Integer, JE802ChannelEntry> theChannels;

  private final Map<Integer, JEWirelessChannel> theAvailableChannels;

  private final double theCoverageRange_m;

  private final double theAttenuationCoeff;

  private final List<JE802Phy> allPhys;

  // lookup table for attenuation factors
  private final Map<Integer, Map<Integer, Double>> attenuationTable;

  public JEWirelessMedium(final JEEventScheduler aScheduler,
      final Random aGenerator, final Node aTopLevelNode) {

    super(aScheduler, aGenerator);
    this.theChannels = new HashMap<Integer, JE802ChannelEntry>();
    this.theAvailableChannels = new HashMap<Integer, JEWirelessChannel>();
    attenuationTable = new HashMap<Integer, Map<Integer, Double>>();
    allPhys = new ArrayList<JE802Phy>();
    Element wirelessElem = (Element) aTopLevelNode;

    if (!wirelessElem.getAttribute("theCoverageRange_m").isEmpty()) {
      this.warning("Deprecated attribute \"theCoverageRange_m\" found. "
          + "Please change this entry to \"CoverageRange_m\" in your xml scenario description (get rid of the \"the\").");			
    }
    String aCoverageRange_m = wirelessElem.getAttribute("CoverageRange_m");
    if (!aCoverageRange_m.isEmpty()) {
      this.theCoverageRange_m = Integer.valueOf(aCoverageRange_m);
    } else {
      this.warning("missing attribute \"CoverageRange_m\", using default 10m.");
      this.theCoverageRange_m = 10;
    }
    String anAttenuationCoeff = wirelessElem.getAttribute("AttenuationCoeff");
    if (!anAttenuationCoeff.isEmpty()) {
      this.theAttenuationCoeff = Double.valueOf(anAttenuationCoeff);
    } else {
      this.warning("missing attribute \"AttenuationCoeff\", using default 2.");
      this.theAttenuationCoeff = 2.0;
    }

    if (!wirelessElem.getAttribute("channelBusyThreshold_dBm").isEmpty()) {
      this.warning("Deprecated attribute \"channelBusyThreshold_dBm\" found. "
          + "Please remove this entry from your xml scenario description. "
          + "The threshold is station dependent and defined in each phy separately.");
    }

    // channel noise, deprecated:
    if (!wirelessElem.getAttribute("noiseLevel_dBm").isEmpty()) {
      this.warning("Deprecated attribute \"noiseLevel_dBm\" found. "
          + "Please remove this entry from your xml scenario description. "
          + "The thermal noise is now based on channel bandwidth only.");			
    }

    XPath xpath = XPathFactory.newInstance().newXPath();
    NodeList channelNodeList;
    try {
      channelNodeList = (NodeList) xpath.evaluate("aChannel", wirelessElem, XPathConstants.NODESET);
      if (channelNodeList != null && channelNodeList.getLength() > 0) {
        for (int i = 0; i < channelNodeList.getLength(); i++) {
          Node channelNode = channelNodeList.item(i);
          JEWirelessChannel aNewChannel = new JEWirelessChannel(channelNode);
          this.theAvailableChannels.put(aNewChannel.getChannelNumber(),aNewChannel);
        }
      } else {
        this.error("XML definition JE802WirelessChannels has no child nodes!");
      }
    } catch (XPathExpressionException e) {
      e.printStackTrace();
    }
    for (JEWirelessChannel aChannel : this.theAvailableChannels.values()) {
      JE802ChannelEntry entry = new JE802ChannelEntry(
          aChannel.getChannelNumber(),
          aChannel.getDot11CenterFreq_MHz(),
          aChannel.getNoiseLevel_mW(),
          this.theAttenuationCoeff);
      this.theChannels.put(aChannel.getChannelNumber(), entry);
    }
  }

  @Override
  public void event_handler(final JEEvent anEvent) {
    String eventName = anEvent.getName();
    if (eventName.equals("MEDIUM_TxStart_req")) {
      this.attenuationUpdate(anEvent);
      this.transmitStart(anEvent);
    } else if (eventName.equals("tx_end")) {
      this.transmitEnd(anEvent);
    } else if (eventName.equals("channel_switch_req")) {
      this.switchChannel(anEvent);
    } else if (eventName.equals("attenuation_update_req")) {
      this.attenuationUpdate(anEvent);
    } else if (eventName.equals("register_req")) {
      this.registerPhy(anEvent);
    } else {
      this.error("Undefined Event " + eventName + " at WirelessMedium");
    }
  }

  // there are two things to do at the start of a transmission,
  // first calculate the interference of all other transmissions for all our
  // receivers. Second, compute the interference introduced by this
  // transmission at all the receivers of the other transmission, also for
  // neighboring channels.
  private void transmitStart(final JEEvent anEvent) {
    JETime now = anEvent.getScheduledTime();
    JE802Ppdu aPpdu = (JE802Ppdu) anEvent.getParameterList().elementAt(1);
    JE802Phy aTxPhy = (JE802Phy) anEvent.getParameterList().elementAt(0);
    // channel on which to transmit
    int channel = aTxPhy.getCurrentChannelId();
    JE802ChannelEntry chan = theChannels.get(channel);
    // register packet transmission
    JE802MediumTxRecord ourTransmission = chan
        .addTransmittingPhy(
            aTxPhy,
            theUniqueEventScheduler.now().plus(
                aPpdu.getMpdu().getTxTime()));
    double txPower_mW = aTxPhy.getCurrentTransmitPower_mW();
    Map<Integer, Double> neighbors = attenuationTable.get(aTxPhy.getMac()
        .getMacAddress());

    // //update interference introduced by this transmission at receivers of
    // all other transmissions on neighboring channels
    for (Integer interferingChannel : chan.getInterferingChannels()) {
      JE802ChannelEntry currentChan = theChannels.get(interferingChannel);
      List<JE802MediumTxRecord> otherChannelTransmissions = currentChan
          .getTransmittingPhys();
      double crossChannelInterference = crossChannelInterference(
          interferingChannel, chan.getChannelId());
      for (JE802MediumTxRecord aTransmission : otherChannelTransmissions) {
        aTransmission.addInterference(aTxPhy, crossChannelInterference);
      }
    }

    // update interference introduced by this transmission at receivers of
    // all other transmissions on this channel
    List<JE802MediumTxRecord> otherTransmissions = chan
        .getTransmittingPhys();
    for (JE802MediumTxRecord aTransmission : otherTransmissions) {
      aTransmission.addInterference(aTxPhy, 1);
    }

    List<JE802Phy> channelPhys = chan.getPhyList();
    int size = channelPhys.size();
    for (int i = 0; i < size; i++) {
      JE802Phy rxPhy = channelPhys.get(i);
      // don't receive at own station and also not if too far away
      if (rxPhy != aTxPhy
          && neighbors.get(rxPhy.getMac().getMacAddress()) != null) {
        double neighborChannelInterferencemW = 0.0;
        // calculate interference for all neighboring channels
        for (Integer interferingChannel : chan.getInterferingChannels()) {
          JE802ChannelEntry currentChan = theChannels.get(interferingChannel);
          double neighborInterference = this.calcInterference(rxPhy, aTxPhy, currentChan);
          // the neighboring channel interference depends on the distance between channels
          double crossChannelInterference = crossChannelInterference(interferingChannel, chan.getChannelId());
          neighborChannelInterferencemW += neighborInterference * crossChannelInterference;
        }

        // calculate interference of own transmissions on own channel
        double currentChannelInterferencemW = this.calcInterference(rxPhy, aTxPhy, chan);
        double factor = getPathloss(rxPhy.getMac().getMacAddress(), aTxPhy.getMac().getMacAddress());
        double totalInterference_mW = currentChannelInterferencemW + neighborChannelInterferencemW;
        ourTransmission.addRxInterference(rxPhy, totalInterference_mW);
        double receptionPower = txPower_mW * factor;
        double SINR = receptionPower / (totalInterference_mW + chan.getNoiseLevel_mW() * this.dBToFactor(rxPhy.getNoiseFloor_dB()));

        // only deliver packet if power to interferenceRatio is bigger than threshold
        JE802Ppdu rxPpdu = aPpdu.clone();
        Vector<Object> parameterList = new Vector<Object>();

        // jam the packet with a certain probability
        double prob = rxPpdu.getMpdu().getPhyMode().getPacketErrorProb(aPpdu.getMpdu().getFrameBodySize(),SINR);
        if (prob > 0) {
          double rand = theUniqueRandomGenerator.nextDouble();
          if (rand < prob) {
            rxPpdu.jam();
          }
        }
        parameterList.addElement(rxPpdu);
        this.send(new JEEvent("MEDIUM_RxStart_ind", rxPhy, now,parameterList));
      }
    }
    Vector<Object> parameterList = new Vector<Object>();
    parameterList.add(aTxPhy);
    parameterList.add(aPpdu);
    this.send(new JEEvent("tx_end", this, now.plus(aPpdu.getMpdu().getTxTime()), parameterList));
  }

  private void transmitEnd(final JEEvent anEvent) {
    JETime now = anEvent.getScheduledTime();
    JE802Ppdu aPpdu = (JE802Ppdu) anEvent.getParameterList().elementAt(1);
    JE802Phy aTxPhy = (JE802Phy) anEvent.getParameterList().elementAt(0);
    JE802ChannelEntry channel = theChannels.get(aTxPhy.getCurrentChannelId());
    JE802MediumTxRecord transmission = channel.removeTransmittingPhy(aTxPhy);

    // create a list of all ongoing transmissions on ownChannel and on interfering channels
    for (Integer neighborChannel : channel.getInterferingChannels()) {
      JE802ChannelEntry neighbor = theChannels.get(neighborChannel);
      List<JE802MediumTxRecord> txOnNeighborChanel = neighbor.getTransmittingPhys();
      double crossChannelInterference = crossChannelInterference(aTxPhy.getCurrentChannelId(), neighborChannel);
      for (JE802MediumTxRecord txNeighbor : txOnNeighborChanel) {
        txNeighbor.decreaseInterference(aTxPhy, crossChannelInterference);
      }
    }
    for (JE802MediumTxRecord ownChannelTx : channel.getTransmittingPhys()) {
      ownChannelTx.decreaseInterference(aTxPhy, 1);
    }

    // deliver packet and indicate whether interference was too high
    for (Integer rxStation : transmission.getRxStations()) {
      double interferenceAtRx = transmission.getMaxInterference(rxStation) + channel.getNoiseLevel_mW(); // TODO: add here rx noise floor
      double attenuationTxRx = getPathloss(aTxPhy.getMac().getMacAddress(), rxStation);
      double powerAtRx = aTxPhy.getCurrentTransmitPower_mW() * attenuationTxRx;
      double SNIR = powerAtRx / interferenceAtRx;      // SINR = Signal to (Noise + Interference) Ratio
      JE802Ppdu aRxPpdu = aPpdu.clone(); // prevent aliasing, otherwise jam signal would be set to one value for all theStations
      double prob = aRxPpdu.getMpdu().getPhyMode().getPacketErrorProb(aPpdu.getMpdu().getFrameBodySize(),SNIR); // jam the packet with a certain probability
      if (prob > 0) {
        double rand = theUniqueRandomGenerator.nextDouble();
        if (rand < prob) {
          aRxPpdu.jam();
        }
      }

      Vector<Object> parameterList = new Vector<Object>();
      parameterList.add(aRxPpdu);
      JE802Phy rxPhy = null;
      // search for rxPhy
      for (JE802Phy aRxPhy : channel.getPhyList()) {
        if (aRxPhy.getMac().getMacAddress() == rxStation) {
          rxPhy = aRxPhy;
          break;
        }
      }
      // rx Station could have switched the channel during the time a packet is transmitted
      if (rxPhy != null && rxPhy != aTxPhy) {
        this.send(new JEEvent("MEDIUM_RxEnd_ind", rxPhy.getHandlerId(), now, parameterList));
      }
    }
  }

  private void switchChannel(JEEvent anEvent) {
    JE802Phy phy = (JE802Phy) anEvent.getParameterList().get(0);
    Integer from = (Integer) anEvent.getParameterList().get(1);
    Integer to = (Integer) anEvent.getParameterList().get(2);
    JE802ChannelEntry fromEntry = theChannels.get(from);
    fromEntry.removePhy(phy);
    JE802ChannelEntry toEntry = theChannels.get(to);
    toEntry.addPhy(phy);
  }

  private double dBToFactor(final double dB) {
    return Math.pow(10, dB / 10);
  }

  private double calcPathloss(final JE802Phy aSourcePhy, final JE802Phy aDestinationPhy, final double aPathLossAt1m_dB, final double aFrequency_MHz) {
    Vector3d src = aSourcePhy.getMobility().getLocation();
    Vector3d dst = aDestinationPhy.getMobility().getLocation();
    if (aSourcePhy.getMobility().isMobile()) {
      src.setAlt(src.getAlt() + JE802KmlGenerator.theStationSize_m);
    } else {
      src.setAlt(src.getAlt() + JE802KmlGenerator.theAccessPointAntennaSize_m);
    }

    if (aDestinationPhy.getMobility().isMobile()) {
      dst.setAlt(dst.getAlt() + JE802KmlGenerator.theStationSize_m);
    } else {
      dst.setAlt(dst.getAlt() + JE802KmlGenerator.theAccessPointAntennaSize_m);
    }

    Vector3d pathDirection = dst.sub(src).normalize();
    double distance = src.getDistanceTo(dst);

    double anAttenuation_dB;
    if (distance > 1.0) {
      double lambda = 3E8 / (aFrequency_MHz * 1E6);
      anAttenuation_dB = this.getAttenuationCoeff() * 10 * Math.log10(lambda / (4 * Math.PI * distance)); 
    } else {
      anAttenuation_dB = aPathLossAt1m_dB;
    }
    // compute directional gains - "effect of mobility, heading, is removed
    //    double srcDirectionalGain_dBi = aSourcePhy.getAntenna().getGainIndBForDirection(pathDirection, src.getLat(),src.getLon(), aSourcePhy.getMobility().getTraceHeading());
    //    double dstDirectionalGain_dBi = aDestinationPhy.getAntenna().getGainIndBForDirection(pathDirection.reflect(), dst.getLat(), dst.getLon(), aDestinationPhy.getMobility().getTraceHeading());
    double srcDirectionalGain_dBi = aSourcePhy.getAntenna().getGainIndBForDirection(pathDirection, src.getLat(),src.getLon(), 0);
    double dstDirectionalGain_dBi = aDestinationPhy.getAntenna().getGainIndBForDirection(pathDirection.reflect(), dst.getLat(), dst.getLon(), 0);
    return dBToFactor(anAttenuation_dB + srcDirectionalGain_dBi	+ dstDirectionalGain_dBi);
  }

  public double calcPathloss(final JE802Phy aSourcePhy, final Vector3d dst, JEWirelessChannel aChannel) {
    // compute the path loss from phy to an arbitrary location, used for visualization
    Vector3d src = aSourcePhy.getMobility().getLocation();

    Vector3d pathDirection = dst.sub(src).normalize();

    double distance = src.getDistanceTo(dst);

    double anAttenuation_dB;
    double lambda = 3E8 / (aChannel.getDot11CenterFreq_MHz() * 1E6);
    if (distance > 1.0) {
      anAttenuation_dB = this.getAttenuationCoeff() * 10 * Math.log10(lambda / (4 * Math.PI * distance)); 
    } else {
      anAttenuation_dB = this.getAttenuationCoeff() * 10 * Math.log10(lambda / (4 * Math.PI * 1 /* one meter distance */));
    }
    // compute directional gains - "effect of mobility, heading, is removed
    // double srcDirectionalGain_dBi = aSourcePhy.getAntenna().getGainIndBForDirection(pathDirection, src.getLat(),src.getLon(), aSourcePhy.getMobility().getTraceHeading());
    double srcDirectionalGain_dBi = aSourcePhy.getAntenna().getGainIndBForDirection(pathDirection, src.getLat(),src.getLon(), 0);
    return dBToFactor(anAttenuation_dB + srcDirectionalGain_dBi);

  }

  private double calcInterference(final JE802Phy atPhy, final JE802Phy txPhy, final JE802ChannelEntry chan) {
    // calculates the interference power level in mW at the position of atPhy
    double interferenceSum_mW = 0.0;
    List<JE802MediumTxRecord> physOnChannel = chan.getTransmittingPhys();
    int size = physOnChannel.size();
    for (int i = 0; i < size; i++) { // performance critical...
      JE802MediumTxRecord otherTx = physOnChannel.get(i);
      JE802Phy currentPhy = otherTx.getTxPhy();
      if (currentPhy != txPhy) {
        // first thing, add other transmissions interference to our own interference
        double attenuationFactor = getPathloss(atPhy.getMac().getMacAddress(), currentPhy.getMac().getMacAddress());
        double powerLevelAtcurrentPhy = currentPhy.getCurrentTransmitPower_mW() * attenuationFactor;
        interferenceSum_mW += powerLevelAtcurrentPhy;
      }
    }
    return interferenceSum_mW;
  }

  private double crossChannelInterference(final int a, final int b) {
    double distance = Math.max(0, theCoverageRange_m - Math.abs(a - b));
    return distance / theCoverageRange_m;
  }

  private void registerPhy(final JEEvent anEvent) {
    JE802Phy newPhy = (JE802Phy) anEvent.getParameterList().elementAt(0);
    int channel = newPhy.getCurrentChannelId();
    JE802ChannelEntry entry = theChannels.get(channel);
    // add phy to existing channel
    entry.addPhy(newPhy);
    // compute the pathloss to all other theStations and store in attenuation table
    allPhys.add(newPhy);
    updateAttenuations(newPhy);
  }

  private void attenuationUpdate(JEEvent anEvent) {
    JE802Phy updatePhy = (JE802Phy) anEvent.getParameterList().get(0);
    // on a location update, we have to update the attenuation table and
    // recalculate the attenuation for the connections to all neighboring
    // theStations
    this.updateAttenuations(updatePhy);
  }

  private void updateAttenuations(JE802Phy newPhy) {
    JE802ChannelEntry entry = theChannels.get(newPhy.getCurrentChannelId());
    Map<Integer, Double> attenuations = attenuationTable.get(newPhy.getMac().getMacAddress());
    if (attenuations == null) {
      attenuations = new HashMap<Integer, Double>();
    }
    for (JE802Phy otherPhy : allPhys) {
      double pathLossFactor = calcPathloss(newPhy, otherPhy, entry.getPathlossAt1m_dB(), entry.getFrequency_MHz());
      // update attenuation from us to other station
      attenuations.put(otherPhy.getMac().getMacAddress(), pathLossFactor);
      // update our attenuation at the other station
      Map<Integer, Double> othersAttenuations = attenuationTable.get(otherPhy.getMac().getMacAddress());
      if (othersAttenuations != null) {
        othersAttenuations.put(newPhy.getMac().getMacAddress(),pathLossFactor);
      } else {
        Map<Integer, Double> map = new HashMap<Integer, Double>();
        map.put(otherPhy.getMac().getMacAddress(), pathLossFactor);
      }
      attenuationTable.put(newPhy.getMac().getMacAddress(), attenuations);
    }
  }

  private double getPathloss(final Integer sta1, final Integer sta2) {
    Map<Integer, Double> mapPhy1 = attenuationTable.get(sta1);
    Double pathloss = mapPhy1.get(sta2);
    if (pathloss != null) {
      return pathloss;
    }
    // outside of transmission range
    this.warning("returning NaN for pathloss calculation");
    return Double.NaN;
  }

  public class JE802ChannelEntry extends JEmula {

    private final List<JE802Phy> phyList;

    private final List<JE802MediumTxRecord> transmittingPhys;

    private final List<Integer> interferingChannels;

    private final int theChannelId;

    private final double thePathlossAt1m_dB;  // reference path loss at 1m distance

    private final double theNoiseLevel_mW;

    private final double theFrequency_MHz;

    public JE802ChannelEntry(final int channel, final double aFrequency_MHz, final double aNoiseLevel_mW, final double anAttenuationCoeff) {
      phyList = new ArrayList<JE802Phy>();
      this.theChannelId = channel;
      interferingChannels = new ArrayList<Integer>();
      transmittingPhys = new ArrayList<JE802MediumTxRecord>();

      for (JEWirelessChannel chan : theAvailableChannels.values()) {
        if (crossChannelInterference(chan.getChannelNumber(), channel) > 0.0
            && chan.getChannelNumber() != channel) {
          interferingChannels.add(chan.getChannelNumber());
        }
      }
      double lambda = 3E8 / (aFrequency_MHz * 1E6);
      this.thePathlossAt1m_dB = anAttenuationCoeff * 10 * Math.log10((lambda / 4 * Math.PI));
      this.theNoiseLevel_mW = aNoiseLevel_mW;
      this.theFrequency_MHz = aFrequency_MHz;
    }

    public double getNoiseLevel_mW() {
      return this.theNoiseLevel_mW;
    }

    public double getFrequency_MHz() {
      return this.theFrequency_MHz;
    }

    public void removePhy(JE802Phy toRemove) {
      phyList.remove(toRemove);
      for (JE802MediumTxRecord rec : transmittingPhys) {
        if (rec.getTxPhy().equals(toRemove)) {
          error("Switching Channel during packet transmission");
        }
      }
    }

    public List<JE802Phy> getPhyList() {
      return phyList;
    }

    public void addPhy(final JE802Phy phy) {
      phyList.add(phy);
      for (JE802MediumTxRecord rec : transmittingPhys) {
        double interference = calcInterference(phy, rec.getTxPhy(), this);
        rec.addRxStation(phy, interference);
      }

    }

    public JE802MediumTxRecord removeTransmittingPhy(final JE802Phy aTxPhy) {
      int size = transmittingPhys.size();
      int index = 0;
      for (int i = 0; i < size; i++) {
        JE802Phy aPhy = transmittingPhys.get(i).getTxPhy();
        if (aPhy.equals(aTxPhy)) {
          index = i;
        }
      }
      return transmittingPhys.remove(index);
    }

    public JE802MediumTxRecord addTransmittingPhy(final JE802Phy txPhy,
        final JETime txEnd) {
      JE802MediumTxRecord txRec = new JE802MediumTxRecord(txPhy, txEnd);
      Map<Integer, Double> neighbors = attenuationTable.get(txPhy
          .getMac().getMacAddress());
      Set<Integer> rxStations = new HashSet<Integer>();
      for (JE802Phy addr : phyList) {
        if (neighbors.containsKey(addr.getMac().getMacAddress())) {
          rxStations.add(addr.getMac().getMacAddress());
        }
      }
      txRec.addRxStations(rxStations);
      transmittingPhys.add(txRec);
      return txRec;
    }

    public double getPathlossAt1m_dB() {
      return this.thePathlossAt1m_dB;
    }

    public List<JE802MediumTxRecord> getTransmittingPhys() {
      return this.transmittingPhys;
    }

    public int getChannelId() {
      return this.theChannelId;
    }

    public List<Integer> getInterferingChannels() {
      return this.interferingChannels;
    }

    @Override
    public String toString() {
      return "Channel " + theChannelId + " #Phys" + phyList.size()
      + " #transmittingPhys " + transmittingPhys.size();
    }
  }

  private class JE802MediumTxRecord {

    private final JETime txEnd;

    private final JE802Phy txPhy;

    // current interference at receiverStation
    private final Map<Integer, Double> rxInterference_mW;

    // maximal interference at receiverStation
    private final Map<Integer, Double> rxMaxInterference_mW;

    public JE802MediumTxRecord(final JE802Phy aPhy, final JETime txEnd) {
      txPhy = aPhy;
      this.txEnd = txEnd;
      rxInterference_mW = new HashMap<Integer, Double>();
      rxMaxInterference_mW = new HashMap<Integer, Double>();
    }

    public void addRxStation(JE802Phy phy, double interference) {
      rxInterference_mW.put(phy.getMac().getMacAddress(), interference);
      rxMaxInterference_mW.put(phy.getMac().getMacAddress(), interference);
    }

    public void addRxStations(final Set<Integer> keySet) {
      for (Integer addr : keySet) {
        rxInterference_mW.put(addr, Double.valueOf(0.0));
        rxMaxInterference_mW.put(addr, Double.valueOf(0.0));
      }
    }

    public double getCurrentInterference(final Integer addr) {
      Double interference = rxInterference_mW.get(addr);
      if (interference != null) {
        return interference;
      } else {
        return 0.0;
      }
    }

    public double getMaxInterference(final Integer addr) {
      Double interference = rxMaxInterference_mW.get(addr);
      if (interference != null) {
        return interference;
      } else {
        return 0.0;
      }
    }

    // update the rx interferences induced by txPhy
    public void addInterference(final JE802Phy aTxPhy,
        final double crossChannelInterference) {
      if (aTxPhy != txPhy) {
        for (Integer rxAddr : rxInterference_mW.keySet()) {
          double attenuation = getPathloss(aTxPhy.getMac()
              .getMacAddress(), rxAddr);
          double interferenceAtRx = aTxPhy
              .getCurrentTransmitPower_mW()
              * attenuation
              * crossChannelInterference;
          Double existingInterference = rxInterference_mW.get(rxAddr);
          double newInterference = existingInterference
              + interferenceAtRx;
          rxInterference_mW.put(rxAddr, newInterference);
          Double maxInterference = rxMaxInterference_mW.get(rxAddr);
          if (maxInterference < newInterference) {
            rxMaxInterference_mW.put(rxAddr, newInterference);
          }
        }
      }
    }

    public void addRxInterference(final JE802Phy aRxPhy,
        final double interferencemW) {
      Integer addr = aRxPhy.getMac().getMacAddress();
      Double interference = rxInterference_mW.get(addr);
      Double newInterference = interference + interferencemW;
      rxInterference_mW.put(addr, newInterference);
      Double maxInterference = rxMaxInterference_mW.get(addr);
      if (maxInterference < newInterference) {
        rxMaxInterference_mW.put(addr, newInterference);
      }
    }

    public void decreaseInterference(final JE802Phy aTxPhy,
        final double crossChannelInterference) {
      if (aTxPhy != txPhy) {
        for (Integer rxAddr : rxInterference_mW.keySet()) {
          double attenuation = getPathloss(aTxPhy.getMac()
              .getMacAddress(), rxAddr);
          double interferenceAtRx = aTxPhy
              .getCurrentTransmitPower_mW()
              * attenuation
              * crossChannelInterference;
          Double existingInterference = rxInterference_mW.get(rxAddr);
          rxInterference_mW.put(rxAddr, existingInterference
              - interferenceAtRx);
        }
      }
    }

    public Set<Integer> getRxStations() {
      return rxInterference_mW.keySet();
    }

    public JE802Phy getTxPhy() {
      return txPhy;
    }

    @Override
    public String toString() {
      return "TxRecord: " + txPhy + " txEnd: " + txEnd;
    }
  }

  public Map<Integer, JE802ChannelEntry> getChannels() {
    return this.theChannels;
  }

  public double getCoverageRange_m() {
    return this.theCoverageRange_m;
  }

  public double getAttenuationCoeff() {
    return this.theAttenuationCoeff;
  }

  public double getNoisePlusInterference_mW(JE802Phy aPhy) {
    double neighborChannelInterference = 0.0;
    JE802ChannelEntry channel = theChannels.get(aPhy.getCurrentChannelId());
    if (channel == null) {
      this.error("Channel " + aPhy.getCurrentChannelId() + " not defined in XML");
      return 0.0;
    } else {
      for (Integer interferingChannel : channel.getInterferingChannels()) {
        JE802ChannelEntry entry = theChannels.get(interferingChannel);
        List<JE802MediumTxRecord> neighborTransmissions = entry.getTransmittingPhys();
        for (JE802MediumTxRecord rec : neighborTransmissions) {
          JE802Phy txPhy = rec.getTxPhy();
          double attenuation = getPathloss(aPhy.getMac().getMacAddress(), txPhy.getMac().getMacAddress());
          double interference = txPhy.getCurrentTransmitPower_mW() * attenuation;
          neighborChannelInterference += interference;
        }
      }
      List<JE802MediumTxRecord> ownTransmssions = channel.getTransmittingPhys();
      double ownChannelInterference = 0.0;
      for (JE802MediumTxRecord rec : ownTransmssions) {
        ownChannelInterference += rec.getCurrentInterference(aPhy.getMac().getMacAddress());
        double attenuation = getPathloss(rec.getTxPhy().getMac().getMacAddress(), aPhy.getMac().getMacAddress());
        double rxPower = rec.getTxPhy().getCurrentTransmitPower_mW() * attenuation;
        ownChannelInterference += rxPower;
      }
      return neighborChannelInterference + ownChannelInterference + channel.getNoiseLevel_mW();
    }
  }

  public Map<Integer, JEWirelessChannel> getAvailableChannels() {
    return this.theAvailableChannels;
  }
}
