/*
 * 
 * This is Jemula.
 *
 *    Copyright (c) 2009 Stefan Mangold, Fabian Dreier, Stefan Schmid
 *    All rights reserved. Urheberrechtlich geschuetzt.
 * 
 *    Redistribution and use in source and binary forms, with or without modification,
 *    are permitted provided that the following conditions are met:
 * 
 *      Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * 
 *      Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution.
 * 
 *      Neither the name of any affiliation of Stefan Mangold nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 * 
 *    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *    IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 *    OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 *    OF SUCH DAMAGE.
 * 
 */

package visualization;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import kernel.JETime;
import layer0_medium.JEWirelessMedium;


import org.w3c.dom.Document;
import org.w3c.dom.Element;

import station.JE802Station;
import util.Vector3d;

public class JE802KmlGeneratorHeatMap extends JE802KmlGenerator {

  private final double maxTxdBm;

  private final double minTxdBm;

  private final int alpha = 255;

  private final String pathInKmz;

  private final String powerFileName = "power.png";

  private final String channelFileName = "channels.png";

  private final String addressFileName = "addresses.png";

  private final double thePixelLength_m; 	// length (width) of a pixel in the images in meters

  private final JEWirelessMedium theUniqueWirelessMedium;

  private String resultPath;

  public JE802KmlGeneratorHeatMap(final Document doc, final List<JE802Station> stations, final String filename, final double aPixelLength_m, final double maxTxdBm, final double minTxdBm, final JEWirelessMedium aMedium, String path) {
    super(doc, stations);
    this.thePixelLength_m = aPixelLength_m;
    this.pathInKmz = "kml/";
    this.minTxdBm = minTxdBm;
    this.maxTxdBm = maxTxdBm;
    this.theUniqueWirelessMedium = aMedium;
    this.resultPath = path;
  }

  @Override
  public Element createDOM() {
    createGroundHeatmaps();

    Element power = createCoverage(this.powerFileName);
    Element channels = createCoverage(this.channelFileName);
    Element address = createCoverage(this.addressFileName);

    List<Element> aListOfPowerHeatmaps = new ArrayList<Element>();
    aListOfPowerHeatmaps.add(power);    
    List<Element> aListOfChannelHeatmaps = new ArrayList<Element>();
    aListOfChannelHeatmaps.add(channels);
    List<Element> aListOfAddressHeatmaps = new ArrayList<Element>();
    aListOfAddressHeatmaps.add(address);

    Element powerFolder = this.createFolder(aListOfPowerHeatmaps, "Radio Power", true,true);
    Element channelFolder = this.createFolder(aListOfChannelHeatmaps, "Freq Channels", true,false);
    Element addressFolder = this.createFolder(aListOfAddressHeatmaps, "MAC Addresses", true,false);

    List<Element> powerHeatmaps = new ArrayList<Element>();
    powerHeatmaps.add(powerFolder);
    powerHeatmaps.add(channelFolder);
    powerHeatmaps.add(addressFolder);
    Element aHeatmapFolder = this.createFolder(powerHeatmaps, "Ground Heatmaps (three options)", false,true);

    return aHeatmapFolder;
  }

  /**
   * Creates images for power, channels and MAC addresses This function does
   * all in one because its faster than calculating the power level of a point
   * for each image separately.
   */
  private void createGroundHeatmaps() {
    double bounds[] = getBounds();
    double latMax = bounds[0];
    double latMin = bounds[1];
    double longMax = bounds[2];
    double longMin = bounds[3];

    double latSquareAngle = meters2DegreesLatitude(this.thePixelLength_m);
    double longSquareAngle = meters2DegreesLongitude(this.thePixelLength_m, latMax);
    double latDifference = latMax - latMin;
    double longDifference = longMax - longMin;

    int latPieces = (int) Math.round(latDifference / latSquareAngle);
    int longPieces = (int) Math.round(longDifference / longSquareAngle);

    Color[][] powerPixels = new Color[longPieces][latPieces];
    Color[][] channelPixels = new Color[longPieces][latPieces];
    Color[][] addressPixels = new Color[longPieces][latPieces];
    int maxAddress = maxMacAddress();

    for (int i = latPieces - 1; i >= 0; i--) {
      Double northEdge = latMax - i * latSquareAngle;
      Double southEdge = latMax - (i + 1) * latSquareAngle;
      for (int j = 0; j < longPieces; j++) {
        Double eastEdge = longMax - j * longSquareAngle;
        Double westEdge = longMax - (j + 1) * longSquareAngle;
        double centerLat = (northEdge + southEdge) / 2.0;
        double centerLong = (westEdge + eastEdge) / 2.0;

        Vector3d here = new Vector3d(1.0, 1.0, 1.0);
        here.setZ(this.earthRadius);
        here.setLatLong(centerLat, centerLong);

        double aMaxRxPower_mW = Double.NEGATIVE_INFINITY;
        JE802Station aDominantStation = null;
        double powerSum_mW = 0.0;

        for (int sta_id = 0; sta_id < this.theStations.size(); sta_id++) { // this is faster than list iterator
          JE802Station aStation = this.theStations.get(sta_id);

          double rxPowerFromStation_mW =  aStation.getPhy().getCurrentTransmitPower_mW() * this.theUniqueWirelessMedium.calcPathloss(aStation.getPhy(), here, aStation.getPhy().getCurrentChannel());
          powerSum_mW += rxPowerFromStation_mW;
          if (rxPowerFromStation_mW > aMaxRxPower_mW) {
            aMaxRxPower_mW = rxPowerFromStation_mW;
            aDominantStation = this.theStations.get(sta_id);
          }
        }
        Color powerColor;
        Color channelColor;
        Color addressColor;
        if (powerSum_mW > aDominantStation.getPhy().getCurrentChannel().getNoiseLevel_mW()) {
          // some heatmap color only if received signal power is ABOVE noise
          powerColor = this.calculatePowerColor(powerSum_mW, this.minTxdBm, this.maxTxdBm, this.alpha);
          channelColor = this.getChannelColor(aDominantStation.getPhy().getCurrentChannelId());
          addressColor = this.calculateAddressColor(aDominantStation.getMac().getMacAddress(), maxAddress);
        } else {
          // else, transparent if received signal power is equal or BELOW noise
          powerColor = new Color(255, true);
          channelColor = new Color(255, true);
          addressColor = new Color(255, true);
        }
        powerPixels[longPieces - j - 1][i] = powerColor;
        channelPixels[longPieces - j - 1][i] = channelColor;
        addressPixels[longPieces - j - 1][i] = addressColor;
      }
    }
    this.writeImage(powerPixels, this.powerFileName);
    this.writeImage(channelPixels, this.channelFileName);
    this.writeImage(addressPixels, this.addressFileName);
  }

  /**
   * Color gradient is red to yellow to green to cyan to blue to magenta, looks fancy
   * 
   * @param macAddress
   *            MACAddress of current station
   * @param maxAddress
   *            the highest MACaddress of all theStations
   * @return Color of current theStations MACAddress
   */
  private Color calculateAddressColor(final int macAddress, final int maxAddress) {
    double gradient = (double) macAddress / maxAddress;
    Double r = 0.0;
    Double g = 0.0;
    Double b = 0.0;
    if (gradient >= 0) {
      if (gradient < 0.2) {
        r = 255.0;
        g = gradient * 5.0 * 255.0;
      } else if (gradient < 0.4) {
        g = 255.0;
        r = 255.0 - (gradient - 0.2) * 5.0 * 255.0;
      } else if (gradient < 0.6) {
        g = 255.0;
        b = (gradient - 0.4) * 5.0 * 255.0;
      } else if (gradient < 0.8) {
        b = 255.0;
        g = 255.0 - (gradient - 0.6) * 5.0 * 255.0;
      } else if (gradient <= 1) {
        b = 255.0;
        r = (gradient - 0.8) * 5.0 * 255.0;
      }
    }
    return new Color(r.intValue(), g.intValue(), b.intValue(), this.alpha);
  }

  /**
   * Returns color according to currentLevel of power in dBm. Creates a color
   * gradient from blue over green to red (red is highest power)
   */
  private Color calculatePowerColor(final double current_mW, final double mindBm, final double maxdBm, int alphaValue) {
    // http://stackoverflow.com/questions/20792445/calculate-rgb-value-for-a-range-of-values-to-create-heat-map

    if (maxdBm <= mindBm) {
      this.error("maxdBm must be larger than mindBm in calculatePowerColor. Check values in XML scenario file!");
    }

    double current_dBm = 10 * Math.log10(current_mW);
    Double ratio = Double.valueOf(0.0);
    Double b = Double.valueOf(0.0);
    Double r = Double.valueOf(0.0);
    Double g = Double.valueOf(0.0);

    alphaValue = 150; // slightly transparent
    
    if (current_dBm > mindBm & current_dBm < maxdBm) {
      ratio = 2 * (current_dBm-mindBm) / (maxdBm - mindBm);      
      b = Double.valueOf(Math.round(Math.min(255, Math.max(0.0, 255*(1 - ratio)))));
      r = Double.valueOf(Math.round(Math.min(255, Math.max(0.0, 255*(ratio - 1)))));
      g = Double.valueOf(Math.round(Math.min(255, Math.max(0.0, 255 - b - r))));
    } else if (current_dBm <= mindBm) {
      // keep it all dark blue
      b=200.0;
 
    } else if (current_dBm >= maxdBm) {
      b = 0.0;
      r = 255.0;
      g = 0.0;
    }
    Color color = new Color(r.intValue(), g.intValue(), b.intValue(), alphaValue);
    return color;
  }


  /**
   * Get bounding box of all theStations +- theCoverageRadius_m
   * 
   * @return bounding coordinates for all theStations
   *         {latitudeMaximum,latitudeMinimum
   *         ,longitudeMaximum,longitudeMinimuim}
   */
  private double[] getBounds() {
    Double latMax = Double.NEGATIVE_INFINITY;
    Double latMin = Double.POSITIVE_INFINITY;
    Double longMax = Double.NEGATIVE_INFINITY;
    Double longMin = Double.POSITIVE_INFINITY;
    for (JE802Station station : this.theStations) {
      JETime startTime = station.getStatEval().getEvaluationStarttime();
      double[] position = convertXYZtoLatLonAlt(station.getXLocation(startTime), station.getYLocation(startTime),
          station.getZLocation(startTime));
      double latHigh = position[0] + meters2DegreesLatitude(this.theUniqueWirelessMedium.getCoverageRange_m());
      if (latHigh > latMax) {
        latMax = latHigh;
      }
      double latLow = position[0] - meters2DegreesLatitude(this.theUniqueWirelessMedium.getCoverageRange_m());
      if (latLow < latMin) {
        latMin = latLow;
      }
      double lonHigh = position[1] + meters2DegreesLongitude(this.theUniqueWirelessMedium.getCoverageRange_m(), position[0]);
      if (lonHigh > longMax) {
        longMax = lonHigh;
      }
      double lonLow = position[1] - meters2DegreesLongitude(this.theUniqueWirelessMedium.getCoverageRange_m(), position[0]);
      if (lonLow < longMin) {
        longMin = lonLow;
      }
    }
    double result[] = { latMax, latMin, longMax, longMin };
    return result;
  }

  /**
   * Gets maximum Mac adress of all theStations
   * 
   * @return maximum mac address over all theStations
   */
  private int maxMacAddress() {
    int maxAddress = Integer.MIN_VALUE;
    for (JE802Station station : this.theStations) {
      if (station.getMac().getMacAddress() > maxAddress) {
        maxAddress = station.getMac().getMacAddress();
      }
    }
    return maxAddress;
  }


  /**
   * Creates a KML Overlay element with the overlay image specified in
   * filename
   * 
   * @param fileName
   *            filename of the overlay image
   * @return KML Overlay element
   */
  private Element createCoverage(final String fileName) {
    double bounds[] = getBounds();
    Double latMax = bounds[0];
    Double latMin = bounds[1];
    Double longMax = bounds[2];
    Double longMin = bounds[3];
    Element north = this.theKmlFile.createElement("north");
    north.appendChild(this.theKmlFile.createTextNode(latMax.toString()));

    Element south = this.theKmlFile.createElement("south");
    south.appendChild(this.theKmlFile.createTextNode(latMin.toString()));

    Element east = this.theKmlFile.createElement("east");
    east.appendChild(this.theKmlFile.createTextNode(longMax.toString()));

    Element west = this.theKmlFile.createElement("west");
    west.appendChild(this.theKmlFile.createTextNode(longMin.toString()));

    Element box = this.theKmlFile.createElement("LatLonBox");
    box.appendChild(north);
    box.appendChild(south);
    box.appendChild(east);
    box.appendChild(west);

    Element icon = this.theKmlFile.createElement("Icon");
    Element href = this.theKmlFile.createElement("href");
    href.appendChild(this.theKmlFile.createTextNode(this.pathInKmz + fileName));
    icon.appendChild(href);
    Element overlay = this.theKmlFile.createElement("GroundOverlay");
    overlay.appendChild(box);
    overlay.appendChild(icon);
    return overlay;
  }

  /**
   * 
   * @param data
   *            Matrix of color values of the image
   * @param fileName
   *            filename of the image to be written
   */
  private void writeImage(final Color data[][], final String fileName) {
    File filesFolder = new File(this.resultPath+"/"+this.pathInKmz);
    if (!filesFolder.exists()) {
      filesFolder.mkdir();
    }
    int width = data.length;

    if (width > 0) {
      int height = data[0].length;
      BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
      WritableRaster wr = image.getRaster();
      for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++) {
          Color col = data[i][j];
          int pixel[] = { col.getRed(), col.getGreen(), col.getBlue(), col.getAlpha() };
          wr.setPixel(i, j, pixel);
        }
      }

      try {
        ImageIO.write(image, "PNG", new File(this.resultPath + "/" +this.pathInKmz + fileName));
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * 
   * @param channel
   *            number of the wireless channel
   * @return Color of the according channel
   */
  private Color getChannelColor(final int channel) {
    switch (channel) {
      case 1:
        return Color.GREEN;
      case 2:
        return Color.BLACK;
      case 3:
        return Color.CYAN;
      case 4:
        return Color.BLUE;
      case 5:
        return Color.YELLOW;
      case 6:
        float[] hsb = Color.RGBtoHSB(200, 240, 180, null);
        return Color.getHSBColor(hsb[0], hsb[1], hsb[2]);
      case 7:
        return new Color(0, 255, 255);
      case 8:
        return new Color(128, 0, 0);
      case 9:
        return new Color(0, 128, 0);
      case 10:
        return new Color(0, 0, 128);
      case 11:
        return Color.RED;
      case 12:
        return new Color(128, 0, 128);
      default:
        break;
    }
    return new Color(255, 255, 255);
  }
}