/*
 * 
 * This is Jemula.
 *
 *    Copyright (c) 2009 Stefan Mangold, Fabian Dreier, Stefan Schmid
 *    All rights reserved. Urheberrechtlich geschuetzt.
 *    
 *    Redistribution and use in source and binary forms, with or without modification,
 *    are permitted provided that the following conditions are met:
 *    
 *      Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer. 
 *    
 *      Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution. 
 *    
 *      Neither the name of any affiliation of Stefan Mangold nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission. 
 *    
 *    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *    IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 *    OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 *    OF SUCH DAMAGE.
 *    
 */

package visualization;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import kernel.JETime;
import layer0_medium.JEWirelessMedium;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import station.JE802Station;

public class JE802KmlGeneratorBackground extends JE802KmlGenerator {

	private final String theFilename;

	private final double theAreaSize;

	private final String theEntryName;
	
	private final JEWirelessMedium theUniqueWirelessMedium;

  private boolean isVisible;

	public JE802KmlGeneratorBackground(Document doc, List<JE802Station> stations, String aFilename, String anEntryName, double aSize, JEWirelessMedium aMedium, final boolean isVisible) {
		super(doc, stations);
		this.theFilename = aFilename;
		this.theEntryName = anEntryName;
		this.theAreaSize = aSize;
		this.theUniqueWirelessMedium = aMedium;
		this.isVisible = isVisible;
	}

	protected double[] getBounds() {
		Double latMax = Double.NEGATIVE_INFINITY;
		Double latMin = Double.POSITIVE_INFINITY;
		Double longMax = Double.NEGATIVE_INFINITY;
		Double longMin = Double.POSITIVE_INFINITY;
		for (JE802Station station : this.theStations) {
			double[] position = convertXYZtoLatLonAlt(station.getXLocation(new JETime()), station.getYLocation(new JETime()), station.getZLocation(new JETime()));
			double latHigh = position[0] + meters2DegreesLatitude(this.theUniqueWirelessMedium.getCoverageRange_m());
			if (latHigh > latMax) {
				latMax = latHigh;
			}
			double latLow = position[0] - meters2DegreesLatitude(this.theUniqueWirelessMedium.getCoverageRange_m());
			if (latLow < latMin) {
				latMin = latLow;
			}
			double lonHigh = position[1] + meters2DegreesLongitude(this.theUniqueWirelessMedium.getCoverageRange_m(), position[0]);
			if (lonHigh > longMax) {
				longMax = lonHigh;
			}
			double lonLow = position[1] - meters2DegreesLongitude(this.theUniqueWirelessMedium.getCoverageRange_m(), position[0]);
			if (lonLow < longMin) {
				longMin = lonLow;
			}
		}
		double result[] = { latMax, latMin, longMax, longMin };
		return result;
	}

	protected Element createBackground() {
		double bounds[] = getBounds();
		Double latMax = bounds[0];
		Double latMin = bounds[1];
		Double longMax = bounds[2];
		Double longMin = bounds[3];
		return createBackground(latMax, latMin, longMax, longMin);
	}

	private Element createBackground(Double latMax, Double latMin, Double longMax, Double longMin) {
		Element north = theKmlFile.createElement("north");
		north.appendChild(theKmlFile.createTextNode(latMax.toString()));

		Element south = theKmlFile.createElement("south");
		south.appendChild(theKmlFile.createTextNode(latMin.toString()));

		Element east = theKmlFile.createElement("east");
		east.appendChild(theKmlFile.createTextNode(longMax.toString()));

		Element west = theKmlFile.createElement("west");
		west.appendChild(theKmlFile.createTextNode(longMin.toString()));

		Element box = theKmlFile.createElement("LatLonBox");
		box.appendChild(north);
		box.appendChild(south);
		box.appendChild(east);
		box.appendChild(west);

		Element icon = theKmlFile.createElement("Icon");
		Element href = theKmlFile.createElement("href");
		href.appendChild(theKmlFile.createTextNode(theFilename));
		icon.appendChild(href);
		Element background = theKmlFile.createElement("GroundOverlay");
		background.appendChild(box);
		background.appendChild(icon);
		return background;
	}

	protected void writeImage(Color data[][]) {
		int width = data.length;
		int height = data[0].length;
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		WritableRaster wr = image.getRaster();
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				Color col = data[i][j];
				int pixel[] = { col.getRed(), col.getGreen(), col.getBlue(), col.getAlpha() };
				wr.setPixel(i, j, pixel);
			}
		}

		try {
			ImageIO.write(image, "PNG", new File(theFilename));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Element createDOM() {
		double[] bounds = getBounds();
		Double latMax = bounds[0] + meters2DegreesLatitude(theAreaSize / 2);
		Double latMin = bounds[1] - meters2DegreesLatitude(theAreaSize / 2);
		Double longMax = bounds[2] + meters2DegreesLongitude(theAreaSize / 2, bounds[0]);
		Double longMin = bounds[3] - meters2DegreesLongitude(theAreaSize / 2, bounds[0]);
		Element background = createBackground(latMax, latMin, longMax, longMin);
	    List<Element> aListOfBackgrounds = new ArrayList<Element>();
	    aListOfBackgrounds.add(background);
		Element backgroundFolder = createFolder(aListOfBackgrounds, theEntryName, true, isVisible);
		return backgroundFolder;
	}
}
