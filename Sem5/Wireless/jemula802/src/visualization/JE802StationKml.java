/*
 * 
 * This is Jemula.
 *
 *    Copyright (c) 2009 Stefan Mangold, Fabian Dreier, Stefan Schmid
 *    All rights reserved. Urheberrechtlich geschuetzt.
 * 
 *    Redistribution and use in source and binary forms, with or without modification,
 *    are permitted provided that the following conditions are met:
 * 
 *      Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 * 
 *      Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution.
 * 
 *      Neither the name of any affiliation of Stefan Mangold nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 * 
 *    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *    IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 *    OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 *    OF SUCH DAMAGE.
 * 
 */

package visualization;

import java.util.ArrayList;
import java.util.List;

import kernel.JETime;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import station.JE802Station;
import util.Vector3d;

public class JE802StationKml extends JE802KmlGenerator {

  private final String antennaModelName = "Antenna.dae";

  private final String smallAntennaModelName = "AntennaSmall.dae";

  private final String userModelName = "user.dae";

  public JE802StationKml(final Document doc, final List<JE802Station> stations, final String filename) {
    super(doc, stations);
  }

  /**
   * creates KML model element for a station. Type of model depends on the
   * fact whether a station is mobile or not
   * 
   * @return KML model element of a station
   */
  private List<Element> createStationModels() {
    ArrayList<Element> models = new ArrayList<Element>();
    for (JE802Station station : this.theStations) {
      int positionCount;
      JETime currentTime = station.getStatEval().getEvaluationStarttime();
      JETime interval = station.getStatEval().getEvaluationInterval();
      if (station.isMobile()) {
        double end = station.getStatEval().getEvaluationEnd().getTimeMs();
        positionCount = (int) ((end - currentTime.getTimeMs()) / interval.getTimeMs());
      } else {
        positionCount = 1;
      }
      Double headingRotation = 0.0;
      for (int i = 0; i < positionCount; i++) {
        Vector3d currentPosition = new Vector3d(station.getXLocation(currentTime), station.getYLocation(currentTime), station.getZLocation(currentTime));
        headingRotation = -station.getMobility().getTraceHeading(currentTime);
        // Location
        Element altitudeMode = this.theKmlFile.createElement("altitudeMode");
        altitudeMode.appendChild(this.theKmlFile.createTextNode("relativeToGround"));

        Element location = this.theKmlFile.createElement("Location");
        Element longitude = this.theKmlFile.createElement("longitude");
        longitude.appendChild(this.theKmlFile.createTextNode(Double.toString(currentPosition.getLon())));
        Element latitude = this.theKmlFile.createElement("latitude");
        latitude.appendChild(this.theKmlFile.createTextNode(Double.toString(currentPosition.getLat())));
        Element altitude = this.theKmlFile.createElement("altitude");
        if (station.isMobile()) {
          altitude.appendChild(this.theKmlFile.createTextNode(Double.toString(0)));
        } else {
          altitude.appendChild(this.theKmlFile.createTextNode(Double.toString(currentPosition.getAlt())));
        }

        location.appendChild(longitude);
        location.appendChild(latitude);
        location.appendChild(altitude);

        // scale
        boolean isMobile = station.isMobile();
        Element scale = this.theKmlFile.createElement("Scale");
        Element x = this.theKmlFile.createElement("x");
        String scaleFactor = null;
        if (isMobile) {
          scaleFactor = "1";
        } else {
          scaleFactor = ".4";
        }

        x.appendChild(this.theKmlFile.createTextNode(scaleFactor));
        Element y = this.theKmlFile.createElement("y");
        y.appendChild(this.theKmlFile.createTextNode(scaleFactor));
        Element z = this.theKmlFile.createElement("z");
        z.appendChild(this.theKmlFile.createTextNode(scaleFactor));
        scale.appendChild(x);
        scale.appendChild(y);
        scale.appendChild(z);

        // orientation
        Element orientation = this.theKmlFile.createElement("Orientation");
        Element heading = this.theKmlFile.createElement("heading");
        heading.appendChild(this.theKmlFile.createTextNode(headingRotation.toString()));
        Element tilt = this.theKmlFile.createElement("tilt");
        tilt.appendChild(this.theKmlFile.createTextNode("0"));
        Element roll = this.theKmlFile.createElement("roll");
        roll.appendChild(this.theKmlFile.createTextNode("0"));
        orientation.appendChild(heading);
        orientation.appendChild(tilt);
        orientation.appendChild(roll);

        // modelfile
        Element link = this.theKmlFile.createElement("Link");
        Element href = this.theKmlFile.createElement("href");

        String path = "kml/";
        if (!isMobile) {
          if (currentPosition.getAlt() > 0.5) {
            path = path + this.smallAntennaModelName;
          } else {
            path = path + this.antennaModelName;
          }
        } else {
          path = path + this.userModelName;
        }
        href.appendChild(this.theKmlFile.createTextNode(path));
        link.appendChild(href);

        // assemble model
        Element model = this.theKmlFile.createElement("Model");
        model.appendChild(altitudeMode);
        model.appendChild(location);
        model.appendChild(scale);
        model.appendChild(orientation);
        model.appendChild(link);

        // placemark wrapping the model tag
        Element placemark = this.theKmlFile.createElement("Placemark");
        Element name = this.theKmlFile.createElement("name");
        Element timeSpan;
        if (station.isMobile()) {
          timeSpan = createTimeSpan(currentTime, currentTime.plus(interval));
        } else {
          timeSpan = createTimeSpan(station.getStatEval().getEvaluationStarttime(), station.getStatEval()
              .getEvaluationEnd());
        }
        currentTime = currentTime.plus(interval);
        name.appendChild(this.theKmlFile.createTextNode(Integer.valueOf(station.getMac().getMacAddress()).toString()));
        placemark.appendChild(name);
        placemark.appendChild(model);
        placemark.appendChild(timeSpan);
        models.add(placemark);
      }
    }
    return models;
  }

  @Override
  public Element createDOM() {
    List<Element> models = createStationModels();
    String folderName = "Stations";
    Element modelFolder = createFolder(models, folderName, true,true);
    return modelFolder;
  }
}
