import random
import numpy as np
import matplotlib.pyplot as plt

# generate 5 lists
l1 = [ random.uniform(-1, 1) * 19918192 / 10000 for _ in range(1000) ]
l2 = [ random.uniform(-1, 1) * 19918192 / 10000 for _ in range(1000) ]
l3 = [ random.uniform(-1, 1) * 19918192 / 10000 for _ in range(1000) ]
l4 = [ random.uniform(-1, 1) * 19918192 / 10000 for _ in range(1000) ]
l5 = [ random.uniform(-1, 1) * 19918192 / 10000 for _ in range(1000) ]

# subtask 1
# noBins = 20
# n, bins, patches = plt.hist(l1, bins=20, weights=np.ones_like(l1) / len(l1),
#                             facecolor='blue', alpha=0.75)
# plt.xticks(ticks=[int(min(l1) + i * (max(l1) - min(l1)) / noBins) for i in range(noBins+1)], rotation='vertical')
# plt.subplots_adjust(bottom=0.25)
# plt.xlabel("Value Ranges of Random Variable X")
# plt.ylabel("P[X is within bar range]")
# plt.title("Probability Distribution of X")
# plt.show()

# subtask 2
# noBins = 20
# n, bins, patches = plt.hist(l1, bins=noBins, weights=np.ones_like(l1) / len(l1),
#                             facecolor='blue', alpha=0.75, cumulative=True)
# plt.xticks(ticks=[int(min(l1) + i * (max(l1) - min(l1)) / noBins) for i in range(noBins+1)], rotation='vertical')
# plt.subplots_adjust(bottom=0.25)
# plt.xlabel("Value Ranges of Random Variable Y")
# plt.ylabel("P[Y is smaller or equal to larger bar edge]")
# plt.title("Cumulative Distribution of Y")
# plt.show()

# subtask 3
# noBins = 20
# l6 = [ 1 for _ in range(1000) ]
# for l in [l1, l2, l3, l4, l5]:
#     for i in range(1000):
#         l6[i] *= l[i]

# n, bins, patches = plt.hist(l6, bins=20, weights=np.ones_like(l6) / len(l6),
#                             facecolor='blue', alpha=0.75)
# plt.xticks(ticks=[int(min(l6) + i * (max(l6) - min(l6)) / noBins) for i in range(noBins+1)], rotation='vertical')                         
# plt.subplots_adjust(bottom=0.25)
# plt.xlabel("Value Ranges of Random Variable MULT")
# plt.ylabel("P[MULT is within bar range]")
# plt.title("Probability Distribution of MULT")
# plt.show()

# subtask 3 (continued)
noBins = 20
l6 = [ 1 for _ in range(1000) ]
for l in [[ random.uniform(-1, 1) * 19918192 / 10000 for _ in range(1000) ] for _ in range(100)]:
    for i in range(1000):
        l6[i] *= l[i]

n, bins, patches = plt.hist(l6, bins=20, weights=np.ones_like(l6) / len(l6),
                            facecolor='blue', alpha=0.75)
# plt.xticks(ticks=[int(min(l6) + i * (max(l6) - min(l6)) / noBins) for i in range(noBins+1)], rotation='vertical')                         
plt.subplots_adjust(bottom=0.25)
plt.xlabel("Value Ranges of Random Variable MULT")
plt.ylabel("P[MULT is within bar range]")
plt.title("Probability Distribution of MULT")
plt.show()