package statistics;

import com.ginsberg.junit.exit.ExpectSystemExitWithStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

class JERandomVarTest {

    private Random random;
    @BeforeEach
    void setUp() {
        random = mock(Random.class);
        when(random.nextDouble()).thenReturn(0.34).thenReturn(0.50).thenReturn(0.0);
    }

    @Test
    @ExpectSystemExitWithStatus(0)
    void JERandomVar1_unknownDistribution__calls_system_exit() {
        new JERandomVar(random, "unknown", 0, 5);
    }
    @Test
    @ExpectSystemExitWithStatus(0)
    void JERandomVar2_unknownDistribution__calls_system_exit() {
        new JERandomVar("unknown", 5);
    }

    @Test
    void nextvalueExp() {
        JERandomVar negExp = new JERandomVar(random, "NegExp", 0, 5);
        assertEquals(2.0775772198083295,negExp.nextvalue());
        assertEquals(3.4657359027997265,negExp.nextvalue());
        assertEquals(0.0,negExp.nextvalue() + 0.0); // need to add 0.0 as -0.0 is return if random returns 0.0
        verify(random, times(3)).nextDouble();
    }

    @Test
    void nextvalueExp_negative_minValue() {
        JERandomVar negExp = new JERandomVar(random, "NegExp",-10, 5);
        assertEquals(2.0775772198083295,negExp.nextvalue());
        assertEquals(3.4657359027997265,negExp.nextvalue());
        assertEquals(0.0,negExp.nextvalue() + 0.0); // need to add 0.0 as -0.0 is return if random returns 0.0
        verify(random, times(3)).nextDouble();
    }

    @Test
    void nextvalueExp_negative_meanValue() {
        double mean_value = -5.0;
        JERandomVar negExp = new JERandomVar(random, "NegExp",0, mean_value);
        assertEquals(1000*mean_value,negExp.nextvalue());
        assertEquals(1000*mean_value,negExp.nextvalue());
        assertEquals(1000*mean_value,negExp.nextvalue());
        verify(random, times(3)).nextDouble();
    }

    @Test
    void nextvalueExp_minvalue_and_meanValue_zero() {
        JERandomVar negExp = new JERandomVar(random, "NegExp",0, 0);
        assertEquals(0.0,negExp.nextvalue());
        assertEquals(0.0,negExp.nextvalue());
        assertEquals(0.0,negExp.nextvalue() + 0.0); // need to add 0.0 as -0.0 is return if random returns 0.0
        verify(random, times(3)).nextDouble();
    }


    @Test
    void nextvalueUnifrom() {
        JERandomVar uniform = new JERandomVar(random, "Uniform", 0, 5);
        assertEquals(3.4,uniform.nextvalue(),0.00001); // delta to assert up to certain accuracy
        assertEquals(5.0,uniform.nextvalue());
        assertEquals(0.0,uniform.nextvalue());
        verify(random, times(3)).nextDouble();
    }

    @Test
    void nextvalueUnifrom_negative_minValue() {
        JERandomVar uniform = new JERandomVar(random, "Uniform", -10, 5);
        assertEquals(-3.1999999999,uniform.nextvalue(),0.00001); // delta to assert up to certain accuracy
        assertEquals(0.0,uniform.nextvalue());
        assertEquals(-10.0,uniform.nextvalue());
        verify(random, times(3)).nextDouble();
    }

    @Test
    void nextvalueUnifrom_negative_meanValue() {
        JERandomVar uniform = new JERandomVar(random, "Uniform", 0, -5);
        assertEquals(-3.4,uniform.nextvalue(),0.00001); // delta to assert up to certain accuracy
        assertEquals(-5.0,uniform.nextvalue());
        assertEquals(0.0,uniform.nextvalue());
        verify(random, times(3)).nextDouble();
    }

    @Test
    void nextvalueUnifrom_minValue_and_meanValue_zero() {
        JERandomVar uniform = new JERandomVar(random, "Uniform", 0, 0);
        assertEquals(0,uniform.nextvalue(),0.00001); // delta to assert up to certain accuracy
        assertEquals(0,uniform.nextvalue());
        assertEquals(0,uniform.nextvalue());
        verify(random, times(3)).nextDouble();
    }

    @Test
    void nextvalueUniformInt() {
        JERandomVar uniformInt = new JERandomVar(random, "UniformInt", 0, 5);
        assertEquals(3,uniformInt.nextvalue());
        assertEquals(5,uniformInt.nextvalue());
        assertEquals(0,uniformInt.nextvalue());
        verify(random, times(3)).nextDouble();
    }

    @Test
    void nextvalueUniformInt_negative_minValue() {
        JERandomVar uniformInt = new JERandomVar(random, "UniformInt", -10, 5);
        assertEquals(-3,uniformInt.nextvalue());
        assertEquals(0,uniformInt.nextvalue());
        assertEquals(-10,uniformInt.nextvalue());
        verify(random, times(3)).nextDouble();
    }

    @Test
    void nextvalueUniformInt_negative_meanValue() {
        JERandomVar uniformInt = new JERandomVar(random, "UniformInt", 0, -5);
        assertEquals(-3,uniformInt.nextvalue());
        assertEquals(-5,uniformInt.nextvalue());
        assertEquals(0,uniformInt.nextvalue());
        verify(random, times(3)).nextDouble();
    }

    @Test
    void nextvalueUniformInt_minValue_and_meanValue_zero() {
        JERandomVar uniformInt = new JERandomVar(random, "UniformInt", 0, 0);
        assertEquals(0,uniformInt.nextvalue());
        assertEquals(0,uniformInt.nextvalue());
        assertEquals(0,uniformInt.nextvalue());
        verify(random, times(3)).nextDouble();
    }

    @Test
    void nextvalueCBR() {
        JERandomVar cbr = new JERandomVar("cbr", 5);
        assertEquals(5,cbr.nextvalue());
        assertEquals(5,cbr.nextvalue());
        assertEquals(5,cbr.nextvalue());
        verify(random, times(0)).nextDouble();
    }

}