package statistics;

import com.ginsberg.junit.exit.ExpectSystemExitWithStatus;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junitpioneer.jupiter.TempDirectory;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(TempDirectory.class)
class JEStatsOutputTest {

    private JEStatsOutput output_m;
    private JEStatsOutput output_csv;
    private JEStatsOutput output_txt;
    private Path tempDir;


    @BeforeEach
    void setUp(@TempDirectory.TempDir Path tempDir) {
        this.tempDir = tempDir;
        output_m = new JEStatsOutput(tempDir.resolve("test.m"), "header");
        output_csv = new JEStatsOutput(tempDir.resolve("test.csv"), "header");
        output_txt = new JEStatsOutput(tempDir.resolve("test.txt"), "header");
    }

    @Test
    @ExpectSystemExitWithStatus(0)
    void unsupportedFileType() {
        Path file = this.tempDir.resolve("test.html");
        new JEStatsOutput(file,"header");
    }

    @Test
    void supportedFileType_uppercase() throws Exception {
        Path file = this.tempDir.resolve("test.CSV");
        JEStatsOutput output =  new JEStatsOutput(file, "header");
        assertEquals("csv", output.getOutputFormat());
    }

    @Test
    void write_empty_string_m() throws Exception {
        write_empty_string(output_m);
    }

    @Test
    void write_empty_string_csv() throws Exception {
       write_empty_string(output_csv);
    }

    @Test
    void write_empty_string_txt() throws Exception {
        write_empty_string(output_txt);
    }

    void write_empty_string(JEStatsOutput out) throws Exception {
        out.write("");
        String s = FileUtils.readFileToString(out.getFile().toFile(), Charset.defaultCharset());
        assertEquals("header\n\n",s);
    }

    @Test
    void write_List_with_empty_string_m() throws Exception {
        write_List_with_empty_string(output_m);
    }

    @Test
    void write_List_with_empty_string_csv() throws Exception {
        write_List_with_empty_string(output_csv);
    }

    @Test
    void write_List_with_empty_string_txt() throws Exception {
        write_List_with_empty_string(output_txt);
    }

    void write_List_with_empty_string(JEStatsOutput out) throws Exception {
        List<String> output = new ArrayList<>();
        output.add("");
        out.write(output);
        String s = FileUtils.readFileToString(out.getFile().toFile(), Charset.defaultCharset());
        assertEquals("header\n\n",s);
    }

    @Test
    void write_List_with_multiple_empty_strings_m() throws Exception {
        write_List_with_multiple_empty_strings(output_m,"header\n\t\t\n");
    }

    @Test
    void write_List_with_multiple_empty_strings_csv() throws Exception {
        write_List_with_multiple_empty_strings(output_csv,"header\n,,\n");
    }

    @Test
    void write_List_with_multiple_empty_strings_txt() throws Exception {
        write_List_with_multiple_empty_strings(output_txt, "header\n  \n");
    }

    void write_List_with_multiple_empty_strings(JEStatsOutput out, String expected) throws Exception {
        List<String> output = new ArrayList<>();
        output.add("");
        output.add("");
        output.add("");
        out.write(output);
        String s = FileUtils.readFileToString(out.getFile().toFile(), Charset.defaultCharset());
        assertEquals(expected,s);
    }

    @Test
    void getHeader() {
        assertEquals("header",output_csv.getHeader());
    }

    @Test
    void getFormat_m() {
        assertEquals("m", output_m.getOutputFormat());
    }

    @Test
    void getFormat_csv() {
        assertEquals("csv", output_csv.getOutputFormat());
    }

    @Test
    void getFormat_txt() {
        assertEquals("txt", output_txt.getOutputFormat());
    }

    @Test
    void getSupportedExtensions() {
        String[] extensions = {"m", "csv", "txt"};
        assertArrayEquals(extensions, JEStatsOutput.getSupportedExtensions());
    }

    @Test
    void isSupportedExtension_returns_false() {
        assertFalse(JEStatsOutput.isSupportedExtension("html"));
    }

    @Test
    void isSupportedExtension_returns_true() {
        assertTrue(JEStatsOutput.isSupportedExtension("m"));
        assertTrue(JEStatsOutput.isSupportedExtension("csv"));
        assertTrue(JEStatsOutput.isSupportedExtension("txt"));
    }

}