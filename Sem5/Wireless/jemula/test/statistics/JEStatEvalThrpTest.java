package statistics;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junitpioneer.jupiter.TempDirectory;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(TempDirectory.class)
class JEStatEvalThrpTest {

    private JEStatEvalThrp eval;
    private String path;
    private String fileName;

    @BeforeEach
    void setup(@TempDirectory.TempDir Path tempDir) {
        this.path = tempDir.toString();
        this.fileName = "test";

        eval = new JEStatEvalThrp(path, fileName);
    }

    @Test
    void parent_folder_not_exists(@TempDirectory.TempDir Path tempDir) {
        assertTrue(Files.exists(tempDir));
        Path path = Paths.get(tempDir.toString(), "childDir");
        assertTrue(path.toString().startsWith(tempDir.toString()));
        assertTrue(Files.exists(tempDir));
        assertFalse(Files.exists(path));

        eval = new JEStatEvalThrp(path.toString(),"test.txt");
        assertTrue(Files.exists(path));

    }

    @Test
    void sampleNoDuplicate_noDuplicate() {
        boolean sampled = eval.sampleNoDuplicate(0.0, 1, 2,3);
        assertTrue(sampled);
    }

    @Test
    void sampleNoDuplicate_duplicate() {
        eval.sampleNoDuplicate(0.0, 1, 2,3);
        boolean sampled = eval.sampleNoDuplicate(0.0, 1, 2,3);
        assertFalse(sampled);
    }

    @Test
    void evaluation_m() throws Exception {
        Path file = Paths.get(this.path, this.fileName+".m");
        String formatSpecific="0.0\t6\t6\t3.0\t18.0\tInfinity\tInfinity";
        evaluation(file, formatSpecific);
    }

    @Test
    void evaluation_csv() throws Exception {
        Path file = Paths.get(this.path, this.fileName+".csv");
        String formatSpecific = "0.0,6,6,3.0,18.0,Infinity,Infinity";
        evaluation(file, formatSpecific);
    }

    void evaluation(Path file, String formatSpecific) throws Exception {
        doSomeSampling();
        eval.evaluation(0.0);
        String s = FileUtils.readFileToString(file.toFile(), Charset.defaultCharset());
        assertTrue(s.contains(formatSpecific));

    }

    private void doSomeSampling() {
        doSomeSampling(1000);
    }

    private void doSomeSampling(int howMany) {
        double[] time = {0.0,1.0,2.0,3.0,4.0,5.0};
        int[] value1 = {0,1,2,3,4,5};
        long[] value2 = {0,10,20,30,40,50};
        double[] value3 = {0.5,1.5,2.5,3.5,4.5,5.5};
        howMany = Math.max(0,Math.min(howMany, time.length));
        for(int i = 0; i < howMany; i++) {
            eval.sample(time[i], value1[i], value2[i], value3[i]);
        }
    }
}