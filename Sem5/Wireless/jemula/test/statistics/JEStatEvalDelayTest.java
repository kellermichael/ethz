package statistics;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junitpioneer.jupiter.TempDirectory;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(TempDirectory.class)
class JEStatEvalDelayTest {

    private JEStatEvalDelay eval;
    private String path;
    private String fileName;

    @BeforeEach
    void setUp(@TempDirectory.TempDir Path tempDir) {
        this.path = tempDir.toString();
        this.fileName = "test";

        eval = new JEStatEvalDelay(path, fileName, 100, 100);
    }

    Path prepare_temp_dir(Path tempDir) {
        assertTrue(Files.exists(tempDir));
        Path path = Paths.get(tempDir.toString(), "childDir");
        assertTrue(path.toString().startsWith(tempDir.toString()));
        assertTrue(Files.exists(tempDir));
        assertFalse(Files.exists(path));
        return path;
    }

    @Test
    void parent_folder_not_exists_using_other_constructors1(@TempDirectory.TempDir Path tempDir) {
        Path path = prepare_temp_dir(tempDir);

        eval = new JEStatEvalDelay(path.toString(), "test.txt");
        assertTrue(Files.exists(path));
    }

    @Test
    void parent_folder_not_exists_using_other_constructors2(@TempDirectory.TempDir Path tempDir) {
        Path path = prepare_temp_dir(tempDir);

        eval = new JEStatEvalDelay(path.toString(), "test.txt", 100,100);
        assertTrue(Files.exists(path));
    }

    @Test
    void super_outputFile_generated_test() throws Exception {
        Path file = Paths.get(this.path, this.fileName+".m");
        String s = FileUtils.readFileToString(file.toFile(), Charset.defaultCharset());
        assertTrue(s.contains(eval.toString()));
        assertTrue(s.contains("result_test = ["));
    }

    @Test
    void evaluation_m() throws Exception {
        Path file = Paths.get(this.path, this.fileName+".m");
        String formatSpecific = "0.0\t6\t6\t3.0\t5.5\t0.0\t0.0\t1.0\t1 1 1 1 1 1 ";
        evaluation(file, formatSpecific);
    }

    @Test
    void evaluation_csv() throws Exception {
        Path file = Paths.get(this.path, this.fileName+".csv");
        String formatSpecific = "0.0,6,6,3.0,5.5,0.0,0.0,1.0,1 1 1 1 1 1 ";
        evaluation(file, formatSpecific);
    }

    void evaluation(Path file, String formatSpecific) throws Exception {
//        Path file = Paths.get(this.path, this.fileName+".m");
        doSomeSampling();
        eval.evaluation(0.0);
        eval.evaluation(12345.6);
        String s = FileUtils.readFileToString(file.toFile(), Charset.defaultCharset());
        assertTrue(s.contains(formatSpecific));
        assertTrue(s.contains("12345.6"));
    }

    @Test
    void sample() {
        eval.sample(0.0, 1, 2,3);
//        assertEquals(1, eval.getSampleCount());
        assertFalse(eval.sampleMap.isEmpty());
        assertTrue(eval.sampleMap.containsKey((long)2));
        assertNotNull(eval.sampleMap.get((long)2));
    }

    @Test
    void sampleNoDuplicate_noDuplicate() {
        boolean sampled = eval.sampleNoDuplicate(0.0, 1, 2,3);
        assertTrue(sampled);
    }

    @Test
    void sampleNoDuplicate_duplicate() {
        eval.sampleNoDuplicate(0.0, 1, 2,3);
        boolean sampled = eval.sampleNoDuplicate(0.0, 1, 2,3);
        assertFalse(sampled);
    }

    @Test
    void end_of_emulation_test() throws Exception {
        Path file = Paths.get(this.path, this.fileName+".m");
        doSomeSampling();
        eval.evaluation(10.0);
        eval.end_of_emulation();
        String s = FileUtils.readFileToString(file.toFile(), Charset.defaultCharset());
        assertTrue(s.contains("]; % "));
    }

    @Test
    void reset_test() throws Exception {
        Path file = Paths.get(this.path, this.fileName+".m");
        String post_reset_evaluation_line = "2.0\t0\t0\tNaN\t0.0\tNaN\tNaN\t1.0\t0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0";
        doSomeSampling();
        assertFalse(eval.theSampleList1.isEmpty());
        assertFalse(eval.theSampleList2.isEmpty());
        assertFalse(eval.theSampleList3.isEmpty());
        eval.evaluation(0.0);

        assertEquals(1, eval.getEvalList1().size());
        assertEquals(0, eval.getEvalList2().size()); // apparently not used in EvalDelay
        assertEquals(1, eval.getEvalList3().size());
        assertEquals(1, eval.getEvalList4().size());
        assertEquals(1, eval.getEvalList5().size());
        assertEquals(1, eval.getEvalList6().size());
        assertEquals(1, eval.getEvalList7().size());
        assertEquals(1, eval.getEvalList8().size());
        eval.evaluation(1.0);

        String s = FileUtils.readFileToString(file.toFile(), Charset.defaultCharset());
//        assertEquals("", s);
        assertTrue(s.contains("0.0\t6\t6\t3.0\t5.5\t0.0\t0.0\t1.0\t1 1 1 1 1 1 0 0 0 "));
        assertFalse(s.contains(post_reset_evaluation_line));

        assertTrue(eval.theSum1 > 0);
        assertTrue(eval.theSum2 > 0);
        assertTrue(eval.theSum3 > 0);

        eval.reset();

        assertEquals(0, eval.getEvalList1().size());
        assertEquals(0, eval.getEvalList2().size()); // apparently not used in EvalDelay
        assertEquals(0, eval.getEvalList3().size());
        assertEquals(0, eval.getEvalList4().size());
        assertEquals(0, eval.getEvalList5().size());
        assertEquals(0, eval.getEvalList6().size());
        assertEquals(0, eval.getEvalList7().size());
        assertEquals(0, eval.getEvalList8().size());
        eval.evaluation(2.0);

        s = FileUtils.readFileToString(file.toFile(), Charset.defaultCharset());
        assertTrue(s.contains(post_reset_evaluation_line));

        assertTrue(eval.theSampleList1.isEmpty());
        assertTrue(eval.theSampleList2.isEmpty());
        assertTrue(eval.theSampleList3.isEmpty());
        assertEquals(0, eval.theSum1);
        assertEquals(0.0, eval.theSum2);
        assertEquals(0.0, eval.theSum3);
    }

    private void doSomeSampling() {
        doSomeSampling(1000);
    }

    private void doSomeSampling(int howMany) {
        double[] time = {0.0,1.0,2.0,3.0,4.0,5.0};
        int[] value1 = {0,1,2,3,4,5};
        long[] value2 = {0,10,20,30,40,50};
        double[] value3 = {0.5,1.5,2.5,3.5,4.5,5.5};
        howMany = Math.max(0,Math.min(howMany, time.length));
        for(int i = 0; i < howMany; i++) {
            eval.sample(time[i], value1[i], value2[i], value3[i]);
        }
    }

}