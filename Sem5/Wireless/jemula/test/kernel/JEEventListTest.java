package kernel;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class JEEventListTest {

	JEEventList events;

	@BeforeEach
	void setUp() {
		events = new JEEventList();
	}


	@AfterEach
	void tearDown() {
	}

	@Test
	void isEmpty_true() {
		JEEvent event = new JEEvent("event_name", 0, new JETime(0));
		assertTrue(events.isEmpty());
		events.add(event);
        events.removeFirst();
        assertTrue(events.isEmpty());
	}

    @Test
    void isEmpty_false() {
        JEEvent event = new JEEvent("event_name", 500, new JETime(123));
        assertTrue(events.isEmpty());
        events.add(event);
        assertFalse(events.isEmpty());
    }

    @Test
    void add_null() {
        events.add(null);
        JEEvent event = events.removeFirst();
        assertNull(event);
    }
	@Test
	void add_time_zero() {
		JEEvent event = new JEEvent("time_zero", 0, new JETime(0));
		events.add(event);
		JEEvent event1 = events.removeFirst();
		assertEquals(event, event1);
	}

    @Test
    void add_same_event_twice() {
        JEEvent event = new JEEvent("time_zero", 0, new JETime(0));
        events.add(event);
        events.add(event);
        List<JEEvent> eventList = events.getList();
        assertEquals(2,eventList.size());
        assertSame(eventList.get(0),eventList.get(1));
    }

    @Test
    void add_events_with_time_decreasing() {
        LinkedList<JEEvent> event_list = new LinkedList<>();
        event_list.add(new JEEvent("time_threeHundred", 0, new JETime(300)));
        event_list.add(new JEEvent("time_twenty", 0, new JETime(20)));
        event_list.add(new JEEvent("time_one", 0, new JETime(1)));
        event_list.add(new JEEvent("time_zero", 0, new JETime(0)));

        for(int i = 0; i < event_list.size(); i++) {
            events.add(event_list.get(i));
            assertEquals(i+1,events.getSize());
        }

        List<JEEvent> event_list_returned = events.getList();

        for(int i = 0; i < event_list.size(); i++) {
            assertSame(event_list.get(3-i), event_list_returned.get(i));
        }
    }

	@Test
	void removeFirst_empty() {
		JEEvent event = events.removeFirst();
		assertNull(event);
	}

    @Test
    void removeFirst_one_event() {
        JEEvent event = new JEEvent("event_name", 0, new JETime(0));
        events.add(event);
        assertEquals(1,events.getSize());
        JEEvent event_returned = events.removeFirst();
        assertSame(event, event_returned);
        assertTrue(events.isEmpty());
    }
    @Test
    void removeFirst_two_events_same_time() {
        JEEvent event = new JEEvent("event_name", 0, new JETime(0));
        events.add(event);
        events.add(event);
        assertEquals(2,events.getSize());
        JEEvent event_returned = events.removeFirst();
        assertFalse(events.isEmpty());
        assertSame(event, event_returned);
    }

    @Test
    void removeFirst_two_events_different_times() {
        JEEvent event0 = new JEEvent("event_name", 0, new JETime(0));
        JEEvent event1 = new JEEvent("event_name", 1, new JETime(1));
        events.add(event0);
        events.add(event1);
        assertEquals(2,events.getSize());
        JEEvent event_returned = events.removeFirst();
        assertFalse(events.isEmpty());
        assertSame(event0, event_returned);
        assertNotSame(event1, event_returned);
    }

	@Test
	void remove_event_list_empty() {
        JEEvent event = new JEEvent("event_name", 0, new JETime(0));
	    assertFalse(events.remove(event));
	}

    @Test
    void remove_event_exists_unique() {
        JEEvent event = new JEEvent("event_name", 0, new JETime(0));
        events.add(event);
        boolean wasRemoved = events.remove(event);
        assertTrue(wasRemoved);
    }

    @Test
    void remove_event_exists_not_unique() {
        JEEvent event = new JEEvent("event_name", 0, new JETime(0));
        events.add(event);
        events.add(event);
        assertTrue(events.remove(event));
        assertEquals(1,events.getSize());

        assertTrue(events.remove(event));
        assertTrue(events.isEmpty());

    }

	@Test
	void getSize_empty() {
	    assertTrue(events.isEmpty());
		assertEquals(0, events.getSize());
	}

    @Test
    void getSize_one_event() {
        JEEvent event = new JEEvent("event_name", 0, new JETime(0));
        events.add(event);
        assertEquals(1, events.getSize());
    }

    @Test
    void getSize_two_same_event_time() {
        JEEvent event = new JEEvent("event_name", 0, new JETime(0));
        events.add(event);
        events.add(event);
        assertEquals(2, events.getSize());
    }

    @Test
    void getSize_two_different_event_times() {
        LinkedList<JEEvent> event_list = new LinkedList<>();
        event_list.add(new JEEvent("time_zero", 0, new JETime(0)));
        event_list.add(new JEEvent("time_one", 0, new JETime(1)));
        event_list.add(new JEEvent("time_twenty", 0, new JETime(20)));
        event_list.add(new JEEvent("time_threeHundred", 0, new JETime(300)));

        for(int i = 0; i < event_list.size(); i++) {
            events.add(event_list.get(i));
            assertEquals(i+1, events.getSize());
        }
    }

	@Test
	void getList_empty() {
	    List<JEEvent> eventList = events.getList();
	    assertTrue(eventList.isEmpty());
    }

	@Test
	void getList_one_event() {
		JEEvent event;
		event = new JEEvent("event_name", 0, new JETime(0));
		events.add(event);
		List<JEEvent> eventList = events.getList();
        assertFalse(eventList.isEmpty());
		assertEquals(1,eventList.size());
		assertEquals(event, eventList.get(0));

	}

    @Test
    void add_same_events_twice_using_getList() {
        JEEvent event = new JEEvent("event_name", 0, new JETime(0));
        events.add(event);
        events.add(event);
        List<JEEvent> event_list_returned = events.getList();
        assertFalse(event_list_returned.isEmpty());
        assertEquals(2,event_list_returned.size());
        assertEquals(event, event_list_returned.get(0));
        assertEquals(event, event_list_returned.get(1));
    }

	@Test
	void add_all_events_same_time_using_getList() {
        List<JEEvent> event_list = new ArrayList<>();
		event_list.add(new JEEvent("event_0",0, new JETime(0)));
		event_list.add(new JEEvent("event_1",0, new JETime(0)));
		event_list.add(new JEEvent("event_2",0, new JETime(0)));
        event_list.add(new JEEvent("event_3",0, new JETime(0)));
        for(JEEvent event: event_list) {
           events.add(event);
        }
		List<JEEvent> event_list_returned = events.getList();
		assertEquals(4,event_list_returned.size());
        for(int i = 0; i < 4; i++) {
            assertSame(event_list.get(i), event_list_returned.get(i));
        }
	}

    @Test
    void add_all_events_different_time_added_increasing_time_using_getList() {
        LinkedList<JEEvent> event_list = new LinkedList<>();
        event_list.add(new JEEvent("time_zero", 0, new JETime(0)));
        event_list.add(new JEEvent("time_one", 0, new JETime(1)));
        event_list.add(new JEEvent("time_twenty", 0, new JETime(20)));
        event_list.add(new JEEvent("time_threeHundred", 0, new JETime(300)));
        for(JEEvent event: event_list){
            events.add(event);
        }
        List<JEEvent> event_list_returned = events.getList();
        for(int i = 0; i < event_list.size(); i++){
            assertSame(event_list.get(i), event_list_returned.get(i));
        }
    }

    @Test
    void add_all_events_different_time_added_decreasing_time_using_getList() {
        LinkedList<JEEvent> event_list = new LinkedList<>();
        event_list.add(new JEEvent("time_threeHundred", 0, new JETime(300)));
        event_list.add(new JEEvent("time_twenty", 0, new JETime(20)));
        event_list.add(new JEEvent("time_one", 0, new JETime(1)));
        event_list.add(new JEEvent("time_zero", 0, new JETime(0)));
            for(JEEvent event: event_list){
                events.add(event);
        }
        List<JEEvent> event_list_returned = events.getList();
        for(int i = 0; i < event_list.size(); i++) {
            assertSame(event_list.get(3-i), event_list_returned.get(i));
        }
    }

    @Test
    void add_all_events_different_time_added_time_not_ordered_using_getList() {
        LinkedList<JEEvent> event_list = new LinkedList<>();
        event_list.add(new JEEvent("time_twenty", 0, new JETime(20)));
        event_list.add(new JEEvent("time_zero", 0, new JETime(0)));
        event_list.add(new JEEvent("time_threeHundred", 0, new JETime(300)));
        event_list.add(new JEEvent("time_one", 0, new JETime(1)));

        for(JEEvent event: event_list){
            events.add(event);
        }
        List<JEEvent> event_list_returned = events.getList();
        for(JEEvent event: event_list){
            assertTrue(event_list_returned.contains(event));
        }
    }
}