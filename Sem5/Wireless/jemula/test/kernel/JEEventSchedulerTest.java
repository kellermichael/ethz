package kernel;

import com.ginsberg.junit.exit.ExpectSystemExitWithStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.lang.reflect.Field;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class JEEventSchedulerTest {
	private JEEventScheduler scheduler;
	private JETime simulationEnd;

	@BeforeEach
	void setUp() throws Exception {
		scheduler = new JEEventScheduler();
		simulationEnd = new JETime(50);
		// Ugly hack to overcome problem of theUniqueHandlerId being private static
		Field theUniqueHandlerId = JEEventHandler.class.getDeclaredField("theUniqueHandlerId");
		theUniqueHandlerId.setAccessible(true);
		theUniqueHandlerId.setInt(null,0);
	}

	@Test
	void queue_event() {
		JEEvent event = new JEEvent("event_name", 1, new JETime(0.0));
		scheduler.queue_event(event);

	}

	@Test
	@ExpectSystemExitWithStatus(0)
	void queue_event_with_time_less_than_now__calls_system_exit() {
	    give_scheduler_a_start();
		JEEvent event = new JEEvent("timer_expired_ind", 1, new JETime(5));
		scheduler.queue_event(event);

	}

	@Test
	void reach_simulation_end() {
		give_scheduler_a_start();
		JEEvent event = new JEEvent("timer_expired_ind", 1, new JETime(100));
		scheduler.queue_event(event);
		scheduler.start();
		assertEquals(0,simulationEnd.compareTo(scheduler.now()));
	}

	private void give_scheduler_a_start() {
		JETimer handler1 = new JETimer(new JEEventScheduler(), new Random(), "timer_expired_ind", 0);
		JETimer handler2 = new JETimer(new JEEventScheduler(), new Random(), "timer_expired_ind", 0);
		handler1.start(new JETime(15));
		handler2.start(new JETime(20));
		scheduler.register_handler(handler1);
		scheduler.register_handler(handler2);
		JEEvent event1 = new JEEvent("timer_expired_ind", 0, new JETime(10));
		JEEvent event2 = new JEEvent("timer_expired_ind", 1, new JETime(15));
		scheduler.queue_event(event1);
		scheduler.queue_event(event2);
		scheduler.start(simulationEnd);
	}

	@Test
	void cancel_event_empty_scheduler() {
		JEEvent event = new JEEvent("event_name", 0, new JETime(0));
	    assertFalse(scheduler.cancel_event(event));
	}

	@Test
	void cancel_event_nonempty_scheduler() {
		JEEvent event = new JEEvent("event_name", 0, new JETime(0));
		scheduler.queue_event(event);
		assertTrue(scheduler.cancel_event(event));
	}

	@Test
	void cancel_event_nonempty_scheduler_not_contain_event() {
		JEEvent event1 = new JEEvent("event_name", 0, new JETime(0));
		JEEvent event2 = new JEEvent("event_name", 0, new JETime(1));
		scheduler.queue_event(event1);
		assertFalse(scheduler.cancel_event(event2));
	}

	@Test
	void cancel_event_nonempty_scheduler_not_first_event() {
		JEEvent event1 = new JEEvent("event_name", 0, new JETime(0));
		JEEvent event2 = new JEEvent("event_name", 0, new JETime(1));
		JEEvent event3 = new JEEvent("event_name", 0, new JETime(2));
		scheduler.queue_event(event1);
		scheduler.queue_event(event2);
		scheduler.queue_event(event3);
		assertTrue(scheduler.cancel_event(event2));
	}

	@Test
	void register_handler() {
		JEEventHandler handler = new JETimer(new JEEventScheduler(), new Random(), "event", 0);
		scheduler.register_handler(handler);
	}

	@Test
	void start_no_argument() {
        JETimer handler = new JETimer(new JEEventScheduler(), new Random(), "timer_expired_ind", 0);
        handler.start(new JETime(5000.0));
		scheduler.register_handler(handler);
        JEEvent event = new JEEvent("timer_expired_ind", handler.getHandlerId(), new JETime(5));
		scheduler.queue_event(event);
		scheduler.start();
		assertEquals(0,scheduler.now().compareTo(new JETime(5)));
	}

	@Test
	void start_with_argument() {
        JETimer handler = new JETimer(new JEEventScheduler(), new Random(), "timer_expired_ind", 0);
        handler.start(new JETime(5000.0));
        scheduler.register_handler(handler);
        JEEvent event = new JEEvent("timer_expired_ind", handler.getHandlerId(), new JETime(5));
        scheduler.queue_event(event);
        JETime endTime = new JETime(5000.0);
        scheduler.start(endTime);
        assertEquals(0,scheduler.now().compareTo(new JETime(5)));
        assertEquals(0,scheduler.getEmulationEnd().compareTo(endTime));
	}

	@Test
	void now() {
	    assertEquals(0,scheduler.now().compareTo(new JETime(0.0)));
	}

	@Test
	void setEmulationEnd() {
		assertEquals(0,scheduler.getEmulationEnd().compareTo(new JETime(1000.0)));
		scheduler.setEmulationEnd(new JETime(10.0));
		assertEquals(0,scheduler.getEmulationEnd().compareTo(new JETime(10.0)));
	}

	@Test
	void getEmulationEnd() {
		//constructor sets this default value
		assertEquals(0,scheduler.getEmulationEnd().compareTo(new JETime(1000.0)));
	}

	@Test
    void toString_test() {
	    assertEquals(scheduler.toString(), scheduler.now().toString());
    }

}