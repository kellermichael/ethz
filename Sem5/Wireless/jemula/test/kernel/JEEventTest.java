package kernel;


import com.ginsberg.junit.exit.ExpectSystemExitWithStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;
import java.util.Vector;

import static org.junit.jupiter.api.Assertions.*;

class JEEventTest {

	private JEEvent event;
	private JEEvent event_with_parameterlist;
	private Vector<Object> parameterList_empty;
	private Vector<Object> parameterList_not_empty;

	@BeforeEach
	void setUp() {
		parameterList_empty = new Vector<Object>(0);
		parameterList_not_empty = new Vector<Object>(5);
		parameterList_not_empty.add(new Object());
		event = new JEEvent("event_name", 500, new JETime(123), parameterList_empty);
		event_with_parameterlist = new JEEvent("event_name", 500, new JETime(1234), parameterList_not_empty);
	}

	@Test
	void constructors() {
		JEEventHandler handler = new JETimer(new JEEventScheduler(), new Random(), "event", 5);
		JEEvent event1 = new JEEvent("event", 5, new JETime(123),parameterList_empty);
		JEEvent event2 = new JEEvent("event", handler, new JETime(123), parameterList_empty);
		JEEvent event2_1 = new JEEvent("event", handler, new JETime(123), parameterList_not_empty);
		JEEvent event3 = new JEEvent("event", 5, new JETime(123));
		JEEvent event4 = new JEEvent("event", handler, new JETime(123));

		assertEquals(0, event1.compareTo(event2));
		assertEquals(0, event1.compareTo(event3));
		assertEquals(0, event1.compareTo(event4));
		assertEquals(0, event2.compareTo(event3));
		assertEquals(0, event2.compareTo(event4));
		assertEquals(0, event3.compareTo(event4));

		assertEquals("event", event1.getName());
		assertEquals("event", event2.getName());
		assertEquals("event", event3.getName());
		assertEquals("event", event4.getName());

		assertEquals(0,new JETime(123).compareTo(event1.getScheduledTime()));
		assertEquals(0,new JETime(123).compareTo(event2.getScheduledTime()));
		assertEquals(0,new JETime(123).compareTo(event3.getScheduledTime()));
		assertEquals(0,new JETime(123).compareTo(event4.getScheduledTime()));

		JEEvent copyEvent = new JEEvent(event);
		JEEvent copyEvent_1 = new JEEvent(event2_1);
		assertEquals(0,event.getScheduledTime().compareTo(copyEvent.getScheduledTime()));
		assertEquals(event.getName(),copyEvent.getName());
		assertEquals(event.getTargetHandlerId(),copyEvent.getTargetHandlerId());
		assertEquals(event.getSourceHandlerId(),copyEvent.getSourceHandlerId());
		assertEquals(event.getParameterList(),copyEvent.getParameterList());
		assertEquals(event2_1.getParameterList(),copyEvent_1.getParameterList());
	}

	@Test
	void getName() {
		assertEquals("event_name", event.getName());
	}

	@Test
	void getScheduledTime() {
		assertEquals(0, new JETime(123).compareTo(event.getScheduledTime()));
	}

	@Test
	void getSourceHandlerId() {
		assertEquals(new Integer(0), event.getSourceHandlerId());
	}

	@Test
	void getTargetHandlerId() {
		assertEquals(new Integer(500), event.getTargetHandlerId());
	}

	@Test
	void getParameterList() {
		Vector<Object> parameters = event.getParameterList();
		assertNull(parameters);

		parameters = event_with_parameterlist.getParameterList();
		assertNotNull(parameters);
		assertEquals(1, parameters.size());
	}

	@Test
	void setSourceHandlerId() {
		assertEquals(new Integer(0), event.getSourceHandlerId());
		event.setSourceHandlerId(50);
		assertEquals(new Integer(50), event.getSourceHandlerId());
	}

	@Test
	void setTargetHandlerId() {
		assertEquals(new Integer(500), event.getTargetHandlerId());
		event.setTargetHandlerId(550);
		assertEquals(new Integer(550), event.getTargetHandlerId());
	}

	@Test
	void setTheScheduledTime() {
		assertEquals(0, new JETime(123).compareTo(event.getScheduledTime()));
		event.setTheScheduledTime(new JETime(10));
		assertEquals(0, new JETime(10).compareTo(event.getScheduledTime()));
	}

	@Test
	void compareTo() {
		assertEquals(-1, event.compareTo(event_with_parameterlist));
		assertEquals(1, event_with_parameterlist.compareTo(event));
		event.setTheScheduledTime(new JETime(1234));
		assertEquals(0, event_with_parameterlist.compareTo(event));

	}

//	Methods from abstract super class JEmula
	@Test
	@ExpectSystemExitWithStatus(0)
	void error() {
		event.error("error");
	}

	@Test
	void isVerbose() {
		assertFalse(event.isVerbose());
	}

	@Test
	void debugLevel() {
		assertEquals(70, event.getDebuglevel());
	}

	@Test
	void setDebugLevel() {
		event.setDebuglevel(0);
		assertEquals(0,event.getDebuglevel());
	}
}