package kernel;

import com.ginsberg.junit.exit.ExpectSystemExitWithStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class JETimerTest {

	JETimer timer;

	@BeforeEach
	void setUp() {
		timer = new JETimer(new JEEventScheduler(), new Random(), "event", 0);
	}

	@Test
	void start() {
		assertTrue(timer.is_idle());
		timer.start(new JETime(10));
		assertTrue(timer.is_active());
		assertFalse(timer.is_idle());
		timer.start(new JETime(10));
		assertTrue(timer.is_active());
	}

	@Test
	@ExpectSystemExitWithStatus(0)
	void start_paused_timer__calls_system_exit() {
		assertTrue(timer.is_idle());
		timer.start(new JETime(10));
		timer.pause();
		timer.start(new JETime(10));
	}

	@Test
	void stop() {
		timer.start(new JETime(10));
		assertTrue(timer.is_active());
		timer.stop();
		assertFalse(timer.is_active());
		assertTrue(timer.is_idle());
		timer.stop();
		assertFalse(timer.is_active());
		assertTrue(timer.is_idle());
	}

	@Test
	void pause() {
		assertFalse(timer.is_active());
		timer.start(new JETime(10));
		assertTrue(timer.is_active());
		timer.pause();
		assertFalse(timer.is_active());
		assertTrue(timer.is_paused());
	}

	@Test
	@ExpectSystemExitWithStatus(0)
	void pause_idle_timer__calls_system_exit() {
		assertTrue(timer.is_idle());
		timer.pause();
    }

	@Test
	void resume() {
		assertFalse(timer.is_active());
		timer.start(new JETime(10));
		assertTrue(timer.is_active());
		timer.pause();
		assertFalse(timer.is_active());
		assertTrue(timer.is_paused());
		timer.resume();
		assertTrue(timer.is_active());
		assertFalse(timer.is_paused());
	}

	@Test
	void event_handler() {
		JEEvent event = new JEEvent("timer_expired_ind",0, new JETime(10));
		timer.start(new JETime(10));
		assertTrue(timer.is_active());
		timer.event_handler(event);
		assertFalse(timer.is_active());

		timer = new JETimer(new JEEventScheduler(), new Random(), event, 0);
		timer.start(new JETime(10));
		assertTrue(timer.is_active());
		timer.event_handler(event);
		assertFalse(timer.is_active());
	}

	@Test
	@ExpectSystemExitWithStatus(0)
	void event_handler_undefined_event__calls_system_exit() {
		JEEvent event = new JEEvent("undefined",0, new JETime(10));
		timer.start(new JETime(10));
		timer.event_handler(event);
	}


	@Test
	void is_active() {
		assertFalse(timer.is_active());
		timer.start(new JETime());
		assertTrue(timer.is_active());
	}

	@Test
	void is_paused() {
		assertFalse(timer.is_paused());
		timer.start(new JETime());
		assertFalse(timer.is_paused());
		timer.pause();
		assertTrue(timer.is_paused());
		assertFalse(timer.is_active());
		assertFalse(timer.is_idle());
	}

	@Test
	void is_idle() {
		assertTrue(timer.is_idle());
		timer.start(new JETime());
		assertFalse(timer.is_idle());
		timer.pause();
		assertFalse(timer.is_idle());
		timer.resume();
		assertFalse(timer.is_idle());
		timer.stop();
		assertTrue(timer.is_idle());
	}

	@Test
	void getRemainingInterval() {
		timer.start(new JETime(10));
		JETime remaining = timer.getRemainingInterval();
		assertEquals(10,remaining.getTime());
	}

	@Test
	void getExpiryTime() {
		JETime expiry = timer.getExpiryTime();
		assertEquals(0,expiry.getTime());
		timer.start(new JETime(10));
		expiry = timer.getExpiryTime();
		assertEquals(10,expiry.getTime());
		timer.pause();
		expiry = timer.getExpiryTime();
		assertEquals(10,expiry.getTime());
		timer.resume();
		expiry = timer.getExpiryTime();
		assertEquals(10,expiry.getTime());
		timer.stop();
		expiry = timer.getExpiryTime();
		assertEquals(10,expiry.getTime());
		timer.start(new JETime(15));
		expiry = timer.getExpiryTime();
		assertEquals(15,expiry.getTime());
	}

}