package kernel;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JETimeTest {

    private long correct_time = 10;
    private JETime time_wo_arg;
    private JETime time_with_double;
    private JETime time_with_long;
    private JETime time_with_JETime;

    @BeforeEach
    void setUp() {
        double time_double = 0.01;
        long time_long = 10;
        time_wo_arg = new JETime();
        time_with_double = new JETime(time_double);
        time_with_long = new JETime(time_long);
        time_with_JETime = new JETime(time_wo_arg);
    }

    @Test
    void constructor_comparison_and_compareTo_test() {
        assertEquals(0,time_wo_arg.compareTo(time_with_JETime));
        assertEquals(0,time_with_double.compareTo(time_with_long));
        assertEquals(-1, time_wo_arg.compareTo(time_with_long));
        assertEquals(1, time_with_double.compareTo(time_with_JETime));
    }

    @Test
    void isEarlierThan_earlier() {
		assertTrue(time_wo_arg.isEarlierThan(time_with_long));
    }

	@Test
	void isEarlierThan_same_time() {
		assertFalse(time_with_double.isEarlierThan(time_with_long));
	}

	@Test
	void isEarlierThan_later() {
		assertFalse(time_with_double.isEarlierThan(time_wo_arg));
	}

    @Test
    void isEarlierEqualThan_earlier() {
	    assertTrue(time_wo_arg.isEarlierEqualThan(time_with_long));
    }

    @Test
    void isEarlierEqualThan_same_time() {
        assertTrue(time_wo_arg.isEarlierEqualThan(time_with_JETime));
        assertTrue(time_with_double.isEarlierEqualThan(time_with_long));
    }

    @Test
    void isEarlierEqualThan_later() {
        assertFalse(time_with_long.isEarlierEqualThan(time_with_JETime));
    }

    @Test
    void isLaterThan_earlier() {
	    assertFalse(time_wo_arg.isLaterThan(time_with_long));
    }

    @Test
    void isLaterThan_same_time() {
        assertFalse(time_with_double.isLaterThan(time_with_long));
    }

    @Test
    void isLaterThan_later() {
        assertTrue(time_with_long.isLaterThan(time_with_JETime));
    }

    @Test
    void isLaterEqualThan_earlier() {
	    assertFalse(time_wo_arg.isLaterEqualThan(time_with_long));
    }

    @Test
    void isLaterEqualThan_same_time() {
        assertTrue(time_with_double.isLaterEqualThan(time_with_long));
    }

    @Test
    void isLaterEqualThan_later() {
        assertTrue(time_with_double.isLaterEqualThan(time_with_JETime));
    }

    @Test
    void getTimeMs_time_zero() {
        assertEquals(0.0, time_wo_arg.getTimeMs());
    }

    @Test
    void getTimeMs_time_nonzero() {
        assertEquals(0.01, time_with_double.getTimeMs());
    }

    @Test
    void getTime_time_zero() {
        assertEquals(0, time_wo_arg.getTime());
    }

    @Test
    void getTime_time_nonzero() {
        assertEquals(10, time_with_double.getTime());
    }

    @Test
    void getTimeS_time_zero() {
        assertEquals(0.0, time_wo_arg.getTimeS());
    }

    @Test
    void getTimeS_time_nonzero() {
        assertEquals(0.00001, time_with_long.getTimeS());
    }

    @Test
    void setTime_time_zero() {
	    assertEquals(correct_time,time_with_long.getTime());
	    time_with_long.setTime(0);
	    assertEquals(0,time_with_long.getTime());
    }

    @Test
    void setTime_time_one_sec() {
        assertEquals(correct_time,time_with_double.getTime());
        time_with_long.setTime(1000000);
        assertEquals(1,time_with_long.getTimeS());
    }

    @Test
    void setTime_time_long_max_value() {
        assertEquals(0,time_wo_arg.getTime());
        time_wo_arg.setTime(Long.MAX_VALUE);
        assertEquals(Long.MAX_VALUE,time_wo_arg.getTime());
    }

    @Test
    void setTime_time_zero_double() {
        assertEquals(correct_time,time_with_long.getTime());
        time_with_long.setTime(0.0);
        assertEquals(0,time_with_long.getTime());
    }

    @Test
    void setTime_time_one_sec_double() {
        assertEquals(correct_time,time_with_double.getTime());
        time_with_long.setTime(1000.0);
        assertEquals(1,time_with_long.getTimeS());
    }

    @Test
    void plus_zero() {
    	JETime new_time;

	    new_time = time_wo_arg.plus(new JETime(0));
	    assertEquals(0,time_wo_arg.compareTo(new_time));
    }

    @Test
    void plus_one_sec() {
        JETime new_time;
        new_time = time_wo_arg.plus(new JETime(1000.0));
        assertEquals(1,new_time.getTimeS());
        assertTrue(time_wo_arg.isEarlierThan(new_time));
    }

    @Test
    void plus_zero_plus_long_max_value() {
        JETime new_time;
        new_time = time_wo_arg.plus(new JETime(Long.MAX_VALUE));
        assertEquals(Long.MAX_VALUE,new_time.getTime());
        assertTrue(time_wo_arg.isEarlierThan(new_time));
    }

    @Test
    void plus_nonzero_plus_long_max_value() {
        JETime new_time;
        new_time = time_with_long.plus(new JETime(Long.MAX_VALUE));
        assertFalse(time_wo_arg.isEarlierThan(new_time)); //overflow
    }

    @Test
    void times_zero_times_zero() {
	    JETime new_time = time_wo_arg.times(0);
	    assertEquals(0,time_wo_arg.compareTo(new_time));
    }

    @Test
    void times_nonzero_times_zero() {
        JETime new_time = time_with_long.times(0);
        assertEquals(1,time_with_long.compareTo(new_time));
        assertEquals(0,new_time.getTime());
    }

    @Test
    void times_nonzero_times_positive() {
        JETime new_time = time_with_long.times(10);
        assertEquals(-1,time_with_long.compareTo(new_time));
        assertEquals(100,new_time.getTime());
    }

    @Test
    void times_nonzero_times_negative() {
        JETime new_time = time_with_long.times(-10);
        assertEquals(1,time_with_long.compareTo(new_time));
        assertEquals(-100,new_time.getTime());
    }

    @Test
    void minus_zero_minus_zero() {
        JETime new_time = time_wo_arg.minus(new JETime(0));
        assertEquals(0,time_wo_arg.compareTo(new_time));
    }

    @Test
    void minus_nonzero_minus_positive() {
        JETime new_time = time_with_long.minus(new JETime(20));
        assertEquals(1,time_with_long.compareTo(new_time));
        assertEquals(-10,new_time.getTime());
    }

    @Test
    void minus_nonzero_minus_negative() {
        JETime new_time = time_with_long.minus(new JETime(-10));
        assertEquals(-1,time_with_long.compareTo(new_time));
        assertEquals(20,new_time.getTime());
    }

    @Test
    void dividedby_zero_devideby_zero_ArithmeticException() {
        Assertions.assertThrows(ArithmeticException.class, () -> time_with_double.dividedby(time_with_JETime));
    }

    @Test
    void dividedby_zero_devideby_positive() {
    	assertEquals(0, time_wo_arg.dividedby(time_with_long));
    }

    @Test
    void dividedby_zero_devideby_negative() {
        assertEquals(0, time_wo_arg.dividedby(new JETime(-0.001)));
    }

    @Test
    void dividedby_nonzero_devideby_positive() {
        assertEquals(1, time_with_double.dividedby(time_with_long));
    }

    @Test
    void dividedby_nonzero_devideby_negative() {
        assertEquals(-10, time_with_double.dividedby(new JETime(-0.001)));
    }

    @Test
    void dividedby_whole_number_division_rounding_results_zero() {
        JETime jetime = new JETime(0.1);
        long time = time_with_double.dividedby(jetime);
        assertEquals(0, time);
    }

    @Test
    void dividedby_whole_number_division_rounding_results_nonzero() {
        JETime jetime = new JETime(0.007);
        long time = time_with_double.dividedby(jetime);
        assertEquals(1, time);

       time = new JETime(144).dividedby(new JETime(5));
       assertEquals(28, time);
    }

    @Test
    void add_nonzero_add_zero() {
		assertEquals(0, JETime.add(time_with_double, time_with_JETime).compareTo(time_with_double));
    }

    @Test
    void add_zero_add_nonzero() {
        assertEquals(0, JETime.add(time_wo_arg, time_with_long).compareTo(time_with_long));
    }

    @Test
    void add_nonzero_add_nonzero() {
        assertEquals(0, new JETime(20).compareTo(JETime.add(time_with_double, time_with_long)));
    }

    @Test
    void compareTo_earlier_later() {
        assertEquals(-1, time_wo_arg.compareTo(time_with_double));
    }

    @Test
    void compareTo_same_time() {
	    assertEquals(0, time_wo_arg.compareTo(time_with_JETime));
	    assertEquals(0, time_with_double.compareTo(time_with_long));
    }

    @Test
    void compareTo_later_earlier() {
        assertEquals(1, time_with_double.compareTo(time_with_JETime));
    }

    @Test
    void toString_zero() {
        assertEquals("0.000 ms", time_wo_arg.toString());
    }

    @Test
    void toString_one_sec() {
        assertEquals("1000.000 ms", new JETime(1000.0).toString());
    }

    @Test
    void toString_negative() {
        assertEquals("-1234.567 ms", new JETime(-1234.567).toString());
    }

    @Test
    void toString_truncating_some_decimals() {
        assertEquals("1234.567 ms", new JETime(1234.5678).toString());
    }
}