/*
 * 
 * This is jemula.
 *
 *    Copyright (c) 2006-2009 Stefan Mangold, Fabian Dreier, Stefan Schmid
 *    All rights reserved. Urheberrechtlich geschuetzt.
 *    
 *    Redistribution and use in source and binary forms, with or without modification,
 *    are permitted provided that the following conditions are met:
 *    
 *      Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer. 
 *    
 *      Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation and/or
 *      other materials provided with the distribution. 
 *    
 *      Neither the name of any affiliation of Stefan Mangold nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission. 
 *    
 *    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *    EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *    IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *    INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *    BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 *    OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 *    OF SUCH DAMAGE.
 *    
 */

package kernel;

import java.util.*;

public class JEEventList {

    private TreeMap<Long, LinkedList<JEEvent>> treeMap;

	public JEEventList() {
	    treeMap = new TreeMap<>();
	}
	
	public boolean isEmpty() {
		return treeMap.isEmpty();
	}
	
	public void add(JEEvent event) {
		if(event != null) {
            long time = event.getScheduledTime().getTime();
            if(treeMap.containsKey(time)) {
                treeMap.get(time).add(event);
            } else {
                LinkedList<JEEvent> events = new LinkedList<>();
                events.add(event);
                treeMap.put(time, events);
            }
        }
	}
	
	public JEEvent removeFirst() {
	    JEEvent event = null;
	    if(!treeMap.isEmpty()){
            Map.Entry<Long,LinkedList<JEEvent>> first = treeMap.firstEntry();
            long time = first.getKey();
            LinkedList<JEEvent> events = first.getValue();
            if(!events.isEmpty()) {
                event = events.removeFirst();
            }

            removeKeyFromMapIfEmpty(time);
        }
        return event;

	}
	
	public boolean remove(JEEvent event) {
	    boolean wasRemoved = false;
		if (event!=null) {
			long time = event.getScheduledTime().getTime();
			LinkedList<JEEvent> events = treeMap.get(time);
			if (events != null) {
				wasRemoved = events.remove(event);
                removeKeyFromMapIfEmpty(time);
			}
		}
		return wasRemoved;
	}
	
	
	public int getSize() {
		return this.getList().size();
	}
	
	public List<JEEvent> getList() {
		List<JEEvent> result = new ArrayList<>();
		Collection<LinkedList<JEEvent>> allValues = treeMap.values();
        for(List<JEEvent> list: allValues){
            result.addAll(list);
        }
        return result;
	}

	private boolean removeKeyFromMapIfEmpty(long time){
	    if(treeMap.get(time).isEmpty()){
	        treeMap.remove(time);
	        return true;
        }
        return false;
    }
	
	
}
