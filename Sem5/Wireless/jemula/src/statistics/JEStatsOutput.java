package statistics;

import kernel.JEmula;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class JEStatsOutput extends JEmula {

	private static final String[] supportedExtensions = {"m", "csv", "txt"};
	private Path file;
	private String extension;

	private String header;

	public JEStatsOutput(Path file, String header) {

		this.extension = getFileExtension(file.toString().toLowerCase());
		if(isSupportedExtension(this.extension)) {
			this.file = file;
			this.header = header;

			makeDirAndFile();
			if(!this.header.isEmpty()) {
				write(this.header);
			}
		} else {
			this.error(String.format("%s is not a supported output format. Only %s are supported.",
					this.extension,
					String.join(", ",getSupportedExtensions())));
		}
	}

	private String getFileExtension(String filename) {
		int lastIndexOf = filename.lastIndexOf(".");
		if (lastIndexOf == -1) {
			return ""; // empty extension
		}
		return filename.substring(lastIndexOf+1);
	}


	private void makeDirAndFile() {
		try {
			if(!Files.exists(this.file.getParent())){
				this.message("creating new folder \"" + this.file.getParent() + "\"", 100);
				Files.createDirectories(this.file.getParent());
			}
			Files.deleteIfExists(this.file);
			Files.createFile(this.file);
		} catch (Exception e){
			this.error("could not delete and create the result directory or file: " + this.file.toString());
		}
	}

	public void write(String output) {
		try (PrintWriter out = new PrintWriter(new FileWriter(this.file.toFile(), true))) {
			out.println(output);
		} catch (IOException e) {
			this.error(String.format("Could not open file %s",this.file.getFileName()));
		}
	}

	public void write(List<String> output) {
		write(toString(output));
	}

	private String toString(List<String> output) {
		switch (this.extension) {
		case "m":
			return to_m(output);
		case "csv":
			return to_csv(output);
		case "txt":
			return to_txt(output);
		default:
			return "";
		}
	}

	private String to_m(List<String> output) {
		return String.join("\t",output);
	}

	private String to_csv(List<String> output) {
		return String.join(",",output);
	}

	private String to_txt(List<String> output) {
		return String.join(" ",output);
	}

	protected String getHeader() {
		return this.header;
	}

	protected String getOutputFormat() {
		return this.extension;
	}

	protected Path getFile() {
		return this.file;
	}

	public static boolean isSupportedExtension(String extension){
		for(String s: supportedExtensions){
			if(extension.equals(s)){
				return true;
			}
		}
		return false;
	}

	public static String[] getSupportedExtensions() {
		return supportedExtensions;
	}
}
