import copy

in_data = [0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 1, 2, 3, 4, 5, 5, 5, 5, 5, 5, 5, 4, 3, 2, 1, 0, 0, 0, 0, 0, -10, 0, 0, 0, 0, 0, 0, 0, 0]
result = [3 for _ in range(40)]

# prop
prop = [result[i] - in_data[i] for i in range(40)]
prop = [0] + prop

# integral
integral = copy.deepcopy(prop)
for i in range(1, 40):
    integral[i] = integral[i-1]

# differential
differential = [0 for _ in range(40)]
for i in range(1, 40):
    differential[i] = prop[i] - prop[i-1]

print(differential)