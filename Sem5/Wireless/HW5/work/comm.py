import sys
import serial
import time
import logging
import re
from threading import Thread

logging.basicConfig(format="\n%(levelname)s:%(message)s", level=logging.INFO)
MESSAGE_FORMAT = re.compile(r"^m\[R,D,(?P<message>\w+)\]$")


def encode(message):
    return str.encode(message, "utf-8")


def decode(bytes):
    return bytes.decode("utf-8")


def write(serial, name, message):
    serial.write(encode(message))
    logging.debug("{}: Writing '{}'".format(name, message.strip("\n")))
    time.sleep(0.1)


def read(name, s):
    logging.debug("{}: Listening..".format(name))
    message = ""
    while True:
        try:
            symbol = decode(s.read(1))
            if symbol == "\n":
                match = MESSAGE_FORMAT.match(message)
                if match:
                    logging.info("{}: Received '{}'".format(
                        name, match.group("message")))
                else:
                    logging.debug("{}: Read '{}'".format(name, message))
                message = ""
            else:
                message += symbol
                logging.debug(message)
        except serial.SerialException:
            continue
        except KeyboardInterrupt:
            sys.exit()


def main(argv):
    dev_port = argv[0]
    dev_name = argv[1]
    dest = argv[2]

    s = serial.Serial(dev_port, 115200, timeout=1)
    time.sleep(2)

    t = Thread(target=read, args=(dev_name, s))
    t.start()
    time.sleep(3)

    for cmd in [
            "a[{}]\n".format(dev_name),
            "c[1,0,5]\n",
            "c[0,1,30]\n",
    ]:
        write(s, dev_name, cmd)
    time.sleep(1)

    while True:
        message = input("enter message: ")
        write(s, dev_name, "m[{}\0,{}]\n".format(message, dest))


if __name__ == "__main__":
    main(sys.argv[1:])
