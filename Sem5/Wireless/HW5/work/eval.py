import sys
import serial
import time
import numpy as np
from threading import Thread

DONE = True


def write(serial, message):
    serial.write(str.encode(message, "utf-8"))
    time.sleep(0.01)


def read(s, duration):
    message = ""
    i = 0
    while i < len(duration):
        try:
            symbol = s.read(1).decode("utf-8")
            if symbol == "\n":
                if "s[R,A,CD->AB" in message:
                    duration[i] = time.time() - duration[i]
                    i += 1
                elif message == "m[D]":
                    global DONE
                    DONE = True
                message = ""
            else:
                message += symbol
        except serial.SerialException:
            continue
        except KeyboardInterrupt:
            sys.exit()


def driver(s, sndName, rcvName, f):
    # setup
    write(s, "a[" + sndName + "]\n")
    write(s, "c[2,0,0]\n")
    write(s, "c[1,0,5]\n")
    write(s, "c[0,1,30]\n")
    f(s, rcvName)


def sender(s, rcv):
    for payload_size in [1, 10, 50, 100, 150, 200]:
        iterations = 50
        duration = [0] * iterations
        t = Thread(
            target=read,
            args=(
                s,
                duration,
            ),
        )
        t.start()

        for i in range(iterations):
            global DONE
            while not DONE:
                pass
            DONE = False
            duration[i] = time.time()
            write(s, "m[{}\0,{}]\n".format("Q" * payload_size, rcv))

        t.join()
        print("Size: " + str(payload_size) + "B, Throughput: " +
              str(payload_size / np.mean(duration)) + "B/s, Deviation: " +
              str(np.std(duration)))


def receiver(s, rcv):
    pass


def main():
    snd = serial.Serial("/dev/ttyACM0", 115200, timeout=1)
    rcv = serial.Serial("/dev/ttyACM1", 115200, timeout=1)
    sndName = "AB"
    rcvName = "CD"
    time.sleep(2)

    t1 = Thread(target=driver, args=(
        snd,
        sndName,
        rcvName,
        sender,
    ))

    t2 = Thread(target=driver, args=(
        rcv,
        rcvName,
        sndName,
        receiver,
    ))
    t1.start()
    t2.start()
    t1.join()
    t2.join()


if __name__ == "__main__":
    main()