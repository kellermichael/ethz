import sys
import serial
import time
from threading import Thread


def write(serial, message):
    serial.write(str.encode(message, "utf-8"))
    time.sleep(0.01)


def read(name, s):
    message = ""
    while True:
        try:
            symbol = s.read(1).decode("utf-8")
            if symbol == "\n":
                if "m[R,D," in message:
                    print(name + ": " +
                          message.replace("m[R,D,", "").replace("]", ""))
                message = ""
            else:
                message += symbol
        except serial.SerialException:
            continue
        except KeyboardInterrupt:
            sys.exit()


def main(argv):
    port = argv[0]
    name = argv[1]
    dest = argv[2]

    s = serial.Serial(port, 115200, timeout=1)
    time.sleep(2)

    t = Thread(target=read, args=(dest, s))
    t.start()
    time.sleep(3)

    write(s, "a[" + name + "]\n")
    write(s, "c[1,0,5]\n")
    write(s, "c[0,1,30]\n")
    time.sleep(1)

    while True:
        message = input("")
        write(s, "m[{}\0,{}]\n".format(message, dest))


if __name__ == "__main__":
    main(sys.argv[1:])
