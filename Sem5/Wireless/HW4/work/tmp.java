package layer2_802Algorithms;

import plot.JEMultiPlotter;
import layer1_802Phy.JE802PhyMode;
import layer2_80211Mac.JE802_11BackoffEntity;
import layer2_80211Mac.JE802_11Mac;
import layer2_80211Mac.JE802_11MacAlgorithm;

public class MobComp_Assignment04 extends JE802_11MacAlgorithm {

	private JE802_11BackoffEntity theBackoffEntityAC01;

	private double theSamplingTime_sec;
	
	// vars for PID
	private String[] modes = {"BPSK12", "BPSK34", "QPSK12", "QPSK34", "16QAM12", "16QAM34", "64QAM23", "64QAM34"};
	private double[] history = new double[100];
	private int currentHistory = 0;
	private double integral = 0;
	
	private double eW = 0.7;
	private double dW = 0.1;
	private double iW = 0.2;
	

	public MobComp_Assignment04(String name, JE802_11Mac mac) {
		super(name, mac);
		this.theBackoffEntityAC01 = this.mac.getBackoffEntity(1);
		message("This is station " + this.dot11MACAddress.toString() + ". MobComp algorithm: '" + this.algorithmName
				+ "'.", 100);
	}

	@Override
	public void compute() {
		message("---------------------------", 10);
		message("I am station " + this.dot11MACAddress.toString() + ". My algorithm is called '" + this.algorithmName
				+ "'.", 10);

		// observe outcome:
		Integer AIFSN_AC01 = theBackoffEntityAC01.getDot11EDCAAIFSN();
		Integer CWmin_AC01 = theBackoffEntityAC01.getDot11EDCACWmin();
		
		theBackoffEntityAC01.setDot11EDCACWmax(CWmin_AC01);

//		theBackoffEntityAC01.getQueueSize();
//		theBackoffEntityAC01.getCurrentQueueSize();

		message("with the following contention window parameters ...", 10);
		message("    AIFSN[AC01] = " + AIFSN_AC01.toString(), 10);
		message("    CWmin[AC01] = " + CWmin_AC01.toString(), 10);
		message("... the backoff entity queues perform like this:", 10);
		
		double error = theBackoffEntityAC01.getQueueSize() - theBackoffEntityAC01.getCurrentQueueSize();
		
		double derivative = (error - history[currentHistory % 100]) / 0.1;
		currentHistory++;
		history[currentHistory % 100] = error;
		
		integral += error;
		integral -= history[(currentHistory + 1) % 100];
		
		double magicValue = eW * error + dW * derivative + iW * integral;
		int newMode = 0;
		
		System.out.println(magicValue);
		
		if(magicValue >= 1500) {
			newMode = 7;
		} else if(magicValue > 1500) {
			newMode = 6;
		} else if(magicValue > 1000) {
			newMode = 5;
		} else if(magicValue > 800) {
			newMode = 4;
		} else if(magicValue > 600) {
			newMode = 3;
		} else if(magicValue > 400) {
			newMode = 2;
		} else if(magicValue > 300) {
			newMode = 1;
		} else {
			newMode = 0;
		}
		
		this.mac.getPhy().setCurrentPhyMode(modes[newMode]);

		// act:
		theBackoffEntityAC01.setDot11EDCAAIFSN(AIFSN_AC01);
		theBackoffEntityAC01.setDot11EDCACWmin(CWmin_AC01);
	}

	@Override
	public void plot() {
		if (plotter == null) {
			plotter = new JEMultiPlotter("PID Controller, Station " + this.dot11MACAddress.toString(), "max", "time [s]", "MAC Queue", this.theUniqueEventScheduler.getEmulationEnd().getTimeMs() / 1000.0, true);
			plotter.addSeries("current");
			plotter.display();
		}
		plotter.plot(((Double) theUniqueEventScheduler.now().getTimeMs()).doubleValue() / 1000.0, theBackoffEntityAC01.getQueueSize(), 0);
		plotter.plot(((Double) theUniqueEventScheduler.now().getTimeMs()).doubleValue() / 1000.0, theBackoffEntityAC01.getCurrentQueueSize(), 1);
	}

}
