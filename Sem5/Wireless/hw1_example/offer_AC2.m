%=============================================================================================================================
% Result "offer_AC2". Wed Oct 20 15:45:24 CEST 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_AC2 = [
500.0	12810	12810	1001.1160031225605	1.2824296E7	Infinity	Infinity
1000.0	12940	25750	988.8794435857806	2.5620396E7	409.926336	204.7376
1500.0	12988	38738	999.7177394518017	3.860473E7	308.83784	207.749344
2000.0	12830	51568	999.9896336710834	5.1434597E7	274.3178506666667	205.277872
2500.0	12928	64496	1000.426051980198	6.4368105E7	257.47242	206.936128
3000.0	12808	77304	995.9841505309182	7.712467E7	246.798944	204.10504
3500.0	13036	90340	996.4955507824486	9.0114986E7	240.30662933333335	207.845056
4000.0	12892	103232	996.7389854173131	1.02964945E8	235.3484457142857	205.599344
4500.0	12764	115996	999.6923378251332	1.15725018E8	231.450036	204.161168
]; % Wed Oct 20 15:45:30 CEST 2021
