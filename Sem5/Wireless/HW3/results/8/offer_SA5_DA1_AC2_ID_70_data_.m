%=============================================================================================================================
% Result "offer_SA5_DA1_AC2_ID_70_data_". Sun Nov 07 23:39:30 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA5_DA1_AC2_ID_70_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	109	109	541.743119266055	59050.0	0.9448	0.9448
1000.0	108	217	570.3703703703703	120650.0	0.9652	0.9855999999999999
1500.0	96	313	606.1666666666666	178842.0	0.953824	0.931072
2000.0	98	411	550.4387755102041	232785.0	0.93114	0.863088
2500.0	80	491	573.5375	278668.0	0.8917376000000001	0.734128
3000.0	94	585	594.3617021276596	334538.0	0.8921013333333333	0.89392
3500.0	113	698	541.929203539823	395776.0	0.9046308571428572	0.979808
4000.0	96	794	576.8645833333334	451155.0	0.90231	0.886064
4500.0	116	910	611.5258620689655	522092.0	0.9281635555555556	1.134992
]; % Sun Nov 07 23:39:35 CET 2021
