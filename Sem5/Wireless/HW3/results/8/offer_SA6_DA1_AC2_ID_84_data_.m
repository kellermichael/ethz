%=============================================================================================================================
% Result "offer_SA6_DA1_AC2_ID_84_data_". Sun Nov 07 23:39:30 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA6_DA1_AC2_ID_84_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	97	97	594.4639175257732	57663.0	0.922608	0.9226080000000001
1000.0	119	216	551.7226890756302	123318.0	0.986544	1.05048
1500.0	109	325	545.9908256880734	182831.0	0.9750986666666667	0.952208
2000.0	93	418	631.9354838709677	241601.0	0.966404	0.9403199999999999
2500.0	99	517	674.9191919191919	308418.0	0.9869376	1.069072
3000.0	92	609	644.7934782608696	367739.0	0.9806373333333334	0.9491360000000001
3500.0	111	720	612.8558558558559	435766.0	0.9960365714285714	1.088432
4000.0	102	822	596.8333333333334	496643.0	0.993286	0.9740320000000001
4500.0	126	948	631.0079365079365	576150.0	1.0242666666666667	1.272112
]; % Sun Nov 07 23:39:35 CET 2021
