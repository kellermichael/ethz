%=============================================================================================================================
% Result "total_offer". Sun Nov 07 23:39:29 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_total_offer = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	787	787	600.8055908513342	472834.0	7.565344	7.565344
1000.0	787	1574	590.4561626429479	937523.0	7.500184	7.435024
1500.0	804	2378	591.2176616915423	1412862.0	7.535264	7.605424
2000.0	842	3220	599.9038004750594	1917981.0	7.671924	8.081904
2500.0	810	4030	620.779012345679	2420812.0	7.7465984	8.045296
3000.0	809	4839	608.8294190358467	2913355.0	7.768946666666667	7.880688
3500.0	792	5631	599.2828282828283	3387987.0	7.743970285714285	7.594112
4000.0	855	6486	591.0736842105263	3893355.0	7.78671	8.085887999999999
4500.0	888	7374	590.8952702702703	4418070.0	7.854346666666666	8.39544
]; % Sun Nov 07 23:39:35 CET 2021
