%=============================================================================================================================
% Result "offer_SA3_DA1_AC2_ID_42_data_". Sun Nov 07 23:39:03 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA3_DA1_AC2_ID_42_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	102	102	600.5490196078431	61256.0	0.980096	0.9800959999999999
1000.0	91	193	624.7802197802198	118111.0	0.944888	0.90968
1500.0	108	301	610.8796296296297	184086.0	0.981792	1.0556
2000.0	98	399	619.2857142857143	244776.0	0.979104	0.9710400000000001
2500.0	103	502	546.0582524271845	301020.0	0.963264	0.899904
3000.0	90	592	584.6444444444444	353638.0	0.9430346666666666	0.8418879999999999
3500.0	117	709	592.4786324786324	422958.0	0.9667611428571429	1.10912
4000.0	105	814	554.0952380952381	481138.0	0.962276	0.93088
4500.0	101	915	561.5940594059406	537859.0	0.9561937777777777	0.907536
]; % Sun Nov 07 23:39:08 CET 2021
