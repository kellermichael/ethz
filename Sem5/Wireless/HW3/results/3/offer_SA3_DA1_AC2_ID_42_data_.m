%=============================================================================================================================
% Result "offer_SA3_DA1_AC2_ID_42_data_". Sun Nov 07 23:51:46 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA3_DA1_AC2_ID_42_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	99	99	588.6767676767677	58279.0	0.932464	0.932464
1000.0	96	195	606.8854166666666	116540.0	0.93232	0.932176
1500.0	103	298	563.9223300970874	174624.0	0.931328	0.929344
2000.0	107	405	605.8411214953271	239449.0	0.957796	1.0372
2500.0	106	511	573.9433962264151	300287.0	0.9609184000000001	0.973408
3000.0	108	619	636.3518518518518	369013.0	0.9840346666666666	1.099616
3500.0	125	744	593.656	443220.0	1.0130742857142856	1.187312
4000.0	112	856	565.2410714285714	506527.0	1.013054	1.012912
4500.0	118	974	576.8983050847457	574601.0	1.021512888888889	1.089184
]; % Sun Nov 07 23:51:47 CET 2021
