%=============================================================================================================================
% Result "thrp_SA10_DA1_AC2_ID_140_data__End". Sun Nov 07 23:52:02 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA10_DA1_AC2_ID_140_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	14	14	577.5	8085.0	0.12936	0.12936
1000.0	40	54	641.125	33730.0	0.26984	0.41032
1500.0	19	73	514.1052631578947	43498.0	0.23198933333333333	0.15628799999999998
2000.0	43	116	594.0697674418604	69043.0	0.276172	0.40872
2500.0	36	152	545.6944444444445	88688.0	0.28380160000000004	0.31432
3000.0	42	194	619.2619047619048	114697.0	0.30585866666666667	0.41614400000000007
3500.0	22	216	577.6818181818181	127406.0	0.29121371428571424	0.20334399999999997
4000.0	33	249	572.0	146282.0	0.292564	0.302016
4500.0	37	286	644.4324324324324	170126.0	0.30244622222222217	0.381504
]; % Sun Nov 07 23:52:06 CET 2021
