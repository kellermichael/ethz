%=============================================================================================================================
% Result "thrp_SA2_DA1_AC2_ID_28_data__End". Sun Nov 07 23:52:02 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA2_DA1_AC2_ID_28_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	53	53	651.2075471698113	34514.0	0.552224	0.552224
1000.0	24	77	643.5833333333334	49960.0	0.39968	0.247136
1500.0	50	127	667.64	83342.0	0.44449066666666665	0.534112
2000.0	35	162	584.8285714285714	103811.0	0.415244	0.327504
2500.0	43	205	633.3023255813954	131043.0	0.41933760000000003	0.43571200000000004
3000.0	52	257	555.7307692307693	159941.0	0.42650933333333335	0.46236800000000006
3500.0	24	281	589.2916666666666	174084.0	0.3979062857142857	0.226288
4000.0	38	319	535.421052631579	194430.0	0.38886	0.325536
4500.0	35	354	543.0857142857143	213438.0	0.3794453333333333	0.304128
]; % Sun Nov 07 23:52:06 CET 2021
