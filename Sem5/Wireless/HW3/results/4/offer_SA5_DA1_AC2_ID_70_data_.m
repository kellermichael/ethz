%=============================================================================================================================
% Result "offer_SA5_DA1_AC2_ID_70_data_". Sun Nov 07 23:51:53 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA5_DA1_AC2_ID_70_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	117	117	623.1111111111111	72904.0	1.166464	1.166464
1000.0	96	213	607.0416666666666	131180.0	1.04944	0.932416
1500.0	121	334	594.396694214876	203102.0	1.0832106666666665	1.150752
2000.0	103	437	584.1067961165048	263265.0	1.05306	0.962608
2500.0	93	530	571.6021505376344	316424.0	1.0125568	0.850544
3000.0	101	631	603.1485148514852	377342.0	1.0062453333333332	0.974688
3500.0	123	754	595.3089430894308	450565.0	1.0298628571428572	1.171568
4000.0	115	869	558.2782608695652	514767.0	1.0295340000000002	1.027232
4500.0	100	969	550.37	569804.0	1.0129848888888888	0.880592
]; % Sun Nov 07 23:51:56 CET 2021
