%=============================================================================================================================
% Result "thrp_SA2_DA1_AC2_ID_28_data__End". Sun Nov 07 23:51:53 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA2_DA1_AC2_ID_28_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	88	88	499.2613636363636	43935.0	0.70296	0.70296
1000.0	93	181	568.3870967741935	96795.0	0.77436	0.84576
1500.0	83	264	557.3493975903615	143055.0	0.76296	0.7401600000000002
2000.0	95	359	569.6105263157895	197168.0	0.788672	0.865808
2500.0	97	456	569.0103092783505	252362.0	0.8075584	0.883104
3000.0	93	549	612.1182795698925	309289.0	0.8247706666666667	0.910832
3500.0	97	646	637.0927835051547	371087.0	0.8481988571428571	0.988768
4000.0	68	714	559.3382352941177	409122.0	0.818244	0.60856
4500.0	103	817	505.6504854368932	461204.0	0.8199182222222222	0.833312
]; % Sun Nov 07 23:51:56 CET 2021
