%=============================================================================================================================
% Result "thrp_SA3_DA1_AC2_ID_42_data__End". Sun Nov 07 23:51:53 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA3_DA1_AC2_ID_42_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	77	77	603.1168831168832	46440.0	0.74304	0.7430400000000001
1000.0	93	170	586.2795698924731	100964.0	0.807712	0.872384
1500.0	87	257	636.080459770115	156303.0	0.833616	0.8854240000000001
2000.0	97	354	570.5463917525773	211646.0	0.846584	0.8854879999999998
2500.0	103	457	550.4271844660194	268340.0	0.858688	0.907104
3000.0	71	528	596.4084507042254	310685.0	0.8284933333333334	0.67752
3500.0	97	625	614.1443298969073	370257.0	0.8463017142857143	0.9531520000000001
4000.0	95	720	610.9684210526316	428299.0	0.856598	0.928672
4500.0	99	819	536.3636363636364	481399.0	0.8558204444444444	0.8496
]; % Sun Nov 07 23:51:56 CET 2021
