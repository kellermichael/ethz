%=============================================================================================================================
% Result "offer_SA7_DA1_AC2_ID_98_data_". Sun Nov 07 23:53:14 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA7_DA1_AC2_ID_98_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	88	88	612.125	53867.0	0.861872	0.861872
1000.0	101	189	562.009900990099	110630.0	0.88504	0.908208
1500.0	103	292	551.5728155339806	167442.0	0.893024	0.908992
2000.0	114	406	636.859649122807	240044.0	0.960176	1.161632
2500.0	102	508	663.5196078431372	307723.0	0.9847136000000001	1.082864
3000.0	107	615	585.0	370318.0	0.9875146666666668	1.00152
3500.0	105	720	544.5142857142857	427492.0	0.9771245714285715	0.914784
4000.0	118	838	624.7796610169491	501216.0	1.002432	1.179584
4500.0	79	917	585.0632911392405	547436.0	0.9732195555555555	0.73952
]; % Sun Nov 07 23:53:27 CET 2021
