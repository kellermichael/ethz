%=============================================================================================================================
% Result "offer_SA18_DA1_AC2_ID_252_data_". Sun Nov 07 23:53:14 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA18_DA1_AC2_ID_252_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	91	91	594.1208791208791	54065.0	0.86504	0.8650399999999999
1000.0	108	199	574.0370370370371	116061.0	0.928488	0.991936
1500.0	111	310	605.8648648648649	183312.0	0.977664	1.076016
2000.0	113	423	605.1238938053098	251691.0	1.006764	1.094064
2500.0	90	513	549.7	301164.0	0.9637247999999999	0.7915680000000002
3000.0	98	611	637.234693877551	363613.0	0.9696346666666668	0.9991839999999999
3500.0	108	719	583.9722222222222	426682.0	0.9752731428571428	1.0091039999999998
4000.0	105	824	587.7714285714286	488398.0	0.976796	0.987456
4500.0	116	940	582.948275862069	556020.0	0.98848	1.081952
]; % Sun Nov 07 23:53:27 CET 2021
