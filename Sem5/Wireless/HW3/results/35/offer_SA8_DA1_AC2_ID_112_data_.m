%=============================================================================================================================
% Result "offer_SA8_DA1_AC2_ID_112_data_". Sun Nov 07 23:53:14 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA8_DA1_AC2_ID_112_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	96	96	549.0208333333334	52706.0	0.843296	0.843296
1000.0	103	199	609.0970873786408	115443.0	0.923544	1.003792
1500.0	102	301	536.0686274509804	170122.0	0.9073173333333333	0.8748640000000001
2000.0	94	395	577.0744680851063	224367.0	0.897468	0.8679199999999999
2500.0	101	496	685.5544554455446	293608.0	0.9395456000000001	1.107856
3000.0	101	597	611.3861386138614	355358.0	0.9476213333333334	0.988
3500.0	107	704	610.2803738317757	420658.0	0.961504	1.0448
4000.0	95	799	573.8842105263158	475177.0	0.950354	0.872304
4500.0	96	895	589.65625	531784.0	0.9453937777777779	0.905712
]; % Sun Nov 07 23:53:27 CET 2021
