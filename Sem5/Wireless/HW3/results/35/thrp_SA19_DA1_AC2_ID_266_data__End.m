%=============================================================================================================================
% Result "thrp_SA19_DA1_AC2_ID_266_data__End". Sun Nov 07 23:53:14 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA19_DA1_AC2_ID_266_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	7	7	708.0	4956.0	0.079296	0.079296
1000.0	8	15	462.5	8656.0	0.069248	0.0592
1500.0	2	17	847.0	10350.0	0.0552	0.027104
2000.0	5	22	436.0	12530.0	0.05012	0.03488
2500.0	14	36	518.2857142857143	19786.0	0.0633152	0.11609600000000002
3000.0	17	53	508.6470588235294	28433.0	0.07582133333333332	0.138352
3500.0	1	54	104.0	28537.0	0.06522742857142858	0.001664
4000.0	17	71	560.4117647058823	38064.0	0.076128	0.152432
4500.0	7	78	673.5714285714286	42779.0	0.07605155555555555	0.07544
]; % Sun Nov 07 23:53:27 CET 2021
