%=============================================================================================================================
% Result "thrp_SA22_DA1_AC2_ID_308_data__End". Sun Nov 07 23:53:15 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA22_DA1_AC2_ID_308_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	18	18	580.3888888888889	10447.0	0.167152	0.167152
1000.0	8	26	658.125	15712.0	0.125696	0.08424
1500.0	9	35	716.6666666666666	22162.0	0.11819733333333333	0.1032
2000.0	8	43	395.875	25329.0	0.101316	0.050672
2500.0	9	52	756.0	32133.0	0.10282559999999999	0.108864
3000.0	21	73	701.6190476190476	46867.0	0.12497866666666667	0.235744
3500.0	5	78	451.2	49123.0	0.11228114285714286	0.036096
4000.0	17	95	590.8235294117648	59167.0	0.118334	0.160704
4500.0	21	116	554.1428571428571	70804.0	0.12587377777777778	0.186192
]; % Sun Nov 07 23:53:27 CET 2021
