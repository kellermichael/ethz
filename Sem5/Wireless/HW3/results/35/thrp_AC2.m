%=============================================================================================================================
% Result "thrp_AC2". Sun Nov 07 23:53:15 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_AC2 = [
500.0	258	258	575.2054263565891	148403.0	Infinity	Infinity
1000.0	288	546	602.3194444444445	321871.0	5.149936	2.775488
1500.0	255	801	604.5411764705882	476029.0	3.808232	2.466528
2000.0	273	1074	574.2893772893773	632810.0	3.3749866666666666	2.508496
2500.0	267	1341	620.2322097378277	798412.0	3.193648	2.649632
3000.0	278	1619	591.043165467626	962722.0	3.0807104	2.6289600000000006
3500.0	275	1894	596.4181818181818	1126737.0	3.004632	2.62424
4000.0	255	2149	638.3725490196078	1289522.0	2.9474788571428574	2.60456
4500.0	274	2423	635.1021897810219	1463540.0	2.92708	2.784288
]; % Sun Nov 07 23:53:27 CET 2021
