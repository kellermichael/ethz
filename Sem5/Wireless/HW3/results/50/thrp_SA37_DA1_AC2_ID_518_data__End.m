%=============================================================================================================================
% Result "thrp_SA37_DA1_AC2_ID_518_data__End". Sun Nov 07 23:54:22 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA37_DA1_AC2_ID_518_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	1	1	142.0	142.0	0.002272	0.002272
1000.0	6	7	445.8333333333333	2817.0	0.022536	0.0428
1500.0	0	7	NaN	2817.0	0.015024	NaN
2000.0	0	7	NaN	2817.0	0.011268	NaN
2500.0	1	8	650.0	3467.0	0.011094399999999999	0.0104
3000.0	0	8	NaN	3467.0	0.009245333333333333	NaN
3500.0	0	8	NaN	3467.0	0.007924571428571428	NaN
4000.0	14	22	752.7857142857143	14006.0	0.028012	0.168624
4500.0	1	23	451.0	14457.0	0.025701333333333336	0.007216
]; % Sun Nov 07 23:54:38 CET 2021
