%=============================================================================================================================
% Result "offer_SA34_DA1_AC2_ID_476_data_". Sun Nov 07 23:54:22 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA34_DA1_AC2_ID_476_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	103	103	599.2912621359224	61727.0	0.987632	0.987632
1000.0	88	191	633.2159090909091	117450.0	0.9396	0.891568
1500.0	105	296	596.6190476190476	180095.0	0.9605066666666666	1.00232
2000.0	99	395	600.8585858585859	239580.0	0.95832	0.9517600000000002
2500.0	104	499	639.7980769230769	306119.0	0.9795807999999999	1.064624
3000.0	127	626	588.9055118110236	380910.0	1.01576	1.196656
3500.0	99	725	606.8383838383838	440987.0	1.0079702857142858	0.961232
4000.0	93	818	664.1827956989247	502756.0	1.005512	0.9883039999999998
4500.0	97	915	611.680412371134	562089.0	0.9992693333333332	0.949328
]; % Sun Nov 07 23:54:38 CET 2021
