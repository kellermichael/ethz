%=============================================================================================================================
% Result "thrp_SA13_DA1_AC2_ID_182_data__End". Sun Nov 07 23:54:22 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA13_DA1_AC2_ID_182_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	0	0	NaN	0.0	0.0	NaN
1000.0	5	5	222.8	1114.0	0.008912	0.017824
1500.0	12	17	700.8333333333334	9524.0	0.05079466666666666	0.13456
2000.0	12	29	551.4166666666666	16141.0	0.064564	0.105872
2500.0	4	33	785.25	19282.0	0.0617024	0.050256
3000.0	19	52	804.5263157894736	34568.0	0.09218133333333332	0.244576
3500.0	3	55	495.6666666666667	36055.0	0.08241142857142858	0.023792
4000.0	15	70	590.3333333333334	44910.0	0.08982	0.14168
4500.0	0	70	NaN	44910.0	0.07984	NaN
]; % Sun Nov 07 23:54:38 CET 2021
