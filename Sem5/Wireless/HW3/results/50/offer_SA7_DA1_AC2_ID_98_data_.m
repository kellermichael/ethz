%=============================================================================================================================
% Result "offer_SA7_DA1_AC2_ID_98_data_". Sun Nov 07 23:54:22 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA7_DA1_AC2_ID_98_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	98	98	618.4081632653061	60604.0	0.969664	0.969664
1000.0	104	202	568.875	119767.0	0.958136	0.946608
1500.0	111	313	641.918918918919	191020.0	1.0187733333333333	1.140048
2000.0	100	413	610.21	252041.0	1.008164	0.976336
2500.0	112	525	582.4464285714286	317275.0	1.01528	1.043744
3000.0	110	635	548.0454545454545	377560.0	1.0068266666666668	0.9645599999999999
3500.0	105	740	524.4190476190477	432624.0	0.988854857142857	0.8810240000000001
4000.0	113	853	606.4159292035398	501149.0	1.002298	1.0964
4500.0	111	964	639.8018018018018	572167.0	1.0171857777777777	1.136288
]; % Sun Nov 07 23:54:38 CET 2021
