%=============================================================================================================================
% Result "offer_SA24_DA1_AC2_ID_336_data_". Sun Nov 07 23:54:22 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA24_DA1_AC2_ID_336_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	104	104	571.0192307692307	59386.0	0.950176	0.9501759999999999
1000.0	98	202	658.6122448979592	123930.0	0.99144	1.032704
1500.0	97	299	562.1649484536083	178460.0	0.9517866666666667	0.8724800000000001
2000.0	117	416	629.1196581196581	252067.0	1.008268	1.177712
2500.0	101	517	558.8217821782179	308508.0	0.9872255999999999	0.9030560000000001
3000.0	95	612	572.6631578947369	362911.0	0.9677626666666667	0.870448
3500.0	119	731	577.6470588235294	431651.0	0.9866308571428571	1.09984
4000.0	116	847	551.0172413793103	495569.0	0.991138	1.022688
4500.0	116	963	610.6120689655172	566400.0	1.0069333333333332	1.133296
]; % Sun Nov 07 23:54:38 CET 2021
