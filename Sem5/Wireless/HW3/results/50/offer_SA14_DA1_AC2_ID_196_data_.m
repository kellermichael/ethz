%=============================================================================================================================
% Result "offer_SA14_DA1_AC2_ID_196_data_". Sun Nov 07 23:54:22 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA14_DA1_AC2_ID_196_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	111	111	628.7657657657658	69793.0	1.116688	1.116688
1000.0	103	214	631.2330097087379	134810.0	1.07848	1.040272
1500.0	91	305	566.3956043956044	186352.0	0.9938773333333334	0.8246720000000001
2000.0	98	403	623.469387755102	247452.0	0.989808	0.9776
2500.0	110	513	560.9	309151.0	0.9892831999999999	0.987184
3000.0	105	618	600.5428571428571	372208.0	0.9925546666666666	1.0089119999999998
3500.0	99	717	567.5757575757576	428398.0	0.9791954285714285	0.8990400000000001
4000.0	112	829	572.6875	492539.0	0.985078	1.026256
4500.0	94	923	609.1170212765958	549796.0	0.977415111111111	0.916112
]; % Sun Nov 07 23:54:38 CET 2021
