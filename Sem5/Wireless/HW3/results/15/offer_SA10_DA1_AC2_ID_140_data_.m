%=============================================================================================================================
% Result "offer_SA10_DA1_AC2_ID_140_data_". Sun Nov 07 23:52:13 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA10_DA1_AC2_ID_140_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	92	92	540.5869565217391	49734.0	0.795744	0.795744
1000.0	106	198	582.4622641509434	111475.0	0.8918	0.987856
1500.0	109	307	621.440366972477	179212.0	0.9557973333333333	1.083792
2000.0	111	418	594.5405405405405	245206.0	0.980824	1.055904
2500.0	104	522	617.7403846153846	309451.0	0.9902432	1.02792
3000.0	112	634	592.2767857142857	375786.0	1.002096	1.06136
3500.0	105	739	638.5714285714286	442836.0	1.0121965714285714	1.0728
4000.0	106	845	606.2358490566038	507097.0	1.014194	1.028176
4500.0	92	937	552.9891304347826	557972.0	0.9919502222222223	0.814
]; % Sun Nov 07 23:52:18 CET 2021
