%=============================================================================================================================
% Result "offer_SA8_DA1_AC2_ID_112_data_". Sun Nov 07 23:52:12 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA8_DA1_AC2_ID_112_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	109	109	661.6788990825688	72123.0	1.153968	1.153968
1000.0	125	234	578.432	144427.0	1.155416	1.156864
1500.0	102	336	605.8627450980392	206225.0	1.0998666666666665	0.988768
2000.0	95	431	597.5368421052632	262991.0	1.051964	0.908256
2500.0	111	542	588.5045045045046	328315.0	1.050608	1.0451840000000001
3000.0	121	663	579.2479338842975	398404.0	1.0624106666666668	1.121424
3500.0	118	781	570.5762711864406	465732.0	1.064530285714286	1.077248
4000.0	116	897	563.3879310344828	531085.0	1.06217	1.045648
4500.0	106	1003	658.0849056603773	600842.0	1.0681635555555555	1.116112
]; % Sun Nov 07 23:52:18 CET 2021
