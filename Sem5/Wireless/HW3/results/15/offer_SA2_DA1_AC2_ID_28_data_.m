%=============================================================================================================================
% Result "offer_SA2_DA1_AC2_ID_28_data_". Sun Nov 07 23:52:12 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA2_DA1_AC2_ID_28_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	94	94	578.8510638297872	54412.0	0.870592	0.870592
1000.0	85	179	589.3294117647059	104505.0	0.83604	0.801488
1500.0	108	287	601.9907407407408	169520.0	0.9041066666666666	1.04024
2000.0	104	391	605.6346153846154	232506.0	0.930024	1.007776
2500.0	107	498	629.5327102803739	299866.0	0.9595712	1.07776
3000.0	107	605	595.1588785046729	363548.0	0.9694613333333333	1.018912
3500.0	116	721	615.551724137931	434952.0	0.994176	1.142464
4000.0	90	811	591.6333333333333	488199.0	0.976398	0.851952
4500.0	97	908	533.0721649484536	539907.0	0.9598346666666666	0.827328
]; % Sun Nov 07 23:52:18 CET 2021
