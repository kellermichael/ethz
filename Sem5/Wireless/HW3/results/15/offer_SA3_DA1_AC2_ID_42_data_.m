%=============================================================================================================================
% Result "offer_SA3_DA1_AC2_ID_42_data_". Sun Nov 07 23:52:12 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA3_DA1_AC2_ID_42_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	108	108	593.3796296296297	64085.0	1.0253599999999998	1.02536
1000.0	88	196	555.4772727272727	112967.0	0.903736	0.782112
1500.0	114	310	605.5175438596491	181996.0	0.9706453333333334	1.104464
2000.0	111	421	603.5405405405405	248989.0	0.995956	1.071888
2500.0	114	535	513.9649122807018	307581.0	0.9842592	0.9374720000000001
3000.0	94	629	611.6808510638298	365079.0	0.973544	0.919968
3500.0	106	735	568.622641509434	425353.0	0.9722354285714285	0.964384
4000.0	92	827	547.7282608695652	475744.0	0.951488	0.806256
4500.0	106	933	600.6037735849056	539408.0	0.9589475555555557	1.018624
]; % Sun Nov 07 23:52:18 CET 2021
