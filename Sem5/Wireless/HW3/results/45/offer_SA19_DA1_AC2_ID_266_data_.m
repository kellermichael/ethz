%=============================================================================================================================
% Result "offer_SA19_DA1_AC2_ID_266_data_". Sun Nov 07 23:53:55 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA19_DA1_AC2_ID_266_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	111	111	571.8378378378378	63474.0	1.015584	1.015584
1000.0	116	227	617.0431034482758	135051.0	1.080408	1.145232
1500.0	95	322	605.5157894736842	192575.0	1.0270666666666666	0.920384
2000.0	98	420	621.9795918367347	253529.0	1.014116	0.975264
2500.0	102	522	636.6960784313726	318472.0	1.0191104	1.039088
3000.0	111	633	623.5495495495495	387686.0	1.0338293333333333	1.107424
3500.0	99	732	560.969696969697	443222.0	1.013078857142857	0.888576
4000.0	112	844	648.6517857142857	515871.0	1.031742	1.162384
4500.0	87	931	609.0114942528736	568855.0	1.0112977777777779	0.847744
]; % Sun Nov 07 23:54:11 CET 2021
