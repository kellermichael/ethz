%=============================================================================================================================
% Result "offer_SA12_DA1_AC2_ID_168_data_". Sun Nov 07 23:53:55 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA12_DA1_AC2_ID_168_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	95	95	559.0421052631579	53109.0	0.849744	0.8497439999999998
1000.0	115	210	533.9826086956522	114517.0	0.916136	0.982528
1500.0	97	307	644.1030927835052	176995.0	0.9439733333333333	0.999648
2000.0	77	384	607.961038961039	223808.0	0.895232	0.749008
2500.0	114	498	553.5263157894736	286910.0	0.918112	1.0096319999999999
3000.0	81	579	637.4691358024692	338545.0	0.9027866666666666	0.82616
3500.0	91	670	615.9120879120879	394593.0	0.9019268571428571	0.896768
4000.0	104	774	571.7115384615385	454051.0	0.908102	0.951328
4500.0	94	868	589.1276595744681	509429.0	0.9056515555555555	0.886048
]; % Sun Nov 07 23:54:11 CET 2021
