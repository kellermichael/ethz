%=============================================================================================================================
% Result "offer_SA16_DA1_AC2_ID_224_data_". Sun Nov 07 23:53:55 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA16_DA1_AC2_ID_224_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	117	117	568.991452991453	66572.0	1.065152	1.065152
1000.0	89	206	587.0112359550562	118816.0	0.950528	0.8359040000000001
1500.0	111	317	637.1081081081081	189535.0	1.0108533333333334	1.131504
2000.0	96	413	619.84375	249040.0	0.99616	0.95208
2500.0	119	532	629.3613445378152	323934.0	1.0365888	1.198304
3000.0	81	613	598.925925925926	372447.0	0.993192	0.7762080000000001
3500.0	116	729	593.6034482758621	441305.0	1.0086971428571427	1.101728
4000.0	89	818	520.943820224719	487669.0	0.975338	0.7418239999999999
4500.0	100	918	593.57	547026.0	0.9724906666666667	0.9497120000000001
]; % Sun Nov 07 23:54:11 CET 2021
