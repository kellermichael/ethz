%=============================================================================================================================
% Result "offer_SA18_DA1_AC2_ID_252_data_". Sun Nov 07 23:53:55 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA18_DA1_AC2_ID_252_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	107	107	540.7663551401869	57862.0	0.925792	0.9257919999999998
1000.0	114	221	587.9122807017544	124884.0	0.999072	1.072352
1500.0	86	307	585.5813953488372	175244.0	0.9346346666666667	0.80576
2000.0	103	410	586.4077669902913	235644.0	0.942576	0.9664
2500.0	108	518	572.0185185185185	297422.0	0.9517504	0.9884479999999999
3000.0	87	605	519.8275862068965	342647.0	0.9137253333333333	0.7236
3500.0	90	695	624.2888888888889	398833.0	0.9116182857142857	0.898976
4000.0	96	791	567.71875	453334.0	0.906668	0.872016
4500.0	94	885	555.4042553191489	505542.0	0.8987413333333334	0.8353279999999998
]; % Sun Nov 07 23:54:11 CET 2021
