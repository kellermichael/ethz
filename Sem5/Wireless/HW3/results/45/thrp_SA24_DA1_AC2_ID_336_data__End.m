%=============================================================================================================================
% Result "thrp_SA24_DA1_AC2_ID_336_data__End". Sun Nov 07 23:53:55 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA24_DA1_AC2_ID_336_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	0	0	NaN	0.0	0.0	NaN
1000.0	1	1	1119.0	1119.0	0.008952	0.017904
1500.0	0	1	NaN	1119.0	0.005968	NaN
2000.0	1	2	1079.0	2198.0	0.008792	0.017264
2500.0	11	13	543.9090909090909	8181.0	0.0261792	0.095728
3000.0	9	22	625.2222222222222	13808.0	0.03682133333333334	0.090032
3500.0	13	35	552.6153846153846	20992.0	0.04798171428571428	0.114944
4000.0	4	39	662.25	23641.0	0.047282	0.042384
4500.0	14	53	633.7857142857143	32514.0	0.05780266666666666	0.141968
]; % Sun Nov 07 23:54:11 CET 2021
