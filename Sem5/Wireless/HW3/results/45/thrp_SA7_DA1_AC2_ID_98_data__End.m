%=============================================================================================================================
% Result "thrp_SA7_DA1_AC2_ID_98_data__End". Sun Nov 07 23:53:55 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA7_DA1_AC2_ID_98_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	2	2	580.5	1161.0	0.018576	0.018576
1000.0	5	7	512.6	3724.0	0.029792	0.041008
1500.0	6	13	705.1666666666666	7955.0	0.04242666666666667	0.067696
2000.0	1	14	1134.0	9089.0	0.036356	0.018144
2500.0	7	21	639.0	13562.0	0.043398400000000004	0.071568
3000.0	2	23	990.5	15543.0	0.041448	0.031696
3500.0	13	36	456.7692307692308	21481.0	0.049099428571428565	0.095008
4000.0	21	57	606.1904761904761	34211.0	0.068422	0.20368
4500.0	14	71	704.5	44074.0	0.07835377777777779	0.157808
]; % Sun Nov 07 23:54:11 CET 2021
