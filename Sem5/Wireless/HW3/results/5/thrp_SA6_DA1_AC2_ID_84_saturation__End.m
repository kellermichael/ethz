%=============================================================================================================================
% Result "thrp_SA6_DA1_AC2_ID_84_saturation__End". Sun Nov 07 17:57:12 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA6_DA1_AC2_ID_84_saturation__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	90	90	568.9888888888889	51209.0	0.819344	0.8193440000000001
1000.0	101	191	602.0693069306931	112018.0	0.896144	0.972944
1500.0	97	288	624.7216494845361	172616.0	0.9206186666666666	0.969568
2000.0	43	331	525.7441860465116	195223.0	0.780892	0.3617119999999999
2500.0	86	417	587.6162790697674	245758.0	0.7864256	0.80856
3000.0	94	511	645.0957446808511	306397.0	0.8170586666666666	0.9702240000000001
3500.0	88	599	656.2159090909091	364144.0	0.8323291428571429	0.923952
4000.0	84	683	627.952380952381	416892.0	0.833784	0.843968
4500.0	104	787	618.0384615384615	481168.0	0.8554097777777777	1.028416
]; % Sun Nov 07 17:57:14 CET 2021
