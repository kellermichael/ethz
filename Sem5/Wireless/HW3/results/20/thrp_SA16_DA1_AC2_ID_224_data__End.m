%=============================================================================================================================
% Result "thrp_SA16_DA1_AC2_ID_224_data__End". Sun Nov 07 23:52:25 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA16_DA1_AC2_ID_224_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	6	6	586.5	3519.0	0.056304	0.056304
1000.0	9	15	730.8888888888889	10097.0	0.080776	0.105248
1500.0	28	43	605.8928571428571	27062.0	0.1443306666666667	0.27144
2000.0	2	45	55.0	27172.0	0.108688	0.00176
2500.0	14	59	696.2857142857143	36920.0	0.118144	0.155968
3000.0	7	66	368.0	39496.0	0.10532266666666666	0.041216
3500.0	21	87	552.6666666666666	51102.0	0.11680457142857144	0.185696
4000.0	18	105	636.7777777777778	62564.0	0.125128	0.183392
4500.0	30	135	679.8666666666667	82960.0	0.14748444444444445	0.326336
]; % Sun Nov 07 23:52:33 CET 2021
