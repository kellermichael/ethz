%=============================================================================================================================
% Result "offer_SA13_DA1_AC2_ID_182_data_". Sun Nov 07 23:52:25 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA13_DA1_AC2_ID_182_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	103	103	576.6893203883495	59399.0	0.950384	0.950384
1000.0	94	197	629.1808510638298	118542.0	0.948336	0.946288
1500.0	105	302	618.7428571428571	183510.0	0.97872	1.039488
2000.0	111	413	615.6126126126126	251843.0	1.007372	1.093328
2500.0	88	501	572.7272727272727	302243.0	0.9671776	0.8064
3000.0	99	600	680.2525252525253	369588.0	0.985568	1.07752
3500.0	111	711	635.2792792792793	440104.0	1.005952	1.128256
4000.0	99	810	600.3434343434343	499538.0	0.999076	0.9509439999999999
4500.0	92	902	598.1195652173913	554565.0	0.9858933333333334	0.8804319999999999
]; % Sun Nov 07 23:52:33 CET 2021
