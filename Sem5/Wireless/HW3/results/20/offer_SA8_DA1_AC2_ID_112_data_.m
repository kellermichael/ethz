%=============================================================================================================================
% Result "offer_SA8_DA1_AC2_ID_112_data_". Sun Nov 07 23:52:25 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA8_DA1_AC2_ID_112_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	105	105	605.8380952380952	63613.0	1.017808	1.017808
1000.0	109	214	612.9082568807339	130420.0	1.0433599999999998	1.068912
1500.0	102	316	523.9411764705883	183862.0	0.9805973333333333	0.8550720000000002
2000.0	101	417	644.3762376237623	248944.0	0.995776	1.0413119999999998
2500.0	105	522	611.4285714285714	313144.0	1.0020608	1.0272
3000.0	102	624	558.2941176470588	370090.0	0.9869066666666666	0.9111359999999998
3500.0	97	721	603.9072164948453	428669.0	0.9798148571428572	0.937264
4000.0	104	825	611.9519230769231	492312.0	0.984624	1.018288
4500.0	110	935	588.0636363636364	556999.0	0.9902204444444443	1.0349920000000001
]; % Sun Nov 07 23:52:33 CET 2021
