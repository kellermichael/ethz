%=============================================================================================================================
% Result "offer_SA12_DA1_AC2_ID_168_data_". Sun Nov 07 23:52:25 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA12_DA1_AC2_ID_168_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	88	88	601.2045454545455	52906.0	0.846496	0.846496
1000.0	100	188	605.98	113504.0	0.908032	0.969568
1500.0	81	269	586.2592592592592	160991.0	0.8586186666666666	0.759792
2000.0	109	378	532.651376146789	219050.0	0.8762	0.9289439999999999
2500.0	96	474	563.625	273158.0	0.8741055999999999	0.865728
3000.0	99	573	594.7070707070707	332034.0	0.885424	0.942016
3500.0	98	671	577.1836734693877	388598.0	0.888224	0.9050239999999998
4000.0	98	769	636.8469387755102	451009.0	0.902018	0.998576
4500.0	133	902	653.3383458646616	537903.0	0.956272	1.390304
]; % Sun Nov 07 23:52:33 CET 2021
