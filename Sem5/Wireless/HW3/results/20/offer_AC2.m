%=============================================================================================================================
% Result "offer_AC2". Sun Nov 07 23:52:25 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_AC2 = [
500.0	2043	2043	596.4371023005384	1218521.0	Infinity	Infinity
1000.0	2032	4075	602.1781496062993	2442147.0	39.074352	19.578016
1500.0	2090	6165	592.8311004784689	3681164.0	29.449312	19.824272
2000.0	2076	8241	590.9749518304432	4908028.0	26.176149333333335	19.629824
2500.0	2057	10298	592.3864851725814	6126567.0	24.506268	19.496624
3000.0	2090	12388	592.6899521531101	7365289.0	23.5689248	19.819552
3500.0	2086	14474	608.1361457334611	8633861.0	23.023629333333336	20.297152
4000.0	2100	16574	594.4042857142857	9882110.0	22.58768	19.971984
4500.0	2130	18704	608.412676056338	1.1178029E7	22.356058	20.734704
]; % Sun Nov 07 23:52:33 CET 2021
