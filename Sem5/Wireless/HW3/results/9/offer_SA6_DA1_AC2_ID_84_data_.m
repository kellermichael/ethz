%=============================================================================================================================
% Result "offer_SA6_DA1_AC2_ID_84_data_". Sun Nov 07 23:39:42 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA6_DA1_AC2_ID_84_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	101	101	560.1980198019802	56580.0	0.90528	0.90528
1000.0	112	213	634.7767857142857	127675.0	1.0214	1.13752
1500.0	89	302	626.6741573033707	183449.0	0.9783946666666667	0.892384
2000.0	115	417	579.6695652173913	250111.0	1.000444	1.066592
2500.0	105	522	519.5333333333333	304662.0	0.9749184000000001	0.872816
3000.0	90	612	565.8	355584.0	0.948224	0.8147519999999999
3500.0	127	739	589.3464566929134	430431.0	0.9838422857142857	1.197552
4000.0	108	847	569.0555555555555	491889.0	0.983778	0.983328
4500.0	86	933	604.0116279069767	543834.0	0.966816	0.83112
]; % Sun Nov 07 23:39:47 CET 2021
