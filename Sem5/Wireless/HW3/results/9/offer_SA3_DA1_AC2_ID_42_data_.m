%=============================================================================================================================
% Result "offer_SA3_DA1_AC2_ID_42_data_". Sun Nov 07 23:39:41 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA3_DA1_AC2_ID_42_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	100	100	610.11	61011.0	0.976176	0.976176
1000.0	102	202	599.9705882352941	122208.0	0.977664	0.979152
1500.0	118	320	570.3813559322034	189513.0	1.010736	1.07688
2000.0	106	426	541.5566037735849	246918.0	0.987672	0.91848
2500.0	87	513	655.8735632183908	303979.0	0.9727328000000001	0.912976
3000.0	111	624	636.009009009009	374576.0	0.9988693333333334	1.129552
3500.0	90	714	542.6222222222223	423412.0	0.9677988571428572	0.781376
4000.0	105	819	641.8666666666667	490808.0	0.981616	1.078336
4500.0	93	912	571.2903225806451	543938.0	0.9670008888888889	0.85008
]; % Sun Nov 07 23:39:47 CET 2021
