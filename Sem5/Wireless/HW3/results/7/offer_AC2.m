%=============================================================================================================================
% Result "offer_AC2". Sun Nov 07 23:39:17 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_AC2 = [
500.0	701	701	606.5563480741797	425196.0	Infinity	Infinity
1000.0	742	1443	614.1078167115903	880864.0	14.093824	7.290688
1500.0	733	2176	581.06275579809	1306783.0	10.454264	6.814704
2000.0	704	2880	602.7173295454545	1731096.0	9.232512	6.789008
2500.0	771	3651	609.9831387808041	2201393.0	8.805572	7.524751999999999
3000.0	720	4371	609.675	2640359.0	8.449148800000001	7.0234559999999995
3500.0	702	5073	614.4415954415955	3071697.0	8.191192	6.901408000000001
4000.0	701	5774	592.6048502139801	3487113.0	7.970544	6.646656
4500.0	703	6477	581.5234708392603	3895924.0	7.791848	6.540976
]; % Sun Nov 07 23:39:21 CET 2021
