%=============================================================================================================================
% Result "offer_SA15_DA1_AC2_ID_210_saturation_". Sun Nov 07 17:03:05 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA15_DA1_AC2_ID_210_saturation_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	2	2	736.0	1472.0	0.023552	0.023552
1000.0	0	2	NaN	1472.0	0.011776	NaN
1500.0	13	15	539.5384615384615	8486.0	0.04525866666666667	0.112224
2000.0	2	17	935.5	10357.0	0.041428	0.029936
2500.0	0	17	NaN	10357.0	0.0331424	NaN
3000.0	0	17	NaN	10357.0	0.027618666666666663	NaN
3500.0	2	19	735.0	11827.0	0.027033142857142856	0.02352
4000.0	8	27	615.125	16748.0	0.033496	0.078736
4500.0	2	29	665.0	18078.0	0.03213866666666666	0.02128
]; % Sun Nov 07 17:03:25 CET 2021
