%=============================================================================================================================
% Result "thrp_SA14_DA1_AC2_ID_196_saturation__End". Sun Nov 07 17:03:05 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA14_DA1_AC2_ID_196_saturation__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	6	6	619.1666666666666	3715.0	0.05944	0.05944
1000.0	0	6	NaN	3715.0	0.02972	NaN
1500.0	3	9	521.3333333333334	5279.0	0.028154666666666668	0.025024
2000.0	12	21	635.75	12908.0	0.051632	0.122064
2500.0	3	24	518.6666666666666	14464.0	0.046284799999999994	0.024896
3000.0	0	24	NaN	14464.0	0.03857066666666666	NaN
3500.0	15	39	677.3333333333334	24624.0	0.056283428571428575	0.16256
4000.0	0	39	NaN	24624.0	0.049248	NaN
4500.0	4	43	258.25	25657.0	0.04561244444444445	0.016528
]; % Sun Nov 07 17:03:25 CET 2021
