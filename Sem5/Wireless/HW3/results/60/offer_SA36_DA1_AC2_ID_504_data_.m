%=============================================================================================================================
% Result "offer_SA36_DA1_AC2_ID_504_data_". Sun Nov 07 23:55:24 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA36_DA1_AC2_ID_504_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	103	103	571.4757281553398	58862.0	0.941792	0.9417919999999999
1000.0	93	196	696.9247311827957	123676.0	0.989408	1.037024
1500.0	131	327	621.3282442748092	205070.0	1.0937066666666668	1.302304
2000.0	92	419	596.3152173913044	259931.0	1.0397239999999999	0.877776
2500.0	84	503	530.5357142857143	304496.0	0.9743872	0.7130400000000001
3000.0	111	614	598.7477477477478	370957.0	0.9892186666666666	1.063376
3500.0	107	721	615.3831775700935	436803.0	0.9984068571428572	1.053536
4000.0	101	822	565.1584158415842	493884.0	0.987768	0.9132960000000001
4500.0	112	934	620.4017857142857	563369.0	1.001544888888889	1.11176
]; % Sun Nov 07 23:55:43 CET 2021
