%=============================================================================================================================
% Result "offer_SA46_DA1_AC2_ID_644_data_". Sun Nov 07 23:55:25 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA46_DA1_AC2_ID_644_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	94	94	548.1063829787234	51522.0	0.824352	0.8243520000000001
1000.0	103	197	659.0388349514564	119403.0	0.955224	1.086096
1500.0	106	303	623.8207547169811	185528.0	0.9894826666666666	1.058
2000.0	118	421	684.9491525423729	266352.0	1.065408	1.293184
2500.0	109	530	627.8348623853211	334786.0	1.0713152	1.094944
3000.0	118	648	539.5169491525423	398449.0	1.0625306666666667	1.018608
3500.0	85	733	546.5882352941177	444909.0	1.0169348571428571	0.74336
4000.0	89	822	592.1573033707865	497611.0	0.995222	0.843232
4500.0	111	933	575.1081081081081	561448.0	0.9981297777777778	1.021392
]; % Sun Nov 07 23:55:43 CET 2021
