%=============================================================================================================================
% Result "thrp_SA50_DA1_AC2_ID_700_data__End". Sun Nov 07 23:55:25 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA50_DA1_AC2_ID_700_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	4	4	379.75	1519.0	0.024304	0.024304
1000.0	0	4	NaN	1519.0	0.012152	NaN
1500.0	4	8	528.5	3633.0	0.019376	0.033824
2000.0	0	8	NaN	3633.0	0.014532	NaN
2500.0	0	8	NaN	3633.0	0.0116256	NaN
3000.0	9	17	474.3333333333333	7902.0	0.021072	0.068304
3500.0	6	23	420.1666666666667	10423.0	0.023824	0.040336
4000.0	9	32	504.1111111111111	14960.0	0.02992	0.072592
4500.0	7	39	572.4285714285714	18967.0	0.03371911111111111	0.064112
]; % Sun Nov 07 23:55:43 CET 2021
