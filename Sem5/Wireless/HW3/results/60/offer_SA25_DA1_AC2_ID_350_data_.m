%=============================================================================================================================
% Result "offer_SA25_DA1_AC2_ID_350_data_". Sun Nov 07 23:55:24 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA25_DA1_AC2_ID_350_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	100	100	601.82	60182.0	0.962912	0.9629120000000001
1000.0	101	201	623.3663366336634	123142.0	0.985136	1.00736
1500.0	104	305	598.2403846153846	185359.0	0.9885813333333332	0.995472
2000.0	88	393	638.3409090909091	241533.0	0.966132	0.898784
2500.0	93	486	619.2150537634409	299120.0	0.957184	0.921392
3000.0	100	586	504.96	349616.0	0.9323093333333333	0.807936
3500.0	94	680	614.7872340425532	407406.0	0.9312137142857143	0.92464
4000.0	105	785	648.6380952380953	475513.0	0.951026	1.089712
4500.0	94	879	561.3404255319149	528279.0	0.9391626666666667	0.844256
]; % Sun Nov 07 23:55:43 CET 2021
