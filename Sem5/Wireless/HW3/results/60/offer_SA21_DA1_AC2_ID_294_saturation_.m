%=============================================================================================================================
% Result "offer_SA21_DA1_AC2_ID_294_saturation_". Sun Nov 07 17:03:05 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA21_DA1_AC2_ID_294_saturation_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	2	2	75.5	151.0	0.002416	0.002416
1000.0	0	2	NaN	151.0	0.001208	NaN
1500.0	12	14	839.0833333333334	10220.0	0.05450666666666666	0.161104
2000.0	13	27	573.0	17669.0	0.070676	0.119184
2500.0	6	33	753.5	22190.0	0.071008	0.072336
3000.0	2	35	401.5	22993.0	0.06131466666666666	0.012848
3500.0	6	41	613.8333333333334	26676.0	0.06097371428571429	0.058928
4000.0	1	42	1155.0	27831.0	0.055662	0.01848
4500.0	13	55	602.6923076923077	35666.0	0.06340622222222222	0.12536000000000003
]; % Sun Nov 07 17:03:25 CET 2021
