%=============================================================================================================================
% Result "thrp_SA8_DA1_AC2_ID_112_data__End". Sun Nov 07 23:55:24 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA8_DA1_AC2_ID_112_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	0	0	NaN	0.0	0.0	NaN
1000.0	0	0	NaN	0.0	0.0	NaN
1500.0	6	6	690.5	4143.0	0.022096	0.066288
2000.0	0	6	NaN	4143.0	0.016572	NaN
2500.0	0	6	NaN	4143.0	0.0132576	NaN
3000.0	0	6	NaN	4143.0	0.011048	NaN
3500.0	2	8	149.0	4441.0	0.010150857142857144	0.004768
4000.0	1	9	898.0	5339.0	0.010678	0.014368
4500.0	19	28	504.7368421052632	14929.0	0.026540444444444443	0.15344
]; % Sun Nov 07 23:55:43 CET 2021
