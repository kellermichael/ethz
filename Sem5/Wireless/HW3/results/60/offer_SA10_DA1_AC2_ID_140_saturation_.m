%=============================================================================================================================
% Result "offer_SA10_DA1_AC2_ID_140_saturation_". Sun Nov 07 17:03:05 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA10_DA1_AC2_ID_140_saturation_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	2	2	432.0	864.0	0.013824	0.013824
1000.0	0	2	NaN	864.0	0.006912	NaN
1500.0	18	20	720.4444444444445	13832.0	0.07377066666666666	0.207488
2000.0	1	21	1095.0	14927.0	0.059708	0.01752
2500.0	4	25	520.5	17009.0	0.054428800000000006	0.033312
3000.0	7	32	523.1428571428571	20671.0	0.05512266666666667	0.058592
3500.0	2	34	543.5	21758.0	0.049732571428571425	0.017392
4000.0	0	34	NaN	21758.0	0.043516	NaN
4500.0	5	39	701.8	25267.0	0.04491911111111112	0.056144
]; % Sun Nov 07 17:03:25 CET 2021
