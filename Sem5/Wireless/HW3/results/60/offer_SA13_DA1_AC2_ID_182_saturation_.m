%=============================================================================================================================
% Result "offer_SA13_DA1_AC2_ID_182_saturation_". Sun Nov 07 17:03:05 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA13_DA1_AC2_ID_182_saturation_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	15	15	512.2666666666667	7684.0	0.122944	0.122944
1000.0	3	18	459.3333333333333	9062.0	0.072496	0.022048
1500.0	11	29	654.0	16256.0	0.08669866666666667	0.115104
2000.0	0	29	NaN	16256.0	0.065024	NaN
2500.0	0	29	NaN	16256.0	0.052019199999999995	NaN
3000.0	9	38	582.5555555555555	21499.0	0.05733066666666666	0.083888
3500.0	7	45	520.0	25139.0	0.05746057142857143	0.05824
4000.0	0	45	NaN	25139.0	0.050278	NaN
4500.0	7	52	653.8571428571429	29716.0	0.05282844444444445	0.073232
]; % Sun Nov 07 17:03:25 CET 2021
