%=============================================================================================================================
% Result "offer_SA52_DA1_AC2_ID_728_data_". Sun Nov 07 23:55:25 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA52_DA1_AC2_ID_728_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	112	112	587.0357142857143	65748.0	1.051968	1.051968
1000.0	86	198	642.6511627906976	121016.0	0.968128	0.884288
1500.0	110	308	624.3454545454546	189694.0	1.0117013333333333	1.098848
2000.0	96	404	570.09375	244423.0	0.977692	0.875664
2500.0	109	513	598.8899082568807	309702.0	0.9910463999999999	1.044464
3000.0	85	598	565.2823529411764	357751.0	0.9540026666666667	0.768784
3500.0	118	716	552.2118644067797	422912.0	0.966656	1.042576
4000.0	90	806	669.0	483122.0	0.966244	0.96336
4500.0	95	901	599.2	540046.0	0.9600817777777777	0.9107840000000001
]; % Sun Nov 07 23:55:43 CET 2021
