%=============================================================================================================================
% Result "offer_SA58_DA1_AC2_ID_812_saturation_". Sun Nov 07 17:03:06 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA58_DA1_AC2_ID_812_saturation_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	2	2	346.0	692.0	0.011072	0.011072
1000.0	11	13	607.1818181818181	7371.0	0.058968	0.10686399999999999
1500.0	8	21	734.25	13245.0	0.07064	0.093984
2000.0	2	23	759.0	14763.0	0.059052	0.024288
2500.0	8	31	520.5	18927.0	0.0605664	0.066624
3000.0	11	42	586.8181818181819	25382.0	0.06768533333333333	0.10328000000000001
3500.0	1	43	583.0	25965.0	0.059348571428571424	0.009328
4000.0	8	51	642.625	31106.0	0.062212	0.082256
4500.0	0	51	NaN	31106.0	0.05529955555555555	NaN
]; % Sun Nov 07 17:03:25 CET 2021
