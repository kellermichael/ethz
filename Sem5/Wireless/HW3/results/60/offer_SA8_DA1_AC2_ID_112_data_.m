%=============================================================================================================================
% Result "offer_SA8_DA1_AC2_ID_112_data_". Sun Nov 07 23:55:24 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA8_DA1_AC2_ID_112_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	105	105	598.3619047619047	62828.0	1.005248	1.005248
1000.0	103	208	589.242718446602	123520.0	0.98816	0.9710720000000002
1500.0	99	307	571.2020202020202	180069.0	0.960368	0.904784
2000.0	91	398	573.7362637362637	232279.0	0.929116	0.83536
2500.0	100	498	571.24	289403.0	0.9260896	0.913984
3000.0	96	594	550.7395833333334	342274.0	0.9127306666666667	0.845936
3500.0	102	696	623.2647058823529	405847.0	0.9276502857142856	1.017168
4000.0	96	792	563.5729166666666	459950.0	0.9199	0.865648
4500.0	102	894	560.4215686274509	517113.0	0.919312	0.9146079999999999
]; % Sun Nov 07 23:55:43 CET 2021
