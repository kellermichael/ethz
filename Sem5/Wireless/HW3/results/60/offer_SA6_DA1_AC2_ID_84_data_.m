%=============================================================================================================================
% Result "offer_SA6_DA1_AC2_ID_84_data_". Sun Nov 07 23:55:24 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA6_DA1_AC2_ID_84_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	104	104	645.9615384615385	67180.0	1.07488	1.07488
1000.0	101	205	598.0891089108911	127587.0	1.020696	0.9665120000000001
1500.0	100	305	593.62	186949.0	0.9970613333333334	0.949792
2000.0	113	418	615.787610619469	256533.0	1.026132	1.113344
2500.0	97	515	616.5567010309278	316339.0	1.0122848	0.956896
3000.0	99	614	602.6666666666666	376003.0	1.0026746666666666	0.9546239999999999
3500.0	91	705	541.4945054945055	425279.0	0.9720662857142858	0.788416
4000.0	106	811	582.1037735849056	486982.0	0.973964	0.987248
4500.0	93	904	597.4301075268817	542543.0	0.9645208888888889	0.8889759999999999
]; % Sun Nov 07 23:55:43 CET 2021
