%=============================================================================================================================
% Result "thrp_SA43_DA1_AC2_ID_602_saturation__End". Sun Nov 07 17:03:06 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA43_DA1_AC2_ID_602_saturation__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	5	5	920.2	4601.0	0.073616	0.073616
1000.0	1	6	593.0	5194.0	0.041552	0.009488
1500.0	5	11	747.0	8929.0	0.047621333333333335	0.05976
2000.0	6	17	825.6666666666666	13883.0	0.055532	0.079264
2500.0	1	18	547.0	14430.0	0.046176	0.008752
3000.0	4	22	564.0	16686.0	0.044496	0.036096
3500.0	5	27	385.8	18615.0	0.04254857142857143	0.030864
4000.0	0	27	NaN	18615.0	0.03723	NaN
4500.0	0	27	NaN	18615.0	0.033093333333333336	NaN
]; % Sun Nov 07 17:03:25 CET 2021
