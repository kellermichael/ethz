%=============================================================================================================================
% Result "offer_SA24_DA1_AC2_ID_336_saturation_". Sun Nov 07 17:03:06 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA24_DA1_AC2_ID_336_saturation_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	5	5	592.2	2961.0	0.047376	0.047376
1000.0	15	20	577.4	11622.0	0.092976	0.138576
1500.0	5	25	618.4	14714.0	0.07847466666666665	0.049472
2000.0	15	40	576.6	23363.0	0.093452	0.138384
2500.0	15	55	749.0	34598.0	0.11071360000000001	0.17976
3000.0	0	55	NaN	34598.0	0.09226133333333335	NaN
3500.0	4	59	412.75	36249.0	0.08285485714285715	0.026416
4000.0	2	61	1083.0	38415.0	0.07683	0.034656
4500.0	7	68	622.4285714285714	42772.0	0.07603911111111111	0.069712
]; % Sun Nov 07 17:03:25 CET 2021
