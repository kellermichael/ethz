%=============================================================================================================================
% Result "offer_SA8_DA1_AC2_ID_112_saturation_". Sun Nov 07 17:03:05 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA8_DA1_AC2_ID_112_saturation_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	5	5	677.4	3387.0	0.054192	0.054192
1000.0	8	13	626.375	8398.0	0.067184	0.080176
1500.0	0	13	NaN	8398.0	0.04478933333333333	NaN
2000.0	9	22	716.2222222222222	14844.0	0.059376	0.103136
2500.0	12	34	570.4166666666666	21689.0	0.06940479999999999	0.10952
3000.0	0	34	NaN	21689.0	0.05783733333333334	NaN
3500.0	0	34	NaN	21689.0	0.049574857142857136	NaN
4000.0	0	34	NaN	21689.0	0.043378	NaN
4500.0	10	44	621.3	27902.0	0.04960355555555556	0.099408
]; % Sun Nov 07 17:03:25 CET 2021
