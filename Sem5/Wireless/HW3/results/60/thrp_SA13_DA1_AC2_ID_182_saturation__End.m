%=============================================================================================================================
% Result "thrp_SA13_DA1_AC2_ID_182_saturation__End". Sun Nov 07 17:03:05 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA13_DA1_AC2_ID_182_saturation__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	13	13	558.0	7254.0	0.116064	0.116064
1000.0	3	16	318.6666666666667	8210.0	0.06568	0.015296
1500.0	11	27	610.1818181818181	14922.0	0.079584	0.10739199999999999
2000.0	0	27	NaN	14922.0	0.059688	NaN
2500.0	0	27	NaN	14922.0	0.0477504	NaN
3000.0	9	36	525.3333333333334	19650.0	0.0524	0.075648
3500.0	7	43	707.2857142857143	24601.0	0.05623085714285714	0.079216
4000.0	0	43	NaN	24601.0	0.049202	NaN
4500.0	6	49	503.1666666666667	27620.0	0.04910222222222223	0.048304
]; % Sun Nov 07 17:03:25 CET 2021
