%=============================================================================================================================
% Result "offer_SA53_DA1_AC2_ID_742_saturation_". Sun Nov 07 17:03:06 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA53_DA1_AC2_ID_742_saturation_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	10	10	429.0	4290.0	0.06864	0.06864
1000.0	4	14	793.25	7463.0	0.059704	0.050768
1500.0	2	16	508.5	8480.0	0.045226666666666665	0.016272
2000.0	13	29	677.0	17281.0	0.069124	0.140816
2500.0	13	42	669.1538461538462	25980.0	0.083136	0.139184
3000.0	23	65	559.1739130434783	38841.0	0.103576	0.205776
3500.0	3	68	740.3333333333334	41062.0	0.093856	0.035536
4000.0	0	68	NaN	41062.0	0.082124	NaN
4500.0	5	73	720.4	44664.0	0.07940266666666666	0.057632
]; % Sun Nov 07 17:03:25 CET 2021
