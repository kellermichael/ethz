%=============================================================================================================================
% Result "offer_SA16_DA1_AC2_ID_224_saturation_". Sun Nov 07 17:03:05 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA16_DA1_AC2_ID_224_saturation_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	15	15	565.1333333333333	8477.0	0.135632	0.135632
1000.0	6	21	536.8333333333334	11698.0	0.093584	0.051536
1500.0	8	29	826.125	18307.0	0.09763733333333333	0.105744
2000.0	0	29	NaN	18307.0	0.073228	NaN
2500.0	1	30	451.0	18758.0	0.0600256	0.007216
3000.0	15	45	559.0	27143.0	0.07238133333333333	0.13416
3500.0	0	45	NaN	27143.0	0.06204114285714286	NaN
4000.0	0	45	NaN	27143.0	0.054286	NaN
4500.0	2	47	814.0	28771.0	0.05114844444444445	0.026048
]; % Sun Nov 07 17:03:25 CET 2021
