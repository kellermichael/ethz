%=============================================================================================================================
% Result "offer_SA23_DA1_AC2_ID_322_data_". Sun Nov 07 23:55:24 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA23_DA1_AC2_ID_322_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	101	101	583.5247524752475	58936.0	0.942976	0.942976
1000.0	101	202	607.019801980198	120245.0	0.96196	0.980944
1500.0	90	292	601.0444444444445	174339.0	0.929808	0.865504
2000.0	101	393	610.0891089108911	235958.0	0.943832	0.9859040000000001
2500.0	106	499	574.6981132075472	296876.0	0.9500031999999999	0.974688
3000.0	92	591	593.2608695652174	351456.0	0.937216	0.87328
3500.0	90	681	625.5222222222222	407753.0	0.9320068571428571	0.900752
4000.0	115	796	611.6608695652174	478094.0	0.956188	1.125456
4500.0	97	893	594.6288659793814	535773.0	0.9524853333333333	0.922864
]; % Sun Nov 07 23:55:43 CET 2021
