%=============================================================================================================================
% Result "thrp_SA8_DA1_AC2_ID_112_saturation__End". Sun Nov 07 17:03:05 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA8_DA1_AC2_ID_112_saturation__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	3	3	679.3333333333334	2038.0	0.032608	0.032608
1000.0	8	11	606.625	6891.0	0.055128	0.077648
1500.0	0	11	NaN	6891.0	0.036752	NaN
2000.0	9	20	734.1111111111111	13498.0	0.053992	0.105712
2500.0	12	32	547.6666666666666	20070.0	0.064224	0.105152
3000.0	0	32	NaN	20070.0	0.05352	NaN
3500.0	0	32	NaN	20070.0	0.04587428571428571	NaN
4000.0	0	32	NaN	20070.0	0.04014	NaN
4500.0	9	41	703.7777777777778	26404.0	0.046940444444444444	0.101344
]; % Sun Nov 07 17:03:25 CET 2021
