%=============================================================================================================================
% Result "thrp_SA53_DA1_AC2_ID_742_data__End". Sun Nov 07 23:55:25 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA53_DA1_AC2_ID_742_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	3	3	609.0	1827.0	0.029232	0.029232
1000.0	15	18	496.6666666666667	9277.0	0.074216	0.1192
1500.0	11	29	672.6363636363636	16676.0	0.08893866666666665	0.118384
2000.0	14	43	647.5	25741.0	0.102964	0.14504
2500.0	10	53	621.8	31959.0	0.1022688	0.099488
3000.0	0	53	NaN	31959.0	0.085224	NaN
3500.0	0	53	NaN	31959.0	0.07304914285714285	NaN
4000.0	1	54	1145.0	33104.0	0.066208	0.01832
4500.0	5	59	879.6	37502.0	0.06667022222222221	0.070368
]; % Sun Nov 07 23:55:43 CET 2021
