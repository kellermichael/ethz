%=============================================================================================================================
% Result "thrp_SA41_DA1_AC2_ID_574_data__End". Sun Nov 07 23:55:25 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA41_DA1_AC2_ID_574_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	8	8	553.0	4424.0	0.070784	0.070784
1000.0	7	15	557.0	8323.0	0.066584	0.062384
1500.0	4	19	380.5	9845.0	0.05250666666666667	0.024352
2000.0	0	19	NaN	9845.0	0.03938	NaN
2500.0	0	19	NaN	9845.0	0.031504	NaN
3000.0	2	21	223.0	10291.0	0.027442666666666667	0.007136
3500.0	10	31	709.4	17385.0	0.03973714285714285	0.113504
4000.0	22	53	611.3181818181819	30834.0	0.061668	0.21518400000000004
4500.0	6	59	619.5	34551.0	0.061424	0.059472
]; % Sun Nov 07 23:55:43 CET 2021
