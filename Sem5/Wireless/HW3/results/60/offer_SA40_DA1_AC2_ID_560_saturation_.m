%=============================================================================================================================
% Result "offer_SA40_DA1_AC2_ID_560_saturation_". Sun Nov 07 17:03:06 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA40_DA1_AC2_ID_560_saturation_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	3	3	983.0	2949.0	0.047184	0.047184
1000.0	4	7	245.25	3930.0	0.03144	0.015696
1500.0	17	24	584.6470588235294	13869.0	0.073968	0.159024
2000.0	11	35	676.5454545454545	21311.0	0.085244	0.119072
2500.0	7	42	501.2857142857143	24820.0	0.079424	0.056144
3000.0	6	48	617.1666666666666	28523.0	0.07606133333333334	0.059248
3500.0	9	57	592.1111111111111	33852.0	0.077376	0.085264
4000.0	1	58	487.0	34339.0	0.068678	0.007792
4500.0	0	58	NaN	34339.0	0.061047111111111114	NaN
]; % Sun Nov 07 17:03:25 CET 2021
