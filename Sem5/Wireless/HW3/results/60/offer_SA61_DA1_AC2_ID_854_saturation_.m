%=============================================================================================================================
% Result "offer_SA61_DA1_AC2_ID_854_saturation_". Sun Nov 07 17:03:06 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA61_DA1_AC2_ID_854_saturation_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	3	3	655.3333333333334	1966.0	0.031456	0.031456
1000.0	0	3	NaN	1966.0	0.015728	NaN
1500.0	4	7	263.5	3020.0	0.016106666666666665	0.016864
2000.0	1	8	987.0	4007.0	0.016027999999999997	0.015792
2500.0	11	19	700.0909090909091	11708.0	0.0374656	0.123216
3000.0	5	24	459.8	14007.0	0.037352	0.036784
3500.0	0	24	NaN	14007.0	0.032016	NaN
4000.0	6	30	781.0	18693.0	0.037386	0.074976
4500.0	4	34	702.25	21502.0	0.03822577777777778	0.044944
]; % Sun Nov 07 17:03:25 CET 2021
