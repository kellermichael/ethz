%=============================================================================================================================
% Result "offer_SA56_DA1_AC2_ID_784_saturation_". Sun Nov 07 17:03:06 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA56_DA1_AC2_ID_784_saturation_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	3	3	972.3333333333334	2917.0	0.046672	0.046672
1000.0	0	3	NaN	2917.0	0.023336	NaN
1500.0	8	11	649.375	8112.0	0.043264	0.08312
2000.0	4	15	442.75	9883.0	0.039532	0.028336
2500.0	4	19	547.75	12074.0	0.038636800000000006	0.035056
3000.0	0	19	NaN	12074.0	0.032197333333333335	NaN
3500.0	0	19	NaN	12074.0	0.027597714285714288	NaN
4000.0	12	31	724.3333333333334	20766.0	0.041532	0.139072
4500.0	4	35	717.5	23636.0	0.04201955555555555	0.04592
]; % Sun Nov 07 17:03:25 CET 2021
