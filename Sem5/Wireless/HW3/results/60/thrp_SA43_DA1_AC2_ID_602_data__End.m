%=============================================================================================================================
% Result "thrp_SA43_DA1_AC2_ID_602_data__End". Sun Nov 07 23:55:25 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA43_DA1_AC2_ID_602_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	4	4	328.75	1315.0	0.02104	0.02104
1000.0	6	10	352.8333333333333	3432.0	0.027456	0.033872
1500.0	8	18	409.625	6709.0	0.03578133333333334	0.052432
2000.0	2	20	93.0	6895.0	0.02758	0.002976
2500.0	0	20	NaN	6895.0	0.022064	NaN
3000.0	9	29	579.4444444444445	12110.0	0.032293333333333334	0.08344
3500.0	6	35	551.8333333333334	15421.0	0.035248	0.052976
4000.0	0	35	NaN	15421.0	0.030842	NaN
4500.0	0	35	NaN	15421.0	0.027415111111111112	NaN
]; % Sun Nov 07 23:55:43 CET 2021
