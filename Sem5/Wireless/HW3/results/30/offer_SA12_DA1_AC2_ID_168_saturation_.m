%=============================================================================================================================
% Result "offer_SA12_DA1_AC2_ID_168_saturation_". Sun Nov 07 17:49:38 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA12_DA1_AC2_ID_168_saturation_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	20	20	500.75	10015.0	0.16024	0.16024
1000.0	5	25	772.2	13876.0	0.111008	0.061776
1500.0	16	41	518.0	22164.0	0.118208	0.132608
2000.0	2	43	906.5	23977.0	0.095908	0.029008
2500.0	5	48	692.8	27441.0	0.08781119999999999	0.055424
3000.0	12	60	635.25	35064.0	0.093504	0.121968
3500.0	0	60	NaN	35064.0	0.08014628571428571	NaN
4000.0	9	69	734.3333333333334	41673.0	0.083346	0.105744
4500.0	10	79	688.9	48562.0	0.08633244444444445	0.110224
]; % Sun Nov 07 17:49:45 CET 2021
