%=============================================================================================================================
% Result "offer_SA30_DA1_AC2_ID_420_saturation_". Sun Nov 07 17:49:38 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA30_DA1_AC2_ID_420_saturation_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	3	3	208.0	624.0	0.009984	0.009984
1000.0	4	7	510.0	2664.0	0.021312	0.03264
1500.0	15	22	551.4	10935.0	0.05832	0.132336
2000.0	15	37	777.0	22590.0	0.09036	0.18648
2500.0	20	57	695.7	36504.0	0.11681280000000001	0.222624
3000.0	25	82	616.44	51915.0	0.13844	0.24657600000000002
3500.0	0	82	NaN	51915.0	0.11866285714285714	NaN
4000.0	3	85	597.0	53706.0	0.107412	0.028656
4500.0	4	89	775.0	56806.0	0.10098844444444444	0.0496
]; % Sun Nov 07 17:49:45 CET 2021
