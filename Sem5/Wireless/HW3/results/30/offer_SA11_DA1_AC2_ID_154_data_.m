%=============================================================================================================================
% Result "offer_SA11_DA1_AC2_ID_154_data_". Sun Nov 07 23:52:56 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA11_DA1_AC2_ID_154_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	103	103	597.242718446602	61516.0	0.984256	0.9842560000000001
1000.0	113	216	631.6017699115044	132887.0	1.063096	1.141936
1500.0	118	334	590.9830508474577	202623.0	1.080656	1.115776
2000.0	125	459	594.368	276919.0	1.107676	1.188736
2500.0	112	571	575.0892857142857	341329.0	1.0922528	1.03056
3000.0	88	659	624.4659090909091	396282.0	1.056752	0.879248
3500.0	99	758	607.10101010101	456385.0	1.0431657142857143	0.9616479999999998
4000.0	92	850	533.3804347826087	505456.0	1.010912	0.7851360000000002
4500.0	132	982	619.2651515151515	587199.0	1.0439093333333334	1.307888
]; % Sun Nov 07 23:53:08 CET 2021
