%=============================================================================================================================
% Result "thrp_SA6_DA1_AC2_ID_84_data__End". Sun Nov 07 23:52:56 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA6_DA1_AC2_ID_84_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	9	9	603.7777777777778	5434.0	0.086944	0.086944
1000.0	17	26	623.7058823529412	16037.0	0.128296	0.169648
1500.0	2	28	382.5	16802.0	0.08961066666666666	0.01224
2000.0	24	52	565.875	30383.0	0.121532	0.217296
2500.0	14	66	555.1428571428571	38155.0	0.122096	0.124352
3000.0	11	77	394.1818181818182	42491.0	0.11330933333333333	0.069376
3500.0	4	81	671.5	45177.0	0.10326171428571429	0.042976
4000.0	0	81	NaN	45177.0	0.090354	NaN
4500.0	4	85	689.5	47935.0	0.08521777777777778	0.044128
]; % Sun Nov 07 23:53:08 CET 2021
