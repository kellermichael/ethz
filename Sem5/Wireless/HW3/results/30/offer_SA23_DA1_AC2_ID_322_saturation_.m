%=============================================================================================================================
% Result "offer_SA23_DA1_AC2_ID_322_saturation_". Sun Nov 07 17:49:38 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA23_DA1_AC2_ID_322_saturation_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	7	7	507.57142857142856	3553.0	0.056848	0.056848
1000.0	33	40	749.8181818181819	28297.0	0.226376	0.395904
1500.0	0	40	NaN	28297.0	0.15091733333333335	NaN
2000.0	0	40	NaN	28297.0	0.113188	NaN
2500.0	16	56	578.5	37553.0	0.1201696	0.148096
3000.0	17	73	510.47058823529414	46231.0	0.12328266666666667	0.138848
3500.0	6	79	643.8333333333334	50094.0	0.11450057142857144	0.061808
4000.0	6	85	561.8333333333334	53465.0	0.10693	0.053936
4500.0	16	101	658.1875	63996.0	0.11377066666666667	0.168496
]; % Sun Nov 07 17:49:45 CET 2021
