%=============================================================================================================================
% Result "thrp_SA7_DA1_AC2_ID_98_data__End". Sun Nov 07 23:52:56 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA7_DA1_AC2_ID_98_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	18	18	539.5555555555555	9712.0	0.155392	0.155392
1000.0	21	39	594.1904761904761	22190.0	0.17752	0.199648
1500.0	22	61	494.3636363636364	33066.0	0.176352	0.174016
2000.0	14	75	691.6428571428571	42749.0	0.170996	0.154928
2500.0	3	78	657.0	44720.0	0.143104	0.031536
3000.0	0	78	NaN	44720.0	0.11925333333333332	NaN
3500.0	2	80	802.5	46325.0	0.10588571428571429	0.02568
4000.0	3	83	996.0	49313.0	0.098626	0.047808
4500.0	18	101	570.3888888888889	59580.0	0.10592	0.164272
]; % Sun Nov 07 23:53:08 CET 2021
