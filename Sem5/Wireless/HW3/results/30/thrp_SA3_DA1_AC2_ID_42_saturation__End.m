%=============================================================================================================================
% Result "thrp_SA3_DA1_AC2_ID_42_saturation__End". Sun Nov 07 17:49:38 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA3_DA1_AC2_ID_42_saturation__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	13	13	675.6923076923077	8784.0	0.140544	0.140544
1000.0	0	13	NaN	8784.0	0.070272	NaN
1500.0	9	22	658.4444444444445	14710.0	0.07845333333333333	0.094816
2000.0	15	37	563.9333333333333	23169.0	0.092676	0.135344
2500.0	0	37	NaN	23169.0	0.0741408	NaN
3000.0	10	47	545.9	28628.0	0.07634133333333334	0.087344
3500.0	18	65	659.1666666666666	40493.0	0.09255542857142858	0.18984
4000.0	21	86	495.8095238095238	50905.0	0.10181	0.166592
4500.0	3	89	858.0	53479.0	0.09507377777777778	0.041184
]; % Sun Nov 07 17:49:45 CET 2021
