%=============================================================================================================================
% Result "thrp_SA11_DA1_AC2_ID_154_saturation__End". Sun Nov 07 17:49:38 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA11_DA1_AC2_ID_154_saturation__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	4	4	601.25	2405.0	0.03848	0.03848
1000.0	0	4	NaN	2405.0	0.01924	NaN
1500.0	24	28	537.125	15296.0	0.08157866666666666	0.206256
2000.0	0	28	NaN	15296.0	0.061184	NaN
2500.0	6	34	738.6666666666666	19728.0	0.06312960000000001	0.070912
3000.0	9	43	573.6666666666666	24891.0	0.066376	0.082608
3500.0	28	71	568.0	40795.0	0.09324571428571429	0.254464
4000.0	7	78	656.5714285714286	45391.0	0.090782	0.073536
4500.0	10	88	623.2	51623.0	0.09177422222222222	0.099712
]; % Sun Nov 07 17:49:45 CET 2021
