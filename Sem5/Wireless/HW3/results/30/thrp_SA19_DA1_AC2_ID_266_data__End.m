%=============================================================================================================================
% Result "thrp_SA19_DA1_AC2_ID_266_data__End". Sun Nov 07 23:52:56 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA19_DA1_AC2_ID_266_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	10	10	841.5	8415.0	0.13464	0.13464
1000.0	0	10	NaN	8415.0	0.06732	NaN
1500.0	0	10	NaN	8415.0	0.04488	NaN
2000.0	18	28	662.1111111111111	20333.0	0.081332	0.190688
2500.0	20	48	585.5	32043.0	0.10253759999999999	0.18736
3000.0	21	69	553.7142857142857	43671.0	0.116456	0.18604799999999996
3500.0	2	71	185.5	44042.0	0.10066742857142857	0.005936
4000.0	0	71	NaN	44042.0	0.088084	NaN
4500.0	1	72	995.0	45037.0	0.08006577777777778	0.01592
]; % Sun Nov 07 23:53:08 CET 2021
