%=============================================================================================================================
% Result "thrp_SA25_DA1_AC2_ID_350_saturation__End". Sun Nov 07 17:49:38 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA25_DA1_AC2_ID_350_saturation__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	1	1	47.0	47.0	7.52E-4	7.52E-4
1000.0	10	11	668.7	6734.0	0.053872	0.106992
1500.0	14	25	608.3571428571429	15251.0	0.08133866666666667	0.136272
2000.0	12	37	692.3333333333334	23559.0	0.094236	0.132928
2500.0	6	43	633.8333333333334	27362.0	0.08755840000000001	0.060848
3000.0	19	62	614.6315789473684	39040.0	0.10410666666666667	0.186848
3500.0	14	76	538.0714285714286	46573.0	0.10645257142857144	0.120528
4000.0	7	83	813.4285714285714	52267.0	0.104534	0.091104
4500.0	12	95	460.6666666666667	57795.0	0.10274666666666667	0.088448
]; % Sun Nov 07 17:49:45 CET 2021
