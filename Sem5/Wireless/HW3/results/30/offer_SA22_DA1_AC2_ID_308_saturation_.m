%=============================================================================================================================
% Result "offer_SA22_DA1_AC2_ID_308_saturation_". Sun Nov 07 17:49:38 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA22_DA1_AC2_ID_308_saturation_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	25	25	535.24	13381.0	0.214096	0.214096
1000.0	24	49	424.4583333333333	23568.0	0.188544	0.162992
1500.0	23	72	630.7391304347826	38075.0	0.20306666666666665	0.232112
2000.0	10	82	552.5	43600.0	0.1744	0.0884
2500.0	3	85	851.6666666666666	46155.0	0.147696	0.04088
3000.0	3	88	563.3333333333334	47845.0	0.12758666666666668	0.02704
3500.0	0	88	NaN	47845.0	0.10936	NaN
4000.0	12	100	642.0	55549.0	0.111098	0.123264
4500.0	20	120	429.05	64130.0	0.1140088888888889	0.137296
]; % Sun Nov 07 17:49:45 CET 2021
