%=============================================================================================================================
% Result "thrp_SA29_DA1_AC2_ID_406_data__End". Sun Nov 07 23:52:56 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA29_DA1_AC2_ID_406_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	0	0	NaN	0.0	0.0	NaN
1000.0	10	10	549.8	5498.0	0.043984	0.087968
1500.0	14	24	654.4285714285714	14660.0	0.07818666666666667	0.146592
2000.0	8	32	579.0	19292.0	0.077168	0.074112
2500.0	2	34	340.0	19972.0	0.0639104	0.01088
3000.0	23	57	470.0869565217391	30784.0	0.08209066666666666	0.172992
3500.0	14	71	497.57142857142856	37750.0	0.08628571428571429	0.111456
4000.0	7	78	498.85714285714283	41242.0	0.082484	0.055872
4500.0	12	90	564.5833333333334	48017.0	0.08536355555555555	0.1084
]; % Sun Nov 07 23:53:08 CET 2021
