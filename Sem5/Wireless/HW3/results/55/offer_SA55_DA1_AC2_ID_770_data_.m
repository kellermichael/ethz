%=============================================================================================================================
% Result "offer_SA55_DA1_AC2_ID_770_data_". Mon Nov 08 08:52:12 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA55_DA1_AC2_ID_770_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	102	102	618.6862745098039	63106.0	1.009696	1.009696
1000.0	113	215	605.3982300884956	131516.0	1.052128	1.09456
1500.0	118	333	556.0847457627119	197134.0	1.0513813333333333	1.049888
2000.0	114	447	593.1754385964912	264756.0	1.059024	1.081952
2500.0	100	547	659.31	330687.0	1.0581984	1.054896
3000.0	114	661	598.0964912280701	398870.0	1.0636533333333336	1.090928
3500.0	105	766	551.1904761904761	456745.0	1.0439885714285715	0.9259999999999999
4000.0	102	868	634.9803921568628	521513.0	1.043026	1.036288
4500.0	104	972	579.0673076923077	581736.0	1.0341973333333332	0.9635680000000001
]; % Mon Nov 08 08:52:37 CET 2021
