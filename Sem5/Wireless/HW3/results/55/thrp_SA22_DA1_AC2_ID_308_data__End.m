%=============================================================================================================================
% Result "thrp_SA22_DA1_AC2_ID_308_data__End". Mon Nov 08 08:52:11 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA22_DA1_AC2_ID_308_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	4	4	434.75	1739.0	0.027824	0.027824
1000.0	0	4	NaN	1739.0	0.013912	NaN
1500.0	0	4	NaN	1739.0	0.009274666666666665	NaN
2000.0	0	4	NaN	1739.0	0.006956	NaN
2500.0	16	20	496.9375	9690.0	0.031008	0.127216
3000.0	5	25	708.6	13233.0	0.035288	0.056688
3500.0	8	33	552.625	17654.0	0.040352	0.070736
4000.0	16	49	598.0625	27223.0	0.054446	0.153104
4500.0	3	52	775.0	29548.0	0.05252977777777778	0.0372
]; % Mon Nov 08 08:52:37 CET 2021
