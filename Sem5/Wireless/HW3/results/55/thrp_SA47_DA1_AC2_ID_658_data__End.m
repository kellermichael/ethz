%=============================================================================================================================
% Result "thrp_SA47_DA1_AC2_ID_658_data__End". Mon Nov 08 08:52:12 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA47_DA1_AC2_ID_658_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	0	0	NaN	0.0	0.0	NaN
1000.0	0	0	NaN	0.0	0.0	NaN
1500.0	0	0	NaN	0.0	0.0	NaN
2000.0	7	7	675.1428571428571	4726.0	0.018904	0.075616
2500.0	0	7	NaN	4726.0	0.015123200000000002	NaN
3000.0	0	7	NaN	4726.0	0.012602666666666667	NaN
3500.0	4	11	502.75	6737.0	0.015398857142857143	0.032176
4000.0	6	17	185.5	7850.0	0.0157	0.017808
4500.0	3	20	859.3333333333334	10428.0	0.01853866666666667	0.041248
]; % Mon Nov 08 08:52:37 CET 2021
