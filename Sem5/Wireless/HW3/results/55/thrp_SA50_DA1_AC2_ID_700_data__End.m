%=============================================================================================================================
% Result "thrp_SA50_DA1_AC2_ID_700_data__End". Mon Nov 08 08:52:12 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA50_DA1_AC2_ID_700_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	6	6	777.8333333333334	4667.0	0.074672	0.074672
1000.0	0	6	NaN	4667.0	0.037336	NaN
1500.0	0	6	NaN	4667.0	0.02489066666666667	NaN
2000.0	2	8	974.0	6615.0	0.02646	0.031168
2500.0	1	9	1184.0	7799.0	0.0249568	0.018944
3000.0	14	23	683.5714285714286	17369.0	0.04631733333333333	0.15312
3500.0	4	27	931.0	21093.0	0.048212571428571424	0.059584
4000.0	11	38	347.54545454545456	24916.0	0.049832	0.061168
4500.0	4	42	465.5	26778.0	0.04760533333333333	0.029792
]; % Mon Nov 08 08:52:37 CET 2021
