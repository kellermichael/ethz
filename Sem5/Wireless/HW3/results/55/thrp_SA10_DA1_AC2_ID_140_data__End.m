%=============================================================================================================================
% Result "thrp_SA10_DA1_AC2_ID_140_data__End". Mon Nov 08 08:52:11 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA10_DA1_AC2_ID_140_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	3	3	499.3333333333333	1498.0	0.023968	0.023968
1000.0	0	3	NaN	1498.0	0.011984	NaN
1500.0	0	3	NaN	1498.0	0.007989333333333334	NaN
2000.0	3	6	552.0	3154.0	0.012616	0.026496
2500.0	12	18	646.1666666666666	10908.0	0.0349056	0.124064
3000.0	3	21	775.3333333333334	13234.0	0.035290666666666665	0.037216
3500.0	0	21	NaN	13234.0	0.03024914285714286	NaN
4000.0	0	21	NaN	13234.0	0.026468	NaN
4500.0	0	21	NaN	13234.0	0.02352711111111111	NaN
]; % Mon Nov 08 08:52:37 CET 2021
