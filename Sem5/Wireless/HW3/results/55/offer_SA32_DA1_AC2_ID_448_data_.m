%=============================================================================================================================
% Result "offer_SA32_DA1_AC2_ID_448_data_". Mon Nov 08 08:52:12 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA32_DA1_AC2_ID_448_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	120	120	564.2916666666666	67715.0	1.08344	1.08344
1000.0	82	202	645.5243902439024	120648.0	0.965184	0.846928
1500.0	94	296	569.1595744680851	174149.0	0.9287946666666667	0.856016
2000.0	102	398	592.7156862745098	234606.0	0.938424	0.967312
2500.0	95	493	618.5473684210526	293368.0	0.9387776	0.940192
3000.0	112	605	655.9107142857143	366830.0	0.9782133333333334	1.175392
3500.0	106	711	602.5754716981132	430703.0	0.984464	1.021968
4000.0	113	824	608.858407079646	499504.0	0.999008	1.100816
4500.0	101	925	610.2574257425742	561140.0	0.9975822222222221	0.986176
]; % Mon Nov 08 08:52:37 CET 2021
