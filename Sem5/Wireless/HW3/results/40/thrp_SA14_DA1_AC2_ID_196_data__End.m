%=============================================================================================================================
% Result "thrp_SA14_DA1_AC2_ID_196_data__End". Sun Nov 07 23:53:34 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA14_DA1_AC2_ID_196_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	6	6	584.8333333333334	3509.0	0.056144	0.056144
1000.0	6	12	867.5	8714.0	0.069712	0.08328
1500.0	12	24	713.5833333333334	17277.0	0.092144	0.137008
2000.0	3	27	542.3333333333334	18904.0	0.075616	0.026032
2500.0	1	28	813.0	19717.0	0.0630944	0.013008
3000.0	8	36	516.5	23849.0	0.06359733333333333	0.066112
3500.0	2	38	1016.0	25881.0	0.059156571428571426	0.032512
4000.0	0	38	NaN	25881.0	0.051762	NaN
4500.0	1	39	327.0	26208.0	0.046592	0.005232
]; % Sun Nov 07 23:53:48 CET 2021
