%=============================================================================================================================
% Result "thrp_SA29_DA1_AC2_ID_406_data__End". Sun Nov 07 23:53:35 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA29_DA1_AC2_ID_406_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	3	3	770.6666666666666	2312.0	0.036992	0.036992
1000.0	6	9	659.5	6269.0	0.050152	0.063312
1500.0	8	17	654.25	11503.0	0.06134933333333334	0.083744
2000.0	12	29	682.0	19687.0	0.078748	0.130944
2500.0	24	53	541.2916666666666	32678.0	0.10456959999999998	0.207856
3000.0	6	59	430.1666666666667	35259.0	0.094024	0.041296
3500.0	12	71	693.5833333333334	43582.0	0.099616	0.133168
4000.0	1	72	917.0	44499.0	0.088998	0.014672
4500.0	3	75	891.3333333333334	47173.0	0.08386311111111111	0.042784
]; % Sun Nov 07 23:53:48 CET 2021
