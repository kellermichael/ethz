%=============================================================================================================================
% Result "offer_SA34_DA1_AC2_ID_476_data_". Sun Nov 07 23:53:35 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA34_DA1_AC2_ID_476_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	121	121	653.5454545454545	79079.0	1.265264	1.265264
1000.0	107	228	521.0841121495328	134835.0	1.07868	0.8920960000000001
1500.0	100	328	661.38	200973.0	1.071856	1.058208
2000.0	105	433	561.7619047619048	259958.0	1.039832	0.9437600000000002
2500.0	117	550	617.8974358974359	332252.0	1.0632064	1.156704
3000.0	109	659	565.0733944954128	393845.0	1.0502533333333333	0.9854879999999999
3500.0	105	764	579.1904761904761	454660.0	1.039222857142857	0.9730399999999999
4000.0	123	887	618.1219512195122	530689.0	1.061378	1.216464
4500.0	98	985	617.6734693877551	591221.0	1.0510595555555555	0.9685119999999999
]; % Sun Nov 07 23:53:48 CET 2021
