%=============================================================================================================================
% Result "thrp_SA26_DA1_AC2_ID_364_data__End". Sun Nov 07 23:53:35 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA26_DA1_AC2_ID_364_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	8	8	662.25	5298.0	0.084768	0.084768
1000.0	8	16	354.375	8133.0	0.065064	0.04536
1500.0	16	32	872.4375	22092.0	0.117824	0.223344
2000.0	12	44	655.8333333333334	29962.0	0.119848	0.12592
2500.0	3	47	774.6666666666666	32286.0	0.10331520000000001	0.037184
3000.0	12	59	733.5833333333334	41089.0	0.10957066666666668	0.140848
3500.0	5	64	931.4	45746.0	0.10456228571428572	0.074512
4000.0	11	75	555.4545454545455	51856.0	0.103712	0.09776
4500.0	14	89	672.9285714285714	61277.0	0.10893688888888889	0.150736
]; % Sun Nov 07 23:53:48 CET 2021
