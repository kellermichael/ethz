%=============================================================================================================================
% Result "thrp_SA41_DA1_AC2_ID_574_data__End". Sun Nov 07 23:53:35 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA41_DA1_AC2_ID_574_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	0	0	NaN	0.0	0.0	NaN
1000.0	12	12	653.5	7842.0	0.062736	0.125472
1500.0	0	12	NaN	7842.0	0.041824	NaN
2000.0	0	12	NaN	7842.0	0.031368	NaN
2500.0	0	12	NaN	7842.0	0.025094400000000003	NaN
3000.0	10	22	720.5	15047.0	0.040125333333333325	0.11528
3500.0	21	43	482.2857142857143	25175.0	0.057542857142857146	0.162048
4000.0	13	56	605.7692307692307	33050.0	0.0661	0.12599999999999997
4500.0	0	56	NaN	33050.0	0.058755555555555554	NaN
]; % Sun Nov 07 23:53:49 CET 2021
