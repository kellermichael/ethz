%=============================================================================================================================
% Result "offer_SA27_DA1_AC2_ID_378_data_". Sun Nov 07 23:53:35 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA27_DA1_AC2_ID_378_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	102	102	610.7843137254902	62300.0	0.9968	0.9968
1000.0	91	193	558.7582417582418	113147.0	0.905176	0.813552
1500.0	117	310	612.2307692307693	184778.0	0.9854826666666666	1.146096
2000.0	103	413	626.0388349514564	249260.0	0.99704	1.0317120000000002
2500.0	90	503	567.0555555555555	300295.0	0.960944	0.81656
3000.0	110	613	618.8181818181819	368365.0	0.9823066666666667	1.08912
3500.0	117	730	615.2991452991453	440355.0	1.0065257142857142	1.15184
4000.0	100	830	630.0	503355.0	1.00671	1.008
4500.0	105	935	605.0380952380953	566884.0	1.0077937777777777	1.016464
]; % Sun Nov 07 23:53:48 CET 2021
