%=============================================================================================================================
% Result "offer_SA23_DA1_AC2_ID_322_data_". Sun Nov 07 23:53:35 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA23_DA1_AC2_ID_322_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	102	102	577.843137254902	58940.0	0.94304	0.94304
1000.0	113	215	649.9203539823009	132381.0	1.059048	1.175056
1500.0	87	302	637.2758620689655	187824.0	1.001728	0.8870879999999999
2000.0	107	409	561.803738317757	247937.0	0.991748	0.9618080000000001
2500.0	97	506	587.3092783505155	304906.0	0.9756992000000001	0.911504
3000.0	108	614	582.7962962962963	367848.0	0.980928	1.007072
3500.0	104	718	574.75	427622.0	0.9774217142857143	0.956384
4000.0	92	810	587.0	481626.0	0.963252	0.864064
4500.0	111	921	581.918918918919	546219.0	0.971056	1.0334880000000002
]; % Sun Nov 07 23:53:48 CET 2021
