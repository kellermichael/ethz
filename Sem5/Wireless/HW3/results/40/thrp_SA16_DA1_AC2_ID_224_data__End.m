%=============================================================================================================================
% Result "thrp_SA16_DA1_AC2_ID_224_data__End". Sun Nov 07 23:53:34 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA16_DA1_AC2_ID_224_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	0	0	NaN	0.0	0.0	NaN
1000.0	1	1	774.0	774.0	0.006192	0.012384
1500.0	5	6	463.0	3089.0	0.01647466666666667	0.03704
2000.0	8	14	634.625	8166.0	0.032664	0.081232
2500.0	10	24	553.2	13698.0	0.0438336	0.088512
3000.0	0	24	NaN	13698.0	0.036528	NaN
3500.0	0	24	NaN	13698.0	0.03130971428571429	NaN
4000.0	17	41	614.4117647058823	24143.0	0.048286	0.16712
4500.0	7	48	733.4285714285714	29277.0	0.052048	0.082144
]; % Sun Nov 07 23:53:48 CET 2021
