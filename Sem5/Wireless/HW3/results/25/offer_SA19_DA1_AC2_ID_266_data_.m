%=============================================================================================================================
% Result "offer_SA19_DA1_AC2_ID_266_data_". Sun Nov 07 23:52:40 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA19_DA1_AC2_ID_266_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	92	92	498.5108695652174	45863.0	0.733808	0.733808
1000.0	109	201	655.5137614678899	117314.0	0.938512	1.143216
1500.0	117	318	568.5982905982906	183840.0	0.98048	1.064416
2000.0	106	424	600.1037735849056	247451.0	0.989804	1.017776
2500.0	99	523	540.2525252525253	300936.0	0.9629951999999999	0.85576
3000.0	101	624	632.7425742574258	364843.0	0.9729146666666666	1.022512
3500.0	87	711	579.2988505747127	415242.0	0.9491245714285714	0.806384
4000.0	110	821	600.4363636363636	481290.0	0.96258	1.056768
4500.0	103	924	600.252427184466	543116.0	0.9655395555555556	0.989216
]; % Sun Nov 07 23:52:50 CET 2021
