%=============================================================================================================================
% Result "offer_SA8_DA1_AC2_ID_112_data_". Sun Nov 07 23:52:40 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA8_DA1_AC2_ID_112_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	106	106	606.6698113207547	64307.0	1.028912	1.0289119999999998
1000.0	97	203	612.9175257731958	123760.0	0.99008	0.951248
1500.0	113	316	620.9026548672566	193922.0	1.0342506666666667	1.122592
2000.0	99	415	626.4242424242424	255938.0	1.023752	0.9922559999999999
2500.0	106	521	595.9622641509434	319110.0	1.021152	1.010752
3000.0	111	632	651.6486486486486	391443.0	1.043848	1.157328
3500.0	111	743	612.1081081081081	459387.0	1.0500274285714286	1.087104
4000.0	117	860	582.957264957265	527593.0	1.055186	1.091296
4500.0	87	947	547.3793103448276	575215.0	1.0226044444444444	0.761952
]; % Sun Nov 07 23:52:50 CET 2021
