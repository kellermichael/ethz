%=============================================================================================================================
% Result "thrp_SA20_DA1_AC2_ID_280_data__End". Sun Nov 07 23:52:40 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA20_DA1_AC2_ID_280_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	11	11	519.0909090909091	5710.0	0.09136	0.09136
1000.0	8	19	692.0	11246.0	0.089968	0.088576
1500.0	16	35	595.25	20770.0	0.11077333333333333	0.152384
2000.0	11	46	512.5454545454545	26408.0	0.105632	0.090208
2500.0	9	55	546.1111111111111	31323.0	0.10023359999999999	0.07864
3000.0	4	59	465.5	33185.0	0.08849333333333334	0.029792
3500.0	0	59	NaN	33185.0	0.07585142857142857	NaN
4000.0	0	59	NaN	33185.0	0.06637	NaN
4500.0	1	60	878.0	34063.0	0.06055644444444445	0.014048
]; % Sun Nov 07 23:52:50 CET 2021
