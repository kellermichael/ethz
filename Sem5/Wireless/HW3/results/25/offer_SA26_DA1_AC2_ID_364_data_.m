%=============================================================================================================================
% Result "offer_SA26_DA1_AC2_ID_364_data_". Sun Nov 07 23:52:40 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA26_DA1_AC2_ID_364_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	107	107	633.9252336448598	67830.0	1.08528	1.08528
1000.0	100	207	544.92	122322.0	0.978576	0.8718719999999999
1500.0	101	308	555.5247524752475	178430.0	0.9516266666666666	0.897728
2000.0	90	398	528.7	226013.0	0.904052	0.7613280000000001
2500.0	106	504	584.6320754716982	287984.0	0.9215488000000001	0.9915360000000001
3000.0	94	598	540.3617021276596	338778.0	0.903408	0.812704
3500.0	108	706	570.824074074074	400427.0	0.9152617142857143	0.9863839999999999
4000.0	108	814	605.3796296296297	465808.0	0.931616	1.0460960000000001
4500.0	108	922	578.4166666666666	528277.0	0.9391591111111112	0.9995039999999998
]; % Sun Nov 07 23:52:50 CET 2021
