%=============================================================================================================================
% Result "offer_SA11_DA1_AC2_ID_154_data_". Sun Nov 07 23:52:40 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA11_DA1_AC2_ID_154_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	102	102	569.8627450980392	58126.0	0.930016	0.930016
1000.0	109	211	633.4495412844037	127172.0	1.017376	1.104736
1500.0	101	312	604.7722772277227	188254.0	1.0040213333333334	0.9773119999999998
2000.0	121	433	556.1818181818181	255552.0	1.022208	1.076768
2500.0	90	523	572.5333333333333	307080.0	0.982656	0.824448
3000.0	103	626	625.1165048543689	371467.0	0.9905786666666666	1.030192
3500.0	111	737	563.3603603603603	434000.0	0.992	1.000528
4000.0	104	841	662.4423076923077	502894.0	1.005788	1.102304
4500.0	89	930	663.123595505618	561912.0	0.9989546666666667	0.9442880000000001
]; % Sun Nov 07 23:52:50 CET 2021
