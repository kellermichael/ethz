%=============================================================================================================================
% Result "offer_SA6_DA1_AC2_ID_84_data_". Sun Nov 07 23:52:40 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA6_DA1_AC2_ID_84_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	105	105	597.2857142857143	62715.0	1.00344	1.00344
1000.0	103	208	561.5922330097087	120559.0	0.964472	0.925504
1500.0	120	328	595.85	192061.0	1.0243253333333335	1.144032
2000.0	109	437	591.394495412844	256523.0	1.0260920000000002	1.0313919999999999
2500.0	87	524	620.1609195402299	310477.0	0.9935264	0.863264
3000.0	112	636	613.0535714285714	379139.0	1.0110373333333333	1.098592
3500.0	110	746	588.5454545454545	443879.0	1.0145805714285714	1.0358399999999999
4000.0	98	844	623.9591836734694	505027.0	1.010054	0.978368
4500.0	113	957	600.212389380531	572851.0	1.0184017777777779	1.085184
]; % Sun Nov 07 23:52:50 CET 2021
