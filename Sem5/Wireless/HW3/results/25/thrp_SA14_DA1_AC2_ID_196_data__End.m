%=============================================================================================================================
% Result "thrp_SA14_DA1_AC2_ID_196_data__End". Sun Nov 07 23:52:40 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall thrp [Mb/s] | (6) tpht last interval [Mb/s]
%=============================================================================================================================
result_thrp_SA14_DA1_AC2_ID_196_data__End = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	8	8	637.5	5100.0	0.0816	0.0816
1000.0	26	34	552.0769230769231	19454.0	0.155632	0.229664
1500.0	28	62	520.6428571428571	34032.0	0.181504	0.233248
2000.0	2	64	620.5	35273.0	0.141092	0.019856
2500.0	15	79	602.2666666666667	44307.0	0.1417824	0.144544
3000.0	13	92	691.7692307692307	53300.0	0.1421333333333333	0.143888
3500.0	8	100	567.875	57843.0	0.13221257142857146	0.072688
4000.0	5	105	691.4	61300.0	0.1226	0.055312
4500.0	15	120	570.0	69850.0	0.12417777777777778	0.1368
]; % Sun Nov 07 23:52:50 CET 2021
