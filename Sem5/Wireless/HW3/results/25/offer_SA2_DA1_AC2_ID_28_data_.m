%=============================================================================================================================
% Result "offer_SA2_DA1_AC2_ID_28_data_". Sun Nov 07 23:52:40 CET 2021.
%-----------------------------------------------------------------------------------------------------------------------------
% (0) time[ms] | (1) #packets last interval | (2) #packets overall | (3) overall avrg. packetsize [byte] | (4) overall sum packetsize [byte] | (5) overall offer [Mb/s] | (6) offer last interval [Mb/s]
%=============================================================================================================================
result_offer_SA2_DA1_AC2_ID_28_data_ = [
0.0	0	0	NaN	0.0	NaN	NaN
500.0	113	113	640.433628318584	72369.0	1.157904	1.157904
1000.0	81	194	606.679012345679	121510.0	0.97208	0.786256
1500.0	101	295	593.3168316831683	181435.0	0.9676533333333333	0.9588
2000.0	96	391	555.5208333333334	234765.0	0.93906	0.85328
2500.0	103	494	594.3009708737864	295978.0	0.9471296	0.9794079999999998
3000.0	89	583	621.6179775280899	351302.0	0.9368053333333334	0.885184
3500.0	110	693	639.709090909091	421670.0	0.9638171428571428	1.125888
4000.0	103	796	616.4271844660194	485162.0	0.970324	1.015872
4500.0	121	917	611.0495867768595	559099.0	0.9939537777777777	1.182992
]; % Sun Nov 07 23:52:50 CET 2021
