#!/bin/bash
# Basic while loop
counter=1
for counter in {5..10..5}
do
    sed -e 's/PLACEHOLDER/'$counter'/g' template.xml > MobComp_assignment03_$counter.xml
	station=$(cat "./station.xml")
    for (( i=1; i<=counter; i++ ))
    do
		awk '{if($2=="STATION") {$2="Hello"} print $0}' MobComp_assignment03_$counter.xml > tmp.txt
		# sed -i 's/STATION_MAC/$i/g' MobComp_assignment03_$counter.xml
    done
    # sed -i 's/STATION/ABC/g' MobComp_assignment03_$counter.xml
done

echo All done