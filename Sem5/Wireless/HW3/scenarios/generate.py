l = [1,2,3,4,5,6,7,8,9,10,15,20,25,30,35,40,45,50,55,60]

for i in l:
    f = open("MobComp_assignment03_" + str(i) + ".xml", "w")

    # set result path
    data = open("template.xml", "r").read().replace("PLACEHOLDER", str(i))

    # add stations
    station = open("station.xml", "r").read()
    for j in range(i):
        data = data.replace("STATION", station)
        data = data.replace("STATION_MAC", str(j+2))
    data = data.replace("STATION", "")
    f.write(data)