package layer2_802Algorithms;

import plot.JEMultiPlotter;

import java.util.Arrays;

import layer1_802Phy.JE802PhyMode;
import layer2_80211Mac.JE802_11BackoffEntity;
import layer2_80211Mac.JE802_11Mac;
import layer2_80211Mac.JE802_11MacAlgorithm;

public class Micados_Ehren extends JE802_11MacAlgorithm {
	
	private JE802_11BackoffEntity theBackoffEntityAC01;
	
	private double theSamplingTime_sec;
	private int previous_error = 0; 
	private int previous_collisions = 0;
	private int previous_discartedPackets = 0;
	private double[] integral;
	private double dt;
	private int curIndex=0;
	private int intLen=50;
	private double kp=0.2;
	private double ki=0.2;
	private double kd=0.2; 
	private int iteration =0;
	private int plotLen = 100;
	private double[] plotOut;
	private int error_global = 0;
	public Micados_Ehren(String name, JE802_11Mac mac) {
		super(name, mac);
		this.theBackoffEntityAC01 = this.mac.getBackoffEntity(1);
		message("This is station " + this.dot11MACAddress.toString() +". MobComp algorithm: '" + this.algorithmName + "'.", 100);
		integral = new double[intLen];
		plotOut = new double[plotLen];
	}
	
	@Override
	public void compute() {

		this.mac.getMlme().setTheIterationPeriod(0.1);  // the sampling period in seconds, which is the time between consecutive calls of this method "compute()"
		this.theSamplingTime_sec =  this.mac.getMlme().getTheIterationPeriod().getTimeS(); // this sampling time can only be read after the MLME was constructed.
		dt = theSamplingTime_sec;
		
		// ASSIGNMENT 04 - observe outcome:  (might need to be stored from iteration to iteration)
		int collisions = this.theBackoffEntityAC01.getTheCollisionCnt();
		int discartedPackets = (int) this.theBackoffEntityAC01.getTheDiscardCnt();
		
//		double aCurrentTxPower_dBm = this.mac.getPhy().getCurrentTransmitPower_dBm();
//		JE802PhyMode aCurrentPhyMode = this.mac.getPhy().getCurrentPhyMode();

		// ASSIGNMENT 04 - add your PID controller here:
		//compute the output guided by the pseudocode from the lecture
		//overwrite queue size to own input
		int error = (collisions - previous_collisions) + (discartedPackets - previous_discartedPackets);
		error_global = error;
		previous_collisions = collisions;
		previous_discartedPackets = discartedPackets;
		
		System.out.println(error);
		
		integral[(curIndex++)%intLen] = error * dt; 
		double derivative = (error - previous_error) / dt;
		double integrated = 0;
		for(int i = 0; i< intLen;i++) {
			integrated += integral[i];
		}
		double out = kp*error + ki*integrated + kd*derivative;
		previous_error = (int)integral[(curIndex-1)%intLen];
		//storing out data to plot
		if(iteration < plotLen){
			plotOut[iteration] = out;
		}
		//printing data
		else if(iteration == plotLen){
			System.out.println(Arrays.toString(plotOut));
		}
		// //now make decision accordingly to out value
		this.mac.getPhy().setCurrentTransmitPower_dBm(0); // it is also possible to change the transmission power (please not higher than 0dBm)
		this.mac.getPhy().setCurrentPhyMode("BPSK12");  // it is possible to change the PhyMode
		theBackoffEntityAC01.setDot11EDCACWmin(8);
		//if out is small we have very few collisions and discarded packets
		//-> increase congestion window
		//-> AIFS not too strong (cooperative to avoid future congestion)
		if(out < 5) {
			theBackoffEntityAC01.setDot11EDCACWmax(theBackoffEntityAC01.getDot11EDCACWmax()*2);
			theBackoffEntityAC01.setDot11EDCAAIFSN(3);
		}
		//if out is normal
		else if(out < 50) {
			theBackoffEntityAC01.setDot11EDCAAIFSN(2);
		}
		//if out is big we have a lot of collisions
		//-> decrease congestion window
		//-> still have low (good) AIFS so with low bandwidth our packets still have a chance
		else {
			if(theBackoffEntityAC01.getDot11EDCACWmax() < 15) {
				theBackoffEntityAC01.setDot11EDCACWmax(15);
			}
			else{
				theBackoffEntityAC01.setDot11EDCACWmax(theBackoffEntityAC01.getDot11EDCACWmax()/2);
			}
			theBackoffEntityAC01.setDot11EDCAAIFSN(1);
		}
		
	}
	
	@Override
	public void plot() {
		if (plotter == null) {
			plotter = new JEMultiPlotter("PID Controller, Station " + this.dot11MACAddress.toString(), "max", "time [s]", "Error", this.theUniqueEventScheduler.getEmulationEnd().getTimeMs() / 1000.0, true);
			plotter.addSeries("current");
			plotter.display();
		}
//		int collisions = this.theBackoffEntityAC01.getTheCollisionCnt();
//		int discartedPackets = (int) this.theBackoffEntityAC01.getTheDiscardCnt();
		plotter.plot(((Double) theUniqueEventScheduler.now().getTimeMs()).doubleValue() / 1000.0, error_global, 0);
		// plotter.plot(((Double) theUniqueEventScheduler.now().getTimeMs()).doubleValue() / 1000.0, theBackoffEntityAC01.getCurrentQueueSize(), 1);
	}

}
