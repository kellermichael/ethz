\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[a4paper,total={6in,9in}]{geometry}
\usepackage[ruled]{algorithm2e}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{physics}
\usepackage{graphicx}
\usepackage{tikz}
\usetikzlibrary{positioning}% To get more advances positioning options
\usetikzlibrary{arrows}% To get more arrow heads
\graphicspath{ {./imgs/} }

\title{
Algorithms, Probability, and Computing: Assignment 1}
\author{Michael Keller}
\date{September 2021}

\begin{document}
\maketitle
\section*{1)}
\subsection*{a)}
[Note: I adaped this from the Skript]\\\\
We choose $p = \frac{2}{3}$. Next we prove that the expected
number of T-heavy edges can be bounded by $\frac{\abs{G.V}}{8}$. To this end
we consider a Graph $G = (V, E)$ on $n$ vertices and $m$ edges
and assume that the edges are ordered according to their weight:
$w(e_1) < \dots < w(e_m)$. Now we run Kruskal's algorithm on
this Graph and intertwine the selection of a random subgraph with
the run of Kruskal's algorithm. That is, we will compute: $(i)$
a subgraph $G' = (V, E')$ of $G$ such that each edge of $G$ belongs
to $G'$ independently with probability $\frac{2}{3}$, $(ii)$
a minimum spanning forest $T$ of $G'$, and, finally, $(iii)$
a subset $F \subset E \setminus E'$ that contains all
edges that are not T-heavy (and possibly some more). From the run of the
algorithm it will follow that the expected number of edges in $F$ is
bounded by $\abs*{G'.V}$. Here is a formal description of this algorithm:
\begin{algorithm}
    \caption{Modified Kruskal 1}
    E', T, F $\gets$ $\emptyset$\;
    \For{$i \in [1, \dots, m]$}{
        \eIf{$e_i$ connects two components of $T$}{
            Flip a biased coin with $P[Heads] = \frac{2}{3}$ in order to decide whether $e_i$ belongs to $G'$\;
            \eIf{Heads}{
                E' $\gets$ E' + $e_i$\;
                T $\gets$ T + $e_i$\;
            }{
                F $\gets$ F + $e_i$
            }
        }{
            Flip a biased coin with $P[Heads] = \frac{2}{3}$ in order to decide whether $e_i$ belongs to $G'$\;
            (Observe that $e_i$ is T-heavy and thus cannot belong to $F$.)
        }
    }
\end{algorithm}\\
Next we need to demonstrate that $E[\abs{F}] \leq \frac{\abs*{G'.V}}{2}$. For this let
\begin{align*}
    (\mathcal{X}_i) & \sim Bernoulli \left(\frac{2}{3} \right) \text{ for i.i.d. } (\mathcal{X}_i)_{i \in \mathbb{N}} \\
    T               & := \min \{n \in \mathbb{N} \; | \; \abs*{ \{i \in [1, \dots, n] \;|\; X_i = 0\} } \geq K \}
\end{align*}
$T$ be the random variable describing the first point in time where
we have seen $K$ zeros. Further let
\begin{align*}
    Z := \sum_{i = 1}^T \mathcal{X}_i
\end{align*}
Then we have (by linearity of expectation):
\begin{align*}
    Z_i := \text{Number of $1$'s while waiting for $i$'th zero} \\
    E[Z] = E[Z_K] = K \cdot E[Z_1] = K \cdot (\frac{1}{\frac{2}{3}} - 1) = \frac{K}{2}
\end{align*}
As this models either adding $e_i$ to $E'$ ($\mathcal{X}_i = 0$) or
adding $e_i$ to $F$ ($\mathcal{X}_i = 1$) and we have bounded
$E[\abs{F}] \leq \frac{\abs*{G'.V}}{2}$, it follows that $E[\abs{F}] \leq \frac{G.n}{8}$.\\ \\
Finally it follows that:
\begin{align*}
    C_B(n + m) + m + C(n/4 + 2m/3) + C_{FH}(n/4 + m) + C(n/4 + n/4 + n/8)
     & \leq C(n + m)
\end{align*}
\subsection*{b)}
We first describe the algorithm, which consists of four steps:
\begin{enumerate}
    \item We invoke $\mathcal{A}_{sample}$ with input probability $p = \frac{n}{2m}$,
          thus obtaining a graph $G'$ that contains each edge of $G$ independently
          with probability $p$. Note: $0 \leq p \leq 1$ because the Graph is connected.
    \item Next, we compute a Minimum Spanning Forest $T'$ of $G'$ by invoking $\mathcal{A}_{MSF}$.
    \item We invoke $\mathcal{A}_{T-heavy}$ to remove all T'-heavy edges from $G$. We call
          the resulting graph $G''$.
    \item We invoke $\mathcal{A}_{MSF}$ with input $G''$ to obtain the Minimum Spanning
          Forest $T$ of $G''$ and the algorithm outputs $T$.
\end{enumerate}
We invoke each of the subroutines at most a constant number of times. Hence, it remains
to argue that the algorithm indeed outputs the Minimum Spanning Tree of $G$
with probability $1 - \mathcal{O}(1/n)$. The reason for that is the following. In the
final step, we output the Minimum Spanning Tree of $G''$. We obtained
$G''$ by removing all the T'-heavy edges of $G$, which re not contained in the
Minimum Spanning Tree of $G$. Hence, all the edges contained in the Minimum Spanning
Tree of $G$ are also edges in the Graph $G''$. Therefore, our output $T$ is indeed
the Minimum Spanning Tree of $G$. We first use the Chernoff Bound to argue that
$G'$ contains $< 100 n^{4/3}$ edges with probability $1 - \mathcal{O}(1/n)$.
To that end, let $E$ denote the set of edges of $G$. For each $e \in E$, let $X_e$
denote the indicator variable for the event that $e$ is contained in $G'$ and
let $X := \sum_{e \in E} X_e$. We have
\begin{align*}
    E[X]                  & = p \cdot \abs*{E} = \frac{n}{2m} \cdot m = \frac{n}{2} \\
    Pr[X \geq 100n^{4/3}] & \leq Pr[X \geq n] \leq Pr[X \leq (1 + 1)E[X]]
    \leq e^{-\frac{1}{3} E[X]} \leq e^{-\theta(n)} = \mathcal{O}(1/n)
\end{align*}
Thus, at most $100n^{4/3}$ edges are sampled with probability $1 - \mathcal{O}(1/n)$.
Next, we argue that $G''$ contains $< 100n^{4/3}$ edges with probability
$1 - \mathcal{O}(1/n)$. To analyze how many not T'-heavy edges remain,
consider the following procedure:
\begin{center}
    \begin{algorithm}[H]
        \caption{Modified Kruskal 2}
        E', T, F $\gets$ $\emptyset$\;
        \For{$i \in [1, \dots, m]$}{
            \eIf{$e_i$ connects two components of $T$}{
                Flip a biased coin with $P[Heads] = \frac{n}{2m}$ in order to decide whether $e_i$ belongs to $G'$\;
                \eIf{Heads}{
                    E' $\gets$ E' + $e_i$\;
                    T $\gets$ T + $e_i$\;
                }{
                    F $\gets$ F + $e_i$
                }
            }{
                Flip a biased coin with $P[Heads] = \frac{n}{2m}$ in order to decide whether $e_i$ belongs to $G'$\;
                (Observe that $e_i$ is T-heavy and thus cannot belong to $F$.)
            }
        }
    \end{algorithm}
\end{center}
Note that this is the same procedure as in the script, except that this time we always throw
a biased coin instead of a fair coin. The reason is that we don't sample each edge with probability
$1/2$, but instead with probability $p = \frac{n}{2m}$. As argued in the script, each edge in
$E \setminus T'$ that is not T'-heavy is contained in $F$. Thus, we only need to show that $F$
contains at most $100n^{4/3} - (n-1)$ edges with probability $1 - \mathcal{O}(1/n)$. Note
that during the run of the algorithm, we can only add edges to $F$ as long as the biased
coin in line 4 comes up heads strictly less than $n-1$ times, as each time the coin comes up heads,
we add one more edge to the forest $T'$. Hence, the probability that F contains more than
$100n^{4/3} - (n-1)$ edges is upper bounded by the probability that out of $100n^{4/3}$
independent biased coin throws, the coin comes up heads less than
$n$ times. The following Chernoff bound demonstrates this:
\begin{align*}
    H          & = \sum_{c \in flips} c                                                             &  & c = 1 \text{ iif. coin is heads else } 0 \\
    E[H]       & = p \cdot \abs*{E} = \frac{n}{2m} \cdot m = \frac{n}{2}                                                                          \\
    P[H < n-1] & \leq P[H \leq n] \leq P[H \leq (1 + 1)E[H]] \leq e^{-\theta(n)} = \mathcal{O}(1/n)
\end{align*}
\end{document}