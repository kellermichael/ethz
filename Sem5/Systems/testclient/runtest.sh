#!/bin/bash
nc localhost 5555 < test.in | diff - test.out
# check diff exit code
if (($? == 0)); then
    echo "Test successful!"
else
    echo "Error!"
fi
